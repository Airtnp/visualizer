"""
Automatically do self assign on __init__ function.
For python version > 3.7, should use @dataclass instead
"""
import inspect
from functools import wraps



def autoinit(func):
    """
    @ref: https://stackoverflow.com/questions/1389180/automatically-initialize-instance-variables
    @ref: https://github.com/python/cpython/blob/3.7/Lib/inspect.py#L2117
    """

    # without inspect: func.__code__.co_varnames, func.__defaults__
    names, varargs, varkwargs, defaults, kwonlynames, kwonlydefaults, _ = inspect.getfullargspec(func)
    assert(names[0] == 'self')
    # assert(varargs is None)
    # assert(varkwargs is None)

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        for name, arg in list(zip(names[1:], args)) + list(kwargs.items()):
            setattr(self, name, arg)

        for name, default in zip(reversed(names), reversed(defaults)):
            if not hasattr(self, name):
                setattr(self, name, default)

        for name, default in kwonlydefaults:
            if not hasattr(self, name):
                setattr(self, name, default)

        func(self, *args, **kwargs)

    return wrapper