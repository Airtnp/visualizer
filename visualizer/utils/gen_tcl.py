"""
Generate TCL clock period
"""
import sys
import os
from subprocess import Popen, PIPE, STDOUT


def call_synthesize(name, headers, sources, tclfile, clock):
    env = os.environ.copy()
    # env['CACHE_NAME'] = ''
    # env['PIPEFILES'] = ''
    env['HEADERS'] = headers
    env['SOURCES'] = sources
    env['DESIGN_NAME'] = name
    env['CLOCK_NET_NAME'] = 'clock'
    env['RESET_NET_NAME'] = 'reset'
    env['CLOCK_PERIOD'] = str(clock)
    is_slack = False
    contain_ff = False
    syn_result = ''
    proc = Popen([
        'dc_shell-t',
        '-f',
        tclfile,
    ], stdout=PIPE, stderr=STDOUT, env=env)
    for line in proc.stdout:
        # sys.stdout.write(line)
        syn_result += line
        if '| Flip-flop |' in line:
            contain_ff = True
    with open('./{}.rep'.format(name)) as rep:
        for line in rep:
            if 'slack (VIOLATED)' in line:
                is_slack = True
                break
    return is_slack


def gen_tcl(name, headers, sources, tclfile):
    clock_upper = 10
    clock_lower = 0
    counter = 0
    while abs(clock_upper - clock_lower) > 0.2:
        clock = (clock_lower + clock_upper) / 2.0
        print("Trial " + str(counter) + " Clock Period = " + str(clock))
        is_slack = call_synthesize(name, headers, sources, tclfile, clock)
        print('Is Slack: ' + str(is_slack))
        if is_slack:
            clock_lower = clock
        else:
            clock_upper = clock
        counter += 1
    return clock_lower, clock_upper


if __name__ == '__main__':
    design_name = 'ISR'
    headers = 'ISA.svh prehistoric.svh sys_defs.svh'
    sources = 'ISR.v pipe_mult.v mult_stage.v'
    tclfile = 'synth/target.tcl'
    l, u = gen_tcl(design_name, headers, sources, tclfile)
    print(l, u)
