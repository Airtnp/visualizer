`timescale 1ns/100ps















import "DPI-C" context function void read_ROB_ENTRY(ROB_ENTRY i);
import "DPI-C" context function void check_ROB_ENTRY(ROB_ENTRY i);
import "DPI-C" context function void step_ROB_ENTRY(ROB_ENTRY i);
import "DPI-C" context function void print_ROB_ENTRY(ROB_ENTRY i);



import "DPI-C" context function void read_BRANCH_PACKET(BRANCH_PACKET i);
import "DPI-C" context function void check_BRANCH_PACKET(BRANCH_PACKET i);
import "DPI-C" context function void step_BRANCH_PACKET(BRANCH_PACKET i);
import "DPI-C" context function void print_BRANCH_PACKET(BRANCH_PACKET i);



import "DPI-C" context function void read_DECODE_MT_PACKET(DECODE_MT_PACKET i);
import "DPI-C" context function void check_DECODE_MT_PACKET(DECODE_MT_PACKET i);
import "DPI-C" context function void step_DECODE_MT_PACKET(DECODE_MT_PACKET i);
import "DPI-C" context function void print_DECODE_MT_PACKET(DECODE_MT_PACKET i);



import "DPI-C" context function void read_DECODE_RS_PACKET(DECODE_RS_PACKET i);
import "DPI-C" context function void check_DECODE_RS_PACKET(DECODE_RS_PACKET i);
import "DPI-C" context function void step_DECODE_RS_PACKET(DECODE_RS_PACKET i);
import "DPI-C" context function void print_DECODE_RS_PACKET(DECODE_RS_PACKET i);



import "DPI-C" context function void read_ROB_MT_PACKET(ROB_MT_PACKET i);
import "DPI-C" context function void check_ROB_MT_PACKET(ROB_MT_PACKET i);
import "DPI-C" context function void step_ROB_MT_PACKET(ROB_MT_PACKET i);
import "DPI-C" context function void print_ROB_MT_PACKET(ROB_MT_PACKET i);



import "DPI-C" context function void read_MT_ROB_PACKET(MT_ROB_PACKET i);
import "DPI-C" context function void check_MT_ROB_PACKET(MT_ROB_PACKET i);
import "DPI-C" context function void step_MT_ROB_PACKET(MT_ROB_PACKET i);
import "DPI-C" context function void print_MT_ROB_PACKET(MT_ROB_PACKET i);



import "DPI-C" context function void read_BRANCH_ROB_PACKET(BRANCH_ROB_PACKET i);
import "DPI-C" context function void check_BRANCH_ROB_PACKET(BRANCH_ROB_PACKET i);
import "DPI-C" context function void step_BRANCH_ROB_PACKET(BRANCH_ROB_PACKET i);
import "DPI-C" context function void print_BRANCH_ROB_PACKET(BRANCH_ROB_PACKET i);



import "DPI-C" context function void read_ST_ROB_PACKET(ST_ROB_PACKET i);
import "DPI-C" context function void check_ST_ROB_PACKET(ST_ROB_PACKET i);
import "DPI-C" context function void step_ST_ROB_PACKET(ST_ROB_PACKET i);
import "DPI-C" context function void print_ST_ROB_PACKET(ST_ROB_PACKET i);



import "DPI-C" context function void read_MT_ENTRY(MT_ENTRY i);
import "DPI-C" context function void check_MT_ENTRY(MT_ENTRY i);
import "DPI-C" context function void step_MT_ENTRY(MT_ENTRY i);
import "DPI-C" context function void print_MT_ENTRY(MT_ENTRY i);



import "DPI-C" context function void read_MT_RS_PACKET(MT_RS_PACKET i);
import "DPI-C" context function void check_MT_RS_PACKET(MT_RS_PACKET i);
import "DPI-C" context function void step_MT_RS_PACKET(MT_RS_PACKET i);
import "DPI-C" context function void print_MT_RS_PACKET(MT_RS_PACKET i);



import "DPI-C" context function void read_CDB_PACKET(CDB_PACKET i);
import "DPI-C" context function void check_CDB_PACKET(CDB_PACKET i);
import "DPI-C" context function void step_CDB_PACKET(CDB_PACKET i);
import "DPI-C" context function void print_CDB_PACKET(CDB_PACKET i);



import "DPI-C" context function void read_RS_ENTRY(RS_ENTRY i);
import "DPI-C" context function void check_RS_ENTRY(RS_ENTRY i);
import "DPI-C" context function void step_RS_ENTRY(RS_ENTRY i);
import "DPI-C" context function void print_RS_ENTRY(RS_ENTRY i);



import "DPI-C" context function void read_RS_PRIORITY(RS_PRIORITY i);
import "DPI-C" context function void check_RS_PRIORITY(RS_PRIORITY i);
import "DPI-C" context function void step_RS_PRIORITY(RS_PRIORITY i);
import "DPI-C" context function void print_RS_PRIORITY(RS_PRIORITY i);



import "DPI-C" context function void read_IF_DECODE_PACKET(IF_DECODE_PACKET i);
import "DPI-C" context function void check_IF_DECODE_PACKET(IF_DECODE_PACKET i);
import "DPI-C" context function void step_IF_DECODE_PACKET(IF_DECODE_PACKET i);
import "DPI-C" context function void print_IF_DECODE_PACKET(IF_DECODE_PACKET i);



import "DPI-C" context function void read_DECODE_ROB_PACKET(DECODE_ROB_PACKET i);
import "DPI-C" context function void check_DECODE_ROB_PACKET(DECODE_ROB_PACKET i);
import "DPI-C" context function void step_DECODE_ROB_PACKET(DECODE_ROB_PACKET i);
import "DPI-C" context function void print_DECODE_ROB_PACKET(DECODE_ROB_PACKET i);



import "DPI-C" context function void read_FU_ISSUE_PACKET(FU_ISSUE_PACKET i);
import "DPI-C" context function void check_FU_ISSUE_PACKET(FU_ISSUE_PACKET i);
import "DPI-C" context function void step_FU_ISSUE_PACKET(FU_ISSUE_PACKET i);
import "DPI-C" context function void print_FU_ISSUE_PACKET(FU_ISSUE_PACKET i);



import "DPI-C" context function void read_FU_COMPLETE_PACKET(FU_COMPLETE_PACKET i);
import "DPI-C" context function void check_FU_COMPLETE_PACKET(FU_COMPLETE_PACKET i);
import "DPI-C" context function void step_FU_COMPLETE_PACKET(FU_COMPLETE_PACKET i);
import "DPI-C" context function void print_FU_COMPLETE_PACKET(FU_COMPLETE_PACKET i);



import "DPI-C" context function void read_COMPLETE_REGFILE_PACKET(COMPLETE_REGFILE_PACKET i);
import "DPI-C" context function void check_COMPLETE_REGFILE_PACKET(COMPLETE_REGFILE_PACKET i);
import "DPI-C" context function void step_COMPLETE_REGFILE_PACKET(COMPLETE_REGFILE_PACKET i);
import "DPI-C" context function void print_COMPLETE_REGFILE_PACKET(COMPLETE_REGFILE_PACKET i);



import "DPI-C" context function void read_option_t(option_t i);
import "DPI-C" context function void check_option_t(option_t i);
import "DPI-C" context function void step_option_t(option_t i);
import "DPI-C" context function void print_option_t(option_t i);



import "DPI-C" context function void read_simple_option_t(simple_option_t i);
import "DPI-C" context function void check_simple_option_t(simple_option_t i);
import "DPI-C" context function void step_simple_option_t(simple_option_t i);
import "DPI-C" context function void print_simple_option_t(simple_option_t i);



import "DPI-C" context function void read_option_is_t(option_is_t i);
import "DPI-C" context function void check_option_is_t(option_is_t i);
import "DPI-C" context function void step_option_is_t(option_is_t i);
import "DPI-C" context function void print_option_is_t(option_is_t i);



import "DPI-C" context function void read_MEM_IF_PACKET(MEM_IF_PACKET i);
import "DPI-C" context function void check_MEM_IF_PACKET(MEM_IF_PACKET i);
import "DPI-C" context function void step_MEM_IF_PACKET(MEM_IF_PACKET i);
import "DPI-C" context function void print_MEM_IF_PACKET(MEM_IF_PACKET i);



import "DPI-C" context function void read_IF_ICACHE_PACKET(IF_ICACHE_PACKET i);
import "DPI-C" context function void check_IF_ICACHE_PACKET(IF_ICACHE_PACKET i);
import "DPI-C" context function void step_IF_ICACHE_PACKET(IF_ICACHE_PACKET i);
import "DPI-C" context function void print_IF_ICACHE_PACKET(IF_ICACHE_PACKET i);



import "DPI-C" context function void read_ICACHE_IF_PACKET(ICACHE_IF_PACKET i);
import "DPI-C" context function void check_ICACHE_IF_PACKET(ICACHE_IF_PACKET i);
import "DPI-C" context function void step_ICACHE_IF_PACKET(ICACHE_IF_PACKET i);
import "DPI-C" context function void print_ICACHE_IF_PACKET(ICACHE_IF_PACKET i);



import "DPI-C" context function void read_ICACHE_ENTRY(ICACHE_ENTRY i);
import "DPI-C" context function void check_ICACHE_ENTRY(ICACHE_ENTRY i);
import "DPI-C" context function void step_ICACHE_ENTRY(ICACHE_ENTRY i);
import "DPI-C" context function void print_ICACHE_ENTRY(ICACHE_ENTRY i);



import "DPI-C" context function void read_REQUEST_POOL_ENTRY(REQUEST_POOL_ENTRY i);
import "DPI-C" context function void check_REQUEST_POOL_ENTRY(REQUEST_POOL_ENTRY i);
import "DPI-C" context function void step_REQUEST_POOL_ENTRY(REQUEST_POOL_ENTRY i);
import "DPI-C" context function void print_REQUEST_POOL_ENTRY(REQUEST_POOL_ENTRY i);



import "DPI-C" context function void read_DCACHE_ADDR(DCACHE_ADDR i);
import "DPI-C" context function void check_DCACHE_ADDR(DCACHE_ADDR i);
import "DPI-C" context function void step_DCACHE_ADDR(DCACHE_ADDR i);
import "DPI-C" context function void print_DCACHE_ADDR(DCACHE_ADDR i);



import "DPI-C" context function void read_PRB_ADDR(PRB_ADDR i);
import "DPI-C" context function void check_PRB_ADDR(PRB_ADDR i);
import "DPI-C" context function void step_PRB_ADDR(PRB_ADDR i);
import "DPI-C" context function void print_PRB_ADDR(PRB_ADDR i);



import "DPI-C" context function void read_VCACHE_ADDR(VCACHE_ADDR i);
import "DPI-C" context function void check_VCACHE_ADDR(VCACHE_ADDR i);
import "DPI-C" context function void step_VCACHE_ADDR(VCACHE_ADDR i);
import "DPI-C" context function void print_VCACHE_ADDR(VCACHE_ADDR i);



import "DPI-C" context function void read_WORD_ADDR(WORD_ADDR i);
import "DPI-C" context function void check_WORD_ADDR(WORD_ADDR i);
import "DPI-C" context function void step_WORD_ADDR(WORD_ADDR i);
import "DPI-C" context function void print_WORD_ADDR(WORD_ADDR i);



import "DPI-C" context function void read_MEM_ADDR(MEM_ADDR i);
import "DPI-C" context function void check_MEM_ADDR(MEM_ADDR i);
import "DPI-C" context function void step_MEM_ADDR(MEM_ADDR i);
import "DPI-C" context function void print_MEM_ADDR(MEM_ADDR i);



import "DPI-C" context function void read_LSQ_LD_PACKET_OUT(LSQ_LD_PACKET_OUT i);
import "DPI-C" context function void check_LSQ_LD_PACKET_OUT(LSQ_LD_PACKET_OUT i);
import "DPI-C" context function void step_LSQ_LD_PACKET_OUT(LSQ_LD_PACKET_OUT i);
import "DPI-C" context function void print_LSQ_LD_PACKET_OUT(LSQ_LD_PACKET_OUT i);



import "DPI-C" context function void read_FU_LSQ_LD_PACKET(FU_LSQ_LD_PACKET i);
import "DPI-C" context function void check_FU_LSQ_LD_PACKET(FU_LSQ_LD_PACKET i);
import "DPI-C" context function void step_FU_LSQ_LD_PACKET(FU_LSQ_LD_PACKET i);
import "DPI-C" context function void print_FU_LSQ_LD_PACKET(FU_LSQ_LD_PACKET i);



import "DPI-C" context function void read_FU_LSQ_ST_PACKET(FU_LSQ_ST_PACKET i);
import "DPI-C" context function void check_FU_LSQ_ST_PACKET(FU_LSQ_ST_PACKET i);
import "DPI-C" context function void step_FU_LSQ_ST_PACKET(FU_LSQ_ST_PACKET i);
import "DPI-C" context function void print_FU_LSQ_ST_PACKET(FU_LSQ_ST_PACKET i);



import "DPI-C" context function void read_DECODE_LSQ_PACKET(DECODE_LSQ_PACKET i);
import "DPI-C" context function void check_DECODE_LSQ_PACKET(DECODE_LSQ_PACKET i);
import "DPI-C" context function void step_DECODE_LSQ_PACKET(DECODE_LSQ_PACKET i);
import "DPI-C" context function void print_DECODE_LSQ_PACKET(DECODE_LSQ_PACKET i);



import "DPI-C" context function void read_LSQ_RS_PACKET(LSQ_RS_PACKET i);
import "DPI-C" context function void check_LSQ_RS_PACKET(LSQ_RS_PACKET i);
import "DPI-C" context function void step_LSQ_RS_PACKET(LSQ_RS_PACKET i);
import "DPI-C" context function void print_LSQ_RS_PACKET(LSQ_RS_PACKET i);



import "DPI-C" context function void read_SQ_ENTRY(SQ_ENTRY i);
import "DPI-C" context function void check_SQ_ENTRY(SQ_ENTRY i);
import "DPI-C" context function void step_SQ_ENTRY(SQ_ENTRY i);
import "DPI-C" context function void print_SQ_ENTRY(SQ_ENTRY i);



import "DPI-C" context function void read_LSQ_DCACHE_LD_PACKET(LSQ_DCACHE_LD_PACKET i);
import "DPI-C" context function void check_LSQ_DCACHE_LD_PACKET(LSQ_DCACHE_LD_PACKET i);
import "DPI-C" context function void step_LSQ_DCACHE_LD_PACKET(LSQ_DCACHE_LD_PACKET i);
import "DPI-C" context function void print_LSQ_DCACHE_LD_PACKET(LSQ_DCACHE_LD_PACKET i);



import "DPI-C" context function void read_LSQ_PRB_ST_PACKET(LSQ_PRB_ST_PACKET i);
import "DPI-C" context function void check_LSQ_PRB_ST_PACKET(LSQ_PRB_ST_PACKET i);
import "DPI-C" context function void step_LSQ_PRB_ST_PACKET(LSQ_PRB_ST_PACKET i);
import "DPI-C" context function void print_LSQ_PRB_ST_PACKET(LSQ_PRB_ST_PACKET i);



import "DPI-C" context function void read_DCACHE_LSQ_LD_PACKET(DCACHE_LSQ_LD_PACKET i);
import "DPI-C" context function void check_DCACHE_LSQ_LD_PACKET(DCACHE_LSQ_LD_PACKET i);
import "DPI-C" context function void step_DCACHE_LSQ_LD_PACKET(DCACHE_LSQ_LD_PACKET i);
import "DPI-C" context function void print_DCACHE_LSQ_LD_PACKET(DCACHE_LSQ_LD_PACKET i);



import "DPI-C" context function void read_ROB_LSQ_PACKET(ROB_LSQ_PACKET i);
import "DPI-C" context function void check_ROB_LSQ_PACKET(ROB_LSQ_PACKET i);
import "DPI-C" context function void step_ROB_LSQ_PACKET(ROB_LSQ_PACKET i);
import "DPI-C" context function void print_ROB_LSQ_PACKET(ROB_LSQ_PACKET i);



import "DPI-C" context function void read_DCACHE_LSQ_ST_PACKET(DCACHE_LSQ_ST_PACKET i);
import "DPI-C" context function void check_DCACHE_LSQ_ST_PACKET(DCACHE_LSQ_ST_PACKET i);
import "DPI-C" context function void step_DCACHE_LSQ_ST_PACKET(DCACHE_LSQ_ST_PACKET i);
import "DPI-C" context function void print_DCACHE_LSQ_ST_PACKET(DCACHE_LSQ_ST_PACKET i);



import "DPI-C" context function void read_DCACHE_ENTRY(DCACHE_ENTRY i);
import "DPI-C" context function void check_DCACHE_ENTRY(DCACHE_ENTRY i);
import "DPI-C" context function void step_DCACHE_ENTRY(DCACHE_ENTRY i);
import "DPI-C" context function void print_DCACHE_ENTRY(DCACHE_ENTRY i);



import "DPI-C" context function void read_DCACHE_CTRL_ST_PACKET(DCACHE_CTRL_ST_PACKET i);
import "DPI-C" context function void check_DCACHE_CTRL_ST_PACKET(DCACHE_CTRL_ST_PACKET i);
import "DPI-C" context function void step_DCACHE_CTRL_ST_PACKET(DCACHE_CTRL_ST_PACKET i);
import "DPI-C" context function void print_DCACHE_CTRL_ST_PACKET(DCACHE_CTRL_ST_PACKET i);



import "DPI-C" context function void read_DCACHE_MEM_PACKET(DCACHE_MEM_PACKET i);
import "DPI-C" context function void check_DCACHE_MEM_PACKET(DCACHE_MEM_PACKET i);
import "DPI-C" context function void step_DCACHE_MEM_PACKET(DCACHE_MEM_PACKET i);
import "DPI-C" context function void print_DCACHE_MEM_PACKET(DCACHE_MEM_PACKET i);



import "DPI-C" context function void read_MSHR_DCACHE_MEM_PACKET(MSHR_DCACHE_MEM_PACKET i);
import "DPI-C" context function void check_MSHR_DCACHE_MEM_PACKET(MSHR_DCACHE_MEM_PACKET i);
import "DPI-C" context function void step_MSHR_DCACHE_MEM_PACKET(MSHR_DCACHE_MEM_PACKET i);
import "DPI-C" context function void print_MSHR_DCACHE_MEM_PACKET(MSHR_DCACHE_MEM_PACKET i);



import "DPI-C" context function void read_MEM_DCACHE_PACKET(MEM_DCACHE_PACKET i);
import "DPI-C" context function void check_MEM_DCACHE_PACKET(MEM_DCACHE_PACKET i);
import "DPI-C" context function void step_MEM_DCACHE_PACKET(MEM_DCACHE_PACKET i);
import "DPI-C" context function void print_MEM_DCACHE_PACKET(MEM_DCACHE_PACKET i);



import "DPI-C" context function void read_MSHR_ENTRY(MSHR_ENTRY i);
import "DPI-C" context function void check_MSHR_ENTRY(MSHR_ENTRY i);
import "DPI-C" context function void step_MSHR_ENTRY(MSHR_ENTRY i);
import "DPI-C" context function void print_MSHR_ENTRY(MSHR_ENTRY i);



import "DPI-C" context function void read_DCACHE_MEMREQ_SIGNAL(DCACHE_MEMREQ_SIGNAL i);
import "DPI-C" context function void check_DCACHE_MEMREQ_SIGNAL(DCACHE_MEMREQ_SIGNAL i);
import "DPI-C" context function void step_DCACHE_MEMREQ_SIGNAL(DCACHE_MEMREQ_SIGNAL i);
import "DPI-C" context function void print_DCACHE_MEMREQ_SIGNAL(DCACHE_MEMREQ_SIGNAL i);



import "DPI-C" context function void read_PRB_ENTRY(PRB_ENTRY i);
import "DPI-C" context function void check_PRB_ENTRY(PRB_ENTRY i);
import "DPI-C" context function void step_PRB_ENTRY(PRB_ENTRY i);
import "DPI-C" context function void print_PRB_ENTRY(PRB_ENTRY i);



import "DPI-C" context function void read_PRB_DCACHE_ST_PACKET(PRB_DCACHE_ST_PACKET i);
import "DPI-C" context function void check_PRB_DCACHE_ST_PACKET(PRB_DCACHE_ST_PACKET i);
import "DPI-C" context function void step_PRB_DCACHE_ST_PACKET(PRB_DCACHE_ST_PACKET i);
import "DPI-C" context function void print_PRB_DCACHE_ST_PACKET(PRB_DCACHE_ST_PACKET i);



import "DPI-C" context function void read_ROB_BP_PACKET(ROB_BP_PACKET i);
import "DPI-C" context function void check_ROB_BP_PACKET(ROB_BP_PACKET i);
import "DPI-C" context function void step_ROB_BP_PACKET(ROB_BP_PACKET i);
import "DPI-C" context function void print_ROB_BP_PACKET(ROB_BP_PACKET i);



import "DPI-C" context function void read_BP_ENTRY(BP_ENTRY i);
import "DPI-C" context function void check_BP_ENTRY(BP_ENTRY i);
import "DPI-C" context function void step_BP_ENTRY(BP_ENTRY i);
import "DPI-C" context function void print_BP_ENTRY(BP_ENTRY i);



import "DPI-C" context function void read_DUMP_IDX_PACKET(DUMP_IDX_PACKET i);
import "DPI-C" context function void check_DUMP_IDX_PACKET(DUMP_IDX_PACKET i);
import "DPI-C" context function void step_DUMP_IDX_PACKET(DUMP_IDX_PACKET i);
import "DPI-C" context function void print_DUMP_IDX_PACKET(DUMP_IDX_PACKET i);





typedef struct packed {
    
    logic [3:0] mem2proc_response;
    
    logic [63:0] mem2proc_data;
    
    logic [3:0] mem2proc_tag;
    
} pipeline_IN_P;

typedef struct packed {
    
    logic [1:0] proc2mem_command;
    
    logic [31:0] proc2mem_addr;
    
    logic [63:0] proc2mem_data;
    
    logic halt_retire;
    
    logic halt;
    
    ss_size_t pipeline_completed_insts;
    
} pipeline_OUT_P;


import "DPI-C" context function void read_pipeline_IN_P(pipeline_IN_P i);
import "DPI-C" context function void check_pipeline_IN_P(pipeline_IN_P i);
import "DPI-C" context function void step_pipeline_IN_P(pipeline_IN_P i);
import "DPI-C" context function void print_pipeline_IN_P(pipeline_IN_P i);



import "DPI-C" context function void read_pipeline_OUT_P(pipeline_OUT_P i);
import "DPI-C" context function void check_pipeline_OUT_P(pipeline_OUT_P i);
import "DPI-C" context function void step_pipeline_OUT_P(pipeline_OUT_P i);
import "DPI-C" context function void print_pipeline_OUT_P(pipeline_OUT_P i);


import "DPI-C" context function void read_pipeline_TB_IN(pipeline_IN_P i);
import "DPI-C" context function void read_pipeline_TB_OUT(pipeline_OUT_P i);
import "DPI-C" context function void step_pipeline_TB();
import "DPI-C" context function int check_pipeline_TB();
import "DPI-C" context function void print_pipeline_TB();




module testbench;

    logic clock, reset;
    int check;

    always begin
        #5;
        clock = ~clock;
    end

    task exit_on_error;
        #1;
        $display("@@@Failed at time %f", $time);
        $finish;
    endtask

    

    pipeline_IN_P in;
    pipeline_OUT_P out;

    task clear_in;
        in = '0;
    endtask


    pipeline pipeline_0 (
        

        
        .mem2proc_response(in.mem2proc_response),
        
        .mem2proc_data(in.mem2proc_data),
        
        .mem2proc_tag(in.mem2proc_tag),
        

        
        .proc2mem_command(out.proc2mem_command),
        
        .proc2mem_addr(out.proc2mem_addr),
        
        .proc2mem_data(out.proc2mem_data),
        
        .halt_retire(out.halt_retire),
        
        .halt(out.halt),
        
        .pipeline_completed_insts(out.pipeline_completed_insts)
        
    );

    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
	task fill_ROB_ENTRY;
		output ROB_ENTRY p;
		input longint told_idx_, t_idx_, dest_reg_idx_, complete_, branch_, branch_target_, rollback_, halt_, PC_, branch_taken_, branch_mispredicted_, store_, tournament_, call_, ret_;
		p.told_idx = told_idx_;
		p.t_idx = t_idx_;
		p.dest_reg_idx = dest_reg_idx_;
		p.complete = complete_;
		p.branch = branch_;
		p.branch_target = branch_target_;
		p.rollback = rollback_;
		p.halt = halt_;
		p.PC = PC_;
		p.branch_taken = branch_taken_;
		p.branch_mispredicted = branch_mispredicted_;
		p.store = store_;
		p.tournament = tournament_;
		p.call = call_;
		p.ret = ret_;
	endtask

	task clear_ROB_ENTRY;
		output ROB_ENTRY p;
		p.told_idx = 0;
		p.t_idx = 0;
		p.dest_reg_idx = 0;
		p.complete = 0;
		p.branch = 0;
		p.branch_target = 0;
		p.rollback = 0;
		p.halt = 0;
		p.PC = 0;
		p.branch_taken = 0;
		p.branch_mispredicted = 0;
		p.store = 0;
		p.tournament = 0;
		p.call = 0;
		p.ret = 0;
	endtask

    
    
	task fill_BRANCH_PACKET;
		output BRANCH_PACKET p;
		input longint valid_, taken_, branch_target_, branch_address_;
		p.valid = valid_;
		p.taken = taken_;
		p.branch_target = branch_target_;
		p.branch_address = branch_address_;
	endtask

	task clear_BRANCH_PACKET;
		output BRANCH_PACKET p;
		p.valid = 0;
		p.taken = 0;
		p.branch_target = 0;
		p.branch_address = 0;
	endtask

    
    
	task fill_DECODE_MT_PACKET;
		output DECODE_MT_PACKET p;
		input longint valid_, rs1_idx_, rs2_idx_, dest_reg_idx_;
		p.valid = valid_;
		p.rs1_idx = rs1_idx_;
		p.rs2_idx = rs2_idx_;
		p.dest_reg_idx = dest_reg_idx_;
	endtask

	task clear_DECODE_MT_PACKET;
		output DECODE_MT_PACKET p;
		p.valid = 0;
		p.rs1_idx = 0;
		p.rs2_idx = 0;
		p.dest_reg_idx = 0;
	endtask

    
    
	task fill_DECODE_RS_PACKET;
		output DECODE_RS_PACKET p;
		input longint NPC_, PC_, opa_select_, opb_select_, inst_, alu_func_, fu_;
		p.NPC = NPC_;
		p.PC = PC_;
		p.opa_select = opa_select_;
		p.opb_select = opb_select_;
		p.inst = inst_;
		p.alu_func = alu_func_;
		p.fu = fu_;
	endtask

	task clear_DECODE_RS_PACKET;
		output DECODE_RS_PACKET p;
		p.NPC = 0;
		p.PC = 0;
		p.opa_select = 0;
		p.opb_select = 0;
		p.inst = 0;
		p.alu_func = 0;
		p.fu = 0;
	endtask

    
    
	task fill_ROB_MT_PACKET;
		output ROB_MT_PACKET p;
		input longint valid_, retire_told_idx_, retire_t_idx_, dest_reg_idx_;
		p.valid = valid_;
		p.retire_told_idx = retire_told_idx_;
		p.retire_t_idx = retire_t_idx_;
		p.dest_reg_idx = dest_reg_idx_;
	endtask

	task clear_ROB_MT_PACKET;
		output ROB_MT_PACKET p;
		p.valid = 0;
		p.retire_told_idx = 0;
		p.retire_t_idx = 0;
		p.dest_reg_idx = 0;
	endtask

    
    
	task fill_MT_ROB_PACKET;
		output MT_ROB_PACKET p;
		input longint told_idx_, t_idx_, dest_reg_idx_;
		p.told_idx = told_idx_;
		p.t_idx = t_idx_;
		p.dest_reg_idx = dest_reg_idx_;
	endtask

	task clear_MT_ROB_PACKET;
		output MT_ROB_PACKET p;
		p.told_idx = 0;
		p.t_idx = 0;
		p.dest_reg_idx = 0;
	endtask

    
    
	task fill_BRANCH_ROB_PACKET;
		output BRANCH_ROB_PACKET p;
		input longint valid_, is_cond_branch_, rob_num_, branch_target_, branch_taken_;
		p.valid = valid_;
		p.is_cond_branch = is_cond_branch_;
		p.rob_num = rob_num_;
		p.branch_target = branch_target_;
		p.branch_taken = branch_taken_;
	endtask

	task clear_BRANCH_ROB_PACKET;
		output BRANCH_ROB_PACKET p;
		p.valid = 0;
		p.is_cond_branch = 0;
		p.rob_num = 0;
		p.branch_target = 0;
		p.branch_taken = 0;
	endtask

    
    
	task fill_ST_ROB_PACKET;
		output ST_ROB_PACKET p;
		input longint valid_, rob_num_;
		p.valid = valid_;
		p.rob_num = rob_num_;
	endtask

	task clear_ST_ROB_PACKET;
		output ST_ROB_PACKET p;
		p.valid = 0;
		p.rob_num = 0;
	endtask

    
    
	task fill_MT_ENTRY;
		output MT_ENTRY p;
		input longint t_idx_, ready_;
		p.t_idx = t_idx_;
		p.ready = ready_;
	endtask

	task clear_MT_ENTRY;
		output MT_ENTRY p;
		p.t_idx = 0;
		p.ready = 0;
	endtask

    
    
    
    
	task fill_CDB_PACKET;
		output CDB_PACKET p;
		input longint valid_, rob_num_, t_idx_;
		p.valid = valid_;
		p.rob_num = rob_num_;
		p.t_idx = t_idx_;
	endtask

	task clear_CDB_PACKET;
		output CDB_PACKET p;
		p.valid = 0;
		p.rob_num = 0;
		p.t_idx = 0;
	endtask

    
    
    
    
	task fill_RS_PRIORITY;
		output RS_PRIORITY p;
		input longint is_empty_, tag_;
		p.is_empty = is_empty_;
		p.tag = tag_;
	endtask

	task clear_RS_PRIORITY;
		output RS_PRIORITY p;
		p.is_empty = 0;
		p.tag = 0;
	endtask

    
    
	task fill_IF_DECODE_PACKET;
		output IF_DECODE_PACKET p;
		input longint valid_, inst_, NPC_, PC_, branch_target_, branch_taken_, tournament_, call_, ret_;
		p.valid = valid_;
		p.inst = inst_;
		p.NPC = NPC_;
		p.PC = PC_;
		p.branch_target = branch_target_;
		p.branch_taken = branch_taken_;
		p.tournament = tournament_;
		p.call = call_;
		p.ret = ret_;
	endtask

	task clear_IF_DECODE_PACKET;
		output IF_DECODE_PACKET p;
		p.valid = 0;
		p.inst = 0;
		p.NPC = 0;
		p.PC = 0;
		p.branch_target = 0;
		p.branch_taken = 0;
		p.tournament = 0;
		p.call = 0;
		p.ret = 0;
	endtask

    
    
    
    
	task fill_FU_ISSUE_PACKET;
		output FU_ISSUE_PACKET p;
		input longint ALU_free_spaces_, ST_free_spaces_, LD_free_spaces_, MUL_free_spaces_, BR_free_spaces_;
		p.ALU_free_spaces = ALU_free_spaces_;
		p.ST_free_spaces = ST_free_spaces_;
		p.LD_free_spaces = LD_free_spaces_;
		p.MUL_free_spaces = MUL_free_spaces_;
		p.BR_free_spaces = BR_free_spaces_;
	endtask

	task clear_FU_ISSUE_PACKET;
		output FU_ISSUE_PACKET p;
		p.ALU_free_spaces = 0;
		p.ST_free_spaces = 0;
		p.LD_free_spaces = 0;
		p.MUL_free_spaces = 0;
		p.BR_free_spaces = 0;
	endtask

    
    
	task fill_FU_COMPLETE_PACKET;
		output FU_COMPLETE_PACKET p;
		input longint valid_, complete_priority_, rob_num_, value_, t_idx_;
		p.valid = valid_;
		p.complete_priority = complete_priority_;
		p.rob_num = rob_num_;
		p.value = value_;
		p.t_idx = t_idx_;
	endtask

	task clear_FU_COMPLETE_PACKET;
		output FU_COMPLETE_PACKET p;
		p.valid = 0;
		p.complete_priority = 0;
		p.rob_num = 0;
		p.value = 0;
		p.t_idx = 0;
	endtask

    
    
	task fill_COMPLETE_REGFILE_PACKET;
		output COMPLETE_REGFILE_PACKET p;
		input longint t_idx_, value_;
		p.t_idx = t_idx_;
		p.value = value_;
	endtask

	task clear_COMPLETE_REGFILE_PACKET;
		output COMPLETE_REGFILE_PACKET p;
		p.t_idx = 0;
		p.value = 0;
	endtask

    
    
	task fill_option_t;
		output option_t p;
		input longint p_, tag_;
		p.p = p_;
		p.tag = tag_;
	endtask

	task clear_option_t;
		output option_t p;
		p.p = 0;
		p.tag = 0;
	endtask

    
    
	task fill_simple_option_t;
		output simple_option_t p;
		input longint valid_, tag_;
		p.valid = valid_;
		p.tag = tag_;
	endtask

	task clear_simple_option_t;
		output simple_option_t p;
		p.valid = 0;
		p.tag = 0;
	endtask

    
    
	task fill_option_is_t;
		output option_is_t p;
		input longint p_, tag_;
		p.p = p_;
		p.tag = tag_;
	endtask

	task clear_option_is_t;
		output option_is_t p;
		p.p = 0;
		p.tag = 0;
	endtask

    
    
	task fill_MEM_IF_PACKET;
		output MEM_IF_PACKET p;
		input longint bus_filled_, mem_in_;
		p.bus_filled = bus_filled_;
		p.mem_in = mem_in_;
	endtask

	task clear_MEM_IF_PACKET;
		output MEM_IF_PACKET p;
		p.bus_filled = 0;
		p.mem_in = 0;
	endtask

    
    
	task fill_IF_ICACHE_PACKET;
		output IF_ICACHE_PACKET p;
		input longint valid_, address_;
		p.valid = valid_;
		p.address = address_;
	endtask

	task clear_IF_ICACHE_PACKET;
		output IF_ICACHE_PACKET p;
		p.valid = 0;
		p.address = 0;
	endtask

    
    
	task fill_ICACHE_IF_PACKET;
		output ICACHE_IF_PACKET p;
		input longint valid_, data_;
		p.valid = valid_;
		p.data = data_;
	endtask

	task clear_ICACHE_IF_PACKET;
		output ICACHE_IF_PACKET p;
		p.valid = 0;
		p.data = 0;
	endtask

    
    
	task fill_ICACHE_ENTRY;
		output ICACHE_ENTRY p;
		input longint valid_, tag_, data_;
		p.valid = valid_;
		p.tag = tag_;
		p.data = data_;
	endtask

	task clear_ICACHE_ENTRY;
		output ICACHE_ENTRY p;
		p.valid = 0;
		p.tag = 0;
		p.data = 0;
	endtask

    
    
	task fill_REQUEST_POOL_ENTRY;
		output REQUEST_POOL_ENTRY p;
		input longint valid_, address_;
		p.valid = valid_;
		p.address = address_;
	endtask

	task clear_REQUEST_POOL_ENTRY;
		output REQUEST_POOL_ENTRY p;
		p.valid = 0;
		p.address = 0;
	endtask

    
    
	task fill_DCACHE_ADDR;
		output DCACHE_ADDR p;
		input longint tag_, index_, offset_;
		p.tag = tag_;
		p.index = index_;
		p.offset = offset_;
	endtask

	task clear_DCACHE_ADDR;
		output DCACHE_ADDR p;
		p.tag = 0;
		p.index = 0;
		p.offset = 0;
	endtask

    
    
	task fill_PRB_ADDR;
		output PRB_ADDR p;
		input longint tag_, offset_;
		p.tag = tag_;
		p.offset = offset_;
	endtask

	task clear_PRB_ADDR;
		output PRB_ADDR p;
		p.tag = 0;
		p.offset = 0;
	endtask

    
    
	task fill_VCACHE_ADDR;
		output VCACHE_ADDR p;
		input longint tag_, offset_;
		p.tag = tag_;
		p.offset = offset_;
	endtask

	task clear_VCACHE_ADDR;
		output VCACHE_ADDR p;
		p.tag = 0;
		p.offset = 0;
	endtask

    
    
	task fill_WORD_ADDR;
		output WORD_ADDR p;
		input longint tag_, offset_;
		p.tag = tag_;
		p.offset = offset_;
	endtask

	task clear_WORD_ADDR;
		output WORD_ADDR p;
		p.tag = 0;
		p.offset = 0;
	endtask

    
    
	task fill_MEM_ADDR;
		output MEM_ADDR p;
		input longint tag_, offset_;
		p.tag = tag_;
		p.offset = offset_;
	endtask

	task clear_MEM_ADDR;
		output MEM_ADDR p;
		p.tag = 0;
		p.offset = 0;
	endtask

    
    
	task fill_LSQ_LD_PACKET_OUT;
		output LSQ_LD_PACKET_OUT p;
		input longint valid_, value_, valid_byte_;
		p.valid = valid_;
		p.value = value_;
		p.valid_byte = valid_byte_;
	endtask

	task clear_LSQ_LD_PACKET_OUT;
		output LSQ_LD_PACKET_OUT p;
		p.valid = 0;
		p.value = 0;
		p.valid_byte = 0;
	endtask

    
    
	task fill_FU_LSQ_LD_PACKET;
		output FU_LSQ_LD_PACKET p;
		input longint valid_, address_, age_, valid_byte_;
		p.valid = valid_;
		p.address = address_;
		p.age = age_;
		p.valid_byte = valid_byte_;
	endtask

	task clear_FU_LSQ_LD_PACKET;
		output FU_LSQ_LD_PACKET p;
		p.valid = 0;
		p.address = 0;
		p.age = 0;
		p.valid_byte = 0;
	endtask

    
    
	task fill_FU_LSQ_ST_PACKET;
		output FU_LSQ_ST_PACKET p;
		input longint valid_, address_, value_, valid_byte_, age_;
		p.valid = valid_;
		p.address = address_;
		p.value = value_;
		p.valid_byte = valid_byte_;
		p.age = age_;
	endtask

	task clear_FU_LSQ_ST_PACKET;
		output FU_LSQ_ST_PACKET p;
		p.valid = 0;
		p.address = 0;
		p.value = 0;
		p.valid_byte = 0;
		p.age = 0;
	endtask

    
    
	task fill_DECODE_LSQ_PACKET;
		output DECODE_LSQ_PACKET p;
		input longint valid_, is_ST_;
		p.valid = valid_;
		p.is_ST = is_ST_;
	endtask

	task clear_DECODE_LSQ_PACKET;
		output DECODE_LSQ_PACKET p;
		p.valid = 0;
		p.is_ST = 0;
	endtask

    
    
	task fill_LSQ_RS_PACKET;
		output LSQ_RS_PACKET p;
		input longint age_;
		p.age = age_;
	endtask

	task clear_LSQ_RS_PACKET;
		output LSQ_RS_PACKET p;
		p.age = 0;
	endtask

    
    
	task fill_SQ_ENTRY;
		output SQ_ENTRY p;
		input longint valid_, address_, value_, valid_byte_;
		p.valid = valid_;
		p.address = address_;
		p.value = value_;
		p.valid_byte = valid_byte_;
	endtask

	task clear_SQ_ENTRY;
		output SQ_ENTRY p;
		p.valid = 0;
		p.address = 0;
		p.value = 0;
		p.valid_byte = 0;
	endtask

    
    
	task fill_LSQ_DCACHE_LD_PACKET;
		output LSQ_DCACHE_LD_PACKET p;
		input longint valid_, address_;
		p.valid = valid_;
		p.address = address_;
	endtask

	task clear_LSQ_DCACHE_LD_PACKET;
		output LSQ_DCACHE_LD_PACKET p;
		p.valid = 0;
		p.address = 0;
	endtask

    
    
	task fill_LSQ_PRB_ST_PACKET;
		output LSQ_PRB_ST_PACKET p;
		input longint valid_, address_, value_, valid_byte_;
		p.valid = valid_;
		p.address = address_;
		p.value = value_;
		p.valid_byte = valid_byte_;
	endtask

	task clear_LSQ_PRB_ST_PACKET;
		output LSQ_PRB_ST_PACKET p;
		p.valid = 0;
		p.address = 0;
		p.value = 0;
		p.valid_byte = 0;
	endtask

    
    
	task fill_DCACHE_LSQ_LD_PACKET;
		output DCACHE_LSQ_LD_PACKET p;
		input longint valid_, value_;
		p.valid = valid_;
		p.value = value_;
	endtask

	task clear_DCACHE_LSQ_LD_PACKET;
		output DCACHE_LSQ_LD_PACKET p;
		p.valid = 0;
		p.value = 0;
	endtask

    
    
	task fill_ROB_LSQ_PACKET;
		output ROB_LSQ_PACKET p;
		input longint valid_;
		p.valid = valid_;
	endtask

	task clear_ROB_LSQ_PACKET;
		output ROB_LSQ_PACKET p;
		p.valid = 0;
	endtask

    
    
	task fill_DCACHE_LSQ_ST_PACKET;
		output DCACHE_LSQ_ST_PACKET p;
		input longint valid_;
		p.valid = valid_;
	endtask

	task clear_DCACHE_LSQ_ST_PACKET;
		output DCACHE_LSQ_ST_PACKET p;
		p.valid = 0;
	endtask

    
    
	task fill_DCACHE_ENTRY;
		output DCACHE_ENTRY p;
		input longint addr_, data_, valid_, dirty_;
		p.addr = addr_;
		p.data = data_;
		p.valid = valid_;
		p.dirty = dirty_;
	endtask

	task clear_DCACHE_ENTRY;
		output DCACHE_ENTRY p;
		p.addr = 0;
		p.data = 0;
		p.valid = 0;
		p.dirty = 0;
	endtask

    
    
	task fill_DCACHE_CTRL_ST_PACKET;
		output DCACHE_CTRL_ST_PACKET p;
		input longint valid_, address_, mask_, data_;
		p.valid = valid_;
		p.address = address_;
		p.mask = mask_;
		p.data = data_;
	endtask

	task clear_DCACHE_CTRL_ST_PACKET;
		output DCACHE_CTRL_ST_PACKET p;
		p.valid = 0;
		p.address = 0;
		p.mask = 0;
		p.data = 0;
	endtask

    
    
	task fill_DCACHE_MEM_PACKET;
		output DCACHE_MEM_PACKET p;
		input longint valid_, command_, addr_, data_;
		p.valid = valid_;
		p.command = command_;
		p.addr = addr_;
		p.data = data_;
	endtask

	task clear_DCACHE_MEM_PACKET;
		output DCACHE_MEM_PACKET p;
		p.valid = 0;
		p.command = 0;
		p.addr = 0;
		p.data = 0;
	endtask

    
    
	task fill_MSHR_DCACHE_MEM_PACKET;
		output MSHR_DCACHE_MEM_PACKET p;
		input longint valid_, command_, addr_, data_, mask_, w_data_;
		p.valid = valid_;
		p.command = command_;
		p.addr = addr_;
		p.data = data_;
		p.mask = mask_;
		p.w_data = w_data_;
	endtask

	task clear_MSHR_DCACHE_MEM_PACKET;
		output MSHR_DCACHE_MEM_PACKET p;
		p.valid = 0;
		p.command = 0;
		p.addr = 0;
		p.data = 0;
		p.mask = 0;
		p.w_data = 0;
	endtask

    
    
	task fill_MEM_DCACHE_PACKET;
		output MEM_DCACHE_PACKET p;
		input longint tag_, response_, data_;
		p.tag = tag_;
		p.response = response_;
		p.data = data_;
	endtask

	task clear_MEM_DCACHE_PACKET;
		output MEM_DCACHE_PACKET p;
		p.tag = 0;
		p.response = 0;
		p.data = 0;
	endtask

    
    
	task fill_MSHR_ENTRY;
		output MSHR_ENTRY p;
		input longint addr_, data_, mask_, w_data_, dirty_;
		p.addr = addr_;
		p.data = data_;
		p.mask = mask_;
		p.w_data = w_data_;
		p.dirty = dirty_;
	endtask

	task clear_MSHR_ENTRY;
		output MSHR_ENTRY p;
		p.addr = 0;
		p.data = 0;
		p.mask = 0;
		p.w_data = 0;
		p.dirty = 0;
	endtask

    
    
	task fill_DCACHE_MEMREQ_SIGNAL;
		output DCACHE_MEMREQ_SIGNAL p;
		input longint ld_, st_;
		p.ld = ld_;
		p.st = st_;
	endtask

	task clear_DCACHE_MEMREQ_SIGNAL;
		output DCACHE_MEMREQ_SIGNAL p;
		p.ld = 0;
		p.st = 0;
	endtask

    
    
	task fill_PRB_ENTRY;
		output PRB_ENTRY p;
		input longint valid_, tag_, valid_byte_, value_, written_;
		p.valid = valid_;
		p.tag = tag_;
		p.valid_byte = valid_byte_;
		p.value = value_;
		p.written = written_;
	endtask

	task clear_PRB_ENTRY;
		output PRB_ENTRY p;
		p.valid = 0;
		p.tag = 0;
		p.valid_byte = 0;
		p.value = 0;
		p.written = 0;
	endtask

    
    
	task fill_PRB_DCACHE_ST_PACKET;
		output PRB_DCACHE_ST_PACKET p;
		input longint valid_, address_, valid_byte_, value_;
		p.valid = valid_;
		p.address = address_;
		p.valid_byte = valid_byte_;
		p.value = value_;
	endtask

	task clear_PRB_DCACHE_ST_PACKET;
		output PRB_DCACHE_ST_PACKET p;
		p.valid = 0;
		p.address = 0;
		p.valid_byte = 0;
		p.value = 0;
	endtask

    
    
	task fill_ROB_BP_PACKET;
		output ROB_BP_PACKET p;
		input longint valid_, taken_, is_insert_, branch_address_, tournament_, branch_mispredicted_, call_, ret_;
		p.valid = valid_;
		p.taken = taken_;
		p.is_insert = is_insert_;
		p.branch_address = branch_address_;
		p.tournament = tournament_;
		p.branch_mispredicted = branch_mispredicted_;
		p.call = call_;
		p.ret = ret_;
	endtask

	task clear_ROB_BP_PACKET;
		output ROB_BP_PACKET p;
		p.valid = 0;
		p.taken = 0;
		p.is_insert = 0;
		p.branch_address = 0;
		p.tournament = 0;
		p.branch_mispredicted = 0;
		p.call = 0;
		p.ret = 0;
	endtask

    
    
	task fill_BP_ENTRY;
		output BP_ENTRY p;
		input longint counter_, counter_inorder_, tournament_, first_time_;
		p.counter = counter_;
		p.counter_inorder = counter_inorder_;
		p.tournament = tournament_;
		p.first_time = first_time_;
	endtask

	task clear_BP_ENTRY;
		output BP_ENTRY p;
		p.counter = 0;
		p.counter_inorder = 0;
		p.tournament = 0;
		p.first_time = 0;
	endtask

    
    
	task fill_DUMP_IDX_PACKET;
		output DUMP_IDX_PACKET p;
		input longint index_, way_;
		p.index = index_;
		p.way = way_;
	endtask

	task clear_DUMP_IDX_PACKET;
		output DUMP_IDX_PACKET p;
		p.index = 0;
		p.way = 0;
	endtask

    


    initial begin
        clock = 1'b0;
        reset = 1'b0;
    
        
        clear_in();
        

        @(negedge clock)
        @(negedge clock)
        reset = 1'b1;
        @(negedge clock)
        @(negedge clock)
        reset = 1'b0;
        @(negedge clock)
        
        
        #1 read_pipeline_TB_IN(in);
        #1 step_pipeline_TB();
        #1 print_pipeline_TB();
        #1 read_pipeline_TB_OUT(out);
        #1 check = check_pipeline_TB();
        if (!check)
            exit_on_error();
        


        $finish;
    end

endmodule