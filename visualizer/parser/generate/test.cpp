#include "svffi.h"
#include <iostream>

// TODO: add color
template <typename T, typename U>
bool print_assert(size_t indent, const char* s, const char* m, T cpp, U verilog) {
    if (cpp != verilog) {
        for (size_t i = 0; i < indent; ++i)
            std::cout << '\t';
        std::cout << "[Incorrect] " << s << "." << m << ": " << cpp << "(cpp)" << " vs " << verilog << "(verilog)\n";
    }
    return cpp == verilog;
}

template <typename T, typename U>
void print_info(size_t indent, const char* s, const char* m, T cpp, U verilog) {
    for (size_t i = 0; i < indent; ++i)
        std::cout << '\t';
    std::cout << "[Info] " << s << "." << m << ": " << cpp << "(cpp)" << " vs " << verilog << "(verilog)\n";
}

void print_header(size_t indent, const char* s, const char* m) {
    for (size_t i = 0; i < indent; ++i)
        std::cout << '\t';
    std::cout << "[Enter] " << s << "." << m << '\n';
}


using INST = PackedLogicArray<32>;



using EXCEPTION_CODE = PackedLogicArray<4>;




using ALU_OPA_SELECT = PackedLogicArray<2>;




using ALU_OPB_SELECT = PackedLogicArray<4>;




using DEST_REG_SEL = PackedLogicArray<2>;




using ALU_FUNC = PackedLogicArray<5>;




using BUS_COMMAND = PackedLogicArray<2>;




struct struct__0000 {
	PackedLogicArray<7> funct7;
	uint64_t funct7_;
	PackedLogicArray<5> rs2;
	uint64_t rs2_;
	PackedLogicArray<5> rs1;
	uint64_t rs1_;
	PackedLogicArray<3> funct3;
	uint64_t funct3_;
	PackedLogicArray<5> rd;
	uint64_t rd_;
	PackedLogicArray<7> opcode;
	uint64_t opcode_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "struct__0000", "funct7", funct7_, funct7.as_int());
		success &= print_assert(indent, "struct__0000", "rs2", rs2_, rs2.as_int());
		success &= print_assert(indent, "struct__0000", "rs1", rs1_, rs1.as_int());
		success &= print_assert(indent, "struct__0000", "funct3", funct3_, funct3.as_int());
		success &= print_assert(indent, "struct__0000", "rd", rd_, rd.as_int());
		success &= print_assert(indent, "struct__0000", "opcode", opcode_, opcode.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "struct__0000", "funct7", funct7_, funct7.as_int());
		print_info(indent, "struct__0000", "rs2", rs2_, rs2.as_int());
		print_info(indent, "struct__0000", "rs1", rs1_, rs1.as_int());
		print_info(indent, "struct__0000", "funct3", funct3_, funct3.as_int());
		print_info(indent, "struct__0000", "rd", rd_, rd.as_int());
		print_info(indent, "struct__0000", "opcode", opcode_, opcode.as_int());
	}
};
SN_REFLECTION(struct__0000, true, funct7, rs2, rs1, funct3, rd, opcode);

struct__0000 struct__0000_instance;

extern "C" void read_struct__0000 (svOpenArrayHandle h) {
    read(struct__0000_instance, h);
}

extern "C" int check_struct__0000 () {
    struct__0000_instance.check();
}

extern "C" void step_struct__0000 () {
    struct__0000_instance.step();
}

extern "C" void print_struct__0000 () {
    struct__0000_instance.print();
}




struct struct__0001 {
	PackedLogicArray<12> imm;
	uint64_t imm_;
	PackedLogicArray<5> rs1;
	uint64_t rs1_;
	PackedLogicArray<3> funct3;
	uint64_t funct3_;
	PackedLogicArray<5> rd;
	uint64_t rd_;
	PackedLogicArray<7> opcode;
	uint64_t opcode_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "struct__0001", "imm", imm_, imm.as_int());
		success &= print_assert(indent, "struct__0001", "rs1", rs1_, rs1.as_int());
		success &= print_assert(indent, "struct__0001", "funct3", funct3_, funct3.as_int());
		success &= print_assert(indent, "struct__0001", "rd", rd_, rd.as_int());
		success &= print_assert(indent, "struct__0001", "opcode", opcode_, opcode.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "struct__0001", "imm", imm_, imm.as_int());
		print_info(indent, "struct__0001", "rs1", rs1_, rs1.as_int());
		print_info(indent, "struct__0001", "funct3", funct3_, funct3.as_int());
		print_info(indent, "struct__0001", "rd", rd_, rd.as_int());
		print_info(indent, "struct__0001", "opcode", opcode_, opcode.as_int());
	}
};
SN_REFLECTION(struct__0001, true, imm, rs1, funct3, rd, opcode);

struct__0001 struct__0001_instance;

extern "C" void read_struct__0001 (svOpenArrayHandle h) {
    read(struct__0001_instance, h);
}

extern "C" int check_struct__0001 () {
    struct__0001_instance.check();
}

extern "C" void step_struct__0001 () {
    struct__0001_instance.step();
}

extern "C" void print_struct__0001 () {
    struct__0001_instance.print();
}




struct struct__0002 {
	PackedLogicArray<7> off;
	uint64_t off_;
	PackedLogicArray<5> rs2;
	uint64_t rs2_;
	PackedLogicArray<5> rs1;
	uint64_t rs1_;
	PackedLogicArray<3> funct3;
	uint64_t funct3_;
	PackedLogicArray<5> set;
	uint64_t set_;
	PackedLogicArray<7> opcode;
	uint64_t opcode_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "struct__0002", "off", off_, off.as_int());
		success &= print_assert(indent, "struct__0002", "rs2", rs2_, rs2.as_int());
		success &= print_assert(indent, "struct__0002", "rs1", rs1_, rs1.as_int());
		success &= print_assert(indent, "struct__0002", "funct3", funct3_, funct3.as_int());
		success &= print_assert(indent, "struct__0002", "set", set_, set.as_int());
		success &= print_assert(indent, "struct__0002", "opcode", opcode_, opcode.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "struct__0002", "off", off_, off.as_int());
		print_info(indent, "struct__0002", "rs2", rs2_, rs2.as_int());
		print_info(indent, "struct__0002", "rs1", rs1_, rs1.as_int());
		print_info(indent, "struct__0002", "funct3", funct3_, funct3.as_int());
		print_info(indent, "struct__0002", "set", set_, set.as_int());
		print_info(indent, "struct__0002", "opcode", opcode_, opcode.as_int());
	}
};
SN_REFLECTION(struct__0002, true, off, rs2, rs1, funct3, set, opcode);

struct__0002 struct__0002_instance;

extern "C" void read_struct__0002 (svOpenArrayHandle h) {
    read(struct__0002_instance, h);
}

extern "C" int check_struct__0002 () {
    struct__0002_instance.check();
}

extern "C" void step_struct__0002 () {
    struct__0002_instance.step();
}

extern "C" void print_struct__0002 () {
    struct__0002_instance.print();
}




struct struct__0003 {
	Logic of;
	bool of_;
	PackedLogicArray<6> s;
	uint64_t s_;
	PackedLogicArray<5> rs2;
	uint64_t rs2_;
	PackedLogicArray<5> rs1;
	uint64_t rs1_;
	PackedLogicArray<3> funct3;
	uint64_t funct3_;
	PackedLogicArray<4> et;
	uint64_t et_;
	Logic f;
	bool f_;
	PackedLogicArray<7> opcode;
	uint64_t opcode_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "struct__0003", "of", of_, of.as_bool());
		success &= print_assert(indent, "struct__0003", "s", s_, s.as_int());
		success &= print_assert(indent, "struct__0003", "rs2", rs2_, rs2.as_int());
		success &= print_assert(indent, "struct__0003", "rs1", rs1_, rs1.as_int());
		success &= print_assert(indent, "struct__0003", "funct3", funct3_, funct3.as_int());
		success &= print_assert(indent, "struct__0003", "et", et_, et.as_int());
		success &= print_assert(indent, "struct__0003", "f", f_, f.as_bool());
		success &= print_assert(indent, "struct__0003", "opcode", opcode_, opcode.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "struct__0003", "of", of_, of.as_bool());
		print_info(indent, "struct__0003", "s", s_, s.as_int());
		print_info(indent, "struct__0003", "rs2", rs2_, rs2.as_int());
		print_info(indent, "struct__0003", "rs1", rs1_, rs1.as_int());
		print_info(indent, "struct__0003", "funct3", funct3_, funct3.as_int());
		print_info(indent, "struct__0003", "et", et_, et.as_int());
		print_info(indent, "struct__0003", "f", f_, f.as_bool());
		print_info(indent, "struct__0003", "opcode", opcode_, opcode.as_int());
	}
};
SN_REFLECTION(struct__0003, true, of, s, rs2, rs1, funct3, et, f, opcode);

struct__0003 struct__0003_instance;

extern "C" void read_struct__0003 (svOpenArrayHandle h) {
    read(struct__0003_instance, h);
}

extern "C" int check_struct__0003 () {
    struct__0003_instance.check();
}

extern "C" void step_struct__0003 () {
    struct__0003_instance.step();
}

extern "C" void print_struct__0003 () {
    struct__0003_instance.print();
}




struct struct__0004 {
	PackedLogicArray<20> imm;
	uint64_t imm_;
	PackedLogicArray<5> rd;
	uint64_t rd_;
	PackedLogicArray<7> opcode;
	uint64_t opcode_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "struct__0004", "imm", imm_, imm.as_int());
		success &= print_assert(indent, "struct__0004", "rd", rd_, rd.as_int());
		success &= print_assert(indent, "struct__0004", "opcode", opcode_, opcode.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "struct__0004", "imm", imm_, imm.as_int());
		print_info(indent, "struct__0004", "rd", rd_, rd.as_int());
		print_info(indent, "struct__0004", "opcode", opcode_, opcode.as_int());
	}
};
SN_REFLECTION(struct__0004, true, imm, rd, opcode);

struct__0004 struct__0004_instance;

extern "C" void read_struct__0004 (svOpenArrayHandle h) {
    read(struct__0004_instance, h);
}

extern "C" int check_struct__0004 () {
    struct__0004_instance.check();
}

extern "C" void step_struct__0004 () {
    struct__0004_instance.step();
}

extern "C" void print_struct__0004 () {
    struct__0004_instance.print();
}




struct struct__0005 {
	Logic of;
	bool of_;
	PackedLogicArray<10> et;
	uint64_t et_;
	Logic s;
	bool s_;
	PackedLogicArray<8> f;
	uint64_t f_;
	PackedLogicArray<5> rd;
	uint64_t rd_;
	PackedLogicArray<7> opcode;
	uint64_t opcode_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "struct__0005", "of", of_, of.as_bool());
		success &= print_assert(indent, "struct__0005", "et", et_, et.as_int());
		success &= print_assert(indent, "struct__0005", "s", s_, s.as_bool());
		success &= print_assert(indent, "struct__0005", "f", f_, f.as_int());
		success &= print_assert(indent, "struct__0005", "rd", rd_, rd.as_int());
		success &= print_assert(indent, "struct__0005", "opcode", opcode_, opcode.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "struct__0005", "of", of_, of.as_bool());
		print_info(indent, "struct__0005", "et", et_, et.as_int());
		print_info(indent, "struct__0005", "s", s_, s.as_bool());
		print_info(indent, "struct__0005", "f", f_, f.as_int());
		print_info(indent, "struct__0005", "rd", rd_, rd.as_int());
		print_info(indent, "struct__0005", "opcode", opcode_, opcode.as_int());
	}
};
SN_REFLECTION(struct__0005, true, of, et, s, f, rd, opcode);

struct__0005 struct__0005_instance;

extern "C" void read_struct__0005 (svOpenArrayHandle h) {
    read(struct__0005_instance, h);
}

extern "C" int check_struct__0005 () {
    struct__0005_instance.check();
}

extern "C" void step_struct__0005 () {
    struct__0005_instance.step();
}

extern "C" void print_struct__0005 () {
    struct__0005_instance.print();
}




using word_t = PackedLogicArray<32>;




using vreg_tag_t = PackedLogicArray<5>;




using preg_tag_t = PackedLogicArray<6>;




using ss_size_t = PackedLogicArray<2>;




using ss_tag_t = PackedLogicArray<1>;




using dtab_size_t = PackedLogicArray<6>;




using dtab_tag_t = PackedLogicArray<6>;




using rob_tag_t = PackedLogicArray<5>;




using rob_size_t = PackedLogicArray<5>;




using rs_tag_t = PackedLogicArray<5>;




using rs_size_t = PackedLogicArray<5>;




using memory_line_t = PackedLogicArray<64>;




using memory_tag_t = PackedLogicArray<3>;




using icache_index_t = PackedLogicArray<5>;




using icache_tag_t = PackedLogicArray<21>;




using sq_tag_t = PackedLogicArray<3>;




using sq_size_t = PackedLogicArray<3>;




using byte_tag_t = PackedLogicArray<4>;




struct ROB_ENTRY {
	PackedLogicArray<6> told_idx;
	uint64_t told_idx_;
	PackedLogicArray<6> t_idx;
	uint64_t t_idx_;
	PackedLogicArray<5> dest_reg_idx;
	uint64_t dest_reg_idx_;
	Logic complete;
	bool complete_;
	Logic branch;
	bool branch_;
	PackedLogicArray<32> branch_target;
	uint64_t branch_target_;
	Logic rollback;
	bool rollback_;
	Logic halt;
	bool halt_;
	PackedLogicArray<32> PC;
	uint64_t PC_;
	Logic branch_taken;
	bool branch_taken_;
	Logic branch_mispredicted;
	bool branch_mispredicted_;
	Logic store;
	bool store_;
	Logic tournament;
	bool tournament_;
	Logic call;
	bool call_;
	Logic ret;
	bool ret_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "ROB_ENTRY", "told_idx", told_idx_, told_idx.as_int());
		success &= print_assert(indent, "ROB_ENTRY", "t_idx", t_idx_, t_idx.as_int());
		success &= print_assert(indent, "ROB_ENTRY", "dest_reg_idx", dest_reg_idx_, dest_reg_idx.as_int());
		success &= print_assert(indent, "ROB_ENTRY", "complete", complete_, complete.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "branch", branch_, branch.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "branch_target", branch_target_, branch_target.as_int());
		success &= print_assert(indent, "ROB_ENTRY", "rollback", rollback_, rollback.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "halt", halt_, halt.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "PC", PC_, PC.as_int());
		success &= print_assert(indent, "ROB_ENTRY", "branch_taken", branch_taken_, branch_taken.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "branch_mispredicted", branch_mispredicted_, branch_mispredicted.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "store", store_, store.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "tournament", tournament_, tournament.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "call", call_, call.as_bool());
		success &= print_assert(indent, "ROB_ENTRY", "ret", ret_, ret.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "ROB_ENTRY", "told_idx", told_idx_, told_idx.as_int());
		print_info(indent, "ROB_ENTRY", "t_idx", t_idx_, t_idx.as_int());
		print_info(indent, "ROB_ENTRY", "dest_reg_idx", dest_reg_idx_, dest_reg_idx.as_int());
		print_info(indent, "ROB_ENTRY", "complete", complete_, complete.as_bool());
		print_info(indent, "ROB_ENTRY", "branch", branch_, branch.as_bool());
		print_info(indent, "ROB_ENTRY", "branch_target", branch_target_, branch_target.as_int());
		print_info(indent, "ROB_ENTRY", "rollback", rollback_, rollback.as_bool());
		print_info(indent, "ROB_ENTRY", "halt", halt_, halt.as_bool());
		print_info(indent, "ROB_ENTRY", "PC", PC_, PC.as_int());
		print_info(indent, "ROB_ENTRY", "branch_taken", branch_taken_, branch_taken.as_bool());
		print_info(indent, "ROB_ENTRY", "branch_mispredicted", branch_mispredicted_, branch_mispredicted.as_bool());
		print_info(indent, "ROB_ENTRY", "store", store_, store.as_bool());
		print_info(indent, "ROB_ENTRY", "tournament", tournament_, tournament.as_bool());
		print_info(indent, "ROB_ENTRY", "call", call_, call.as_bool());
		print_info(indent, "ROB_ENTRY", "ret", ret_, ret.as_bool());
	}
};
SN_REFLECTION(ROB_ENTRY, true, told_idx, t_idx, dest_reg_idx, complete, branch, branch_target, rollback, halt, PC, branch_taken, branch_mispredicted, store, tournament, call, ret);

ROB_ENTRY ROB_ENTRY_instance;

extern "C" void read_ROB_ENTRY (svOpenArrayHandle h) {
    read(ROB_ENTRY_instance, h);
}

extern "C" int check_ROB_ENTRY () {
    ROB_ENTRY_instance.check();
}

extern "C" void step_ROB_ENTRY () {
    ROB_ENTRY_instance.step();
}

extern "C" void print_ROB_ENTRY () {
    ROB_ENTRY_instance.print();
}




using RS_FU = PackedLogicArray<3>;




struct BRANCH_PACKET {
	Logic valid;
	bool valid_;
	Logic taken;
	bool taken_;
	PackedLogicArray<32> branch_target;
	uint64_t branch_target_;
	PackedLogicArray<32> branch_address;
	uint64_t branch_address_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "BRANCH_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "BRANCH_PACKET", "taken", taken_, taken.as_bool());
		success &= print_assert(indent, "BRANCH_PACKET", "branch_target", branch_target_, branch_target.as_int());
		success &= print_assert(indent, "BRANCH_PACKET", "branch_address", branch_address_, branch_address.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "BRANCH_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "BRANCH_PACKET", "taken", taken_, taken.as_bool());
		print_info(indent, "BRANCH_PACKET", "branch_target", branch_target_, branch_target.as_int());
		print_info(indent, "BRANCH_PACKET", "branch_address", branch_address_, branch_address.as_int());
	}
};
SN_REFLECTION(BRANCH_PACKET, true, valid, taken, branch_target, branch_address);

BRANCH_PACKET BRANCH_PACKET_instance;

extern "C" void read_BRANCH_PACKET (svOpenArrayHandle h) {
    read(BRANCH_PACKET_instance, h);
}

extern "C" int check_BRANCH_PACKET () {
    BRANCH_PACKET_instance.check();
}

extern "C" void step_BRANCH_PACKET () {
    BRANCH_PACKET_instance.step();
}

extern "C" void print_BRANCH_PACKET () {
    BRANCH_PACKET_instance.print();
}




struct DECODE_MT_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<5> rs1_idx;
	uint64_t rs1_idx_;
	PackedLogicArray<5> rs2_idx;
	uint64_t rs2_idx_;
	PackedLogicArray<5> dest_reg_idx;
	uint64_t dest_reg_idx_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DECODE_MT_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "DECODE_MT_PACKET", "rs1_idx", rs1_idx_, rs1_idx.as_int());
		success &= print_assert(indent, "DECODE_MT_PACKET", "rs2_idx", rs2_idx_, rs2_idx.as_int());
		success &= print_assert(indent, "DECODE_MT_PACKET", "dest_reg_idx", dest_reg_idx_, dest_reg_idx.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DECODE_MT_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "DECODE_MT_PACKET", "rs1_idx", rs1_idx_, rs1_idx.as_int());
		print_info(indent, "DECODE_MT_PACKET", "rs2_idx", rs2_idx_, rs2_idx.as_int());
		print_info(indent, "DECODE_MT_PACKET", "dest_reg_idx", dest_reg_idx_, dest_reg_idx.as_int());
	}
};
SN_REFLECTION(DECODE_MT_PACKET, true, valid, rs1_idx, rs2_idx, dest_reg_idx);

DECODE_MT_PACKET DECODE_MT_PACKET_instance;

extern "C" void read_DECODE_MT_PACKET (svOpenArrayHandle h) {
    read(DECODE_MT_PACKET_instance, h);
}

extern "C" int check_DECODE_MT_PACKET () {
    DECODE_MT_PACKET_instance.check();
}

extern "C" void step_DECODE_MT_PACKET () {
    DECODE_MT_PACKET_instance.step();
}

extern "C" void print_DECODE_MT_PACKET () {
    DECODE_MT_PACKET_instance.print();
}




struct DECODE_RS_PACKET {
	PackedLogicArray<32> NPC;
	uint64_t NPC_;
	PackedLogicArray<32> PC;
	uint64_t PC_;
	PackedLogicArray<2> opa_select;
	uint64_t opa_select_;
	PackedLogicArray<4> opb_select;
	uint64_t opb_select_;
	INST inst;
	uint32_t inst_;
	PackedLogicArray<5> alu_func;
	uint64_t alu_func_;
	PackedLogicArray<3> fu;
	uint64_t fu_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DECODE_RS_PACKET", "NPC", NPC_, NPC.as_int());
		success &= print_assert(indent, "DECODE_RS_PACKET", "PC", PC_, PC.as_int());
		success &= print_assert(indent, "DECODE_RS_PACKET", "opa_select", opa_select_, opa_select.as_int());
		success &= print_assert(indent, "DECODE_RS_PACKET", "opb_select", opb_select_, opb_select.as_int());
		success &= print_assert(indent, "DECODE_RS_PACKET", "inst", inst_, inst.as_int());
		success &= print_assert(indent, "DECODE_RS_PACKET", "alu_func", alu_func_, alu_func.as_int());
		success &= print_assert(indent, "DECODE_RS_PACKET", "fu", fu_, fu.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DECODE_RS_PACKET", "NPC", NPC_, NPC.as_int());
		print_info(indent, "DECODE_RS_PACKET", "PC", PC_, PC.as_int());
		print_info(indent, "DECODE_RS_PACKET", "opa_select", opa_select_, opa_select.as_int());
		print_info(indent, "DECODE_RS_PACKET", "opb_select", opb_select_, opb_select.as_int());
		print_info(indent, "DECODE_RS_PACKET", "inst", inst_, inst.as_int());
		print_info(indent, "DECODE_RS_PACKET", "alu_func", alu_func_, alu_func.as_int());
		print_info(indent, "DECODE_RS_PACKET", "fu", fu_, fu.as_int());
	}
};
SN_REFLECTION(DECODE_RS_PACKET, true, NPC, PC, opa_select, opb_select, inst, alu_func, fu);

DECODE_RS_PACKET DECODE_RS_PACKET_instance;

extern "C" void read_DECODE_RS_PACKET (svOpenArrayHandle h) {
    read(DECODE_RS_PACKET_instance, h);
}

extern "C" int check_DECODE_RS_PACKET () {
    DECODE_RS_PACKET_instance.check();
}

extern "C" void step_DECODE_RS_PACKET () {
    DECODE_RS_PACKET_instance.step();
}

extern "C" void print_DECODE_RS_PACKET () {
    DECODE_RS_PACKET_instance.print();
}




struct ROB_MT_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<6> retire_told_idx;
	uint64_t retire_told_idx_;
	PackedLogicArray<6> retire_t_idx;
	uint64_t retire_t_idx_;
	PackedLogicArray<5> dest_reg_idx;
	uint64_t dest_reg_idx_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "ROB_MT_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "ROB_MT_PACKET", "retire_told_idx", retire_told_idx_, retire_told_idx.as_int());
		success &= print_assert(indent, "ROB_MT_PACKET", "retire_t_idx", retire_t_idx_, retire_t_idx.as_int());
		success &= print_assert(indent, "ROB_MT_PACKET", "dest_reg_idx", dest_reg_idx_, dest_reg_idx.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "ROB_MT_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "ROB_MT_PACKET", "retire_told_idx", retire_told_idx_, retire_told_idx.as_int());
		print_info(indent, "ROB_MT_PACKET", "retire_t_idx", retire_t_idx_, retire_t_idx.as_int());
		print_info(indent, "ROB_MT_PACKET", "dest_reg_idx", dest_reg_idx_, dest_reg_idx.as_int());
	}
};
SN_REFLECTION(ROB_MT_PACKET, true, valid, retire_told_idx, retire_t_idx, dest_reg_idx);

ROB_MT_PACKET ROB_MT_PACKET_instance;

extern "C" void read_ROB_MT_PACKET (svOpenArrayHandle h) {
    read(ROB_MT_PACKET_instance, h);
}

extern "C" int check_ROB_MT_PACKET () {
    ROB_MT_PACKET_instance.check();
}

extern "C" void step_ROB_MT_PACKET () {
    ROB_MT_PACKET_instance.step();
}

extern "C" void print_ROB_MT_PACKET () {
    ROB_MT_PACKET_instance.print();
}




struct MT_ROB_PACKET {
	PackedLogicArray<6> told_idx;
	uint64_t told_idx_;
	PackedLogicArray<6> t_idx;
	uint64_t t_idx_;
	PackedLogicArray<5> dest_reg_idx;
	uint64_t dest_reg_idx_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "MT_ROB_PACKET", "told_idx", told_idx_, told_idx.as_int());
		success &= print_assert(indent, "MT_ROB_PACKET", "t_idx", t_idx_, t_idx.as_int());
		success &= print_assert(indent, "MT_ROB_PACKET", "dest_reg_idx", dest_reg_idx_, dest_reg_idx.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "MT_ROB_PACKET", "told_idx", told_idx_, told_idx.as_int());
		print_info(indent, "MT_ROB_PACKET", "t_idx", t_idx_, t_idx.as_int());
		print_info(indent, "MT_ROB_PACKET", "dest_reg_idx", dest_reg_idx_, dest_reg_idx.as_int());
	}
};
SN_REFLECTION(MT_ROB_PACKET, true, told_idx, t_idx, dest_reg_idx);

MT_ROB_PACKET MT_ROB_PACKET_instance;

extern "C" void read_MT_ROB_PACKET (svOpenArrayHandle h) {
    read(MT_ROB_PACKET_instance, h);
}

extern "C" int check_MT_ROB_PACKET () {
    MT_ROB_PACKET_instance.check();
}

extern "C" void step_MT_ROB_PACKET () {
    MT_ROB_PACKET_instance.step();
}

extern "C" void print_MT_ROB_PACKET () {
    MT_ROB_PACKET_instance.print();
}




struct BRANCH_ROB_PACKET {
	Logic valid;
	bool valid_;
	Logic is_cond_branch;
	bool is_cond_branch_;
	PackedLogicArray<5> rob_num;
	uint64_t rob_num_;
	PackedLogicArray<32> branch_target;
	uint64_t branch_target_;
	Logic branch_taken;
	bool branch_taken_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "BRANCH_ROB_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "BRANCH_ROB_PACKET", "is_cond_branch", is_cond_branch_, is_cond_branch.as_bool());
		success &= print_assert(indent, "BRANCH_ROB_PACKET", "rob_num", rob_num_, rob_num.as_int());
		success &= print_assert(indent, "BRANCH_ROB_PACKET", "branch_target", branch_target_, branch_target.as_int());
		success &= print_assert(indent, "BRANCH_ROB_PACKET", "branch_taken", branch_taken_, branch_taken.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "BRANCH_ROB_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "BRANCH_ROB_PACKET", "is_cond_branch", is_cond_branch_, is_cond_branch.as_bool());
		print_info(indent, "BRANCH_ROB_PACKET", "rob_num", rob_num_, rob_num.as_int());
		print_info(indent, "BRANCH_ROB_PACKET", "branch_target", branch_target_, branch_target.as_int());
		print_info(indent, "BRANCH_ROB_PACKET", "branch_taken", branch_taken_, branch_taken.as_bool());
	}
};
SN_REFLECTION(BRANCH_ROB_PACKET, true, valid, is_cond_branch, rob_num, branch_target, branch_taken);

BRANCH_ROB_PACKET BRANCH_ROB_PACKET_instance;

extern "C" void read_BRANCH_ROB_PACKET (svOpenArrayHandle h) {
    read(BRANCH_ROB_PACKET_instance, h);
}

extern "C" int check_BRANCH_ROB_PACKET () {
    BRANCH_ROB_PACKET_instance.check();
}

extern "C" void step_BRANCH_ROB_PACKET () {
    BRANCH_ROB_PACKET_instance.step();
}

extern "C" void print_BRANCH_ROB_PACKET () {
    BRANCH_ROB_PACKET_instance.print();
}




struct ST_ROB_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<5> rob_num;
	uint64_t rob_num_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "ST_ROB_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "ST_ROB_PACKET", "rob_num", rob_num_, rob_num.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "ST_ROB_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "ST_ROB_PACKET", "rob_num", rob_num_, rob_num.as_int());
	}
};
SN_REFLECTION(ST_ROB_PACKET, true, valid, rob_num);

ST_ROB_PACKET ST_ROB_PACKET_instance;

extern "C" void read_ST_ROB_PACKET (svOpenArrayHandle h) {
    read(ST_ROB_PACKET_instance, h);
}

extern "C" int check_ST_ROB_PACKET () {
    ST_ROB_PACKET_instance.check();
}

extern "C" void step_ST_ROB_PACKET () {
    ST_ROB_PACKET_instance.step();
}

extern "C" void print_ST_ROB_PACKET () {
    ST_ROB_PACKET_instance.print();
}




struct MT_ENTRY {
	PackedLogicArray<6> t_idx;
	uint64_t t_idx_;
	Logic ready;
	bool ready_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "MT_ENTRY", "t_idx", t_idx_, t_idx.as_int());
		success &= print_assert(indent, "MT_ENTRY", "ready", ready_, ready.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "MT_ENTRY", "t_idx", t_idx_, t_idx.as_int());
		print_info(indent, "MT_ENTRY", "ready", ready_, ready.as_bool());
	}
};
SN_REFLECTION(MT_ENTRY, true, t_idx, ready);

MT_ENTRY MT_ENTRY_instance;

extern "C" void read_MT_ENTRY (svOpenArrayHandle h) {
    read(MT_ENTRY_instance, h);
}

extern "C" int check_MT_ENTRY () {
    MT_ENTRY_instance.check();
}

extern "C" void step_MT_ENTRY () {
    MT_ENTRY_instance.step();
}

extern "C" void print_MT_ENTRY () {
    MT_ENTRY_instance.print();
}




struct MT_RS_PACKET {
	MT_ENTRY t1;
	MT_ENTRY t2;
	PackedLogicArray<6> t_idx;
	uint64_t t_idx_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		print_header(indent, "MT_RS_PACKET", "t1");
		success &= t1.check(indent + 1);
		print_header(indent, "MT_RS_PACKET", "t2");
		success &= t2.check(indent + 1);
		success &= print_assert(indent, "MT_RS_PACKET", "t_idx", t_idx_, t_idx.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_header(indent, "MT_RS_PACKET", "t1");
		t1.print(indent + 1);
		print_header(indent, "MT_RS_PACKET", "t2");
		t2.print(indent + 1);
		print_info(indent, "MT_RS_PACKET", "t_idx", t_idx_, t_idx.as_int());
	}
};
SN_REFLECTION(MT_RS_PACKET, true, t1, t2, t_idx);

MT_RS_PACKET MT_RS_PACKET_instance;

extern "C" void read_MT_RS_PACKET (svOpenArrayHandle h) {
    read(MT_RS_PACKET_instance, h);
}

extern "C" int check_MT_RS_PACKET () {
    MT_RS_PACKET_instance.check();
}

extern "C" void step_MT_RS_PACKET () {
    MT_RS_PACKET_instance.step();
}

extern "C" void print_MT_RS_PACKET () {
    MT_RS_PACKET_instance.print();
}




struct CDB_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<5> rob_num;
	uint64_t rob_num_;
	PackedLogicArray<6> t_idx;
	uint64_t t_idx_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "CDB_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "CDB_PACKET", "rob_num", rob_num_, rob_num.as_int());
		success &= print_assert(indent, "CDB_PACKET", "t_idx", t_idx_, t_idx.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "CDB_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "CDB_PACKET", "rob_num", rob_num_, rob_num.as_int());
		print_info(indent, "CDB_PACKET", "t_idx", t_idx_, t_idx.as_int());
	}
};
SN_REFLECTION(CDB_PACKET, true, valid, rob_num, t_idx);

CDB_PACKET CDB_PACKET_instance;

extern "C" void read_CDB_PACKET (svOpenArrayHandle h) {
    read(CDB_PACKET_instance, h);
}

extern "C" int check_CDB_PACKET () {
    CDB_PACKET_instance.check();
}

extern "C" void step_CDB_PACKET () {
    CDB_PACKET_instance.step();
}

extern "C" void print_CDB_PACKET () {
    CDB_PACKET_instance.print();
}




struct RS_ENTRY {
	Logic busy;
	bool busy_;
	DECODE_RS_PACKET inst_info;
	MT_RS_PACKET t_info;
	PackedLogicArray<5> rob_num;
	uint64_t rob_num_;
	PackedLogicArray<3> age;
	uint64_t age_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "RS_ENTRY", "busy", busy_, busy.as_bool());
		print_header(indent, "RS_ENTRY", "inst_info");
		success &= inst_info.check(indent + 1);
		print_header(indent, "RS_ENTRY", "t_info");
		success &= t_info.check(indent + 1);
		success &= print_assert(indent, "RS_ENTRY", "rob_num", rob_num_, rob_num.as_int());
		success &= print_assert(indent, "RS_ENTRY", "age", age_, age.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "RS_ENTRY", "busy", busy_, busy.as_bool());
		print_header(indent, "RS_ENTRY", "inst_info");
		inst_info.print(indent + 1);
		print_header(indent, "RS_ENTRY", "t_info");
		t_info.print(indent + 1);
		print_info(indent, "RS_ENTRY", "rob_num", rob_num_, rob_num.as_int());
		print_info(indent, "RS_ENTRY", "age", age_, age.as_int());
	}
};
SN_REFLECTION(RS_ENTRY, true, busy, inst_info, t_info, rob_num, age);

RS_ENTRY RS_ENTRY_instance;

extern "C" void read_RS_ENTRY (svOpenArrayHandle h) {
    read(RS_ENTRY_instance, h);
}

extern "C" int check_RS_ENTRY () {
    RS_ENTRY_instance.check();
}

extern "C" void step_RS_ENTRY () {
    RS_ENTRY_instance.step();
}

extern "C" void print_RS_ENTRY () {
    RS_ENTRY_instance.print();
}




struct RS_PRIORITY {
	Logic is_empty;
	bool is_empty_;
	PackedLogicArray<5> tag;
	uint64_t tag_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "RS_PRIORITY", "is_empty", is_empty_, is_empty.as_bool());
		success &= print_assert(indent, "RS_PRIORITY", "tag", tag_, tag.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "RS_PRIORITY", "is_empty", is_empty_, is_empty.as_bool());
		print_info(indent, "RS_PRIORITY", "tag", tag_, tag.as_int());
	}
};
SN_REFLECTION(RS_PRIORITY, true, is_empty, tag);

RS_PRIORITY RS_PRIORITY_instance;

extern "C" void read_RS_PRIORITY (svOpenArrayHandle h) {
    read(RS_PRIORITY_instance, h);
}

extern "C" int check_RS_PRIORITY () {
    RS_PRIORITY_instance.check();
}

extern "C" void step_RS_PRIORITY () {
    RS_PRIORITY_instance.step();
}

extern "C" void print_RS_PRIORITY () {
    RS_PRIORITY_instance.print();
}




struct IF_DECODE_PACKET {
	Logic valid;
	bool valid_;
	INST inst;
	uint32_t inst_;
	PackedLogicArray<32> NPC;
	uint64_t NPC_;
	PackedLogicArray<32> PC;
	uint64_t PC_;
	PackedLogicArray<32> branch_target;
	uint64_t branch_target_;
	Logic branch_taken;
	bool branch_taken_;
	Logic tournament;
	bool tournament_;
	Logic call;
	bool call_;
	Logic ret;
	bool ret_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "IF_DECODE_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "IF_DECODE_PACKET", "inst", inst_, inst.as_int());
		success &= print_assert(indent, "IF_DECODE_PACKET", "NPC", NPC_, NPC.as_int());
		success &= print_assert(indent, "IF_DECODE_PACKET", "PC", PC_, PC.as_int());
		success &= print_assert(indent, "IF_DECODE_PACKET", "branch_target", branch_target_, branch_target.as_int());
		success &= print_assert(indent, "IF_DECODE_PACKET", "branch_taken", branch_taken_, branch_taken.as_bool());
		success &= print_assert(indent, "IF_DECODE_PACKET", "tournament", tournament_, tournament.as_bool());
		success &= print_assert(indent, "IF_DECODE_PACKET", "call", call_, call.as_bool());
		success &= print_assert(indent, "IF_DECODE_PACKET", "ret", ret_, ret.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "IF_DECODE_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "IF_DECODE_PACKET", "inst", inst_, inst.as_int());
		print_info(indent, "IF_DECODE_PACKET", "NPC", NPC_, NPC.as_int());
		print_info(indent, "IF_DECODE_PACKET", "PC", PC_, PC.as_int());
		print_info(indent, "IF_DECODE_PACKET", "branch_target", branch_target_, branch_target.as_int());
		print_info(indent, "IF_DECODE_PACKET", "branch_taken", branch_taken_, branch_taken.as_bool());
		print_info(indent, "IF_DECODE_PACKET", "tournament", tournament_, tournament.as_bool());
		print_info(indent, "IF_DECODE_PACKET", "call", call_, call.as_bool());
		print_info(indent, "IF_DECODE_PACKET", "ret", ret_, ret.as_bool());
	}
};
SN_REFLECTION(IF_DECODE_PACKET, true, valid, inst, NPC, PC, branch_target, branch_taken, tournament, call, ret);

IF_DECODE_PACKET IF_DECODE_PACKET_instance;

extern "C" void read_IF_DECODE_PACKET (svOpenArrayHandle h) {
    read(IF_DECODE_PACKET_instance, h);
}

extern "C" int check_IF_DECODE_PACKET () {
    IF_DECODE_PACKET_instance.check();
}

extern "C" void step_IF_DECODE_PACKET () {
    IF_DECODE_PACKET_instance.step();
}

extern "C" void print_IF_DECODE_PACKET () {
    IF_DECODE_PACKET_instance.print();
}




struct DECODE_ROB_PACKET {
	Logic valid;
	bool valid_;
	Logic halt;
	bool halt_;
	Logic store;
	bool store_;
	BRANCH_PACKET branch_info;
	Logic tournament;
	bool tournament_;
	Logic call;
	bool call_;
	Logic ret;
	bool ret_;
	Logic nop;
	bool nop_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DECODE_ROB_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "DECODE_ROB_PACKET", "halt", halt_, halt.as_bool());
		success &= print_assert(indent, "DECODE_ROB_PACKET", "store", store_, store.as_bool());
		print_header(indent, "DECODE_ROB_PACKET", "branch_info");
		success &= branch_info.check(indent + 1);
		success &= print_assert(indent, "DECODE_ROB_PACKET", "tournament", tournament_, tournament.as_bool());
		success &= print_assert(indent, "DECODE_ROB_PACKET", "call", call_, call.as_bool());
		success &= print_assert(indent, "DECODE_ROB_PACKET", "ret", ret_, ret.as_bool());
		success &= print_assert(indent, "DECODE_ROB_PACKET", "nop", nop_, nop.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DECODE_ROB_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "DECODE_ROB_PACKET", "halt", halt_, halt.as_bool());
		print_info(indent, "DECODE_ROB_PACKET", "store", store_, store.as_bool());
		print_header(indent, "DECODE_ROB_PACKET", "branch_info");
		branch_info.print(indent + 1);
		print_info(indent, "DECODE_ROB_PACKET", "tournament", tournament_, tournament.as_bool());
		print_info(indent, "DECODE_ROB_PACKET", "call", call_, call.as_bool());
		print_info(indent, "DECODE_ROB_PACKET", "ret", ret_, ret.as_bool());
		print_info(indent, "DECODE_ROB_PACKET", "nop", nop_, nop.as_bool());
	}
};
SN_REFLECTION(DECODE_ROB_PACKET, true, valid, halt, store, branch_info, tournament, call, ret, nop);

DECODE_ROB_PACKET DECODE_ROB_PACKET_instance;

extern "C" void read_DECODE_ROB_PACKET (svOpenArrayHandle h) {
    read(DECODE_ROB_PACKET_instance, h);
}

extern "C" int check_DECODE_ROB_PACKET () {
    DECODE_ROB_PACKET_instance.check();
}

extern "C" void step_DECODE_ROB_PACKET () {
    DECODE_ROB_PACKET_instance.step();
}

extern "C" void print_DECODE_ROB_PACKET () {
    DECODE_ROB_PACKET_instance.print();
}




using fu_tag_t = PackedLogicArray<4>;




using ld_hot_t = PackedLogicArray<3>;




using st_tag_t = PackedLogicArray<2>;




using sq_hot_t = PackedLogicArray<8.0>;




struct FU_ISSUE_PACKET {
	PackedLogicArray<5> ALU_free_spaces;
	uint64_t ALU_free_spaces_;
	PackedLogicArray<4> ST_free_spaces;
	uint64_t ST_free_spaces_;
	PackedLogicArray<3> LD_free_spaces;
	uint64_t LD_free_spaces_;
	PackedLogicArray<3> MUL_free_spaces;
	uint64_t MUL_free_spaces_;
	PackedLogicArray<1> BR_free_spaces;
	uint64_t BR_free_spaces_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "FU_ISSUE_PACKET", "ALU_free_spaces", ALU_free_spaces_, ALU_free_spaces.as_int());
		success &= print_assert(indent, "FU_ISSUE_PACKET", "ST_free_spaces", ST_free_spaces_, ST_free_spaces.as_int());
		success &= print_assert(indent, "FU_ISSUE_PACKET", "LD_free_spaces", LD_free_spaces_, LD_free_spaces.as_int());
		success &= print_assert(indent, "FU_ISSUE_PACKET", "MUL_free_spaces", MUL_free_spaces_, MUL_free_spaces.as_int());
		success &= print_assert(indent, "FU_ISSUE_PACKET", "BR_free_spaces", BR_free_spaces_, BR_free_spaces.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "FU_ISSUE_PACKET", "ALU_free_spaces", ALU_free_spaces_, ALU_free_spaces.as_int());
		print_info(indent, "FU_ISSUE_PACKET", "ST_free_spaces", ST_free_spaces_, ST_free_spaces.as_int());
		print_info(indent, "FU_ISSUE_PACKET", "LD_free_spaces", LD_free_spaces_, LD_free_spaces.as_int());
		print_info(indent, "FU_ISSUE_PACKET", "MUL_free_spaces", MUL_free_spaces_, MUL_free_spaces.as_int());
		print_info(indent, "FU_ISSUE_PACKET", "BR_free_spaces", BR_free_spaces_, BR_free_spaces.as_int());
	}
};
SN_REFLECTION(FU_ISSUE_PACKET, true, ALU_free_spaces, ST_free_spaces, LD_free_spaces, MUL_free_spaces, BR_free_spaces);

FU_ISSUE_PACKET FU_ISSUE_PACKET_instance;

extern "C" void read_FU_ISSUE_PACKET (svOpenArrayHandle h) {
    read(FU_ISSUE_PACKET_instance, h);
}

extern "C" int check_FU_ISSUE_PACKET () {
    FU_ISSUE_PACKET_instance.check();
}

extern "C" void step_FU_ISSUE_PACKET () {
    FU_ISSUE_PACKET_instance.step();
}

extern "C" void print_FU_ISSUE_PACKET () {
    FU_ISSUE_PACKET_instance.print();
}




using p_width = PackedLogicArray<2>;




using priority_t = PackedLogicArray<2>;




using priority_is_t = PackedLogicArray<5>;




struct FU_COMPLETE_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<2> complete_priority;
	uint64_t complete_priority_;
	PackedLogicArray<5> rob_num;
	uint64_t rob_num_;
	PackedLogicArray<32> value;
	uint64_t value_;
	PackedLogicArray<6> t_idx;
	uint64_t t_idx_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "FU_COMPLETE_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "FU_COMPLETE_PACKET", "complete_priority", complete_priority_, complete_priority.as_int());
		success &= print_assert(indent, "FU_COMPLETE_PACKET", "rob_num", rob_num_, rob_num.as_int());
		success &= print_assert(indent, "FU_COMPLETE_PACKET", "value", value_, value.as_int());
		success &= print_assert(indent, "FU_COMPLETE_PACKET", "t_idx", t_idx_, t_idx.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "FU_COMPLETE_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "FU_COMPLETE_PACKET", "complete_priority", complete_priority_, complete_priority.as_int());
		print_info(indent, "FU_COMPLETE_PACKET", "rob_num", rob_num_, rob_num.as_int());
		print_info(indent, "FU_COMPLETE_PACKET", "value", value_, value.as_int());
		print_info(indent, "FU_COMPLETE_PACKET", "t_idx", t_idx_, t_idx.as_int());
	}
};
SN_REFLECTION(FU_COMPLETE_PACKET, true, valid, complete_priority, rob_num, value, t_idx);

FU_COMPLETE_PACKET FU_COMPLETE_PACKET_instance;

extern "C" void read_FU_COMPLETE_PACKET (svOpenArrayHandle h) {
    read(FU_COMPLETE_PACKET_instance, h);
}

extern "C" int check_FU_COMPLETE_PACKET () {
    FU_COMPLETE_PACKET_instance.check();
}

extern "C" void step_FU_COMPLETE_PACKET () {
    FU_COMPLETE_PACKET_instance.step();
}

extern "C" void print_FU_COMPLETE_PACKET () {
    FU_COMPLETE_PACKET_instance.print();
}




struct COMPLETE_REGFILE_PACKET {
	PackedLogicArray<6> t_idx;
	uint64_t t_idx_;
	PackedLogicArray<32> value;
	uint64_t value_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "COMPLETE_REGFILE_PACKET", "t_idx", t_idx_, t_idx.as_int());
		success &= print_assert(indent, "COMPLETE_REGFILE_PACKET", "value", value_, value.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "COMPLETE_REGFILE_PACKET", "t_idx", t_idx_, t_idx.as_int());
		print_info(indent, "COMPLETE_REGFILE_PACKET", "value", value_, value.as_int());
	}
};
SN_REFLECTION(COMPLETE_REGFILE_PACKET, true, t_idx, value);

COMPLETE_REGFILE_PACKET COMPLETE_REGFILE_PACKET_instance;

extern "C" void read_COMPLETE_REGFILE_PACKET (svOpenArrayHandle h) {
    read(COMPLETE_REGFILE_PACKET_instance, h);
}

extern "C" int check_COMPLETE_REGFILE_PACKET () {
    COMPLETE_REGFILE_PACKET_instance.check();
}

extern "C" void step_COMPLETE_REGFILE_PACKET () {
    COMPLETE_REGFILE_PACKET_instance.step();
}

extern "C" void print_COMPLETE_REGFILE_PACKET () {
    COMPLETE_REGFILE_PACKET_instance.print();
}




struct option_t {
	PackedLogicArray<2> p;
	uint64_t p_;
	PackedLogicArray<4> tag;
	uint64_t tag_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "option_t", "p", p_, p.as_int());
		success &= print_assert(indent, "option_t", "tag", tag_, tag.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "option_t", "p", p_, p.as_int());
		print_info(indent, "option_t", "tag", tag_, tag.as_int());
	}
};
SN_REFLECTION(option_t, true, p, tag);

option_t option_t_instance;

extern "C" void read_option_t (svOpenArrayHandle h) {
    read(option_t_instance, h);
}

extern "C" int check_option_t () {
    option_t_instance.check();
}

extern "C" void step_option_t () {
    option_t_instance.step();
}

extern "C" void print_option_t () {
    option_t_instance.print();
}




struct simple_option_t {
	Logic valid;
	bool valid_;
	PackedLogicArray<4> tag;
	uint64_t tag_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "simple_option_t", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "simple_option_t", "tag", tag_, tag.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "simple_option_t", "valid", valid_, valid.as_bool());
		print_info(indent, "simple_option_t", "tag", tag_, tag.as_int());
	}
};
SN_REFLECTION(simple_option_t, true, valid, tag);

simple_option_t simple_option_t_instance;

extern "C" void read_simple_option_t (svOpenArrayHandle h) {
    read(simple_option_t_instance, h);
}

extern "C" int check_simple_option_t () {
    simple_option_t_instance.check();
}

extern "C" void step_simple_option_t () {
    simple_option_t_instance.step();
}

extern "C" void print_simple_option_t () {
    simple_option_t_instance.print();
}




struct option_is_t {
	PackedLogicArray<5> p;
	uint64_t p_;
	PackedLogicArray<5> tag;
	uint64_t tag_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "option_is_t", "p", p_, p.as_int());
		success &= print_assert(indent, "option_is_t", "tag", tag_, tag.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "option_is_t", "p", p_, p.as_int());
		print_info(indent, "option_is_t", "tag", tag_, tag.as_int());
	}
};
SN_REFLECTION(option_is_t, true, p, tag);

option_is_t option_is_t_instance;

extern "C" void read_option_is_t (svOpenArrayHandle h) {
    read(option_is_t_instance, h);
}

extern "C" int check_option_is_t () {
    option_is_t_instance.check();
}

extern "C" void step_option_is_t () {
    option_is_t_instance.step();
}

extern "C" void print_option_is_t () {
    option_is_t_instance.print();
}




struct MEM_IF_PACKET {
	Logic bus_filled;
	bool bus_filled_;
	PackedLogicArray<64> mem_in;
	uint64_t mem_in_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "MEM_IF_PACKET", "bus_filled", bus_filled_, bus_filled.as_bool());
		success &= print_assert(indent, "MEM_IF_PACKET", "mem_in", mem_in_, mem_in.as_long());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "MEM_IF_PACKET", "bus_filled", bus_filled_, bus_filled.as_bool());
		print_info(indent, "MEM_IF_PACKET", "mem_in", mem_in_, mem_in.as_long());
	}
};
SN_REFLECTION(MEM_IF_PACKET, true, bus_filled, mem_in);

MEM_IF_PACKET MEM_IF_PACKET_instance;

extern "C" void read_MEM_IF_PACKET (svOpenArrayHandle h) {
    read(MEM_IF_PACKET_instance, h);
}

extern "C" int check_MEM_IF_PACKET () {
    MEM_IF_PACKET_instance.check();
}

extern "C" void step_MEM_IF_PACKET () {
    MEM_IF_PACKET_instance.step();
}

extern "C" void print_MEM_IF_PACKET () {
    MEM_IF_PACKET_instance.print();
}




struct IF_ICACHE_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<32> address;
	uint64_t address_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "IF_ICACHE_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "IF_ICACHE_PACKET", "address", address_, address.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "IF_ICACHE_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "IF_ICACHE_PACKET", "address", address_, address.as_int());
	}
};
SN_REFLECTION(IF_ICACHE_PACKET, true, valid, address);

IF_ICACHE_PACKET IF_ICACHE_PACKET_instance;

extern "C" void read_IF_ICACHE_PACKET (svOpenArrayHandle h) {
    read(IF_ICACHE_PACKET_instance, h);
}

extern "C" int check_IF_ICACHE_PACKET () {
    IF_ICACHE_PACKET_instance.check();
}

extern "C" void step_IF_ICACHE_PACKET () {
    IF_ICACHE_PACKET_instance.step();
}

extern "C" void print_IF_ICACHE_PACKET () {
    IF_ICACHE_PACKET_instance.print();
}




struct ICACHE_IF_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<32> data;
	uint64_t data_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "ICACHE_IF_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "ICACHE_IF_PACKET", "data", data_, data.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "ICACHE_IF_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "ICACHE_IF_PACKET", "data", data_, data.as_int());
	}
};
SN_REFLECTION(ICACHE_IF_PACKET, true, valid, data);

ICACHE_IF_PACKET ICACHE_IF_PACKET_instance;

extern "C" void read_ICACHE_IF_PACKET (svOpenArrayHandle h) {
    read(ICACHE_IF_PACKET_instance, h);
}

extern "C" int check_ICACHE_IF_PACKET () {
    ICACHE_IF_PACKET_instance.check();
}

extern "C" void step_ICACHE_IF_PACKET () {
    ICACHE_IF_PACKET_instance.step();
}

extern "C" void print_ICACHE_IF_PACKET () {
    ICACHE_IF_PACKET_instance.print();
}




struct ICACHE_ENTRY {
	Logic valid;
	bool valid_;
	PackedLogicArray<32> tag;
	uint64_t tag_;
	PackedLogicArray<64> data;
	uint64_t data_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "ICACHE_ENTRY", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "ICACHE_ENTRY", "tag", tag_, tag.as_int());
		success &= print_assert(indent, "ICACHE_ENTRY", "data", data_, data.as_long());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "ICACHE_ENTRY", "valid", valid_, valid.as_bool());
		print_info(indent, "ICACHE_ENTRY", "tag", tag_, tag.as_int());
		print_info(indent, "ICACHE_ENTRY", "data", data_, data.as_long());
	}
};
SN_REFLECTION(ICACHE_ENTRY, true, valid, tag, data);

ICACHE_ENTRY ICACHE_ENTRY_instance;

extern "C" void read_ICACHE_ENTRY (svOpenArrayHandle h) {
    read(ICACHE_ENTRY_instance, h);
}

extern "C" int check_ICACHE_ENTRY () {
    ICACHE_ENTRY_instance.check();
}

extern "C" void step_ICACHE_ENTRY () {
    ICACHE_ENTRY_instance.step();
}

extern "C" void print_ICACHE_ENTRY () {
    ICACHE_ENTRY_instance.print();
}




struct REQUEST_POOL_ENTRY {
	Logic valid;
	bool valid_;
	PackedLogicArray<32> address;
	uint64_t address_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "REQUEST_POOL_ENTRY", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "REQUEST_POOL_ENTRY", "address", address_, address.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "REQUEST_POOL_ENTRY", "valid", valid_, valid.as_bool());
		print_info(indent, "REQUEST_POOL_ENTRY", "address", address_, address.as_int());
	}
};
SN_REFLECTION(REQUEST_POOL_ENTRY, true, valid, address);

REQUEST_POOL_ENTRY REQUEST_POOL_ENTRY_instance;

extern "C" void read_REQUEST_POOL_ENTRY (svOpenArrayHandle h) {
    read(REQUEST_POOL_ENTRY_instance, h);
}

extern "C" int check_REQUEST_POOL_ENTRY () {
    REQUEST_POOL_ENTRY_instance.check();
}

extern "C" void step_REQUEST_POOL_ENTRY () {
    REQUEST_POOL_ENTRY_instance.step();
}

extern "C" void print_REQUEST_POOL_ENTRY () {
    REQUEST_POOL_ENTRY_instance.print();
}




using state_t = PackedLogicArray<2>;




using dcache_tag_t = PackedLogicArray<26>;




using dcache_index_t = PackedLogicArray<3>;




using dcache_offset_t = PackedLogicArray<3>;




using vcache_tag_t = PackedLogicArray<29>;




using prb_tag_t = PackedLogicArray<4>;




using prb_size_t = PackedLogicArray<4>;




using prb_cache_tag_t = PackedLogicArray<30>;




using prb_cache_offset_t = PackedLogicArray<2>;




struct DCACHE_ADDR {
	PackedLogicArray<26> tag;
	uint64_t tag_;
	PackedLogicArray<3> index;
	uint64_t index_;
	PackedLogicArray<3> offset;
	uint64_t offset_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DCACHE_ADDR", "tag", tag_, tag.as_int());
		success &= print_assert(indent, "DCACHE_ADDR", "index", index_, index.as_int());
		success &= print_assert(indent, "DCACHE_ADDR", "offset", offset_, offset.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DCACHE_ADDR", "tag", tag_, tag.as_int());
		print_info(indent, "DCACHE_ADDR", "index", index_, index.as_int());
		print_info(indent, "DCACHE_ADDR", "offset", offset_, offset.as_int());
	}
};
SN_REFLECTION(DCACHE_ADDR, true, tag, index, offset);

DCACHE_ADDR DCACHE_ADDR_instance;

extern "C" void read_DCACHE_ADDR (svOpenArrayHandle h) {
    read(DCACHE_ADDR_instance, h);
}

extern "C" int check_DCACHE_ADDR () {
    DCACHE_ADDR_instance.check();
}

extern "C" void step_DCACHE_ADDR () {
    DCACHE_ADDR_instance.step();
}

extern "C" void print_DCACHE_ADDR () {
    DCACHE_ADDR_instance.print();
}




struct PRB_ADDR {
	PackedLogicArray<30> tag;
	uint64_t tag_;
	PackedLogicArray<2> offset;
	uint64_t offset_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "PRB_ADDR", "tag", tag_, tag.as_int());
		success &= print_assert(indent, "PRB_ADDR", "offset", offset_, offset.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "PRB_ADDR", "tag", tag_, tag.as_int());
		print_info(indent, "PRB_ADDR", "offset", offset_, offset.as_int());
	}
};
SN_REFLECTION(PRB_ADDR, true, tag, offset);

PRB_ADDR PRB_ADDR_instance;

extern "C" void read_PRB_ADDR (svOpenArrayHandle h) {
    read(PRB_ADDR_instance, h);
}

extern "C" int check_PRB_ADDR () {
    PRB_ADDR_instance.check();
}

extern "C" void step_PRB_ADDR () {
    PRB_ADDR_instance.step();
}

extern "C" void print_PRB_ADDR () {
    PRB_ADDR_instance.print();
}




struct VCACHE_ADDR {
	PackedLogicArray<29> tag;
	uint64_t tag_;
	PackedLogicArray<3> offset;
	uint64_t offset_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "VCACHE_ADDR", "tag", tag_, tag.as_int());
		success &= print_assert(indent, "VCACHE_ADDR", "offset", offset_, offset.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "VCACHE_ADDR", "tag", tag_, tag.as_int());
		print_info(indent, "VCACHE_ADDR", "offset", offset_, offset.as_int());
	}
};
SN_REFLECTION(VCACHE_ADDR, true, tag, offset);

VCACHE_ADDR VCACHE_ADDR_instance;

extern "C" void read_VCACHE_ADDR (svOpenArrayHandle h) {
    read(VCACHE_ADDR_instance, h);
}

extern "C" int check_VCACHE_ADDR () {
    VCACHE_ADDR_instance.check();
}

extern "C" void step_VCACHE_ADDR () {
    VCACHE_ADDR_instance.step();
}

extern "C" void print_VCACHE_ADDR () {
    VCACHE_ADDR_instance.print();
}




struct WORD_ADDR {
	PackedLogicArray<30> tag;
	uint64_t tag_;
	PackedLogicArray<2> offset;
	uint64_t offset_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "WORD_ADDR", "tag", tag_, tag.as_int());
		success &= print_assert(indent, "WORD_ADDR", "offset", offset_, offset.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "WORD_ADDR", "tag", tag_, tag.as_int());
		print_info(indent, "WORD_ADDR", "offset", offset_, offset.as_int());
	}
};
SN_REFLECTION(WORD_ADDR, true, tag, offset);

WORD_ADDR WORD_ADDR_instance;

extern "C" void read_WORD_ADDR (svOpenArrayHandle h) {
    read(WORD_ADDR_instance, h);
}

extern "C" int check_WORD_ADDR () {
    WORD_ADDR_instance.check();
}

extern "C" void step_WORD_ADDR () {
    WORD_ADDR_instance.step();
}

extern "C" void print_WORD_ADDR () {
    WORD_ADDR_instance.print();
}




struct MEM_ADDR {
	PackedLogicArray<29> tag;
	uint64_t tag_;
	PackedLogicArray<3> offset;
	uint64_t offset_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "MEM_ADDR", "tag", tag_, tag.as_int());
		success &= print_assert(indent, "MEM_ADDR", "offset", offset_, offset.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "MEM_ADDR", "tag", tag_, tag.as_int());
		print_info(indent, "MEM_ADDR", "offset", offset_, offset.as_int());
	}
};
SN_REFLECTION(MEM_ADDR, true, tag, offset);

MEM_ADDR MEM_ADDR_instance;

extern "C" void read_MEM_ADDR (svOpenArrayHandle h) {
    read(MEM_ADDR_instance, h);
}

extern "C" int check_MEM_ADDR () {
    MEM_ADDR_instance.check();
}

extern "C" void step_MEM_ADDR () {
    MEM_ADDR_instance.step();
}

extern "C" void print_MEM_ADDR () {
    MEM_ADDR_instance.print();
}




struct LSQ_LD_PACKET_OUT {
	Logic valid;
	bool valid_;
	PackedLogicArray<32> value;
	uint64_t value_;
	PackedLogicArray<4> valid_byte;
	uint64_t valid_byte_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "LSQ_LD_PACKET_OUT", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "LSQ_LD_PACKET_OUT", "value", value_, value.as_int());
		success &= print_assert(indent, "LSQ_LD_PACKET_OUT", "valid_byte", valid_byte_, valid_byte.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "LSQ_LD_PACKET_OUT", "valid", valid_, valid.as_bool());
		print_info(indent, "LSQ_LD_PACKET_OUT", "value", value_, value.as_int());
		print_info(indent, "LSQ_LD_PACKET_OUT", "valid_byte", valid_byte_, valid_byte.as_int());
	}
};
SN_REFLECTION(LSQ_LD_PACKET_OUT, true, valid, value, valid_byte);

LSQ_LD_PACKET_OUT LSQ_LD_PACKET_OUT_instance;

extern "C" void read_LSQ_LD_PACKET_OUT (svOpenArrayHandle h) {
    read(LSQ_LD_PACKET_OUT_instance, h);
}

extern "C" int check_LSQ_LD_PACKET_OUT () {
    LSQ_LD_PACKET_OUT_instance.check();
}

extern "C" void step_LSQ_LD_PACKET_OUT () {
    LSQ_LD_PACKET_OUT_instance.step();
}

extern "C" void print_LSQ_LD_PACKET_OUT () {
    LSQ_LD_PACKET_OUT_instance.print();
}




struct FU_LSQ_LD_PACKET {
	Logic valid;
	bool valid_;
	ADDR_UNION address;
	uint32_t address_;
	PackedLogicArray<32> age;
	uint64_t age_;
	PackedLogicArray<4> valid_byte;
	uint64_t valid_byte_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "FU_LSQ_LD_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "FU_LSQ_LD_PACKET", "address", address_, address.as_int());
		success &= print_assert(indent, "FU_LSQ_LD_PACKET", "age", age_, age.as_int());
		success &= print_assert(indent, "FU_LSQ_LD_PACKET", "valid_byte", valid_byte_, valid_byte.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "FU_LSQ_LD_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "FU_LSQ_LD_PACKET", "address", address_, address.as_int());
		print_info(indent, "FU_LSQ_LD_PACKET", "age", age_, age.as_int());
		print_info(indent, "FU_LSQ_LD_PACKET", "valid_byte", valid_byte_, valid_byte.as_int());
	}
};
SN_REFLECTION(FU_LSQ_LD_PACKET, true, valid, address, age, valid_byte);

FU_LSQ_LD_PACKET FU_LSQ_LD_PACKET_instance;

extern "C" void read_FU_LSQ_LD_PACKET (svOpenArrayHandle h) {
    read(FU_LSQ_LD_PACKET_instance, h);
}

extern "C" int check_FU_LSQ_LD_PACKET () {
    FU_LSQ_LD_PACKET_instance.check();
}

extern "C" void step_FU_LSQ_LD_PACKET () {
    FU_LSQ_LD_PACKET_instance.step();
}

extern "C" void print_FU_LSQ_LD_PACKET () {
    FU_LSQ_LD_PACKET_instance.print();
}




struct FU_LSQ_ST_PACKET {
	Logic valid;
	bool valid_;
	ADDR_UNION address;
	uint32_t address_;
	PackedLogicArray<32> value;
	uint64_t value_;
	PackedLogicArray<4> valid_byte;
	uint64_t valid_byte_;
	PackedLogicArray<3> age;
	uint64_t age_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "FU_LSQ_ST_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "FU_LSQ_ST_PACKET", "address", address_, address.as_int());
		success &= print_assert(indent, "FU_LSQ_ST_PACKET", "value", value_, value.as_int());
		success &= print_assert(indent, "FU_LSQ_ST_PACKET", "valid_byte", valid_byte_, valid_byte.as_int());
		success &= print_assert(indent, "FU_LSQ_ST_PACKET", "age", age_, age.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "FU_LSQ_ST_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "FU_LSQ_ST_PACKET", "address", address_, address.as_int());
		print_info(indent, "FU_LSQ_ST_PACKET", "value", value_, value.as_int());
		print_info(indent, "FU_LSQ_ST_PACKET", "valid_byte", valid_byte_, valid_byte.as_int());
		print_info(indent, "FU_LSQ_ST_PACKET", "age", age_, age.as_int());
	}
};
SN_REFLECTION(FU_LSQ_ST_PACKET, true, valid, address, value, valid_byte, age);

FU_LSQ_ST_PACKET FU_LSQ_ST_PACKET_instance;

extern "C" void read_FU_LSQ_ST_PACKET (svOpenArrayHandle h) {
    read(FU_LSQ_ST_PACKET_instance, h);
}

extern "C" int check_FU_LSQ_ST_PACKET () {
    FU_LSQ_ST_PACKET_instance.check();
}

extern "C" void step_FU_LSQ_ST_PACKET () {
    FU_LSQ_ST_PACKET_instance.step();
}

extern "C" void print_FU_LSQ_ST_PACKET () {
    FU_LSQ_ST_PACKET_instance.print();
}




struct DECODE_LSQ_PACKET {
	Logic valid;
	bool valid_;
	Logic is_ST;
	bool is_ST_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DECODE_LSQ_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "DECODE_LSQ_PACKET", "is_ST", is_ST_, is_ST.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DECODE_LSQ_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "DECODE_LSQ_PACKET", "is_ST", is_ST_, is_ST.as_bool());
	}
};
SN_REFLECTION(DECODE_LSQ_PACKET, true, valid, is_ST);

DECODE_LSQ_PACKET DECODE_LSQ_PACKET_instance;

extern "C" void read_DECODE_LSQ_PACKET (svOpenArrayHandle h) {
    read(DECODE_LSQ_PACKET_instance, h);
}

extern "C" int check_DECODE_LSQ_PACKET () {
    DECODE_LSQ_PACKET_instance.check();
}

extern "C" void step_DECODE_LSQ_PACKET () {
    DECODE_LSQ_PACKET_instance.step();
}

extern "C" void print_DECODE_LSQ_PACKET () {
    DECODE_LSQ_PACKET_instance.print();
}




struct LSQ_RS_PACKET {
	PackedLogicArray<3> age;
	uint64_t age_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "LSQ_RS_PACKET", "age", age_, age.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "LSQ_RS_PACKET", "age", age_, age.as_int());
	}
};
SN_REFLECTION(LSQ_RS_PACKET, true, age);

LSQ_RS_PACKET LSQ_RS_PACKET_instance;

extern "C" void read_LSQ_RS_PACKET (svOpenArrayHandle h) {
    read(LSQ_RS_PACKET_instance, h);
}

extern "C" int check_LSQ_RS_PACKET () {
    LSQ_RS_PACKET_instance.check();
}

extern "C" void step_LSQ_RS_PACKET () {
    LSQ_RS_PACKET_instance.step();
}

extern "C" void print_LSQ_RS_PACKET () {
    LSQ_RS_PACKET_instance.print();
}




struct SQ_ENTRY {
	Logic valid;
	bool valid_;
	ADDR_UNION address;
	uint32_t address_;
	PackedLogicArray<32> value;
	uint64_t value_;
	PackedLogicArray<4> valid_byte;
	uint64_t valid_byte_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "SQ_ENTRY", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "SQ_ENTRY", "address", address_, address.as_int());
		success &= print_assert(indent, "SQ_ENTRY", "value", value_, value.as_int());
		success &= print_assert(indent, "SQ_ENTRY", "valid_byte", valid_byte_, valid_byte.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "SQ_ENTRY", "valid", valid_, valid.as_bool());
		print_info(indent, "SQ_ENTRY", "address", address_, address.as_int());
		print_info(indent, "SQ_ENTRY", "value", value_, value.as_int());
		print_info(indent, "SQ_ENTRY", "valid_byte", valid_byte_, valid_byte.as_int());
	}
};
SN_REFLECTION(SQ_ENTRY, true, valid, address, value, valid_byte);

SQ_ENTRY SQ_ENTRY_instance;

extern "C" void read_SQ_ENTRY (svOpenArrayHandle h) {
    read(SQ_ENTRY_instance, h);
}

extern "C" int check_SQ_ENTRY () {
    SQ_ENTRY_instance.check();
}

extern "C" void step_SQ_ENTRY () {
    SQ_ENTRY_instance.step();
}

extern "C" void print_SQ_ENTRY () {
    SQ_ENTRY_instance.print();
}




struct LSQ_DCACHE_LD_PACKET {
	Logic valid;
	bool valid_;
	ADDR_UNION address;
	uint32_t address_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "LSQ_DCACHE_LD_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "LSQ_DCACHE_LD_PACKET", "address", address_, address.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "LSQ_DCACHE_LD_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "LSQ_DCACHE_LD_PACKET", "address", address_, address.as_int());
	}
};
SN_REFLECTION(LSQ_DCACHE_LD_PACKET, true, valid, address);

LSQ_DCACHE_LD_PACKET LSQ_DCACHE_LD_PACKET_instance;

extern "C" void read_LSQ_DCACHE_LD_PACKET (svOpenArrayHandle h) {
    read(LSQ_DCACHE_LD_PACKET_instance, h);
}

extern "C" int check_LSQ_DCACHE_LD_PACKET () {
    LSQ_DCACHE_LD_PACKET_instance.check();
}

extern "C" void step_LSQ_DCACHE_LD_PACKET () {
    LSQ_DCACHE_LD_PACKET_instance.step();
}

extern "C" void print_LSQ_DCACHE_LD_PACKET () {
    LSQ_DCACHE_LD_PACKET_instance.print();
}




struct LSQ_PRB_ST_PACKET {
	Logic valid;
	bool valid_;
	ADDR_UNION address;
	uint32_t address_;
	PackedLogicArray<32> value;
	uint64_t value_;
	PackedLogicArray<4> valid_byte;
	uint64_t valid_byte_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "LSQ_PRB_ST_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "LSQ_PRB_ST_PACKET", "address", address_, address.as_int());
		success &= print_assert(indent, "LSQ_PRB_ST_PACKET", "value", value_, value.as_int());
		success &= print_assert(indent, "LSQ_PRB_ST_PACKET", "valid_byte", valid_byte_, valid_byte.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "LSQ_PRB_ST_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "LSQ_PRB_ST_PACKET", "address", address_, address.as_int());
		print_info(indent, "LSQ_PRB_ST_PACKET", "value", value_, value.as_int());
		print_info(indent, "LSQ_PRB_ST_PACKET", "valid_byte", valid_byte_, valid_byte.as_int());
	}
};
SN_REFLECTION(LSQ_PRB_ST_PACKET, true, valid, address, value, valid_byte);

LSQ_PRB_ST_PACKET LSQ_PRB_ST_PACKET_instance;

extern "C" void read_LSQ_PRB_ST_PACKET (svOpenArrayHandle h) {
    read(LSQ_PRB_ST_PACKET_instance, h);
}

extern "C" int check_LSQ_PRB_ST_PACKET () {
    LSQ_PRB_ST_PACKET_instance.check();
}

extern "C" void step_LSQ_PRB_ST_PACKET () {
    LSQ_PRB_ST_PACKET_instance.step();
}

extern "C" void print_LSQ_PRB_ST_PACKET () {
    LSQ_PRB_ST_PACKET_instance.print();
}




struct DCACHE_LSQ_LD_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<32> value;
	uint64_t value_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DCACHE_LSQ_LD_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "DCACHE_LSQ_LD_PACKET", "value", value_, value.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DCACHE_LSQ_LD_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "DCACHE_LSQ_LD_PACKET", "value", value_, value.as_int());
	}
};
SN_REFLECTION(DCACHE_LSQ_LD_PACKET, true, valid, value);

DCACHE_LSQ_LD_PACKET DCACHE_LSQ_LD_PACKET_instance;

extern "C" void read_DCACHE_LSQ_LD_PACKET (svOpenArrayHandle h) {
    read(DCACHE_LSQ_LD_PACKET_instance, h);
}

extern "C" int check_DCACHE_LSQ_LD_PACKET () {
    DCACHE_LSQ_LD_PACKET_instance.check();
}

extern "C" void step_DCACHE_LSQ_LD_PACKET () {
    DCACHE_LSQ_LD_PACKET_instance.step();
}

extern "C" void print_DCACHE_LSQ_LD_PACKET () {
    DCACHE_LSQ_LD_PACKET_instance.print();
}




struct ROB_LSQ_PACKET {
	Logic valid;
	bool valid_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "ROB_LSQ_PACKET", "valid", valid_, valid.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "ROB_LSQ_PACKET", "valid", valid_, valid.as_bool());
	}
};
SN_REFLECTION(ROB_LSQ_PACKET, true, valid);

ROB_LSQ_PACKET ROB_LSQ_PACKET_instance;

extern "C" void read_ROB_LSQ_PACKET (svOpenArrayHandle h) {
    read(ROB_LSQ_PACKET_instance, h);
}

extern "C" int check_ROB_LSQ_PACKET () {
    ROB_LSQ_PACKET_instance.check();
}

extern "C" void step_ROB_LSQ_PACKET () {
    ROB_LSQ_PACKET_instance.step();
}

extern "C" void print_ROB_LSQ_PACKET () {
    ROB_LSQ_PACKET_instance.print();
}




struct DCACHE_LSQ_ST_PACKET {
	Logic valid;
	bool valid_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DCACHE_LSQ_ST_PACKET", "valid", valid_, valid.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DCACHE_LSQ_ST_PACKET", "valid", valid_, valid.as_bool());
	}
};
SN_REFLECTION(DCACHE_LSQ_ST_PACKET, true, valid);

DCACHE_LSQ_ST_PACKET DCACHE_LSQ_ST_PACKET_instance;

extern "C" void read_DCACHE_LSQ_ST_PACKET (svOpenArrayHandle h) {
    read(DCACHE_LSQ_ST_PACKET_instance, h);
}

extern "C" int check_DCACHE_LSQ_ST_PACKET () {
    DCACHE_LSQ_ST_PACKET_instance.check();
}

extern "C" void step_DCACHE_LSQ_ST_PACKET () {
    DCACHE_LSQ_ST_PACKET_instance.step();
}

extern "C" void print_DCACHE_LSQ_ST_PACKET () {
    DCACHE_LSQ_ST_PACKET_instance.print();
}




struct DCACHE_ENTRY {
	ADDR_UNION addr;
	uint32_t addr_;
	MEM_DATA_UNION data;
	uint32_t data_;
	Logic valid;
	bool valid_;
	Logic dirty;
	bool dirty_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DCACHE_ENTRY", "addr", addr_, addr.as_int());
		success &= print_assert(indent, "DCACHE_ENTRY", "data", data_, data.as_int());
		success &= print_assert(indent, "DCACHE_ENTRY", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "DCACHE_ENTRY", "dirty", dirty_, dirty.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DCACHE_ENTRY", "addr", addr_, addr.as_int());
		print_info(indent, "DCACHE_ENTRY", "data", data_, data.as_int());
		print_info(indent, "DCACHE_ENTRY", "valid", valid_, valid.as_bool());
		print_info(indent, "DCACHE_ENTRY", "dirty", dirty_, dirty.as_bool());
	}
};
SN_REFLECTION(DCACHE_ENTRY, true, addr, data, valid, dirty);

DCACHE_ENTRY DCACHE_ENTRY_instance;

extern "C" void read_DCACHE_ENTRY (svOpenArrayHandle h) {
    read(DCACHE_ENTRY_instance, h);
}

extern "C" int check_DCACHE_ENTRY () {
    DCACHE_ENTRY_instance.check();
}

extern "C" void step_DCACHE_ENTRY () {
    DCACHE_ENTRY_instance.step();
}

extern "C" void print_DCACHE_ENTRY () {
    DCACHE_ENTRY_instance.print();
}




struct DCACHE_CTRL_ST_PACKET {
	Logic valid;
	bool valid_;
	ADDR_UNION address;
	uint32_t address_;
	PackedLogicArray<64> mask;
	uint64_t mask_;
	PackedLogicArray<64> data;
	uint64_t data_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DCACHE_CTRL_ST_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "DCACHE_CTRL_ST_PACKET", "address", address_, address.as_int());
		success &= print_assert(indent, "DCACHE_CTRL_ST_PACKET", "mask", mask_, mask.as_long());
		success &= print_assert(indent, "DCACHE_CTRL_ST_PACKET", "data", data_, data.as_long());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DCACHE_CTRL_ST_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "DCACHE_CTRL_ST_PACKET", "address", address_, address.as_int());
		print_info(indent, "DCACHE_CTRL_ST_PACKET", "mask", mask_, mask.as_long());
		print_info(indent, "DCACHE_CTRL_ST_PACKET", "data", data_, data.as_long());
	}
};
SN_REFLECTION(DCACHE_CTRL_ST_PACKET, true, valid, address, mask, data);

DCACHE_CTRL_ST_PACKET DCACHE_CTRL_ST_PACKET_instance;

extern "C" void read_DCACHE_CTRL_ST_PACKET (svOpenArrayHandle h) {
    read(DCACHE_CTRL_ST_PACKET_instance, h);
}

extern "C" int check_DCACHE_CTRL_ST_PACKET () {
    DCACHE_CTRL_ST_PACKET_instance.check();
}

extern "C" void step_DCACHE_CTRL_ST_PACKET () {
    DCACHE_CTRL_ST_PACKET_instance.step();
}

extern "C" void print_DCACHE_CTRL_ST_PACKET () {
    DCACHE_CTRL_ST_PACKET_instance.print();
}




using ST_STATE = PackedLogicArray<2>;




struct DCACHE_MEM_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<2> command;
	uint64_t command_;
	ADDR_UNION addr;
	uint32_t addr_;
	PackedLogicArray<64> data;
	uint64_t data_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DCACHE_MEM_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "DCACHE_MEM_PACKET", "command", command_, command.as_int());
		success &= print_assert(indent, "DCACHE_MEM_PACKET", "addr", addr_, addr.as_int());
		success &= print_assert(indent, "DCACHE_MEM_PACKET", "data", data_, data.as_long());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DCACHE_MEM_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "DCACHE_MEM_PACKET", "command", command_, command.as_int());
		print_info(indent, "DCACHE_MEM_PACKET", "addr", addr_, addr.as_int());
		print_info(indent, "DCACHE_MEM_PACKET", "data", data_, data.as_long());
	}
};
SN_REFLECTION(DCACHE_MEM_PACKET, true, valid, command, addr, data);

DCACHE_MEM_PACKET DCACHE_MEM_PACKET_instance;

extern "C" void read_DCACHE_MEM_PACKET (svOpenArrayHandle h) {
    read(DCACHE_MEM_PACKET_instance, h);
}

extern "C" int check_DCACHE_MEM_PACKET () {
    DCACHE_MEM_PACKET_instance.check();
}

extern "C" void step_DCACHE_MEM_PACKET () {
    DCACHE_MEM_PACKET_instance.step();
}

extern "C" void print_DCACHE_MEM_PACKET () {
    DCACHE_MEM_PACKET_instance.print();
}




struct MSHR_DCACHE_MEM_PACKET {
	Logic valid;
	bool valid_;
	PackedLogicArray<2> command;
	uint64_t command_;
	ADDR_UNION addr;
	uint32_t addr_;
	PackedLogicArray<64> data;
	uint64_t data_;
	PackedLogicArray<64> mask;
	uint64_t mask_;
	PackedLogicArray<64> w_data;
	uint64_t w_data_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "MSHR_DCACHE_MEM_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "MSHR_DCACHE_MEM_PACKET", "command", command_, command.as_int());
		success &= print_assert(indent, "MSHR_DCACHE_MEM_PACKET", "addr", addr_, addr.as_int());
		success &= print_assert(indent, "MSHR_DCACHE_MEM_PACKET", "data", data_, data.as_long());
		success &= print_assert(indent, "MSHR_DCACHE_MEM_PACKET", "mask", mask_, mask.as_long());
		success &= print_assert(indent, "MSHR_DCACHE_MEM_PACKET", "w_data", w_data_, w_data.as_long());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "MSHR_DCACHE_MEM_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "MSHR_DCACHE_MEM_PACKET", "command", command_, command.as_int());
		print_info(indent, "MSHR_DCACHE_MEM_PACKET", "addr", addr_, addr.as_int());
		print_info(indent, "MSHR_DCACHE_MEM_PACKET", "data", data_, data.as_long());
		print_info(indent, "MSHR_DCACHE_MEM_PACKET", "mask", mask_, mask.as_long());
		print_info(indent, "MSHR_DCACHE_MEM_PACKET", "w_data", w_data_, w_data.as_long());
	}
};
SN_REFLECTION(MSHR_DCACHE_MEM_PACKET, true, valid, command, addr, data, mask, w_data);

MSHR_DCACHE_MEM_PACKET MSHR_DCACHE_MEM_PACKET_instance;

extern "C" void read_MSHR_DCACHE_MEM_PACKET (svOpenArrayHandle h) {
    read(MSHR_DCACHE_MEM_PACKET_instance, h);
}

extern "C" int check_MSHR_DCACHE_MEM_PACKET () {
    MSHR_DCACHE_MEM_PACKET_instance.check();
}

extern "C" void step_MSHR_DCACHE_MEM_PACKET () {
    MSHR_DCACHE_MEM_PACKET_instance.step();
}

extern "C" void print_MSHR_DCACHE_MEM_PACKET () {
    MSHR_DCACHE_MEM_PACKET_instance.print();
}




struct MEM_DCACHE_PACKET {
	PackedLogicArray<3> tag;
	uint64_t tag_;
	PackedLogicArray<3> response;
	uint64_t response_;
	PackedLogicArray<64> data;
	uint64_t data_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "MEM_DCACHE_PACKET", "tag", tag_, tag.as_int());
		success &= print_assert(indent, "MEM_DCACHE_PACKET", "response", response_, response.as_int());
		success &= print_assert(indent, "MEM_DCACHE_PACKET", "data", data_, data.as_long());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "MEM_DCACHE_PACKET", "tag", tag_, tag.as_int());
		print_info(indent, "MEM_DCACHE_PACKET", "response", response_, response.as_int());
		print_info(indent, "MEM_DCACHE_PACKET", "data", data_, data.as_long());
	}
};
SN_REFLECTION(MEM_DCACHE_PACKET, true, tag, response, data);

MEM_DCACHE_PACKET MEM_DCACHE_PACKET_instance;

extern "C" void read_MEM_DCACHE_PACKET (svOpenArrayHandle h) {
    read(MEM_DCACHE_PACKET_instance, h);
}

extern "C" int check_MEM_DCACHE_PACKET () {
    MEM_DCACHE_PACKET_instance.check();
}

extern "C" void step_MEM_DCACHE_PACKET () {
    MEM_DCACHE_PACKET_instance.step();
}

extern "C" void print_MEM_DCACHE_PACKET () {
    MEM_DCACHE_PACKET_instance.print();
}




struct MSHR_ENTRY {
	ADDR_UNION addr;
	uint32_t addr_;
	MEM_DATA_UNION data;
	uint32_t data_;
	PackedLogicArray<64> mask;
	uint64_t mask_;
	PackedLogicArray<64> w_data;
	uint64_t w_data_;
	Logic dirty;
	bool dirty_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "MSHR_ENTRY", "addr", addr_, addr.as_int());
		success &= print_assert(indent, "MSHR_ENTRY", "data", data_, data.as_int());
		success &= print_assert(indent, "MSHR_ENTRY", "mask", mask_, mask.as_long());
		success &= print_assert(indent, "MSHR_ENTRY", "w_data", w_data_, w_data.as_long());
		success &= print_assert(indent, "MSHR_ENTRY", "dirty", dirty_, dirty.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "MSHR_ENTRY", "addr", addr_, addr.as_int());
		print_info(indent, "MSHR_ENTRY", "data", data_, data.as_int());
		print_info(indent, "MSHR_ENTRY", "mask", mask_, mask.as_long());
		print_info(indent, "MSHR_ENTRY", "w_data", w_data_, w_data.as_long());
		print_info(indent, "MSHR_ENTRY", "dirty", dirty_, dirty.as_bool());
	}
};
SN_REFLECTION(MSHR_ENTRY, true, addr, data, mask, w_data, dirty);

MSHR_ENTRY MSHR_ENTRY_instance;

extern "C" void read_MSHR_ENTRY (svOpenArrayHandle h) {
    read(MSHR_ENTRY_instance, h);
}

extern "C" int check_MSHR_ENTRY () {
    MSHR_ENTRY_instance.check();
}

extern "C" void step_MSHR_ENTRY () {
    MSHR_ENTRY_instance.step();
}

extern "C" void print_MSHR_ENTRY () {
    MSHR_ENTRY_instance.print();
}




struct DCACHE_MEMREQ_SIGNAL {
	PackedLogicArray<3> ld;
	uint64_t ld_;
	Logic st;
	bool st_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DCACHE_MEMREQ_SIGNAL", "ld", ld_, ld.as_int());
		success &= print_assert(indent, "DCACHE_MEMREQ_SIGNAL", "st", st_, st.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DCACHE_MEMREQ_SIGNAL", "ld", ld_, ld.as_int());
		print_info(indent, "DCACHE_MEMREQ_SIGNAL", "st", st_, st.as_bool());
	}
};
SN_REFLECTION(DCACHE_MEMREQ_SIGNAL, true, ld, st);

DCACHE_MEMREQ_SIGNAL DCACHE_MEMREQ_SIGNAL_instance;

extern "C" void read_DCACHE_MEMREQ_SIGNAL (svOpenArrayHandle h) {
    read(DCACHE_MEMREQ_SIGNAL_instance, h);
}

extern "C" int check_DCACHE_MEMREQ_SIGNAL () {
    DCACHE_MEMREQ_SIGNAL_instance.check();
}

extern "C" void step_DCACHE_MEMREQ_SIGNAL () {
    DCACHE_MEMREQ_SIGNAL_instance.step();
}

extern "C" void print_DCACHE_MEMREQ_SIGNAL () {
    DCACHE_MEMREQ_SIGNAL_instance.print();
}




struct PRB_ENTRY {
	Logic valid;
	bool valid_;
	PackedLogicArray<30> tag;
	uint64_t tag_;
	PackedLogicArray<4> valid_byte;
	uint64_t valid_byte_;
	PackedLogicArray<32> value;
	uint64_t value_;
	Logic written;
	bool written_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "PRB_ENTRY", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "PRB_ENTRY", "tag", tag_, tag.as_int());
		success &= print_assert(indent, "PRB_ENTRY", "valid_byte", valid_byte_, valid_byte.as_int());
		success &= print_assert(indent, "PRB_ENTRY", "value", value_, value.as_int());
		success &= print_assert(indent, "PRB_ENTRY", "written", written_, written.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "PRB_ENTRY", "valid", valid_, valid.as_bool());
		print_info(indent, "PRB_ENTRY", "tag", tag_, tag.as_int());
		print_info(indent, "PRB_ENTRY", "valid_byte", valid_byte_, valid_byte.as_int());
		print_info(indent, "PRB_ENTRY", "value", value_, value.as_int());
		print_info(indent, "PRB_ENTRY", "written", written_, written.as_bool());
	}
};
SN_REFLECTION(PRB_ENTRY, true, valid, tag, valid_byte, value, written);

PRB_ENTRY PRB_ENTRY_instance;

extern "C" void read_PRB_ENTRY (svOpenArrayHandle h) {
    read(PRB_ENTRY_instance, h);
}

extern "C" int check_PRB_ENTRY () {
    PRB_ENTRY_instance.check();
}

extern "C" void step_PRB_ENTRY () {
    PRB_ENTRY_instance.step();
}

extern "C" void print_PRB_ENTRY () {
    PRB_ENTRY_instance.print();
}




struct PRB_DCACHE_ST_PACKET {
	Logic valid;
	bool valid_;
	ADDR_UNION address;
	uint32_t address_;
	PackedLogicArray<4> valid_byte;
	uint64_t valid_byte_;
	PackedLogicArray<32> value;
	uint64_t value_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "PRB_DCACHE_ST_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "PRB_DCACHE_ST_PACKET", "address", address_, address.as_int());
		success &= print_assert(indent, "PRB_DCACHE_ST_PACKET", "valid_byte", valid_byte_, valid_byte.as_int());
		success &= print_assert(indent, "PRB_DCACHE_ST_PACKET", "value", value_, value.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "PRB_DCACHE_ST_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "PRB_DCACHE_ST_PACKET", "address", address_, address.as_int());
		print_info(indent, "PRB_DCACHE_ST_PACKET", "valid_byte", valid_byte_, valid_byte.as_int());
		print_info(indent, "PRB_DCACHE_ST_PACKET", "value", value_, value.as_int());
	}
};
SN_REFLECTION(PRB_DCACHE_ST_PACKET, true, valid, address, valid_byte, value);

PRB_DCACHE_ST_PACKET PRB_DCACHE_ST_PACKET_instance;

extern "C" void read_PRB_DCACHE_ST_PACKET (svOpenArrayHandle h) {
    read(PRB_DCACHE_ST_PACKET_instance, h);
}

extern "C" int check_PRB_DCACHE_ST_PACKET () {
    PRB_DCACHE_ST_PACKET_instance.check();
}

extern "C" void step_PRB_DCACHE_ST_PACKET () {
    PRB_DCACHE_ST_PACKET_instance.step();
}

extern "C" void print_PRB_DCACHE_ST_PACKET () {
    PRB_DCACHE_ST_PACKET_instance.print();
}




struct ROB_BP_PACKET {
	Logic valid;
	bool valid_;
	Logic taken;
	bool taken_;
	Logic is_insert;
	bool is_insert_;
	PackedLogicArray<32> branch_address;
	uint64_t branch_address_;
	Logic tournament;
	bool tournament_;
	Logic branch_mispredicted;
	bool branch_mispredicted_;
	Logic call;
	bool call_;
	Logic ret;
	bool ret_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "ROB_BP_PACKET", "valid", valid_, valid.as_bool());
		success &= print_assert(indent, "ROB_BP_PACKET", "taken", taken_, taken.as_bool());
		success &= print_assert(indent, "ROB_BP_PACKET", "is_insert", is_insert_, is_insert.as_bool());
		success &= print_assert(indent, "ROB_BP_PACKET", "branch_address", branch_address_, branch_address.as_int());
		success &= print_assert(indent, "ROB_BP_PACKET", "tournament", tournament_, tournament.as_bool());
		success &= print_assert(indent, "ROB_BP_PACKET", "branch_mispredicted", branch_mispredicted_, branch_mispredicted.as_bool());
		success &= print_assert(indent, "ROB_BP_PACKET", "call", call_, call.as_bool());
		success &= print_assert(indent, "ROB_BP_PACKET", "ret", ret_, ret.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "ROB_BP_PACKET", "valid", valid_, valid.as_bool());
		print_info(indent, "ROB_BP_PACKET", "taken", taken_, taken.as_bool());
		print_info(indent, "ROB_BP_PACKET", "is_insert", is_insert_, is_insert.as_bool());
		print_info(indent, "ROB_BP_PACKET", "branch_address", branch_address_, branch_address.as_int());
		print_info(indent, "ROB_BP_PACKET", "tournament", tournament_, tournament.as_bool());
		print_info(indent, "ROB_BP_PACKET", "branch_mispredicted", branch_mispredicted_, branch_mispredicted.as_bool());
		print_info(indent, "ROB_BP_PACKET", "call", call_, call.as_bool());
		print_info(indent, "ROB_BP_PACKET", "ret", ret_, ret.as_bool());
	}
};
SN_REFLECTION(ROB_BP_PACKET, true, valid, taken, is_insert, branch_address, tournament, branch_mispredicted, call, ret);

ROB_BP_PACKET ROB_BP_PACKET_instance;

extern "C" void read_ROB_BP_PACKET (svOpenArrayHandle h) {
    read(ROB_BP_PACKET_instance, h);
}

extern "C" int check_ROB_BP_PACKET () {
    ROB_BP_PACKET_instance.check();
}

extern "C" void step_ROB_BP_PACKET () {
    ROB_BP_PACKET_instance.step();
}

extern "C" void print_ROB_BP_PACKET () {
    ROB_BP_PACKET_instance.print();
}




using ras_tag_t = PackedLogicArray<5>;




using ras_size_t = PackedLogicArray<5>;




using gshare_tag_t = PackedLogicArray<7>;




using gshare_tag_tag_t = PackedLogicArray<2>;




struct BP_ENTRY {
	PackedLogicArray<2> counter;
	uint64_t counter_;
	PackedLogicArray<2> counter_inorder;
	uint64_t counter_inorder_;
	PackedLogicArray<2> tournament;
	uint64_t tournament_;
	Logic first_time;
	bool first_time_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "BP_ENTRY", "counter", counter_, counter.as_int());
		success &= print_assert(indent, "BP_ENTRY", "counter_inorder", counter_inorder_, counter_inorder.as_int());
		success &= print_assert(indent, "BP_ENTRY", "tournament", tournament_, tournament.as_int());
		success &= print_assert(indent, "BP_ENTRY", "first_time", first_time_, first_time.as_bool());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "BP_ENTRY", "counter", counter_, counter.as_int());
		print_info(indent, "BP_ENTRY", "counter_inorder", counter_inorder_, counter_inorder.as_int());
		print_info(indent, "BP_ENTRY", "tournament", tournament_, tournament.as_int());
		print_info(indent, "BP_ENTRY", "first_time", first_time_, first_time.as_bool());
	}
};
SN_REFLECTION(BP_ENTRY, true, counter, counter_inorder, tournament, first_time);

BP_ENTRY BP_ENTRY_instance;

extern "C" void read_BP_ENTRY (svOpenArrayHandle h) {
    read(BP_ENTRY_instance, h);
}

extern "C" int check_BP_ENTRY () {
    BP_ENTRY_instance.check();
}

extern "C" void step_BP_ENTRY () {
    BP_ENTRY_instance.step();
}

extern "C" void print_BP_ENTRY () {
    BP_ENTRY_instance.print();
}




struct DUMP_IDX_PACKET {
	PackedLogicArray<3> index;
	uint64_t index_;
	PackedLogicArray<2> way;
	uint64_t way_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "DUMP_IDX_PACKET", "index", index_, index.as_int());
		success &= print_assert(indent, "DUMP_IDX_PACKET", "way", way_, way.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "DUMP_IDX_PACKET", "index", index_, index.as_int());
		print_info(indent, "DUMP_IDX_PACKET", "way", way_, way.as_int());
	}
};
SN_REFLECTION(DUMP_IDX_PACKET, true, index, way);

DUMP_IDX_PACKET DUMP_IDX_PACKET_instance;

extern "C" void read_DUMP_IDX_PACKET (svOpenArrayHandle h) {
    read(DUMP_IDX_PACKET_instance, h);
}

extern "C" int check_DUMP_IDX_PACKET () {
    DUMP_IDX_PACKET_instance.check();
}

extern "C" void step_DUMP_IDX_PACKET () {
    DUMP_IDX_PACKET_instance.step();
}

extern "C" void print_DUMP_IDX_PACKET () {
    DUMP_IDX_PACKET_instance.print();
}




using lru_t = PackedLogicArray<2>;




struct pipeline_IN_P {
	PackedLogicArray<4> mem2proc_response;
	uint64_t mem2proc_response_;
	PackedLogicArray<64> mem2proc_data;
	uint64_t mem2proc_data_;
	PackedLogicArray<4> mem2proc_tag;
	uint64_t mem2proc_tag_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "pipeline_IN_P", "mem2proc_response", mem2proc_response_, mem2proc_response.as_int());
		success &= print_assert(indent, "pipeline_IN_P", "mem2proc_data", mem2proc_data_, mem2proc_data.as_long());
		success &= print_assert(indent, "pipeline_IN_P", "mem2proc_tag", mem2proc_tag_, mem2proc_tag.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "pipeline_IN_P", "mem2proc_response", mem2proc_response_, mem2proc_response.as_int());
		print_info(indent, "pipeline_IN_P", "mem2proc_data", mem2proc_data_, mem2proc_data.as_long());
		print_info(indent, "pipeline_IN_P", "mem2proc_tag", mem2proc_tag_, mem2proc_tag.as_int());
	}
};
SN_REFLECTION(pipeline_IN_P, true, mem2proc_response, mem2proc_data, mem2proc_tag);

pipeline_IN_P pipeline_IN_P_instance;

extern "C" void read_pipeline_IN_P (svOpenArrayHandle h) {
    read(pipeline_IN_P_instance, h);
}

extern "C" int check_pipeline_IN_P () {
    pipeline_IN_P_instance.check();
}

extern "C" void step_pipeline_IN_P () {
    pipeline_IN_P_instance.step();
}

extern "C" void print_pipeline_IN_P () {
    pipeline_IN_P_instance.print();
}




struct pipeline_OUT_P {
	PackedLogicArray<2> proc2mem_command;
	uint64_t proc2mem_command_;
	PackedLogicArray<32> proc2mem_addr;
	uint64_t proc2mem_addr_;
	PackedLogicArray<64> proc2mem_data;
	uint64_t proc2mem_data_;
	Logic halt_retire;
	bool halt_retire_;
	Logic halt;
	bool halt_;
	PackedLogicArray<2> pipeline_completed_insts;
	uint64_t pipeline_completed_insts_;
	void step() {

	}
	bool check(size_t indent=0) {
		bool success = true;
		success &= print_assert(indent, "pipeline_OUT_P", "proc2mem_command", proc2mem_command_, proc2mem_command.as_int());
		success &= print_assert(indent, "pipeline_OUT_P", "proc2mem_addr", proc2mem_addr_, proc2mem_addr.as_int());
		success &= print_assert(indent, "pipeline_OUT_P", "proc2mem_data", proc2mem_data_, proc2mem_data.as_long());
		success &= print_assert(indent, "pipeline_OUT_P", "halt_retire", halt_retire_, halt_retire.as_bool());
		success &= print_assert(indent, "pipeline_OUT_P", "halt", halt_, halt.as_bool());
		success &= print_assert(indent, "pipeline_OUT_P", "pipeline_completed_insts", pipeline_completed_insts_, pipeline_completed_insts.as_int());
		return success;
	}
	void print(size_t indent=0) {
		print_info(indent, "pipeline_OUT_P", "proc2mem_command", proc2mem_command_, proc2mem_command.as_int());
		print_info(indent, "pipeline_OUT_P", "proc2mem_addr", proc2mem_addr_, proc2mem_addr.as_int());
		print_info(indent, "pipeline_OUT_P", "proc2mem_data", proc2mem_data_, proc2mem_data.as_long());
		print_info(indent, "pipeline_OUT_P", "halt_retire", halt_retire_, halt_retire.as_bool());
		print_info(indent, "pipeline_OUT_P", "halt", halt_, halt.as_bool());
		print_info(indent, "pipeline_OUT_P", "pipeline_completed_insts", pipeline_completed_insts_, pipeline_completed_insts.as_int());
	}
};
SN_REFLECTION(pipeline_OUT_P, true, proc2mem_command, proc2mem_addr, proc2mem_data, halt_retire, halt, pipeline_completed_insts);

pipeline_OUT_P pipeline_OUT_P_instance;

extern "C" void read_pipeline_OUT_P (svOpenArrayHandle h) {
    read(pipeline_OUT_P_instance, h);
}

extern "C" int check_pipeline_OUT_P () {
    pipeline_OUT_P_instance.check();
}

extern "C" void step_pipeline_OUT_P () {
    pipeline_OUT_P_instance.step();
}

extern "C" void print_pipeline_OUT_P () {
    pipeline_OUT_P_instance.print();
}






struct pipeline_TB {
    pipeline_IN_P in;
    pipeline_OUT_P out;

    pipeline_TB () {}
    
    bool check(size_t indent=0) {
        bool success = true;
        print_header(indent, "pipeline_TB", "out");
        success &= out.check(indent + 1);
        return success;
    }

    void print(size_t indent=0) {
        print_header(indent, "pipeline_TB", "in");
        in.print(indent + 1);
        print_header(indent, "pipeline_TB", "out");
        out.print(indent + 1);
    }

    void step() {}
};

pipeline_TB pipeline_TB_instance;

extern "C" void read_pipeline_TB_IN(svOpenArrayHandle h) {
    read(pipeline_TB_instance.in, h);
}

extern "C" void read_pipeline_TB_OUT(svOpenArrayHandle h) {
    read(pipeline_TB_instance.out, h);
}

extern "C" void print_pipeline_TB(svOpenArrayHandle h) {
    pipeline_TB_instance.print();
}

extern "C" void step_pipeline_TB(svOpenArrayHandle h) {
    pipeline_TB_instance.step();
}

extern "C" int check_pipeline_TB(svOpenArrayHandle h) {
    return pipeline_TB_instance.check();
}

