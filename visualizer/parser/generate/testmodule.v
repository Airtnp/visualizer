
`noportcoerce
`noinline
`timescale 1 ns / 100 ps
	localparam		RV32_instr_width_gp
						= 32;
	localparam		RV32_reg_data_width_gp
						= 32;
	localparam		RV32_reg_addr_width_gp
						= 5;
	localparam		RV32_shamt_width_gp
						= 5;
	localparam		RV32_opcode_width_gp
						= 7;
	localparam		RV32_funct3_width_gp
						= 3;
	localparam		RV32_funct7_width_gp
						= 7;
	localparam		RV32_Iimm_width_gp
						= 12;
	localparam		RV32_Simm_width_gp
						= 12;
	localparam		RV32_Bimm_width_gp
						= 12;
	localparam		RV32_Uimm_width_gp
						= 20;
	localparam		RV32_Jimm_width_gp
						= 20;

	typedef enum logic
		[3:0] {
		INST_ADDR_MISALIGN = 4'b0,
		INST_ACCESS_FAULT = 4'b1,
		ILLEGAL_INST = 4'h2,
		BREAKPOINT = 4'h3,
		LOAD_ADDR_MISALIGN = 4'h4,
		LOAD_ACCESS_FAULT = 4'h5,
		STORE_ADDR_MISALIGN = 4'h6,
		STORE_ACCESS_FAULT = 4'h7,
		ECALL_U_MODE = 4'h8,
		ECALL_S_MODE = 4'h9,
		NO_ERROR = 4'ha,
		ECALL_M_MODE = 4'hb,
		INST_PAGE_FAULT = 4'hc,
		LOAD_PAGE_FAULT = 4'hd,
		HALTED_ON_WFI = 4'he,
		STORE_PAGE_FAULT = 4'hf
	} EXCEPTION_CODE;

	typedef enum logic
		[1:0] {
		OPA_IS_RS1 = 2'b0,
		OPA_IS_NPC = 2'b1,
		OPA_IS_PC = 2'h2,
		OPA_IS_ZERO = 2'h3
	} ALU_OPA_SELECT;

	typedef enum logic
		[3:0] {
		OPB_IS_RS2 = 4'b0,
		OPB_IS_I_IMM = 4'b1,
		OPB_IS_S_IMM = 4'h2,
		OPB_IS_B_IMM = 4'h3,
		OPB_IS_U_IMM = 4'h4,
		OPB_IS_J_IMM = 4'h5
	} ALU_OPB_SELECT;

	typedef enum logic
		[1:0] {
		DEST_RD = 2'b0,
		DEST_NONE = 2'b1
	} DEST_REG_SEL;

	typedef enum logic
		[4:0] {
		ALU_ADD = 5'b0,
		ALU_SUB = 5'b1,
		ALU_SLT = 5'h02,
		ALU_SLTU = 5'h03,
		ALU_AND = 5'h04,
		ALU_OR = 5'h05,
		ALU_XOR = 5'h06,
		ALU_SLL = 5'h07,
		ALU_SRL = 5'h08,
		ALU_SRA = 5'h09,
		ALU_MUL = 5'h0a,
		ALU_MULH = 5'h0b,
		ALU_MULHSU = 5'h0c,
		ALU_MULHU = 5'h0d,
		ALU_DIV = 5'h0e,
		ALU_DIVU = 5'h0f,
		ALU_REM = 5'h10,
		ALU_REMU = 5'h11
	} ALU_FUNC;

	typedef enum logic
		[1:0] {
		BUS_NONE = 2'b0,
		BUS_LOAD = 2'b1,
		BUS_STORE = 2'h2
	} BUS_COMMAND;

	typedef struct packed {
	logic	[6:0]		funct7;
	logic	[4:0]		rs2;
	logic	[4:0]		rs1;
	logic	[2:0]		funct3;
	logic	[4:0]		rd;
	logic	[6:0]		opcode;
	} struct__0000;

	typedef struct packed {
	logic	[11:0]		imm;
	logic	[4:0]		rs1;
	logic	[2:0]		funct3;
	logic	[4:0]		rd;
	logic	[6:0]		opcode;
	} struct__0001;

	typedef struct packed {
	logic	[6:0]		off;
	logic	[4:0]		rs2;
	logic	[4:0]		rs1;
	logic	[2:0]		funct3;
	logic	[4:0]		set;
	logic	[6:0]		opcode;
	} struct__0002;

	typedef struct packed {
	logic			of;
	logic	[5:0]		s;
	logic	[4:0]		rs2;
	logic	[4:0]		rs1;
	logic	[2:0]		funct3;
	logic	[3:0]		et;
	logic			f;
	logic	[6:0]		opcode;
	} struct__0003;

	typedef struct packed {
	logic	[19:0]		imm;
	logic	[4:0]		rd;
	logic	[6:0]		opcode;
	} struct__0004;

	typedef struct packed {
	logic			of;
	logic	[9:0]		et;
	logic			s;
	logic	[7:0]		f;
	logic	[4:0]		rd;
	logic	[6:0]		opcode;
	} struct__0005;

	typedef union packed {
	logic	[31:0]		inst;
	struct__0000		r;
	struct__0001		i;
	struct__0002		s;
	struct__0003		b;
	struct__0004		u;
	struct__0005		j;
	} INST;
	typedef logic[(32 - 1):0] word_t;
	typedef logic[($clog2(32) - 1):0] vreg_tag_t;
	typedef logic[($clog2((32 + 32)) - 1):0] preg_tag_t;
	typedef logic[($clog2((3 + 1)) - 1):0] ss_size_t;
	typedef logic[($clog2(3) - 1):0] ss_tag_t;
	typedef logic[($clog2((64 + 1)) - 1):0] dtab_size_t;
	typedef logic[($clog2(64) - 1):0] dtab_tag_t;
	typedef logic[($clog2(32) - 1):0] rob_tag_t;
	typedef logic[($clog2((32 + 1)) - 1):0] rob_size_t;
	typedef logic[($clog2(32) - 1):0] rs_tag_t;
	typedef logic[($clog2((32 + 1)) - 1):0] rs_size_t;
	typedef logic[(64 - 1):0] memory_line_t;
	typedef logic[($clog2(15) - 1):0] memory_tag_t;
	typedef logic[($clog2(32) - 1):0] icache_index_t;
	typedef logic[(((32 - $clog2(32)) - $clog2(64)) - 1):0] icache_tag_t;
	typedef logic[($clog2((32 / 4)) - 1):0] sq_tag_t;
	typedef logic[($clog2(((32 / 4) + 1)) - 1):0] sq_size_t;
	typedef logic[3:0] byte_tag_t;

	typedef struct packed {
	preg_tag_t		told_idx;
	preg_tag_t		t_idx;
	vreg_tag_t		dest_reg_idx;
	logic			complete;
	logic			branch;
	word_t			branch_target;
	logic			rollback;
	logic			halt;
	word_t			PC;
	logic			branch_taken;
	logic			branch_mispredicted;
	logic			store;
	logic			tournament;
	logic			call;
	logic			ret;
	} ROB_ENTRY;

	typedef enum logic
		[2:0] {
		FU_ALU = 3'b0,
		FU_MULT = 3'b1,
		FU_LOAD = 3'h2,
		FU_STORE = 3'h3,
		FU_UNCOND_BRANCH = 3'h4,
		FU_COND_BRANCH = 3'h5,
		FU_HALT = 3'h6
	} RS_FU;

	typedef struct packed {
	logic			valid;
	logic			taken;
	word_t			branch_target;
	word_t			branch_address;
	} BRANCH_PACKET;

	typedef struct packed {
	logic			valid;
	vreg_tag_t		rs1_idx;
	vreg_tag_t		rs2_idx;
	vreg_tag_t		dest_reg_idx;
	} DECODE_MT_PACKET;

	typedef struct packed {
	word_t			NPC;
	word_t			PC;
	ALU_OPA_SELECT		opa_select;
	ALU_OPB_SELECT		opb_select;
	INST			inst;
	ALU_FUNC		alu_func;
	RS_FU			fu;
	} DECODE_RS_PACKET;

	typedef struct packed {
	logic			valid;
	preg_tag_t		retire_told_idx;
	preg_tag_t		retire_t_idx;
	vreg_tag_t		dest_reg_idx;
	} ROB_MT_PACKET;

	typedef struct packed {
	preg_tag_t		told_idx;
	preg_tag_t		t_idx;
	vreg_tag_t		dest_reg_idx;
	} MT_ROB_PACKET;

	typedef struct packed {
	logic			valid;
	logic			is_cond_branch;
	rob_tag_t		rob_num;
	word_t			branch_target;
	logic			branch_taken;
	} BRANCH_ROB_PACKET;

	typedef struct packed {
	logic			valid;
	rob_tag_t		rob_num;
	} ST_ROB_PACKET;

	typedef struct packed {
	preg_tag_t		t_idx;
	logic			ready;
	} MT_ENTRY;

	typedef struct packed {
	MT_ENTRY		t1;
	MT_ENTRY		t2;
	preg_tag_t		t_idx;
	} MT_RS_PACKET;

	typedef struct packed {
	logic			valid;
	rob_tag_t		rob_num;
	preg_tag_t		t_idx;
	} CDB_PACKET;

	typedef struct packed {
	logic			busy;
	DECODE_RS_PACKET	inst_info;
	MT_RS_PACKET		t_info;
	rob_tag_t		rob_num;
	sq_tag_t		age;
	} RS_ENTRY;

	typedef struct packed {
	logic			is_empty;
	rs_tag_t		tag;
	} RS_PRIORITY;

	typedef struct packed {
	logic			valid;
	INST			inst;
	word_t			NPC;
	word_t			PC;
	word_t			branch_target;
	logic			branch_taken;
	logic			tournament;
	logic			call;
	logic			ret;
	} IF_DECODE_PACKET;

	typedef struct packed {
	logic			valid;
	logic			halt;
	logic			store;
	BRANCH_PACKET		branch_info;
	logic			tournament;
	logic			call;
	logic			ret;
	logic			nop;
	} DECODE_ROB_PACKET;
	typedef logic[($clog2(((((5 + 4) + 3) + 3) + 1)) - 1):0] fu_tag_t;
	typedef logic[(3 - 1):0] ld_hot_t;
	typedef logic[($clog2(4) - 1):0] st_tag_t;
	typedef logic[((32 / 4) - 1):0] sq_hot_t;

	typedef struct packed {
	logic	[(5 - 1):0]	ALU_free_spaces;
	logic	[(4 - 1):0]	ST_free_spaces;
	logic	[(3 - 1):0]	LD_free_spaces;
	logic	[(3 - 1):0]	MUL_free_spaces;
	logic	[(1 - 1):0]	BR_free_spaces;
	} FU_ISSUE_PACKET;
	typedef logic[(2 - 1):0] p_width;
	typedef logic[(2 - 1):0] priority_t;
	typedef rob_tag_t priority_is_t;

	typedef struct packed {
	logic			valid;
	priority_t		complete_priority;
	rob_tag_t		rob_num;
	word_t			value;
	preg_tag_t		t_idx;
	} FU_COMPLETE_PACKET;

	typedef struct packed {
	preg_tag_t		t_idx;
	word_t			value;
	} COMPLETE_REGFILE_PACKET;

	typedef struct packed {
	priority_t		p;
	fu_tag_t		tag;
	} option_t;

	typedef struct packed {
	logic			valid;
	fu_tag_t		tag;
	} simple_option_t;

	typedef struct packed {
	priority_is_t		p;
	rs_tag_t		tag;
	} option_is_t;

	typedef struct packed {
	logic			bus_filled;
	logic	[63:0]		mem_in;
	} MEM_IF_PACKET;

	typedef struct packed {
	logic			valid;
	word_t			address;
	} IF_ICACHE_PACKET;

	typedef struct packed {
	logic			valid;
	word_t			data;
	} ICACHE_IF_PACKET;

	typedef struct packed {
	logic			valid;
	word_t			tag;
	memory_line_t		data;
	} ICACHE_ENTRY;

	typedef struct packed {
	logic			valid;
	word_t			address;
	} REQUEST_POOL_ENTRY;

	typedef enum logic
		[1:0] {
		IDLE,
		BUSY,
		COMPLETE
	} state_t;
	typedef logic[(((32 - $clog2((32 / 4))) - 3) - 1):0] dcache_tag_t;
	typedef logic[($clog2((32 / 4)) - 1):0] dcache_index_t;
	typedef logic[(3 - 1):0] dcache_offset_t;
	typedef logic[((32 - 3) - 1):0] vcache_tag_t;
	typedef logic[($clog2((64 / 4)) - 1):0] prb_tag_t;
	typedef logic[($clog2(((64 / 4) + 1)) - 1):0] prb_size_t;
	typedef logic[((32 - $clog2(4)) - 1):0] prb_cache_tag_t;
	typedef logic[($clog2(4) - 1):0] prb_cache_offset_t;

	typedef struct packed {
	dcache_tag_t		tag;
	dcache_index_t		index;
	dcache_offset_t		offset;
	} DCACHE_ADDR;

	typedef struct packed {
	prb_cache_tag_t		tag;
	prb_cache_offset_t	offset;
	} PRB_ADDR;

	typedef struct packed {
	vcache_tag_t		tag;
	dcache_offset_t		offset;
	} VCACHE_ADDR;

	typedef struct packed {
	logic	[((32 - 2) - 1):0]
				tag;
	logic	[1:0]		offset;
	} WORD_ADDR;

	typedef struct packed {
	logic	[((32 - 3) - 1):0]
				tag;
	logic	[2:0]		offset;
	} MEM_ADDR;

	typedef union packed {
	word_t			addr;
	DCACHE_ADDR		d_addr;
	PRB_ADDR		prb_addr;
	VCACHE_ADDR		v_addr;
	WORD_ADDR		w_addr;
	MEM_ADDR		m_addr;
	} ADDR_UNION;

	typedef union packed {
	memory_line_t		mem;
	word_t[1:0]		block;
	} MEM_DATA_UNION;

	typedef struct packed {
	logic			valid;
	word_t			value;
	byte_tag_t		valid_byte;
	} LSQ_LD_PACKET_OUT;

	typedef struct packed {
	logic			valid;
	ADDR_UNION		address;
	word_t			age;
	byte_tag_t		valid_byte;
	} FU_LSQ_LD_PACKET;

	typedef struct packed {
	logic			valid;
	ADDR_UNION		address;
	word_t			value;
	byte_tag_t		valid_byte;
	sq_tag_t		age;
	} FU_LSQ_ST_PACKET;

	typedef struct packed {
	logic			valid;
	logic			is_ST;
	} DECODE_LSQ_PACKET;

	typedef struct packed {
	sq_tag_t		age;
	} LSQ_RS_PACKET;

	typedef struct packed {
	logic			valid;
	ADDR_UNION		address;
	word_t			value;
	byte_tag_t		valid_byte;
	} SQ_ENTRY;

	typedef struct packed {
	logic			valid;
	ADDR_UNION		address;
	} LSQ_DCACHE_LD_PACKET;

	typedef struct packed {
	logic			valid;
	ADDR_UNION		address;
	word_t			value;
	byte_tag_t		valid_byte;
	} LSQ_PRB_ST_PACKET;

	typedef struct packed {
	logic			valid;
	word_t			value;
	} DCACHE_LSQ_LD_PACKET;

	typedef struct packed {
	logic			valid;
	} ROB_LSQ_PACKET;

	typedef struct packed {
	logic			valid;
	} DCACHE_LSQ_ST_PACKET;

	typedef struct packed {
	ADDR_UNION		addr;
	MEM_DATA_UNION		data;
	logic			valid;
	logic			dirty;
	} DCACHE_ENTRY;

	typedef struct packed {
	logic			valid;
	ADDR_UNION		address;
	memory_line_t		mask;
	memory_line_t		data;
	} DCACHE_CTRL_ST_PACKET;

	typedef enum logic
		[1:0] {
		NONE,
		DONE
	} ST_STATE;

	typedef struct packed {
	logic			valid;
	BUS_COMMAND		command;
	ADDR_UNION		addr;
	memory_line_t		data;
	} DCACHE_MEM_PACKET;

	typedef struct packed {
	logic			valid;
	BUS_COMMAND		command;
	ADDR_UNION		addr;
	memory_line_t		data;
	memory_line_t		mask;
	memory_line_t		w_data;
	} MSHR_DCACHE_MEM_PACKET;

	typedef struct packed {
	memory_tag_t		tag;
	memory_tag_t		response;
	memory_line_t		data;
	} MEM_DCACHE_PACKET;

	typedef struct packed {
	ADDR_UNION		addr;
	MEM_DATA_UNION		data;
	memory_line_t		mask;
	memory_line_t		w_data;
	logic			dirty;
	} MSHR_ENTRY;

	typedef struct packed {
	logic	[(3 - 1):0]	ld;
	logic			st;
	} DCACHE_MEMREQ_SIGNAL;

	typedef struct packed {
	logic			valid;
	prb_cache_tag_t		tag;
	byte_tag_t		valid_byte;
	word_t			value;
	logic			written;
	} PRB_ENTRY;

	typedef struct packed {
	logic			valid;
	ADDR_UNION		address;
	byte_tag_t		valid_byte;
	word_t			value;
	} PRB_DCACHE_ST_PACKET;

	typedef struct packed {
	logic			valid;
	logic			taken;
	logic			is_insert;
	word_t			branch_address;
	logic			tournament;
	logic			branch_mispredicted;
	logic			call;
	logic			ret;
	} ROB_BP_PACKET;
	typedef logic[($clog2(32) - 1):0] ras_tag_t;
	typedef logic[($clog2((32 + 1)) - 1):0] ras_size_t;
	typedef logic[($clog2(128) - 1):0] gshare_tag_t;
	typedef logic[($clog2($clog2(128)) - 1):0] gshare_tag_tag_t;

	typedef struct packed {
	logic	[1:0]		counter;
	logic	[1:0]		counter_inorder;
	logic	[1:0]		tournament;
	logic			first_time;
	} BP_ENTRY;

	typedef struct packed {
	logic	[($clog2((32 / 4)) - 1):0]
				index;
	logic	[($clog2(4) - 1):0]
				way;
	} DUMP_IDX_PACKET;
	typedef logic[($clog2(4) - 1):0] lru_t;

`portcoerce
`inline
`timescale 1 ns / 100 ps
module wand_sel(req, gnt);
	parameter		WIDTH		= 64;
	input	[(WIDTH - 1):0]	req;
	output	[(WIDTH - 1):0]	gnt;

	wand	[(WIDTH - 1):0]	gnt;
	wire	[(WIDTH - 1):0]	req_r;
	wand	[(WIDTH - 1):0]	gnt_r;
	genvar 			i;

	assign gnt_r[(WIDTH - 1)] = req_r[(WIDTH - 1)];

	for (i = 0; (i < WIDTH); i = (i + 1)) begin : reverse 

	  assign req_r[((WIDTH - 1) - i)] = req[i];
	  assign gnt[((WIDTH - 1) - i)] = gnt_r[i];
	end

	for (i = 0; (i < (WIDTH - 1)); i = (i + 1)) begin : foo 

	  assign gnt_r[(WIDTH - 1):i] = {{((WIDTH - 1) - i) {(~req_r[i])}},
		  req_r[i]};
	end
endmodule

`timescale 1 ns / 100 ps
module selector_website(req, gnt, gnt_bus);
	parameter		WIDTH		= 32;
	parameter		REQS		= 3;
	input	[(WIDTH - 1):0]	req;
	output	[(WIDTH - 1):0]	gnt;

	wor	[(WIDTH - 1):0]	gnt;
	output	[((WIDTH * REQS) - 1):0]
				gnt_bus;
	wand	[((WIDTH * REQS) - 1):0]
				gnt_bus;
	wire	[((WIDTH * REQS) - 1):0]
				tmp_reqs;
	wire	[((WIDTH * REQS) - 1):0]
				tmp_reqs_rev;
	wire	[((WIDTH * REQS) - 1):0]
				tmp_gnts;
	wire	[((WIDTH * REQS) - 1):0]
				tmp_gnts_rev;
	wire			empty;
	genvar 			j;
	genvar 			k;

	assign empty = (~(|req));

	for (j = 0; (j < REQS); j = (j + 1)) begin : foo 
	  wand_sel #(WIDTH) psel(
		.req				(tmp_reqs[(((j + 1) * WIDTH) -
		  1)-:WIDTH]), 
		.gnt				(tmp_gnts[(((j + 1) * WIDTH) -
		  1)-:WIDTH]));

	  if (j == 0) begin : gen_zero 

	    assign tmp_reqs[(WIDTH - 1):0] = req[(WIDTH - 1):0];
	    assign gnt_bus[(WIDTH - 1):0] = tmp_gnts[(WIDTH - 1):0];
	  end
	  else 
	  if (j == 1) begin : gen_nonzero 

	    assign gnt_bus[((2 * WIDTH) - 1)-:WIDTH] = (tmp_gnts_rev[((2 *
		    WIDTH) - 1)-:WIDTH] & (~tmp_gnts[(WIDTH - 1):0]));

	    for (k = 0; (k < WIDTH); k = (k + 1)) begin : Jone 

	      assign tmp_reqs[(((2 * WIDTH) - 1) - k)] = req[k];
	    end
	  end
	  else  begin : genblk1 

	    assign tmp_reqs[(((j + 1) * WIDTH) - 1)-:WIDTH] = (tmp_reqs[(((j -
		    1) * WIDTH) - 1)-:WIDTH] & (~tmp_gnts[(((j - 1) * WIDTH) -
		    1)-:WIDTH]));

	    if ((j % 2) == 0) begin : genblk1 

	      assign gnt_bus[(((j + 1) * WIDTH) - 1)-:WIDTH] = tmp_gnts[(((j +
		      1) * WIDTH) - 1)-:WIDTH];
	    end
	    else  begin : genblk1_0 

	      assign gnt_bus[(((j + 1) * WIDTH) - 1)-:WIDTH] = tmp_gnts_rev[(((j
		      + 1) * WIDTH) - 1)-:WIDTH];
	    end
	  end

	  for (k = 0; (k < WIDTH); k = (k + 1)) begin : rev 

	    assign tmp_gnts_rev[((((j + 1) * WIDTH) - 1) - k)] = tmp_gnts[((j *
		    WIDTH) + k)];
	  end

	  for (k = (j + 1); (k < REQS); k = (k + 2)) begin : gnt_mask 

	    assign gnt_bus[(((k + 1) * WIDTH) - 1)-:WIDTH] = (~gnt_bus[(((j + 1)
		    * WIDTH) - 1)-:WIDTH]);
	  end
	end

	for (k = 0; (k < REQS); k = (k + 1)) begin : final_gnt 

	  assign gnt = gnt_bus[(((k + 1) * WIDTH) - 1)-:WIDTH];
	end
endmodule

`timescale 1 ns / 100 ps
module lsq(clock, reset_global, fu_in_LD, fu_in_ST, decode_in, rollback,
	rob_in_retire, LD_result, age, dcache_out_LD, dcache_out_ST, space_out,
	head_out, LD_age_ready);
	input			clock;
	input			reset_global;
	input[(3 - 1):0]	fu_in_LD;

	$unit::FU_LSQ_LD_PACKET[(3 - 1):0]
				fu_in_LD;
	input[(4 - 1):0]	fu_in_ST;
	$unit::FU_LSQ_ST_PACKET[(4 - 1):0]
				fu_in_ST;
	input[(3 - 1):0]	decode_in;
	$unit::DECODE_LSQ_PACKET[(3 - 1):0]
				decode_in;
	input			rollback;
	input[(3 - 1):0]	rob_in_retire;
	$unit::ROB_LSQ_PACKET[(3 - 1):0]
				rob_in_retire;
	output[(3 - 1):0]	LD_result;
	$unit::LSQ_LD_PACKET_OUT[(3 - 1):0]
				LD_result;
	output[(3 - 1):0]	age;
	$unit::LSQ_RS_PACKET[(3 - 1):0]
				age;
	output[(3 - 1):0]	dcache_out_LD;
	$unit::LSQ_DCACHE_LD_PACKET[(3 - 1):0]
				dcache_out_LD;
	output[(3 - 1):0]	dcache_out_ST;
	$unit::LSQ_PRB_ST_PACKET[(3 - 1):0]
				dcache_out_ST;
	output			space_out;
	$unit::ss_size_t	space_out;
	output			head_out;
	$unit::sq_tag_t		head_out;
	output	[((32 / 4) - 1):0]
				LD_age_ready;
	logic	[((32 / 4) - 1):0]
				LD_age_ready;
	$unit::SQ_ENTRY[((32 / 4) - 1):0]
				sq;
	$unit::SQ_ENTRY[((32 / 4) - 1):0]
				next_sq;
	$unit::sq_tag_t		head;
	$unit::sq_tag_t		tail;
	$unit::sq_size_t	space_local;
	$unit::sq_size_t	size;
	$unit::sq_tag_t		head_increase;
	$unit::sq_tag_t		tail_increase;
	$unit::sq_hot_t[((32 / 4) - 1):0]
				LD_mask;
	logic			reset;
	genvar 			i;

	assign space_local = ((32 / 4) - ((size + tail_increase) - head_increase
		));
	assign space_out = ((space_local > 3) ? 3 : space_local);
	assign head_out = head;
	assign reset = (reset_global | rollback);

	always_comb  begin
	  next_sq = sq;
	  head_increase = '0;
	  tail_increase = '0;
	  age = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      if (decode_in[i].valid) begin
		age[i].age = ((tail + tail_increase) & {$clog2((32 / 4)) {1'b1}}
			);
		if (decode_in[i].is_ST) begin
		  next_sq[((tail + tail_increase) & {$clog2((32 / 4))
			  {1'b1}})].valid = 1'b0;
		  next_sq[((tail + tail_increase) & {$clog2((32 / 4))
			  {1'b1}})].address = 32'b0;
		  next_sq[((tail + tail_increase) & {$clog2((32 / 4))
			  {1'b1}})].value = '0;
		  next_sq[((tail + tail_increase) & {$clog2((32 / 4))
			  {1'b1}})].valid_byte = '0;
		end
	      end
	      tail_increase += (decode_in[i].valid & decode_in[i].is_ST);
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 4); i += 1) begin
	      if (fu_in_ST[i].valid) begin
		next_sq[fu_in_ST[i].age].valid = 1'b1;
		next_sq[fu_in_ST[i].age].address = fu_in_ST[i].address;
		next_sq[fu_in_ST[i].age].value = fu_in_ST[i].value;
		next_sq[fu_in_ST[i].age].valid_byte = fu_in_ST[i].valid_byte;
	      end
	    end
	  end
	  LD_age_ready = {(32 / 4) {1'b1}};
	  LD_mask = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < (32 / 4)); i += 1) begin
	      if (i > head) begin
		begin

		  automatic $unit::sq_size_t
				j;
		  for (j = 0; (j < (32 / 4)); j += 1) begin
		    LD_mask[i][j] = ((j < i) & (j >= head));
		  end
		end
	      end
	      else if (i < head) begin
		begin

		  automatic $unit::sq_size_t
				j;
		  for (j = 0; (j < (32 / 4)); j += 1) begin
		    LD_mask[i][j] = ((j < i) | (j >= head));
		  end
		end
	      end
	      else
		begin
		  LD_mask[i] = '0;
		end
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < (32 / 4)); i += 1) begin
	      begin

		automatic $unit::sq_size_t
				j;
		for (j = 0; (j < (32 / 4)); j += 1) begin
		  LD_age_ready[i] = (LD_age_ready[i] & ((~LD_mask[i][j]) | 
			  sq[j].valid));
		end
	      end
	    end
	  end
	  LD_result = '0;
	  dcache_out_LD = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      dcache_out_LD[i].valid = fu_in_LD[i].valid;
	      dcache_out_LD[i].address = fu_in_LD[i].address;
	      if (fu_in_LD[i].valid & LD_age_ready[fu_in_LD[i].age]) begin
		LD_result[i].valid = 1'b1;
		begin

		  automatic $unit::sq_size_t
				j;
		  for (j = 0; (j < (32 / 4)); j += 1) begin

		    $unit::sq_size_t
				idx;
		    idx = ((j + head) & {$clog2((32 / 4)) {1'b1}});
		    if (LD_mask[fu_in_LD[i].age][idx] & (
			    sq[idx].address.w_addr.tag == 
			    fu_in_LD[i].address.w_addr.tag)) begin
		      LD_result[i].valid_byte |= sq[idx].valid_byte;
		      LD_result[i].value[7:0] = (sq[idx].valid_byte[0] ? 
			      sq[idx].value[7:0] : LD_result[i].value[7:0]);
		      LD_result[i].value[15:8] = (sq[idx].valid_byte[1] ? 
			      sq[idx].value[15:8] : LD_result[i].value[15:8]);
		      LD_result[i].value[23:16] = (sq[idx].valid_byte[2] ? 
			      sq[idx].value[23:16] : LD_result[i].value[23:16]);
		      LD_result[i].value[31:24] = (sq[idx].valid_byte[3] ? 
			      sq[idx].value[31:24] : LD_result[i].value[31:24]);
		    end
		  end
		end
	      end
	    end
	  end
	  dcache_out_ST = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      dcache_out_ST[i].valid = rob_in_retire[i].valid;
	      dcache_out_ST[i].address = sq[((head + head_increase) &
		      {$clog2((32 / 4)) {1'b1}})].address;
	      dcache_out_ST[i].value = sq[((head + head_increase) & {$clog2((32
		      / 4)) {1'b1}})].value;
	      dcache_out_ST[i].valid_byte = sq[((head + head_increase) &
		      {$clog2((32 / 4)) {1'b1}})].valid_byte;
	      if (rob_in_retire[i].valid) begin
		next_sq[((head + head_increase) & {$clog2((32 / 4)) {1'b1}})] = 
			1'b0;
	      end
	      head_increase += rob_in_retire[i].valid;
	    end
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    head <= #(1) '0;
	    tail <= #(1) '0;
	    sq <= #(1) '0;
	    size <= #(1) '0;
	  end
	  else
	    begin
	      head <= #(1) ((head + head_increase) & {$clog2((32 / 4)) {1'b1}});
	      tail <= #(1) ((tail + tail_increase) & {$clog2((32 / 4)) {1'b1}});
	      sq <= #(1) next_sq;
	      size <= #(1) (((size + tail_increase) - head_increase) & 
		      {$clog2(((32 / 4) + 1)) {1'b1}});
	    end
	end

	for (i = 0; (i < 3); i = (i + 1)) begin : wtf 
	end
endmodule

`timescale 1 ns / 100 ps
module if_stage(clock, reset, branch_taken, decode_space_in, icache_in, bp_in,
	bp_out, icache_out, decode_out);
	input			clock;
	input			reset;
	input			branch_taken;

	$unit::BRANCH_PACKET	branch_taken;
	input			decode_space_in;
	$unit::ss_size_t	decode_space_in;
	input[(3 - 1):0]	icache_in;
	$unit::ICACHE_IF_PACKET[(3 - 1):0]
				icache_in;
	input[(3 - 1):0]	bp_in;
	$unit::BRANCH_PACKET[(3 - 1):0]
				bp_in;
	output[(3 - 1):0]	bp_out;
	$unit::BRANCH_PACKET[(3 - 1):0]
				bp_out;
	output[(3 - 1):0]	icache_out;
	$unit::IF_ICACHE_PACKET[(3 - 1):0]
				icache_out;
	output[(3 - 1):0]	decode_out;
	$unit::IF_DECODE_PACKET[(3 - 1):0]
				decode_out;
	$unit::word_t		PC_reg;
	$unit::word_t		PC_next;
	logic	[(3 - 1):0]	output_valid_bit;
	$unit::ss_size_t	valid_inst_count;
	genvar 			k;

	assign output_valid_bit[0] = (icache_in[0].valid & (~branch_taken.valid)
		);

	always_comb  begin
	  decode_out = '0;
	  bp_out = '0;
	  PC_next = PC_reg;
	  valid_inst_count = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      icache_out[i] = '{1'b1, (PC_reg + (4 * i))};
	      bp_out[i].branch_address = (PC_reg + (4 * i));
	      bp_out[i].branch_target = icache_in[i].data;
	      if (output_valid_bit[i] & (i < decode_space_in)) begin
		decode_out[valid_inst_count] = '{(icache_in[i].data != '0),
			icache_in[i].data, (PC_reg + (4 * (i + 1))), (PC_reg +
			(4 * i)), bp_in[i].branch_target, bp_in[i].taken,
			bp_in[i].valid, bp_in[i].branch_address[0],
			bp_in[i].branch_address[1]};
		bp_out[i].valid = (((icache_in[i].data[6:0] == 7'h63) | (
			icache_in[i].data[6:0] == 7'h67)) | (
			icache_in[i].data[6:0] == 7'h6f));
		PC_next = (PC_reg + (4 * (i + 1)));
	      end
	      valid_inst_count += (icache_in[i].data != '0);
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = (3 - 1); (i >= 0); i -= 1) begin
	      if (bp_in[i].taken & bp_out[i].valid) begin
		PC_next = bp_in[i].branch_target;
	      end
	    end
	  end
	  if (branch_taken.valid) begin
	    PC_next = branch_taken.branch_target;
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    PC_reg <= 0;
	  end
	  else
	    begin
	      PC_reg <= PC_next;
	    end
	end

	for (k = 1; (k < 3); k = (k + 1)) begin : ss_gen_loop2 

	  assign output_valid_bit[k] = ((output_valid_bit[(k - 1)] & ((~bp_in[(k
		  - 1)].taken) | (~bp_out[(k - 1)].valid))) & icache_in[k].valid
		  );
	end
endmodule

`timescale 1 ns / 100 ps
module complete(fu_results, cdb, fu_grants, reg_out);
	input[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				fu_results;

	$unit::FU_COMPLETE_PACKET[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				fu_results;
	output[(3 - 1):0]	cdb;
	$unit::CDB_PACKET[(3 - 1):0]
				cdb;
	output	[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				fu_grants;
	logic	[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				fu_grants;
	output[(3 - 1):0]	reg_out;
	$unit::COMPLETE_REGFILE_PACKET[(3 - 1):0]
				reg_out;
	$unit::option_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				fu_options;
	$unit::option_t[(3 - 1):0]
				selected;
	genvar 			i;
	ps #(.width(((((5 + 4) + 3) + 3) + 1))) selector(fu_options, selected);

	always_comb  begin

	  logic	[((((5 + 4) + 3) + 3) + 1):0]
				j;
	  $unit::ss_size_t	i;
	  cdb = '0;
	  reg_out = '0;
	  for (j = 0; (j < ((((5 + 4) + 3) + 3) + 1)); j += 1)
	    fu_grants[j] = 1'b0;
	  for (i = 0; (i < 3); i += 1) begin
	    if (selected[i].p) begin
	      fu_grants[selected[i].tag] = 1'b1;
	      cdb[i].valid = (selected[i].p ? 1'b1 : 1'b0);
	      cdb[i].rob_num = fu_results[selected[i].tag].rob_num;
	      cdb[i].t_idx = fu_results[selected[i].tag].t_idx;
	      reg_out[i].t_idx = (selected[i].p ? 
		      fu_results[selected[i].tag].t_idx : 5'b0);
	      reg_out[i].value = fu_results[selected[i].tag].value;
	    end
	  end
	end

	for (i = 0; (i < ((((5 + 4) + 3) + 3) + 1)); i = (i + 1)) begin : 
		restructure 

	  assign fu_options[i] = {fu_results[i].complete_priority,
		  i[($clog2(((((5 + 4) + 3) + 3) + 1)) - 1):0]};
	end
endmodule

`timescale 1 ns / 100 ps
module rob(clock, reset_global, decode_in, map_table_in, cdb_in, branch_in,
	st_in, lsq_size_in, map_table_out, bp_out, rob_num, space_out,
	rollback_resolve, rollback_retire, halt, head, lsq_out, completed_insts)
	;
	input			clock;
	input			reset_global;
	input[(3 - 1):0]	decode_in;

	$unit::DECODE_ROB_PACKET[(3 - 1):0]
				decode_in;
	input[(3 - 1):0]	map_table_in;
	$unit::MT_ROB_PACKET[(3 - 1):0]
				map_table_in;
	input[(3 - 1):0]	cdb_in;
	$unit::CDB_PACKET[(3 - 1):0]
				cdb_in;
	input			branch_in;
	$unit::BRANCH_ROB_PACKET
				branch_in;
	input[(4 - 1):0]	st_in;
	$unit::ST_ROB_PACKET[(4 - 1):0]
				st_in;
	input			lsq_size_in;
	$unit::ss_size_t	lsq_size_in;
	output[(3 - 1):0]	map_table_out;
	$unit::ROB_MT_PACKET[(3 - 1):0]
				map_table_out;
	output[(3 - 1):0]	bp_out;
	$unit::ROB_BP_PACKET[(3 - 1):0]
				bp_out;
	output[(3 - 1):0]	rob_num;
	$unit::rob_tag_t[(3 - 1):0]
				rob_num;
	output			space_out;
	$unit::ss_size_t	space_out;
	output			rollback_resolve;
	$unit::BRANCH_PACKET	rollback_resolve;
	output			rollback_retire;
	logic			rollback_retire;
	output			halt;
	logic			halt;
	output			head;
	$unit::rob_tag_t	head;
	output[(3 - 1):0]	lsq_out;
	$unit::ROB_LSQ_PACKET[(3 - 1):0]
				lsq_out;
	output			completed_insts;
	$unit::ss_size_t	completed_insts;
	$unit::ROB_ENTRY[(32 - 1):0]
				rob_table;
	$unit::ROB_ENTRY[(32 - 1):0]
				rob_table_next;
	$unit::rob_size_t	size;
	$unit::rob_tag_t	tail;
	$unit::ss_size_t	tail_increase;
	$unit::ss_size_t	head_increase;
	$unit::rob_size_t	space_local;
	logic	[(3 - 1):0]	retire_valid_bits;
	$unit::rob_tag_t	rollback_position;
	$unit::rob_tag_t	rollback_position_next;
	logic			rollback_exist;
	logic			rollback_exist_next;
	$unit::ss_size_t[(3 - 1):0]
				retire_store_count;
	logic			halt_state;
	logic			reset;
	genvar 			k;

	assign reset = (reset_global | rollback_retire);
	assign tail = (head + size);
	assign space_local = (32 - ((size + tail_increase) - head_increase));
	assign space_out = ((rollback_exist | halt_state) ? 0 : ((space_local > 
		3) ? 3 : space_local));
	assign retire_store_count[0] = rob_table[head].store;
	assign retire_valid_bits[0] = ((((size > 0) & rob_table[head].complete) 
		& (retire_store_count[0] < lsq_size_in)) & (~halt_state));

	always_comb  begin
	  rob_table_next = rob_table;
	  map_table_out = '0;
	  bp_out = '0;
	  bp_out[0].is_insert = rollback_exist;
	  lsq_out = '0;
	  completed_insts = '0;
	  rollback_retire = 1'b0;
	  halt = 1'b0;
	  begin

	    automatic $unit::ss_size_t
				i;
	    for (i = 0; (i < 3); i += 1) begin
	      if (decode_in[i].valid) begin
		rob_table_next[(tail + i)] = '{map_table_in[i].told_idx,
			map_table_in[i].t_idx, map_table_in[i].dest_reg_idx,
			(decode_in[i].halt | decode_in[i].nop),
			decode_in[i].branch_info.valid,
			decode_in[i].branch_info.branch_target, 1'b0,
			decode_in[i].halt,
			decode_in[i].branch_info.branch_address,
			decode_in[i].branch_info.taken, 1'b0,
			decode_in[i].store, decode_in[i].tournament,
			decode_in[i].call, decode_in[i].ret};
	      end
	      if (retire_valid_bits[i]) begin
		map_table_out[i] = '{1'b1, rob_table[(head + i)].told_idx,
			rob_table[(head + i)].t_idx, rob_table[(head +
			i)].dest_reg_idx};
		bp_out[i] = '{rob_table[(head + i)].branch, rob_table[(head +
			i)].branch_taken, rollback_exist, rob_table[(head +
			i)].PC, rob_table[(head + i)].tournament,
			rob_table[(head + i)].branch_mispredicted,
			rob_table[(head + i)].call, rob_table[(head + i)].ret};
		lsq_out[i].valid = rob_table[(head + i)].store;
		halt = rob_table[(head + i)].halt;
		rollback_retire = (rob_table[(head + i)].rollback | halt);
		completed_insts += 1'b1;
	      end
	      rob_num[i] = (tail + i);
	    end
	  end
	  begin

	    automatic $unit::ss_size_t
				i;
	    for (i = 0; (i < 3); i += 1) begin
	      if (cdb_in[i].valid) begin
		rob_table_next[cdb_in[i].rob_num].complete = 1'b1;
	      end
	    end
	  end
	  rollback_resolve = '0;
	  rollback_position_next = rollback_position;
	  rollback_exist_next = rollback_exist;
	  if (branch_in.valid) begin
	    if (branch_in.is_cond_branch) begin
	      rob_table_next[branch_in.rob_num].complete = 1'b1;
	    end
	    if (branch_in.branch_taken != 
		    rob_table[branch_in.rob_num].branch_taken) begin
	      rob_table_next[branch_in.rob_num].branch_mispredicted = 1'b1;
	      rob_table_next[branch_in.rob_num].branch_taken = 
		      branch_in.branch_taken;
	    end
	    if (((~rollback_exist) | ((rollback_position - head) > (
		    branch_in.rob_num - head))) & (
		    rob_table[branch_in.rob_num].branch_target != 
		    branch_in.branch_target)) begin
	      rob_table_next[branch_in.rob_num].rollback = 1'b1;
	      rollback_resolve = '{1'b1, 1'b0, branch_in.branch_target,
		      rob_table[branch_in.rob_num].PC};
	      rollback_position_next = branch_in.rob_num;
	      rollback_exist_next = 1'b1;
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 4); i += 1) begin
	      if (st_in[i].valid) begin
		rob_table_next[st_in[i].rob_num].complete = 1'b1;
	      end
	    end
	  end
	  tail_increase = '0;
	  head_increase = '0;
	  begin

	    automatic $unit::ss_size_t
				i;
	    for (i = 0; (i < 3); i += 1) begin
	      tail_increase += decode_in[i].valid;
	      head_increase += retire_valid_bits[i];
	    end
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    rob_table <= #(1) '0;
	    head <= #(1) '0;
	    size <= #(1) '0;
	    rollback_exist <= #(1) '0;
	    rollback_position <= #(1) '0;
	    halt_state <= halt;
	  end
	  else
	    begin
	      rob_table <= #(1) rob_table_next;
	      head <= #(1) (head + head_increase);
	      size <= #(1) ((size + tail_increase) - head_increase);
	      rollback_exist <= #(1) rollback_exist_next;
	      rollback_position <= #(1) rollback_position_next;
	      halt_state <= (halt_state | halt);
	    end
	end

	for (k = 1; (k < 3); k = (k + 1)) begin : gen_valid1 

	  assign retire_store_count[k] = (rob_table[(head + k[($clog2(3) -
		  1):0])].store + retire_store_count[(k - 1)]);
	  assign retire_valid_bits[k] = ((((((size > k) & retire_valid_bits[(k -
		  1)]) & rob_table[(head + k[($clog2(3) - 1):0])].complete) & (~
		  rob_table[((head + k[($clog2(3) - 1):0]) - 1'b1)].halt)) & (~
		  rob_table[((head + k[($clog2(3) - 1):0]) - 1'b1)].rollback)) &
		  (retire_store_count[k] < lsq_size_in));
	end
endmodule

`timescale 1 ns / 100 ps
module pipeline(clock, reset, mem2proc_response, mem2proc_data, mem2proc_tag,
	proc2mem_command, proc2mem_addr, proc2mem_data, halt_retire, halt,
	pipeline_completed_insts);
	input			clock;
	input			reset;
	input	[3:0]		mem2proc_response;
	input	[63:0]		mem2proc_data;
	input	[3:0]		mem2proc_tag;
	output	[1:0]		proc2mem_command;

	logic	[1:0]		proc2mem_command;
	output	[(32 - 1):0]	proc2mem_addr;
	logic	[(32 - 1):0]	proc2mem_addr;
	output	[63:0]		proc2mem_data;
	logic	[63:0]		proc2mem_data;
	output			halt_retire;
	logic			halt_retire;
	output			halt;
	logic			halt;
	output			pipeline_completed_insts;
	$unit::ss_size_t	pipeline_completed_insts;
	$unit::ICACHE_IF_PACKET[(3 - 1):0]
				icache_if_packet_out;
	$unit::word_t		icache_mem_out;
	logic			icache_valid_out;
	$unit::IF_ICACHE_PACKET[(3 - 1):0]
				if_icache_packet_out;
	$unit::IF_DECODE_PACKET[(3 - 1):0]
				if_decode_packet_out;
	$unit::BRANCH_PACKET[(3 - 1):0]
				if_bp_packet_out;
	$unit::BRANCH_PACKET[(3 - 1):0]
				bp_if_packet_out;
	$unit::ss_size_t	decode_space_out;
	$unit::DECODE_ROB_PACKET[(3 - 1):0]
				decode_rob_packet_out;
	$unit::DECODE_MT_PACKET[(3 - 1):0]
				decode_mt_packet_out;
	$unit::DECODE_RS_PACKET[(3 - 1):0]
				decode_rs_packet_out;
	$unit::DECODE_LSQ_PACKET[(3 - 1):0]
				decode_lsq_packet_out;
	$unit::ROB_MT_PACKET[(3 - 1):0]
				rob_mt_packet_out;
	$unit::ROB_BP_PACKET[(3 - 1):0]
				rob_bp_packet_out;
	$unit::ROB_LSQ_PACKET[(3 - 1):0]
				rob_lsq_packet_out;
	$unit::rob_tag_t[(3 - 1):0]
				rob_rs_robnum_out;
	$unit::ss_size_t	rob_space_out;
	$unit::BRANCH_PACKET	rob_rollback_resolve;
	logic			rob_rollback_retire;
	logic			rob_halt_out;
	$unit::rob_tag_t	rob_is_head_out;
	$unit::RS_ENTRY[(32 - 1):0]
				rs_is_packet_out;
	$unit::ss_size_t	rs_space_out;
	$unit::MT_ROB_PACKET[(3 - 1):0]
				mt_rob_packet_out;
	$unit::MT_RS_PACKET[(3 - 1):0]
				mt_rs_packet_out;
	$unit::RS_ENTRY[(5 - 1):0]
				is_fu_alu_packet;
	$unit::RS_ENTRY[(3 - 1):0]
				is_fu_ld_packet;
	$unit::RS_ENTRY[(4 - 1):0]
				is_fu_st_packet;
	$unit::RS_ENTRY[(3 - 1):0]
				is_fu_mul_packet;
	$unit::RS_ENTRY[(1 - 1):0]
				is_fu_br_packet;
	logic	[(32 - 1):0]	is_rs_packet;
	$unit::preg_tag_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				fu_rf_taga;
	$unit::preg_tag_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				fu_rf_tagb;
	$unit::word_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rf_fu_value_a;
	$unit::word_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rf_fu_value_b;
	$unit::FU_ISSUE_PACKET	fu_is_packet;
	$unit::FU_COMPLETE_PACKET[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				fu_complete_packet;
	$unit::FU_LSQ_LD_PACKET[(3 - 1):0]
				fu_lsq_LD_packet;
	$unit::FU_LSQ_ST_PACKET[(4 - 1):0]
				fu_lsq_ST_packet;
	$unit::LSQ_LD_PACKET_OUT[(3 - 1):0]
				ctrller_fu_LD_result_out;
	$unit::ST_ROB_PACKET[(4 - 1):0]
				st_rob_packet;
	$unit::BRANCH_ROB_PACKET
				branch_rob_packet_out;
	$unit::BRANCH_PACKET	branch_bp_packet_out;
	logic	[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				complete_fu_grant;
	$unit::COMPLETE_REGFILE_PACKET[(3 - 1):0]
				complete_rf_packet;
	$unit::CDB_PACKET[(3 - 1):0]
				cdb;
	$unit::ss_size_t	lsq_space_out;
	$unit::sq_tag_t		lsq_head_out;
	$unit::LSQ_LD_PACKET_OUT[(3 - 1):0]
				lsq_LD_result_out;
	$unit::LSQ_RS_PACKET[(3 - 1):0]
				lsq_rs_packet_out;
	$unit::LSQ_DCACHE_LD_PACKET[(3 - 1):0]
				lsq_dcache_LD_packet_out;
	$unit::LSQ_PRB_ST_PACKET[(3 - 1):0]
				lsq_prb_ST_out;
	$unit::ss_size_t	prb_space_out;
	$unit::LSQ_LD_PACKET_OUT[(3 - 1):0]
				prb_LD_result_out;
	$unit::PRB_DCACHE_ST_PACKET
				prb_dcache_ST_out;
	$unit::DCACHE_LSQ_LD_PACKET[(3 - 1):0]
				dcache_LD_result_out;
	$unit::ST_STATE		dcache_prb_ST_state;
	$unit::MEM_DCACHE_PACKET
				mem_dcache_packet_out;
	$unit::DCACHE_MEM_PACKET
				dcache_mem_packet_out;
	logic			dcache_halt_out;
	branch_predictor bp(
		.clock				(clock), 
		.reset				(reset), 
		.if_in				(if_bp_packet_out), 
		.branch_in			(branch_bp_packet_out), 
		.rollback			(rob_rollback_resolve.valid), 
		.rob_in				(rob_bp_packet_out), 
		.if_out				(bp_if_packet_out));
	if_stage if_stage0(
		.clock				(clock), 
		.reset				(reset), 
		.branch_taken			(rob_rollback_resolve), 
		.decode_space_in		(decode_space_out), 
		.icache_in			(icache_if_packet_out), 
		.bp_in				(bp_if_packet_out), 
		.bp_out				(if_bp_packet_out), 
		.icache_out			(if_icache_packet_out), 
		.decode_out			(if_decode_packet_out));
	icache_t icache(
		.clock				(clock), 
		.reset				(reset), 
		.mem2proc_response		(mem2proc_response), 
		.mem2proc_data			(mem2proc_data), 
		.mem2proc_tag			(mem2proc_tag), 
		.if_in				(if_icache_packet_out), 
		.dcache_requesting		(dcache_mem_packet_out.valid), 
		.mem_address_out		(icache_mem_out), 
		.mem_out_valid			(icache_valid_out), 
		.if_out				(icache_if_packet_out));
	$unit::IF_DECODE_PACKET[(3 - 1):0]
				if_decode_packet_in;
	$unit::ss_size_t	decode_space_in_temp;
	$unit::ss_size_t	decode_space_in;
	id_stage id_stage0(
		.clock				(clock), 
		.reset_global			(reset), 
		.branch_taken			(rob_rollback_resolve.valid), 
		.valid_num_in			(decode_space_in), 
		.if_id_packet_in		(if_decode_packet_in), 
		.d_rob_packet_out		(decode_rob_packet_out), 
		.d_rs_packet_out		(decode_rs_packet_out), 
		.d_mt_packet_out		(decode_mt_packet_out), 
		.d_lsq_packet_out		(decode_lsq_packet_out), 
		.dtab_space_out			(decode_space_out));
	$unit::DECODE_ROB_PACKET[(3 - 1):0]
				decode_rob_packet_in;
	$unit::DECODE_RS_PACKET[(3 - 1):0]
				decode_rs_packet_in;
	$unit::DECODE_MT_PACKET[(3 - 1):0]
				decode_mt_packet_in;
	$unit::DECODE_LSQ_PACKET[(3 - 1):0]
				decode_lsq_packet_in;
	rob rob_0(
		.clock				(clock), 
		.reset_global			(reset), 
		.decode_in			(decode_rob_packet_in), 
		.map_table_in			(mt_rob_packet_out), 
		.cdb_in				(cdb), 
		.branch_in			(branch_rob_packet_out), 
		.st_in				(st_rob_packet), 
		.lsq_size_in			(prb_space_out), 
		.map_table_out			(rob_mt_packet_out), 
		.bp_out				(rob_bp_packet_out), 
		.rob_num			(rob_rs_robnum_out), 
		.space_out			(rob_space_out), 
		.rollback_resolve		(rob_rollback_resolve), 
		.rollback_retire		(rob_rollback_retire), 
		.halt				(rob_halt_out), 
		.head				(rob_is_head_out), 
		.lsq_out			(rob_lsq_packet_out), 
		.completed_insts		(pipeline_completed_insts));
	rs rs_0(
		.clock				(clock), 
		.reset_global			(reset), 
		.branch_taken			(rob_rollback_retire), 
		.decode_in			(decode_rs_packet_in), 
		.mt_in				(mt_rs_packet_out), 
		.rob_in				(rob_rs_robnum_out), 
		.cdb_in				(cdb), 
		.issue_in			(is_rs_packet), 
		.lsq_in				(lsq_rs_packet_out), 
		.rs_next			(rs_is_packet_out), 
		.space_out			(rs_space_out));
	map_table mt_0(
		.clock				(clock), 
		.reset				(reset), 
		.branch_taken			(rob_rollback_retire), 
		.decode_in			(decode_mt_packet_in), 
		.rob_in				(rob_mt_packet_out), 
		.cdb_in				(cdb), 
		.rob_out			(mt_rob_packet_out), 
		.rs_out				(mt_rs_packet_out));
	$unit::RS_ENTRY[(32 - 1):0]
				rs_is_packet_in;
	$unit::rob_tag_t	rob_is_head_in;
	logic	[((32 / 4) - 1):0]
				lsq_is_packet_in;
	logic	[((32 / 4) - 1):0]
				lsq_is_packet_out;
	logic			is_reset_in;
	$unit::RS_ENTRY[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				is_fu_packet;
	issue_new is_stage_0(
		.rs				(rs_is_packet_in), 
		.fu_in				(fu_is_packet), 
		.sq_head			(lsq_head_out), 
		.LD_age_ready			(lsq_is_packet_in), 
		.ALU_packets			(is_fu_alu_packet), 
		.LD_packets			(is_fu_ld_packet), 
		.ST_packets			(is_fu_st_packet), 
		.MUL_packets			(is_fu_mul_packet), 
		.BR_packets			(is_fu_br_packet), 
		.rs_free			(is_rs_packet));
	logic			fu_reset;
	alu alu_0[(5 - 1):0](
		.clock				(clock), 
		.reset				(fu_reset), 
		.insn				(is_fu_packet[(5 - 1):0]), 
		.grant				(complete_fu_grant[(5 - 1):0]), 
		.value_a			(rf_fu_value_a[(5 - 1):0]), 
		.value_b			(rf_fu_value_b[(5 - 1):0]), 
		.issue_ready			(fu_is_packet.ALU_free_spaces), 
		.req				(fu_complete_packet[(5 - 1):0]),
		.tag_a				(fu_rf_taga[(5 - 1):0]), 
		.tag_b				(fu_rf_tagb[(5 - 1):0]));
	mult mul_0[(3 - 1):0](
		.clock				(clock), 
		.reset				(fu_reset), 
		.insn				(is_fu_packet[((((5 + 3) - 1) +
		4) + 3):((((5 + 3) - 1) + 4) + 1)]), 
		.grant				(complete_fu_grant[((((5 + 3) -
		1) + 4) + 3):((((5 + 3) - 1) + 4) + 1)]), 
		.value_a			(rf_fu_value_a[((((5 + 3) - 1) +
		4) + 3):((((5 + 3) - 1) + 4) + 1)]), 
		.value_b			(rf_fu_value_b[((((5 + 3) - 1) +
		4) + 3):((((5 + 3) - 1) + 4) + 1)]), 
		.issue_ready			(fu_is_packet.MUL_free_spaces), 
		.req				(fu_complete_packet[((((5 + 3) -
		1) + 4) + 3):((((5 + 3) - 1) + 4) + 1)]), 
		.tag_a				(fu_rf_taga[((((5 + 3) - 1) + 4)
		+ 3):((((5 + 3) - 1) + 4) + 1)]), 
		.tag_b				(fu_rf_tagb[((((5 + 3) - 1) + 4)
		+ 3):((((5 + 3) - 1) + 4) + 1)]));
	ldu ldu_0[(3 - 1):0](
		.clock				(clock), 
		.reset				(fu_reset), 
		.insn				(is_fu_packet[((5 + 3) - 1):5]),
		.grant				(complete_fu_grant[((5 + 3) -
		1):5]), 
		.load_in			(ctrller_fu_LD_result_out), 
		.value_a			(rf_fu_value_a[((5 + 3) -
		1):5]), 
		.value_b			(rf_fu_value_b[((5 + 3) -
		1):5]), 
		.issue_ready			(fu_is_packet.LD_free_spaces), 
		.req				(fu_complete_packet[((5 + 3) -
		1):5]), 
		.tag_a				(fu_rf_taga[((5 + 3) - 1):5]), 
		.tag_b				(fu_rf_tagb[((5 + 3) - 1):5]), 
		.load_out			(fu_lsq_LD_packet));
	stu stu_0[(4 - 1):0](
		.clock				(clock), 
		.reset				(fu_reset), 
		.insn				(is_fu_packet[(((5 + 3) - 1) +
		4):(((5 + 3) - 1) + 1)]), 
		.value_a			(rf_fu_value_a[(((5 + 3) - 1) +
		4):(((5 + 3) - 1) + 1)]), 
		.value_b			(rf_fu_value_b[(((5 + 3) - 1) +
		4):(((5 + 3) - 1) + 1)]), 
		.issue_ready			(fu_is_packet.ST_free_spaces), 
		.tag_a				(fu_rf_taga[(((5 + 3) - 1) +
		4):(((5 + 3) - 1) + 1)]), 
		.tag_b				(fu_rf_tagb[(((5 + 3) - 1) +
		4):(((5 + 3) - 1) + 1)]), 
		.store_out			(fu_lsq_ST_packet), 
		.rob_out			(st_rob_packet));
	bru her_majesty(.clock, 
		.reset				(fu_reset), 
		.insn				(is_fu_packet[(((((5 + 3) - 1) +
		4) + 3) + 1):(((((5 + 3) - 1) + 4) + 3) + 1)]), 
		.issue_ready			(fu_is_packet.BR_free_spaces), 
		.req				(fu_complete_packet[(((((5 + 3)
		- 1) + 4) + 3) + 1):(((((5 + 3) - 1) + 4) + 3) + 1)]), 
		.grant				(complete_fu_grant[(((((5 + 3) -
		1) + 4) + 3) + 1):(((((5 + 3) - 1) + 4) + 3) + 1)]), 
		.branch_info			(branch_rob_packet_out), 
		.bp_out				(branch_bp_packet_out), 
		.tag_a				(fu_rf_taga[(((((5 + 3) - 1) +
		4) + 3) + 1):(((((5 + 3) - 1) + 4) + 3) + 1)]), 
		.tag_b				(fu_rf_tagb[(((((5 + 3) - 1) +
		4) + 3) + 1):(((((5 + 3) - 1) + 4) + 3) + 1)]), 
		.value_a			(rf_fu_value_a[(((((5 + 3) - 1)
		+ 4) + 3) + 1):(((((5 + 3) - 1) + 4) + 3) + 1)]), 
		.value_b			(rf_fu_value_b[(((((5 + 3) - 1)
		+ 4) + 3) + 1):(((((5 + 3) - 1) + 4) + 3) + 1)]));
	complete complete_0(
		.fu_results			(fu_complete_packet), 
		.cdb				(cdb), 
		.fu_grants			(complete_fu_grant), 
		.reg_out			(complete_rf_packet));
	regfile rf_stage_0(
		.clock				(clock), 
		.reset				(reset), 
		.wr				(complete_rf_packet), 
		.rda_idx			(fu_rf_taga), 
		.rdb_idx			(fu_rf_tagb), 
		.rda_out			(rf_fu_value_a), 
		.rdb_out			(rf_fu_value_b));
	lsq lsq_0(
		.clock				(clock), 
		.reset_global			(reset), 
		.fu_in_LD			(fu_lsq_LD_packet), 
		.fu_in_ST			(fu_lsq_ST_packet), 
		.decode_in			(decode_lsq_packet_in), 
		.rob_in_retire			(rob_lsq_packet_out), 
		.rollback			(rob_rollback_retire), 
		.LD_result			(lsq_LD_result_out), 
		.age				(lsq_rs_packet_out), 
		.dcache_out_LD			(lsq_dcache_LD_packet_out), 
		.dcache_out_ST			(lsq_prb_ST_out), 
		.space_out			(lsq_space_out), 
		.head_out			(lsq_head_out), 
		.LD_age_ready			(lsq_is_packet_out));
	post_retirement prb_0(
		.clock				(clock), 
		.reset				(reset), 
		.st_in_origin			(lsq_prb_ST_out), 
		.ld_in				(fu_lsq_LD_packet), 
		.st_taken			(dcache_prb_ST_state), 
		.ld_out				(prb_LD_result_out), 
		.st_out				(prb_dcache_ST_out), 
		.space_out			(prb_space_out));
	lsq_controller lsq_controller_0(
		.fu_in_LD			(fu_lsq_LD_packet), 
		.LD_result_lsq			(lsq_LD_result_out), 
		.LD_result_prb			(prb_LD_result_out), 
		.LD_result_dcache		(dcache_LD_result_out), 
		.LD_combined_result		(ctrller_fu_LD_result_out));
	dcache dcache_0(
		.clock				(clock), 
		.reset				(reset), 
		.ld_fu_in			(lsq_dcache_LD_packet_out), 
		.st_fu_in			(prb_dcache_ST_out), 
		.mem_in				(mem_dcache_packet_out), 
		.halt_retire			(rob_halt_out), 
		.st_state			(dcache_prb_ST_state), 
		.ld_out				(dcache_LD_result_out), 
		.mem_req			(dcache_mem_packet_out), 
		.halt				(dcache_halt_out));

	assign proc2mem_addr = (dcache_mem_packet_out.valid ? 
		dcache_mem_packet_out.addr : icache_mem_out);
	assign proc2mem_command = (reset ? $unit::BUS_NONE : (
		dcache_mem_packet_out.valid ? dcache_mem_packet_out.command : (
		icache_valid_out ? $unit::BUS_LOAD : $unit::BUS_NONE)));
	assign proc2mem_data = (dcache_mem_packet_out.valid ? 
		dcache_mem_packet_out.data : '0);
	assign halt_retire = rob_halt_out;
	assign halt = dcache_halt_out;
	assign decode_space_in_temp = ((rob_space_out > rs_space_out) ? 
		rs_space_out : rob_space_out);
	assign decode_space_in = ((decode_space_in_temp > lsq_space_out) ? 
		lsq_space_out : decode_space_in_temp);
	assign is_reset_in = (reset | rob_rollback_retire);
	assign is_fu_packet = {is_fu_br_packet, is_fu_mul_packet,
		is_fu_st_packet, is_fu_ld_packet, is_fu_alu_packet};
	assign fu_reset = (reset | rob_rollback_retire);
	assign fu_complete_packet[(((5 + 3) - 1) + 4):(((5 + 3) - 1) + 1)] = '0;
	assign mem_dcache_packet_out.tag = mem2proc_tag;
	assign mem_dcache_packet_out.response = mem2proc_response;
	assign mem_dcache_packet_out.data = mem2proc_data;

	always_ff @(posedge clock) begin
	  if (reset) begin
	    if_decode_packet_in <= #(1) 0;
	  end
	  else
	    begin
	      if_decode_packet_in <= #(1) if_decode_packet_out;
	    end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    decode_rob_packet_in <= #(1) 0;
	    decode_mt_packet_in <= #(1) 0;
	    decode_lsq_packet_in <= #(1) 0;
	    begin

	      automatic int	i;
	      for (i = 0; (i < 3); i += 1) begin
		decode_rs_packet_in[i] <= #(1) '{0, 0, $unit::OPA_IS_RS1,
			$unit::OPB_IS_RS2, 32'h00000013, $unit::ALU_ADD,
			$unit::FU_ALU};
	      end
	    end
	  end
	  else
	    begin
	      decode_rob_packet_in <= #(1) decode_rob_packet_out;
	      decode_mt_packet_in <= #(1) decode_mt_packet_out;
	      decode_lsq_packet_in <= #(1) decode_lsq_packet_out;
	      decode_rs_packet_in <= #(1) decode_rs_packet_out;
	    end
	end
	always_ff @(posedge clock) begin
	  if (is_reset_in) begin
	    rob_is_head_in <= #(1) 0;
	    rs_is_packet_in <= #(1) 0;
	    lsq_is_packet_in <= #(1) 0;
	  end
	  else
	    begin
	      rob_is_head_in <= #(1) rob_is_head_out;
	      rs_is_packet_in <= #(1) rs_is_packet_out;
	      lsq_is_packet_in <= #(1) lsq_is_packet_out;
	    end
	end
endmodule

`timescale 1 ns / 100 ps
module ps_(options, selected);
	parameter		width		= 32;
	parameter		sel_num_out	= 3;
	input[(width - 1):0]	options;

	$unit::option_is_t[(width - 1):0]
				options;
	output[(sel_num_out - 1):0]
				selected;
	$unit::option_is_t[(sel_num_out - 1):0]
				selected;
	$unit::option_is_t[(sel_num_out - 1):0]
				upper;
	$unit::option_is_t[(sel_num_out - 1):0]
				lower;
	$unit::option_is_t[(sel_num_out - 1):0]
				u;
	$unit::option_is_t[(sel_num_out - 1):0]
				l;
	genvar 			j;

	if (width > 1) begin : recursive_case 
	  localparam		outputs		= ((width < sel_num_out) ? width
		  : sel_num_out);
	  ps_ #(.width((width / 2)), .sel_num_out(sel_num_out)) sub_ps[1:0](
		  options, {u, l});

	  always_comb  begin
	    upper = u;
	    lower = l;
	    begin

	      automatic int	i;
	      for (i = 0; (i < outputs); i += 1) begin
		if (upper[0].p >= lower[0].p) begin
		  selected[i] = upper[0];
		  if (sel_num_out == 1) begin
		    upper = {{(2 + $clog2(32)) {1'b0}}};
		  end
		  else begin
		    upper = {{(2 + $clog2(32)) {1'b0}}, upper[(sel_num_out -
			    1):1]};
		  end
		end
		else
		  begin
		    selected[i] = lower[0];
		    if (sel_num_out == 1) begin
		      lower = {{(2 + $clog2(32)) {1'b0}}};
		    end
		    else begin
		      lower = {{(2 + $clog2(32)) {1'b0}}, lower[(sel_num_out -
			      1):1]};
		    end
		  end
	      end
	    end
	  end

	  for (j = outputs; (j < sel_num_out); j = (j + 1)) begin : fill_null 

	    assign selected[j] = {(2 + $clog2(32)) {1'b0}};
	  end
	end
	else  begin : base_case 

	  assign selected[0] = options[0];

	  for (j = 1; (j < sel_num_out); j = (j + 1)) begin : fill_null_as_well 

	    assign selected[j] = {(2 + $clog2(32)) {1'b0}};
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module map_table(clock, reset, branch_taken, decode_in, rob_in, cdb_in, rob_out,
	rs_out);
	input			clock;
	input			reset;
	input			branch_taken;
	input[(3 - 1):0]	decode_in;

	$unit::DECODE_MT_PACKET[(3 - 1):0]
				decode_in;
	input[(3 - 1):0]	rob_in;
	$unit::ROB_MT_PACKET[(3 - 1):0]
				rob_in;
	input[(3 - 1):0]	cdb_in;
	$unit::CDB_PACKET[(3 - 1):0]
				cdb_in;
	output[(3 - 1):0]	rob_out;
	$unit::MT_ROB_PACKET[(3 - 1):0]
				rob_out;
	output[(3 - 1):0]	rs_out;
	$unit::MT_RS_PACKET[(3 - 1):0]
				rs_out;
	$unit::preg_tag_t[(32 - 1):0]
				at;
	$unit::preg_tag_t[(32 - 1):0]
				at_next;
	$unit::MT_ENTRY[(32 - 1):0]
				mt;
	$unit::MT_ENTRY[(32 - 1):0]
				mt_next;
	$unit::preg_tag_t[(32 - 1):0]
				fl;
	$unit::preg_tag_t[(32 - 1):0]
				fl_next;
	$unit::rob_tag_t	fl_head;
	$unit::rob_size_t	fl_size;
	$unit::rob_tag_t	fl_tail;
	$unit::ss_size_t	fl_head_increase;
	$unit::ss_size_t	fl_tail_increase;
	logic[(32 - 1):0][(3 - 1):0]
				t_idx_array;
	$unit::ss_size_t[3:0]	fl_idx_array;
	genvar 			inner;
	genvar 			outer;
	genvar 			j;

	assign fl_idx_array[0] = 0;
	assign fl_head_increase = fl_idx_array[3];
	assign fl_tail = (fl_head + fl_size);

	always_comb  begin
	  mt_next = mt;
	  at_next = at;
	  fl_next = fl;
	  rob_out = '0;
	  rs_out = '0;
	  fl_tail_increase = '0;
	  begin

	    automatic int	i;
	    for (i = 1; (i < 32); i += 1) begin
	      if (|t_idx_array[i]) begin
		mt_next[i].ready = 1'b1;
	      end
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      if (decode_in[i].valid) begin
		if (decode_in[i].dest_reg_idx != 5'b0) begin
		  rs_out[i] = '{mt_next[decode_in[i].rs1_idx],
			  mt_next[decode_in[i].rs2_idx], fl[(fl_head +
			  fl_idx_array[i])]};
		  rob_out[i] = '{mt[decode_in[i].dest_reg_idx].t_idx,
			  fl[(fl_head + fl_idx_array[i])],
			  decode_in[i].dest_reg_idx};
		  begin

		    automatic int
				k;
		    for (k = 0; (k < i); k += 1) begin
		      if (decode_in[i].dest_reg_idx == decode_in[k].dest_reg_idx
			      ) begin
			rob_out[i].told_idx = fl[(fl_head + fl_idx_array[k])];
		      end
		    end
		  end
		  mt_next[decode_in[i].dest_reg_idx] = '{fl[(fl_head +
			  fl_idx_array[i])], 1'b0};
		end
		else
		  begin
		    rob_out[i] = '{0, 0, 5'b0};
		    rs_out[i] = '{mt_next[decode_in[i].rs1_idx],
			    mt_next[decode_in[i].rs2_idx], 0};
		  end
	      end
	      if (rob_in[i].valid) begin
		if (rob_in[i].dest_reg_idx != 5'b0) begin
		  fl_next[(fl_tail + fl_tail_increase)] = 
			  rob_in[i].retire_told_idx;
		  fl_tail_increase += 1;
		end
		at_next[rob_in[i].dest_reg_idx] = rob_in[i].retire_t_idx;
	      end
	    end
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    begin

	      automatic int	i;
	      for (i = 0; (i < 32); i += 1) begin
		mt[i] <= #(1) '{i, 1'b1};
		at[i] <= #(1) i;
	      end
	    end
	    begin

	      automatic int	i;
	      for (i = 0; (i < 32); i += 1) begin
		fl[i] <= #(1) (i + 32);
	      end
	    end
	    fl_head <= #(1) '0;
	    fl_size <= #(1) 32;
	  end
	  else if (branch_taken) begin
	    begin

	      automatic int	i;
	      for (i = 0; (i < 32); i += 1) begin
		mt[i] <= #(1) '{at_next[i], 1'b1};
	      end
	    end
	    at <= #(1) at_next;
	    fl <= #(1) fl_next;
	    fl_head <= #(1) '0;
	    fl_size <= #(1) 32;
	  end
	  else
	    begin
	      mt <= #(1) mt_next;
	      at <= #(1) at_next;
	      fl <= #(1) fl_next;
	      fl_head <= #(1) (fl_head + fl_head_increase);
	      fl_size <= #(1) ((fl_size + fl_tail_increase) - fl_head_increase);
	    end
	end

	for (outer = 0; (outer < 32); outer = (outer + 1)) begin : outer_loop 

	  for (inner = 0; (inner < 3); inner = (inner + 1)) begin : inner_loop 

	    assign t_idx_array[outer][inner] = ((mt[outer].t_idx == 
		    cdb_in[inner].t_idx) & cdb_in[inner].valid);
	  end
	end

	for (j = 1; (j < (3 + 1)); j = (j + 1)) begin : fl_idx_array_loop 

	  assign fl_idx_array[j] = (fl_idx_array[(j - 1)] + (decode_in[(j -
		  1)].valid & (decode_in[(j - 1)].dest_reg_idx != 5'b0)));
	end
endmodule

`timescale 1 ns / 100 ps
module icache_t(clock, reset, mem2proc_response, mem2proc_data, mem2proc_tag,
	if_in, dcache_requesting, mem_address_out, mem_out_valid, if_out);
	input			clock;
	input			reset;
	input			mem2proc_response;

	$unit::memory_tag_t	mem2proc_response;
	input			mem2proc_data;
	$unit::memory_line_t	mem2proc_data;
	input			mem2proc_tag;
	$unit::memory_tag_t	mem2proc_tag;
	input[(3 - 1):0]	if_in;
	$unit::IF_ICACHE_PACKET[(3 - 1):0]
				if_in;
	input			dcache_requesting;
	logic			dcache_requesting;
	output			mem_address_out;
	$unit::word_t		mem_address_out;
	output			mem_out_valid;
	logic			mem_out_valid;
	output[(3 - 1):0]	if_out;
	$unit::ICACHE_IF_PACKET[(3 - 1):0]
				if_out;
	$unit::ICACHE_ENTRY[(32 - 1):0]
				icache;
	$unit::ICACHE_ENTRY[(32 - 1):0]
				icache_next;
	$unit::REQUEST_POOL_ENTRY[(15 - 1):0]
				request_pool;
	$unit::REQUEST_POOL_ENTRY[(15 - 1):0]
				request_pool_next;
	$unit::icache_index_t[(3 - 1):0]
				if_in_address_index;
	$unit::icache_tag_t[(3 - 1):0]
				if_in_address_tag;
	logic	[(3 - 1):0]	if_out_valid_bit;
	$unit::word_t[(4 - 1):0]
				prefetch_address_fixed;
	$unit::icache_index_t[(4 - 1):0]
				prefetch_address_index;
	$unit::icache_tag_t[(4 - 1):0]
				prefetch_address_tag;
	logic[(4 - 1):0][(15 - 1):0]
				already_requested;
	genvar 			k;
	genvar 			inner;
	genvar 			outer;
	genvar 			j;

	assign if_out_valid_bit[0] = ((if_in[0].valid & 
		icache[if_in_address_index[0]].valid) & (
		icache[if_in_address_index[0]].tag == if_in_address_tag[0]));

	always_comb  begin
	  icache_next = icache;
	  mem_out_valid = 1'b0;
	  mem_address_out = '0;
	  if (~dcache_requesting) begin
	    begin

	      automatic int	i;
	      for (i = (4 - 1); (i >= 0); i -= 1) begin
		if ((~(|already_requested[i])) & ((~
			icache[prefetch_address_index[i]].valid) | (
			icache[prefetch_address_index[i]].tag != 
			prefetch_address_tag[i]))) begin
		  mem_address_out = prefetch_address_fixed[i];
		  mem_out_valid = 1'b1;
		end
	      end
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      if_out[i] = '{if_out_valid_bit[i], ((if_in[i].address[2] == '0) ?
		      icache[if_in_address_index[i]].data[31:0] :
		      icache[if_in_address_index[i]].data[63:32])};
	    end
	  end
	  request_pool_next = request_pool;
	  if (mem_out_valid & (mem2proc_response != '0)) begin
	    request_pool_next[(mem2proc_response - 1)] = '{1'b1,
		    mem_address_out};
	  end
	  if ((mem2proc_tag != '0) & request_pool[(mem2proc_tag - 1)].valid) 
		  begin
	    icache_next[request_pool_next[(mem2proc_tag - 1)].address[7:3]] = 
		    '{1'b1, request_pool_next[(mem2proc_tag - 1)][31:8],
		    mem2proc_data};
	    request_pool_next[(mem2proc_tag - 1)] = '0;
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    icache <= '0;
	    request_pool <= '0;
	  end
	  else
	    begin
	      icache <= icache_next;
	      request_pool <= request_pool_next;
	    end
	end

	for (k = 0; (k < 3); k = (k + 1)) begin : if_fix_loop 

	  assign if_in_address_index[k] = if_in[k].address[7:3];
	  assign if_in_address_tag[k] = if_in[k].address[31:8];
	end

	for (outer = 0; (outer < 4); outer = (outer + 1)) begin : 
		request_pool_outer 

	  assign prefetch_address_fixed[outer] = ({if_in[0].address[31:3], 3'b0}
		  + (8 * outer));
	  assign prefetch_address_index[outer] = 
		  prefetch_address_fixed[outer][7:3];
	  assign prefetch_address_tag[outer] = 
		  prefetch_address_fixed[outer][31:8];

	  for (inner = 0; (inner < 15); inner = (inner + 1)) begin : 
		  request_pool_inner 

	    assign already_requested[outer][inner] = (request_pool[inner].valid 
		    & (request_pool[inner].address == 
		    prefetch_address_fixed[outer]));
	  end
	end

	for (j = 1; (j < 3); j = (j + 1)) begin : if_out_valid_loop 

	  assign if_out_valid_bit[j] = (((if_out_valid_bit[(j - 1)] & 
		  if_in[j].valid) & icache[if_in_address_index[j]].valid) & (
		  icache[if_in_address_index[j]].tag == if_in_address_tag[j]));
	end
endmodule

`timescale 1 ns / 100 ps
module ps_rs(options, selected);
	parameter		width		= 8;
	input[(width - 1):0]	options;

	$unit::RS_PRIORITY[(width - 1):0]
				options;
	output[(3 - 1):0]	selected;
	$unit::RS_PRIORITY[(3 - 1):0]
				selected;
	$unit::RS_PRIORITY[(3 - 1):0]
				upper;
	$unit::RS_PRIORITY[(3 - 1):0]
				lower;
	$unit::RS_PRIORITY[(3 - 1):0]
				u;
	$unit::RS_PRIORITY[(3 - 1):0]
				l;
	genvar 			j;

	if (width > 1) begin : recursive_case 
	  localparam		outputs		= ((width < 3) ? width : 3);
	  ps_rs #(.width((width / 2))) sub_ps[1:0](options, {u, l});

	  always_comb  begin
	    upper = u;
	    lower = l;
	    begin

	      automatic int	i;
	      for (i = 0; (i < outputs); i += 1) begin
		if (upper[0].is_empty > lower[0].is_empty) begin
		  selected[i] = upper[0];
		  upper = {{(1 + $clog2(32)) {1'b0}}, upper[(3 - 1):1]};
		end
		else
		  begin
		    selected[i] = lower[0];
		    lower = {{(1 + $clog2(32)) {1'b0}}, lower[(3 - 1):1]};
		  end
	      end
	    end
	  end

	  for (j = outputs; (j < 3); j = (j + 1)) begin : fill_null 

	    assign selected[j] = {(2 + $clog2(((((5 + 4) + 3) + 3) + 1)))
		    {1'b0}};
	  end
	end
	else  begin : base_case 

	  assign selected[0] = options[0];

	  for (j = 1; (j < 3); j = (j + 1)) begin : fill_null_as_well 

	    assign selected[j] = {(2 + $clog2(((((5 + 4) + 3) + 3) + 1)))
		    {1'b0}};
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module rs(clock, reset_global, branch_taken, decode_in, mt_in, rob_in, cdb_in,
	issue_in, lsq_in, rs_next, space_out);
	input			clock;
	input			reset_global;
	input			branch_taken;
	input[(3 - 1):0]	decode_in;

	$unit::DECODE_RS_PACKET[(3 - 1):0]
				decode_in;
	input[(3 - 1):0]	mt_in;
	$unit::MT_RS_PACKET[(3 - 1):0]
				mt_in;
	input[(3 - 1):0]	rob_in;
	$unit::rob_tag_t[(3 - 1):0]
				rob_in;
	input[(3 - 1):0]	cdb_in;
	$unit::CDB_PACKET[(3 - 1):0]
				cdb_in;
	input	[(32 - 1):0]	issue_in;
	input[(3 - 1):0]	lsq_in;
	$unit::LSQ_RS_PACKET[(3 - 1):0]
				lsq_in;
	output[(32 - 1):0]	rs_next;
	$unit::RS_ENTRY[(32 - 1):0]
				rs_next;
	output			space_out;
	$unit::ss_size_t	space_out;
	$unit::RS_ENTRY[(32 - 1):0]
				rs_table;
	$unit::RS_PRIORITY[(32 - 1):0]
				rs_priority;
	$unit::RS_PRIORITY[(3 - 1):0]
				rs_priority_out;
	logic			reset;
	$unit::rs_size_t	busy_in_count;
	$unit::rs_size_t	free_in_count;
	$unit::rs_size_t	space_count;
	$unit::rs_size_t	space_count_next;
	genvar 			j;
	ps_rs #(.width(32)) selector(rs_priority, rs_priority_out);

	assign reset = (branch_taken | reset_global);
	assign space_count_next = ((space_count - busy_in_count) + free_in_count
		);
	assign space_out = (((space_count - busy_in_count) > 3) ? 3 : (
		space_count - busy_in_count));

	always_comb  begin
	  rs_next = rs_table;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 32); i += 1) begin
	      if (issue_in[i]) begin
		rs_next[i].busy = 1'b0;
	      end
	      begin

		automatic int	k;
		for (k = 0; (k < 3); k += 1) begin
		  if ((cdb_in[k].t_idx == rs_table[i].t_info.t1.t_idx) & 
			  cdb_in[k].valid) begin
		    rs_next[i].t_info.t1.ready = 1'b1;
		  end
		  if ((cdb_in[k].t_idx == rs_table[i].t_info.t2.t_idx) & 
			  cdb_in[k].valid) begin
		    rs_next[i].t_info.t2.ready = 1'b1;
		  end
		end
	      end
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      if (decode_in[i].inst != 32'h00000013) begin
		rs_next[rs_priority_out[i].tag] = '{1'b1, decode_in[i],
			mt_in[i], rob_in[i], lsq_in[i].age};
	      end
	    end
	  end
	end
	always_comb  begin
	  busy_in_count = '0;
	  free_in_count = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      busy_in_count += (decode_in[i].inst != 32'h00000013);
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 32); i += 1) begin
	      free_in_count += issue_in[i];
	    end
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    rs_table <= '0;
	    space_count <= 32;
	  end
	  else
	    begin
	      rs_table <= rs_next;
	      space_count <= space_count_next;
	    end
	end

	for (j = (32 - 1); (j >= 0); j = (j - 1)) begin : rs_priority_loop 

	  assign rs_priority[j].is_empty = (~rs_table[j].busy);
	  assign rs_priority[j].tag = j;
	end
endmodule

`timescale 1 ns / 100 ps
module lsq_controller(fu_in_LD, LD_result_lsq, LD_result_prb, LD_result_dcache,
	LD_combined_result);
	input[(3 - 1):0]	fu_in_LD;

	$unit::FU_LSQ_LD_PACKET[(3 - 1):0]
				fu_in_LD;
	input[(3 - 1):0]	LD_result_lsq;
	input[(3 - 1):0]	LD_result_prb;
	$unit::LSQ_LD_PACKET_OUT[(3 - 1):0]
				LD_result_lsq;
	$unit::LSQ_LD_PACKET_OUT[(3 - 1):0]
				LD_result_prb;
	input[(3 - 1):0]	LD_result_dcache;
	$unit::DCACHE_LSQ_LD_PACKET[(3 - 1):0]
				LD_result_dcache;
	output[(3 - 1):0]	LD_combined_result;
	$unit::LSQ_LD_PACKET_OUT[(3 - 1):0]
				LD_combined_result;
	genvar 			i;

	always_comb  begin
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      LD_combined_result[i].value = '0;
	      LD_combined_result[i].valid_byte = '0;
	      if (LD_result_lsq[i].valid) begin
		if (LD_result_dcache[i].valid) begin
		  LD_combined_result[i].value = LD_result_dcache[i].value;
		  LD_combined_result[i].valid_byte = 4'b1111;
		end
		if (LD_result_lsq[i].valid_byte[0]) begin
		  LD_combined_result[i].value[7:0] = LD_result_lsq[i].value[7:0]
			  ;
		  LD_combined_result[i].valid_byte[0] = 1'b1;
		end
		else if (LD_result_prb[i].valid_byte[0]) begin
		  LD_combined_result[i].value[7:0] = LD_result_prb[i].value[7:0]
			  ;
		  LD_combined_result[i].valid_byte[0] = 1'b1;
		end
		if (LD_result_lsq[i].valid_byte[1]) begin
		  LD_combined_result[i].value[15:8] = 
			  LD_result_lsq[i].value[15:8];
		  LD_combined_result[i].valid_byte[1] = 1'b1;
		end
		else if (LD_result_prb[i].valid_byte[1]) begin
		  LD_combined_result[i].value[15:8] = 
			  LD_result_prb[i].value[15:8];
		  LD_combined_result[i].valid_byte[1] = 1'b1;
		end
		if (LD_result_lsq[i].valid_byte[2]) begin
		  LD_combined_result[i].value[23:16] = 
			  LD_result_lsq[i].value[23:16];
		  LD_combined_result[i].valid_byte[2] = 1'b1;
		end
		else if (LD_result_prb[i].valid_byte[2]) begin
		  LD_combined_result[i].value[23:16] = 
			  LD_result_prb[i].value[23:16];
		  LD_combined_result[i].valid_byte[2] = 1'b1;
		end
		if (LD_result_lsq[i].valid_byte[3]) begin
		  LD_combined_result[i].value[31:24] = 
			  LD_result_lsq[i].value[31:24];
		  LD_combined_result[i].valid_byte[3] = 1'b1;
		end
		else if (LD_result_prb[i].valid_byte[3]) begin
		  LD_combined_result[i].value[31:24] = 
			  LD_result_prb[i].value[31:24];
		  LD_combined_result[i].valid_byte[3] = 1'b1;
		end
	      end
	    end
	  end
	end

	for (i = 0; (i < 3); i = (i + 1)) begin : combined_result 

	  assign LD_combined_result[i].valid = (fu_in_LD[i].valid & (
		  fu_in_LD[i].valid_byte == (LD_combined_result[i].valid_byte & 
		  fu_in_LD[i].valid_byte)));
	end
endmodule

`timescale 1 ns / 100 ps
module regfile(clock, reset, wr, rda_idx, rdb_idx, rda_out, rdb_out);
	input			clock;
	input			reset;
	input[(3 - 1):0]	wr;

	$unit::COMPLETE_REGFILE_PACKET[(3 - 1):0]
				wr;
	input[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rda_idx;
	input[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rdb_idx;
	$unit::preg_tag_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rda_idx;
	$unit::preg_tag_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rdb_idx;
	output[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rda_out;
	output[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rdb_out;
	$unit::word_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rda_out;
	$unit::word_t[(((((5 + 4) + 3) + 3) + 1) - 1):0]
				rdb_out;
	$unit::word_t[((32 + 32) - 1):0]
				registers;
	$unit::word_t[((32 + 32) - 1):0]
				registers_next;
	genvar 			i;

	always_comb  begin

	  logic	[(3 - 1):0]	j;
	  registers_next = registers;
	  for (j = 0; (j < 3); j += 1) begin
	    if (wr[j].t_idx != 5'b0) begin
	      registers_next[wr[j].t_idx] = wr[j].value;
	    end
	  end
	end
	always_ff @(posedge clock) if (reset) begin
	  registers <= #(1) 0;
	end
	else
	  begin
	    registers <= #(1) {registers_next[((32 + 32) - 1):1], 32'b0};
	  end

	for (i = 0; (i < ((((5 + 4) + 3) + 3) + 1)); i = (i + 1)) begin : 
		readregfile 

	  assign rda_out[i] = registers[rda_idx[i]];
	  assign rdb_out[i] = registers[rdb_idx[i]];
	end
endmodule

`timescale 1 ns / 100 ps
module ps(options, selected);
	parameter		width		= 32;
	input[(width - 1):0]	options;

	$unit::option_t[(width - 1):0]
				options;
	output[(3 - 1):0]	selected;
	$unit::option_t[(3 - 1):0]
				selected;
	$unit::option_t[(3 - 1):0]
				upper;
	$unit::option_t[(3 - 1):0]
				lower;
	$unit::option_t[(3 - 1):0]
				u;
	$unit::option_t[(3 - 1):0]
				l;
	genvar 			j;

	if (width > 1) begin : recursive_case 
	  localparam		outputs		= ((width < 3) ? width : 3);
	  ps #(.width((width / 2))) sub_ps[1:0](options, {u, l});

	  always_comb  begin
	    upper = u;
	    lower = l;
	    begin

	      automatic int	i;
	      for (i = 0; (i < outputs); i += 1) begin
		if (upper[0].p >= lower[0].p) begin
		  selected[i] = upper[0];
		  upper = {{(2 + $clog2(((((5 + 4) + 3) + 3) + 1))) {1'b0}},
			  upper[(3 - 1):1]};
		end
		else
		  begin
		    selected[i] = lower[0];
		    lower = {{(2 + $clog2(((((5 + 4) + 3) + 3) + 1))) {1'b0}},
			    lower[(3 - 1):1]};
		  end
	      end
	    end
	  end

	  for (j = outputs; (j < 3); j = (j + 1)) begin : fill_null 

	    assign selected[j] = {(2 + $clog2(((((5 + 4) + 3) + 3) + 1)))
		    {1'b0}};
	  end
	end
	else  begin : base_case 

	  assign selected[0] = options[0];

	  for (j = 1; (j < 3); j = (j + 1)) begin : fill_null_as_well 

	    assign selected[j] = {(2 + $clog2(((((5 + 4) + 3) + 3) + 1)))
		    {1'b0}};
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module ps_top(clock, reset, options, selected);
	input			clock;
	input			reset;

	logic			clock;
	logic			reset;
	input[31:0]		options;
	$unit::option_t[31:0]	options;
	output[(3 - 1):0]	selected;
	$unit::option_t[(3 - 1):0]
				selected;
	$unit::option_t[(3 - 1):0]
				next_selected;
	ps #(.width(32)) p(.options, 
		.selected			(next_selected));

	always_ff @(posedge clock) if (reset) begin
	  selected <= #(1) selected;
	end
	else begin
	  selected <= #(1) next_selected;
	end
endmodule

`timescale 1 ns / 100 ps
module selector(options, selected, selected_size);
	parameter		input_width	= 32;
	input[(input_width - 1):0]
				options;

	$unit::simple_option_t[(input_width - 1):0]
				options;
	parameter		output_width	= 16;
	output[(output_width - 1):0]
				selected;
	$unit::simple_option_t[(output_width - 1):0]
				selected;
	output	[($clog2((output_width + 1)) - 1):0]
				selected_size;
	logic	[($clog2((output_width + 1)) - 1):0]
				selected_size;
	$unit::simple_option_t[(output_width - 1):0]
				u;
	$unit::simple_option_t[(output_width - 1):0]
				l;
	$unit::simple_option_t[(output_width - 1):0]
				upper;
	$unit::simple_option_t[(output_width - 1):0]
				lower;
	logic	[($clog2((output_width + 1)) - 1):0]
				u_size;
	logic	[($clog2((output_width + 1)) - 1):0]
				l_size;
	logic	[($clog2((output_width + 1)) - 1):0]
				i;
	genvar 			j;

	if (input_width > 1) begin : recursive_case 
	  selector #(.input_width((input_width / 2)), .output_width(
		  output_width)) sub_ps[1:0](options, {u, l}, {u_size, l_size});

	  assign selected_size = ((((l_size + u_size) - 1) < output_width) ? (
		  l_size + u_size) : output_width);

	  always_comb  begin
	    for (i = 0; (i < selected_size); i += 1) begin
	      if (i < l_size) begin
		selected[i] = l[i];
	      end
	      else begin
		selected[i] = u[(i - l_size)];
	      end
	    end
	  end
	end
	else  begin : base_case 

	  assign selected[0] = options[0];
	  assign selected_size = (options[0].valid ? 1'b1 : 1'b0);

	  for (j = 1; (j < 3); j = (j + 1)) begin : fill_null_as_well 

	    assign selected[j] = {(1 + $clog2(((((5 + 4) + 3) + 3) + 1)))
		    {1'b0}};
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module s_top(clock, reset, options, selected);
	input			clock;
	input			reset;

	logic			clock;
	logic			reset;
	input[31:0]		options;
	$unit::simple_option_t[31:0]
				options;
	output[(3 - 1):0]	selected;
	$unit::simple_option_t[(3 - 1):0]
				selected;
	$unit::simple_option_t[(3 - 1):0]
				next_selected;
	$unit::ss_size_t	selected_size;
	selector #(.input_width(32), .output_width(3)) s(.options, 
		.selected			(next_selected), .selected_size)
		;

	always_ff @(posedge clock) if (reset) begin
	  selected <= #(1) selected;
	end
	else begin
	  selected <= #(1) next_selected;
	end
endmodule

`timescale 1 ns / 100 ps
module decoder(if_packet, opa_select, opb_select, dest_reg, alu_func, fu,
	valid_inst);
	input			if_packet;

	$unit::IF_DECODE_PACKET	if_packet;
	output			opa_select;
	$unit::ALU_OPA_SELECT	opa_select;
	output			opb_select;
	$unit::ALU_OPB_SELECT	opb_select;
	output			dest_reg;
	$unit::DEST_REG_SEL	dest_reg;
	output			alu_func;
	$unit::ALU_FUNC		alu_func;
	output			fu;
	$unit::RS_FU		fu;
	output			valid_inst;
	logic			valid_inst;
	$unit::INST		inst;
	logic			valid_inst_in;
	logic			illegal;

	assign inst = if_packet.inst;
	assign valid_inst_in = if_packet.valid;
	assign valid_inst = (valid_inst_in & (~illegal));

	always_comb  begin
	  opa_select = $unit::OPA_IS_RS1;
	  opb_select = $unit::OPB_IS_RS2;
	  alu_func = $unit::ALU_ADD;
	  dest_reg = $unit::DEST_NONE;
	  fu = $unit::FU_ALU;
	  illegal = 1'b0;
	  if (valid_inst_in) begin
	    casez (inst)
	      32'h00000013: begin
		illegal = 1'b0;
	      end
	      {{20 {1'bz}}, {5 {1'bz}}, 7'b0110111}: begin
		dest_reg = $unit::DEST_RD;
		opa_select = $unit::OPA_IS_ZERO;
		opb_select = $unit::OPB_IS_U_IMM;
	      end
	      {{20 {1'bz}}, {5 {1'bz}}, 7'b0010111}: begin
		dest_reg = $unit::DEST_RD;
		opa_select = $unit::OPA_IS_PC;
		opb_select = $unit::OPB_IS_U_IMM;
	      end
	      {{20 {1'bz}}, {5 {1'bz}}, 7'b1101111}: begin
		dest_reg = $unit::DEST_RD;
		opa_select = $unit::OPA_IS_PC;
		opb_select = $unit::OPB_IS_J_IMM;
		fu = $unit::FU_UNCOND_BRANCH;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b1100111}: begin
		dest_reg = $unit::DEST_RD;
		opa_select = $unit::OPA_IS_RS1;
		opb_select = $unit::OPB_IS_I_IMM;
		fu = $unit::FU_UNCOND_BRANCH;
	      end
	      {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}},
		      7'b1100011}, {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5
		      {1'bz}}, 7'b1100011}, {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}},
		      3'b100, {5 {1'bz}}, 7'b1100011}, {{7 {1'bz}}, {5 {1'bz}},
		      {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b1100011}, {{7 {1'bz}},
		      {5 {1'bz}}, {5 {1'bz}}, 3'b110, {5 {1'bz}}, 7'b1100011}, 
		      {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b111, {5 {1'bz}},
		      7'b1100011}: begin
		opa_select = $unit::OPA_IS_PC;
		opb_select = $unit::OPB_IS_B_IMM;
		fu = $unit::FU_COND_BRANCH;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0000011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0000011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0000011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b100, {5 {1'bz}}, 7'b0000011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b0000011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		fu = $unit::FU_LOAD;
	      end
	      {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}},
		      7'b0100011}, {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5
		      {1'bz}}, 7'b0100011}, {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}},
		      3'b010, {5 {1'bz}}, 7'b0100011}: begin
		opb_select = $unit::OPB_IS_S_IMM;
		fu = $unit::FU_STORE;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SLT;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b011, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SLTU;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b111, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_AND;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b110, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_OR;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b100, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_XOR;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0010011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SLL;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b0010011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SRL;
	      end
	      {7'b0100000, {5 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}},
		      7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SRA;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
	      end
	      {7'b0100000, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0110011}
		      : begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SUB;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SLT;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b011, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SLTU;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b111, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_AND;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b110, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_OR;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b100, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_XOR;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SLL;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SRL;
	      end
	      {7'b0100000, {5 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}},
		      7'b0110011}: begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SRA;
	      end
	      {7'b1, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_MUL;
		fu = $unit::FU_MULT;
	      end
	      {7'b1, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_MULH;
		fu = $unit::FU_MULT;
	      end
	      {7'b1, {5 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_MULHSU;
		fu = $unit::FU_MULT;
	      end
	      {7'b1, {5 {1'bz}}, {5 {1'bz}}, 3'b011, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_MULHU;
		fu = $unit::FU_MULT;
	      end
	      {12'b000100000101, 13'b0, 7'b1110011}: begin
		fu = $unit::FU_HALT;
	      end
	      default:
		illegal = 1'b1;
	    endcase
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module get_rs1_idx(inst, opa, fu, rs1_idx);
	input			inst;

	$unit::INST		inst;
	input			opa;
	$unit::ALU_OPA_SELECT	opa;
	input			fu;
	$unit::RS_FU		fu;
	output			rs1_idx;
	$unit::vreg_tag_t	rs1_idx;

	always_comb  begin
	  rs1_idx = 5'b0;
	  if (opa == $unit::OPA_IS_RS1) begin
	    rs1_idx = inst.r.rs1;
	  end
	  else if (fu == $unit::FU_COND_BRANCH) begin
	    rs1_idx = inst.r.rs1;
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module get_rs2_idx(inst, opb, fu, rs2_idx);
	input			inst;

	$unit::INST		inst;
	input			opb;
	$unit::ALU_OPB_SELECT	opb;
	input			fu;
	$unit::RS_FU		fu;
	output			rs2_idx;
	$unit::vreg_tag_t	rs2_idx;

	always_comb  begin
	  rs2_idx = 5'b0;
	  if (opb == $unit::OPB_IS_RS2) begin
	    rs2_idx = inst.r.rs2;
	  end
	  else if (fu == $unit::FU_COND_BRANCH) begin
	    rs2_idx = inst.r.rs2;
	  end
	  else if (fu == $unit::FU_STORE) begin
	    rs2_idx = inst.r.rs2;
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module id_stage(clock, reset_global, branch_taken, valid_num_in, if_id_packet_in
	, d_rob_packet_out, d_rs_packet_out, d_mt_packet_out, d_lsq_packet_out,
	dtab_space_out);
	input			clock;
	input			reset_global;
	input			branch_taken;
	input			valid_num_in;

	$unit::ss_size_t	valid_num_in;
	input[(3 - 1):0]	if_id_packet_in;
	$unit::IF_DECODE_PACKET[(3 - 1):0]
				if_id_packet_in;
	output[(3 - 1):0]	d_rob_packet_out;
	$unit::DECODE_ROB_PACKET[(3 - 1):0]
				d_rob_packet_out;
	output[(3 - 1):0]	d_rs_packet_out;
	$unit::DECODE_RS_PACKET[(3 - 1):0]
				d_rs_packet_out;
	output[(3 - 1):0]	d_mt_packet_out;
	$unit::DECODE_MT_PACKET[(3 - 1):0]
				d_mt_packet_out;
	output[(3 - 1):0]	d_lsq_packet_out;
	$unit::DECODE_LSQ_PACKET[(3 - 1):0]
				d_lsq_packet_out;
	output			dtab_space_out;
	$unit::ss_size_t	dtab_space_out;
	$unit::DECODE_ROB_PACKET[(3 - 1):0]
				d_rob_p;
	$unit::DECODE_RS_PACKET[(3 - 1):0]
				d_rs_p;
	$unit::DECODE_MT_PACKET[(3 - 1):0]
				d_mt_p;
	$unit::DECODE_LSQ_PACKET[(3 - 1):0]
				d_lsq_p;
	$unit::DEST_REG_SEL[(3 - 1):0]
				dest_reg_select;
	logic			reset;
	$unit::IF_DECODE_PACKET[(64 - 1):0]
				dtab;
	$unit::IF_DECODE_PACKET[(64 - 1):0]
				next_dtab;
	$unit::dtab_tag_t	head;
	$unit::dtab_size_t	size;
	$unit::dtab_tag_t	tail;
	$unit::ss_size_t	head_inc;
	$unit::ss_size_t	tail_inc;
	$unit::dtab_tag_t	next_head;
	$unit::dtab_size_t	next_size;
	$unit::dtab_size_t	dtab_space_local;
	$unit::IF_DECODE_PACKET[(3 - 1):0]
				pass_if_id_packet;
	$unit::ss_size_t	valid_count;
	genvar 			i;

	assign reset = (reset_global | branch_taken);
	assign tail = ((head + size) & {$clog2(64) {1'b1}});
	assign next_size = (((((size + tail_inc) - head_inc) > 0) ? ((size + 
		tail_inc) - head_inc) : 0) & {$clog2((64 + 1)) {1'b1}});
	assign next_head = ((head + head_inc) & {$clog2(64) {1'b1}});
	assign dtab_space_local = ((64 - size) & {$clog2((64 + 1)) {1'b1}});
	assign dtab_space_out = (((dtab_space_local - tail_inc) > 3) ? 3 : (
		dtab_space_local - tail_inc));

	always_comb  begin
	  tail_inc = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      tail_inc += if_id_packet_in[i].valid;
	    end
	  end
	  head_inc = ((((size + tail_inc) >= valid_num_in) ? valid_num_in : (
		  size + tail_inc)) & {$clog2((3 + 1)) {1'b1}});
	  next_dtab = dtab;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      if (if_id_packet_in[i].valid) begin
		next_dtab[((tail + i) & {$clog2(64) {1'b1}})] = 
			if_id_packet_in[i];
	      end
	      pass_if_id_packet[i] = next_dtab[((head + i) & {$clog2(64)
		      {1'b1}})];
	      pass_if_id_packet[i].valid = ((i < head_inc) ? 1 : 0);
	    end
	  end
	end
	always_comb  begin
	  valid_count = '0;
	  d_rob_packet_out = '0;
	  d_rs_packet_out = '0;
	  d_mt_packet_out = '0;
	  d_lsq_packet_out = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      d_rs_packet_out[i].inst = 32'h00000013;
	      if (d_rob_p[i].valid) begin
		d_rob_packet_out[valid_count] = d_rob_p[i];
		d_rs_packet_out[valid_count] = d_rs_p[i];
		d_mt_packet_out[valid_count] = d_mt_p[i];
		d_lsq_packet_out[valid_count] = d_lsq_p[i];
		valid_count += 1'b1;
	      end
	    end
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    head <= #(1) '0;
	    size <= #(1) '0;
	    dtab <= #(1) '0;
	  end
	  else
	    begin
	      dtab <= #(1) next_dtab;
	      head <= #(1) next_head;
	      size <= #(1) next_size;
	    end
	end

	for (i = 0; (i < 3); i = (i + 1)) begin : gen_pass 
	  decoder dc(
		.if_packet			(pass_if_id_packet[i]), 
		.opa_select			(d_rs_p[i].opa_select), 
		.opb_select			(d_rs_p[i].opb_select), 
		.alu_func			(d_rs_p[i].alu_func), 
		.dest_reg			(dest_reg_select[i]), 
		.fu				(d_rs_p[i].fu), 
		.valid_inst			(d_rob_p[i].valid));
	  get_rs1_idx rs1(
		.inst				(pass_if_id_packet[i].inst), 
		.opa				(d_rs_p[i].opa_select), 
		.fu				(d_rs_p[i].fu), 
		.rs1_idx			(d_mt_p[i].rs1_idx));
	  get_rs2_idx rs2(
		.inst				(pass_if_id_packet[i].inst), 
		.opb				(d_rs_p[i].opb_select), 
		.fu				(d_rs_p[i].fu), 
		.rs2_idx			(d_mt_p[i].rs2_idx));

	  assign d_rob_p[i].branch_info.valid = ((d_rs_p[i].fu == 
		  $unit::FU_COND_BRANCH) || (d_rs_p[i].fu == 
		  $unit::FU_UNCOND_BRANCH));
	  assign d_rob_p[i].branch_info.branch_target = 
		  pass_if_id_packet[i].branch_target;
	  assign d_rob_p[i].branch_info.branch_address = pass_if_id_packet[i].PC
		  ;
	  assign d_rob_p[i].branch_info.taken = 
		  pass_if_id_packet[i].branch_taken;
	  assign d_rob_p[i].halt = (d_rs_p[i].fu == $unit::FU_HALT);
	  assign d_rob_p[i].store = (d_rs_p[i].fu == $unit::FU_STORE);
	  assign d_rob_p[i].tournament = pass_if_id_packet[i].tournament;
	  assign d_rob_p[i].call = pass_if_id_packet[i].call;
	  assign d_rob_p[i].ret = pass_if_id_packet[i].ret;
	  assign d_rob_p[i].nop = (pass_if_id_packet[i].inst == 32'h00000013);
	  assign d_rs_p[i].inst = (d_rob_p[i].valid ? pass_if_id_packet[i].inst
		  : 32'h00000013);
	  assign d_rs_p[i].NPC = pass_if_id_packet[i].NPC;
	  assign d_rs_p[i].PC = pass_if_id_packet[i].PC;
	  assign d_mt_p[i].valid = d_rob_p[i].valid;
	  assign d_mt_p[i].dest_reg_idx = ((dest_reg_select[i] == $unit::DEST_RD
		  ) ? pass_if_id_packet[i].inst.r.rd : 5'b0);
	  assign d_lsq_p[i].valid = (d_rob_p[i].valid & ((d_rs_p[i].fu == 
		  $unit::FU_STORE) | (d_rs_p[i].fu == $unit::FU_LOAD)));
	  assign d_lsq_p[i].is_ST = (d_rob_p[i].valid & (d_rs_p[i].fu == 
		  $unit::FU_STORE));
	end
endmodule

`timescale 1 ns / 100 ps
module branch_predictor(clock, reset, if_in, branch_in, rollback, rob_in, if_out
	);
	input			clock;
	input			reset;
	input[(3 - 1):0]	if_in;

	$unit::BRANCH_PACKET[(3 - 1):0]
				if_in;
	input			branch_in;
	$unit::BRANCH_PACKET	branch_in;
	input			rollback;
	logic			rollback;
	input[(3 - 1):0]	rob_in;
	$unit::ROB_BP_PACKET[(3 - 1):0]
				rob_in;
	output[(3 - 1):0]	if_out;
	$unit::BRANCH_PACKET[(3 - 1):0]
				if_out;
	$unit::BP_ENTRY[(256 - 1):0]
				bp;
	$unit::BP_ENTRY[(256 - 1):0]
				bp_next;
	$unit::BP_ENTRY[(256 - 1):0]
				bp_temp;
	$unit::word_t[(256 - 1):0]
				btb;
	$unit::word_t[(256 - 1):0]
				btb_next;
	$unit::gshare_tag_t	ghistory;
	$unit::gshare_tag_t	ghistory_temp;
	$unit::gshare_tag_t	ghistory_next;
	$unit::gshare_tag_t	ghistory_inorder;
	$unit::gshare_tag_t	ghistory_inorder_next;
	$unit::gshare_tag_tag_t	ginsert_position;
	$unit::gshare_tag_tag_t	ginsert_position_next;
	logic	[(128 - 1):0]	gcounter;
	logic	[(128 - 1):0]	gcounter_next;
	$unit::word_t[(32 - 1):0]
				ras;
	$unit::word_t[(32 - 1):0]
				ras_temp;
	$unit::word_t[(32 - 1):0]
				ras_next;
	$unit::word_t[(32 - 1):0]
				ras_inorder_next;
	$unit::word_t[(32 - 1):0]
				ras_inorder;
	$unit::ras_size_t	come_into_the_world;
	$unit::ras_size_t	come_into_the_world_next;
	$unit::ras_size_t	ras_size;
	$unit::ras_size_t	ras_size_temp;
	$unit::ras_size_t	ras_size_next;
	$unit::ras_size_t	ras_size_inorder;
	$unit::ras_size_t	ras_size_inorder_next;
	logic	[(3 - 1):0]	is_call;
	logic	[(3 - 1):0]	is_ret;

	always_comb  begin
	  bp_temp = bp;
	  btb_next = btb;
	  ginsert_position_next = ginsert_position;
	  ghistory_temp = ghistory;
	  ghistory_inorder_next = ghistory_inorder;
	  gcounter_next = gcounter;
	  ras_temp = ras;
	  ras_size_temp = ras_size;
	  ras_inorder_next = ras_inorder;
	  ras_size_inorder_next = ras_size_inorder;
	  come_into_the_world_next = come_into_the_world;
	  begin

	    automatic $unit::ss_size_t
				i;
	    for (i = 0; (i < 3); i += 1) begin
	      if ((rob_in[i].valid & rob_in[i].is_insert) & (ginsert_position > 
		      '0)) begin
		begin

		  automatic int	j;
		  for (j = 0; (j < $clog2(128)); j += 1) begin
		    if (j < (ginsert_position - 1)) begin
		      ghistory_temp[j] = ghistory[(j + 1)];
		    end
		    else if (j == (ginsert_position - 1)) begin
		      ghistory_temp[j] = rob_in[i].taken;
		    end
		  end
		end
	      end
	      if (rob_in[i].valid & rob_in[i].is_insert) begin
		if (rob_in[i].taken & (bp[((rob_in[i].branch_address >> 2) &
			{$clog2(256) {1'b1}})].counter != 2'b11)) begin
		  bp_temp[((rob_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].counter = (bp[((rob_in[i].branch_address >>
			  2) & {$clog2(256) {1'b1}})].counter + 1);
		end
		else if ((~rob_in[i].taken) & (bp[((rob_in[i].branch_address >>
			2) & {$clog2(256) {1'b1}})].counter != 2'b0)) begin
		  bp_temp[((rob_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].counter = (bp[((rob_in[i].branch_address >>
			  2) & {$clog2(256) {1'b1}})].counter - 1);
		end
	      end
	      if (rob_in[i].valid & rob_in[i].is_insert) begin
		if ((rob_in[i].call & (ras_size != 32)) & (come_into_the_world 
			!= 32)) begin
		  begin

		    automatic int
				j;
		    for (j = (32 - 1); (j >= 0); j -= 1) begin
		      if (j > come_into_the_world) begin
			ras_temp[j] = ras[(j - 1)];
		      end
		      else if (j == come_into_the_world) begin
			ras_temp[j] = (rob_in[i].branch_address + 4);
		      end
		    end
		  end
		  come_into_the_world_next = (come_into_the_world + 1);
		  ras_size_temp = (ras_size + 1);
		end
		if ((rob_in[i].ret & (ras_size != '0)) & (come_into_the_world !=
			'0)) begin
		  begin

		    automatic int
				j;
		    for (j = 0; (j < (32 - 1)); j += 1) begin
		      if (j >= (come_into_the_world - 1)) begin
			ras_temp[j] = ras[(j + 1)];
		      end
		    end
		  end
		  come_into_the_world_next = (come_into_the_world - 1);
		  ras_size_temp = (ras_size - 1);
		end
	      end
	    end
	  end
	  bp_next = bp_temp;
	  ras_next = ras_temp;
	  ras_size_next = ras_size_temp;
	  ghistory_next = ghistory_temp;
	  begin

	    automatic $unit::ss_size_t
				i;
	    for (i = 0; (i < 3); i += 1) begin
	      is_call[i] = (((if_in[i].branch_target[6:0] == 7'h67) & (
		      if_in[i].branch_target[19:15] == 5'b1)) & (
		      if_in[i].branch_target[11:7] == 5'b1));
	      is_ret[i] = ((((if_in[i].branch_target[6:0] == 7'h67) & (
		      if_in[i].branch_target[19:15] == 5'b1)) & (
		      if_in[i].branch_target[11:7] == 5'b0)) & (
		      if_in[i].branch_target[31:20] == 10'b0));
	      if (if_in[i].valid) begin
		if (bp_temp[((if_in[i].branch_address >> 2) & {$clog2(256)
			{1'b1}})].counter == 2'b10) begin
		  bp_next[((if_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].counter = 2'b11;
		end
		else if (bp_temp[((if_in[i].branch_address >> 2) & {$clog2(256)
			{1'b1}})].counter == 2'b1) begin
		  bp_next[((if_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].counter = 2'b0;
		end
		ghistory_next = {gcounter[(ghistory_temp ^
			((if_in[i].branch_address >> 2) & {$clog2(128)
			{1'b1}}))], ghistory_inorder_next[($clog2(128) - 2):0]};
		if (ginsert_position_next > '0) begin
		  ginsert_position_next -= 1;
		end
		if (is_call[i] & (ras_size_temp != 32)) begin
		  ras_next[ras_size_temp] = (if_in[i].branch_address + 4);
		  ras_size_next = (ras_size_temp + 1);
		end
		if (is_ret[i] & (ras_size_temp != '0)) begin
		  ras_size_next = (ras_size_temp - 1);
		end
	      end
	      if (rob_in[i].valid) begin
		if (rob_in[i].taken & (bp[((rob_in[i].branch_address >> 2) &
			{$clog2(256) {1'b1}})].counter_inorder != 2'b11)) begin
		  bp_next[((rob_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].counter_inorder = (
			  bp[((rob_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].counter_inorder + 1);
		end
		else if ((~rob_in[i].taken) & (bp[((rob_in[i].branch_address >>
			2) & {$clog2(256) {1'b1}})].counter_inorder != 2'b0)) 
			begin
		  bp_next[((rob_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].counter_inorder = (
			  bp[((rob_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].counter_inorder - 1);
		end
		gcounter_next[(ghistory_inorder_next ^
			((rob_in[i].branch_address >> 2) & {$clog2(128)
			{1'b1}}))] = rob_in[i].taken;
		ghistory_inorder_next = {rob_in[i].taken,
			ghistory_inorder_next[($clog2(128) - 2):0]};
		if (((rob_in[i].branch_mispredicted & (~rob_in[i].tournament)) |
			((~rob_in[i].branch_mispredicted) & rob_in[i].tournament
			)) & (bp[((rob_in[i].branch_address >> 2) & {$clog2(256)
			{1'b1}})].tournament != 2'b11)) begin
		  bp_next[((rob_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].tournament = (bp[((rob_in[i].branch_address
			  >> 2) & {$clog2(256) {1'b1}})].tournament + 1);
		end
		else if (((rob_in[i].branch_mispredicted & rob_in[i].tournament)
			| ((~rob_in[i].branch_mispredicted) & (~
			rob_in[i].tournament))) & (bp[((rob_in[i].branch_address
			>> 2) & {$clog2(256) {1'b1}})].tournament != 2'b0)) 
			begin
		  bp_next[((rob_in[i].branch_address >> 2) & {$clog2(256)
			  {1'b1}})].tournament = (bp[((rob_in[i].branch_address
			  >> 2) & {$clog2(256) {1'b1}})].tournament - 1);
		end
		if (rob_in[i].call & (ras_size_inorder != 32)) begin
		  ras_inorder_next[ras_size_inorder] = (rob_in[i].branch_address
			  + 4);
		  ras_size_inorder_next = (ras_size_inorder + 1);
		end
		if (rob_in[i].ret & (ras_size_inorder != '0)) begin
		  ras_size_inorder_next = (ras_size_inorder - 1);
		end
	      end
	      if_out[i].taken = (is_ret[i] ? 1 : (bp[((if_in[i].branch_address
		      >> 2) & {$clog2(256) {1'b1}})].tournament[1] ? 
		      bp[((if_in[i].branch_address >> 2) & {$clog2(256)
		      {1'b1}})].counter[1] : gcounter[(ghistory ^
		      ((if_in[i].branch_address >> 2) & {$clog2(128) {1'b1}}))])
		      );
	      if_out[i].valid = bp[((if_in[i].branch_address >> 2) &
		      {$clog2(256) {1'b1}})].tournament[1];
	      if_out[i].branch_address = {30'b0, is_ret[i], is_call[i]};
	      if_out[i].branch_target = ((~if_out[i].taken) ? (
		      if_in[i].branch_address + 4) : ((((ras_size_temp != '0) & 
		      is_ret[i]) & (~rob_in[0].is_insert)) ? 
		      ras_temp[(ras_size_temp - 1)] : 
		      btb_next[((if_in[i].branch_address >> 2) & {$clog2(256)
		      {1'b1}})]));
	    end
	  end
	  if (branch_in.valid) begin
	    btb_next[((branch_in.branch_address >> 2) & {$clog2(256) {1'b1}})] =
		    branch_in.branch_target;
	    if (bp[((branch_in.branch_address >> 2) & {$clog2(256)
		    {1'b1}})].first_time) begin
	      bp_next[((branch_in.branch_address >> 2) & {$clog2(256)
		      {1'b1}})].tournament = 2'b11;
	      bp_next[((branch_in.branch_address >> 2) & {$clog2(256)
		      {1'b1}})].first_time = 1'b0;
	    end
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    begin

	      automatic int	i;
	      for (i = 0; (i < 256); i += 1) begin
		bp[i] <= '{2'b1, 2'b10, 2'b10, 1'b1};
	      end
	    end
	    btb <= '0;
	    ghistory <= '0;
	    ghistory_inorder <= '0;
	    gcounter <= {$clog2(128) {1'b1}};
	    ginsert_position <= $clog2(128);
	    ras <= '0;
	    ras_size <= '0;
	    ras_inorder <= '0;
	    ras_size_inorder <= '0;
	    come_into_the_world <= '0;
	  end
	  else
	    begin
	      btb <= btb_next;
	      begin

		automatic int	i;
		for (i = 0; (i < 256); i += 1) begin
		  bp[i].counter_inorder <= bp_next[i].counter_inorder;
		  bp[i].counter <= (rollback ? bp_next[i].counter_inorder : 
			  bp_next[i].counter);
		  bp[i].tournament <= bp_next[i].tournament;
		  bp[i].first_time <= bp_next[i].first_time;
		end
	      end
	      ghistory <= (rollback ? ghistory_inorder_next : ghistory_next);
	      ghistory_inorder <= ghistory_inorder_next;
	      gcounter <= gcounter_next;
	      ginsert_position <= (rollback ? $clog2(128) : 
		      ginsert_position_next);
	      ras <= (rollback ? ras_inorder_next : ras_next);
	      ras_size <= (rollback ? ras_size_inorder_next : ras_size_next);
	      ras_inorder <= ras_inorder_next;
	      ras_size_inorder <= ras_size_inorder_next;
	      come_into_the_world <= (rollback ? ras_size_inorder_next : 
		      come_into_the_world_next);
	    end
	end
endmodule

`timescale 1 ns / 100 ps
module one_hot_decoder(gnt, rs, packet);
	input	[(32 - 1):0]	gnt;
	input[(32 - 1):0]	rs;

	$unit::RS_ENTRY[(32 - 1):0]
				rs;
	output			packet;
	$unit::RS_ENTRY		packet;

	always_comb  begin
	  case (gnt)
	    32'b0:
	      packet = 0;
	    32'b1:
	      packet = rs[0];
	    32'b00000000000000000000000000000010:
	      packet = rs[1];
	    32'b00000000000000000000000000000100:
	      packet = rs[2];
	    32'b00000000000000000000000000001000:
	      packet = rs[3];
	    32'b00000000000000000000000000010000:
	      packet = rs[4];
	    32'b00000000000000000000000000100000:
	      packet = rs[5];
	    32'b00000000000000000000000001000000:
	      packet = rs[6];
	    32'b00000000000000000000000010000000:
	      packet = rs[7];
	    32'b00000000000000000000000100000000:
	      packet = rs[8];
	    32'b00000000000000000000001000000000:
	      packet = rs[9];
	    32'b00000000000000000000010000000000:
	      packet = rs[10];
	    32'b00000000000000000000100000000000:
	      packet = rs[11];
	    32'b00000000000000000001000000000000:
	      packet = rs[12];
	    32'b00000000000000000010000000000000:
	      packet = rs[13];
	    32'b00000000000000000100000000000000:
	      packet = rs[14];
	    32'b00000000000000001000000000000000:
	      packet = rs[15];
	    32'b00000000000000010000000000000000:
	      packet = rs[16];
	    32'b00000000000000100000000000000000:
	      packet = rs[17];
	    32'b00000000000001000000000000000000:
	      packet = rs[18];
	    32'b00000000000010000000000000000000:
	      packet = rs[19];
	    32'b00000000000100000000000000000000:
	      packet = rs[20];
	    32'b00000000001000000000000000000000:
	      packet = rs[21];
	    32'b00000000010000000000000000000000:
	      packet = rs[22];
	    32'b00000000100000000000000000000000:
	      packet = rs[23];
	    32'b00000001000000000000000000000000:
	      packet = rs[24];
	    32'b00000010000000000000000000000000:
	      packet = rs[25];
	    32'b00000100000000000000000000000000:
	      packet = rs[26];
	    32'b00001000000000000000000000000000:
	      packet = rs[27];
	    32'b00010000000000000000000000000000:
	      packet = rs[28];
	    32'b00100000000000000000000000000000:
	      packet = rs[29];
	    32'b01000000000000000000000000000000:
	      packet = rs[30];
	    32'b10000000000000000000000000000000:
	      packet = rs[31];
	    default:
	      packet = 0;
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module issue_new(rs, fu_in, sq_head, LD_age_ready, ALU_packets, LD_packets,
	ST_packets, MUL_packets, BR_packets, rs_free);
	input[(32 - 1):0]	rs;

	$unit::RS_ENTRY[(32 - 1):0]
				rs;
	input			fu_in;
	$unit::FU_ISSUE_PACKET	fu_in;
	input			sq_head;
	$unit::sq_tag_t		sq_head;
	input	[((32 / 4) - 1):0]
				LD_age_ready;
	logic	[((32 / 4) - 1):0]
				LD_age_ready;
	output[(5 - 1):0]	ALU_packets;
	$unit::RS_ENTRY[(5 - 1):0]
				ALU_packets;
	output[(3 - 1):0]	LD_packets;
	$unit::RS_ENTRY[(3 - 1):0]
				LD_packets;
	output[(4 - 1):0]	ST_packets;
	$unit::RS_ENTRY[(4 - 1):0]
				ST_packets;
	output[(3 - 1):0]	MUL_packets;
	$unit::RS_ENTRY[(3 - 1):0]
				MUL_packets;
	output[(1 - 1):0]	BR_packets;
	$unit::RS_ENTRY[(1 - 1):0]
				BR_packets;
	output	[(32 - 1):0]	rs_free;
	logic	[(32 - 1):0]	rs_free;
	logic	[(32 - 1):0]	instr_ready;
	logic	[(32 - 1):0]	ALU_instr_ready;
	logic	[(32 - 1):0]	LD_instr_ready_temp;
	logic	[(32 - 1):0]	LD_instr_ready;
	logic	[(32 - 1):0]	ST_instr_ready;
	logic	[(32 - 1):0]	MUL_instr_ready;
	logic	[(32 - 1):0]	BR_instr_ready;
	logic	[(32 - 1):0]	ALU_gnt_cmbined;
	logic	[(32 - 1):0]	LD_gnt_cmbined;
	logic	[(32 - 1):0]	ST_gnt_cmbined;
	logic	[(32 - 1):0]	MUL_gnt_cmbined;
	logic	[(32 - 1):0]	BR_gnt_cmbined;
	logic[(5 - 1):0][(32 - 1):0]
				ALU_gnt_indi_temp;
	logic[(5 - 1):0][(32 - 1):0]
				ALU_gnt_indi;
	logic[(3 - 1):0][(32 - 1):0]
				LD_gnt_indi_temp;
	logic[(3 - 1):0][(32 - 1):0]
				LD_gnt_indi;
	logic[(4 - 1):0][(32 - 1):0]
				ST_gnt_indi_temp;
	logic[(4 - 1):0][(32 - 1):0]
				ST_gnt_indi;
	logic[(3 - 1):0][(32 - 1):0]
				MUL_gnt_indi_temp;
	logic[(3 - 1):0][(32 - 1):0]
				MUL_gnt_indi;
	logic[(1 - 1):0][(32 - 1):0]
				BR_gnt_indi_temp;
	logic[(1 - 1):0][(32 - 1):0]
				BR_gnt_indi;
	logic	[(32 - 1):0]	is_ALU;
	logic	[(32 - 1):0]	is_LD;
	logic	[(32 - 1):0]	is_ST;
	logic	[(32 - 1):0]	is_MUL;
	logic	[(32 - 1):0]	is_BR;
	logic	[(32 - 1):0]	is_HALT;
	$unit::RS_ENTRY[(5 - 1):0]
				ALU_packets_temp;
	$unit::RS_ENTRY[(3 - 1):0]
				LD_packets_temp;
	$unit::RS_ENTRY[(4 - 1):0]
				ST_packets_temp;
	$unit::RS_ENTRY[(3 - 1):0]
				MUL_packets_temp;
	$unit::RS_ENTRY[(1 - 1):0]
				BR_packets_temp;
	logic			oldest_LD_found;
	$unit::rs_tag_t		oldest_LD_idx;
	$unit::sq_tag_t		oldest_LD_age;
	$unit::sq_tag_t		temp_age;
	genvar 			i;
	one_hot_decoder d_ALU[(5 - 1):0](
		.gnt				(ALU_gnt_indi), 
		.rs				(rs), 
		.packet				(ALU_packets_temp));
	one_hot_decoder d_LD[(3 - 1):0](
		.gnt				(LD_gnt_indi), 
		.rs				(rs), 
		.packet				(LD_packets_temp));
	one_hot_decoder d_ST[(4 - 1):0](
		.gnt				(ST_gnt_indi), 
		.rs				(rs), 
		.packet				(ST_packets_temp));
	one_hot_decoder d_BR[(1 - 1):0](
		.gnt				(BR_gnt_indi), 
		.rs				(rs), 
		.packet				(BR_packets_temp));
	one_hot_decoder d_MUL[(3 - 1):0](
		.gnt				(MUL_gnt_indi), 
		.rs				(rs), 
		.packet				(MUL_packets_temp));
	selector_website #(.WIDTH(32), .REQS(5)) sel_ALU(
		.req				(ALU_instr_ready), 
		.gnt				(ALU_gnt_cmbined), 
		.gnt_bus			(ALU_gnt_indi_temp));
	selector_website #(.WIDTH(32), .REQS(3)) sel_MUL(
		.req				(MUL_instr_ready), 
		.gnt				(MUL_gnt_cmbined), 
		.gnt_bus			(MUL_gnt_indi_temp));
	selector_website #(.WIDTH(32), .REQS(3)) sel_LD(
		.req				(LD_instr_ready), 
		.gnt				(LD_gnt_cmbined), 
		.gnt_bus			(LD_gnt_indi_temp));
	selector_website #(.WIDTH(32), .REQS(4)) sel_ST(
		.req				(ST_instr_ready), 
		.gnt				(ST_gnt_cmbined), 
		.gnt_bus			(ST_gnt_indi_temp));
	selector_website #(.WIDTH(32), .REQS(1)) sel_BR(
		.req				(BR_instr_ready), 
		.gnt				(BR_gnt_cmbined), 
		.gnt_bus			(BR_gnt_indi_temp));

	always_comb  begin
	  rs_free = is_HALT;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 5); i += 1) begin
	      rs_free = (rs_free | ALU_gnt_indi[i]);
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      rs_free = (rs_free | LD_gnt_indi[i]);
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 4); i += 1) begin
	      rs_free = (rs_free | ST_gnt_indi[i]);
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      rs_free = (rs_free | MUL_gnt_indi[i]);
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 1); i += 1) begin
	      rs_free = (rs_free | BR_gnt_indi[i]);
	    end
	  end
	end

	for (i = 0; (i < 32); i = (i + 1)) begin : gen_ready 

	  assign is_ALU[i] = (rs[i].inst_info.fu == $unit::FU_ALU);
	  assign is_LD[i] = (rs[i].inst_info.fu == $unit::FU_LOAD);
	  assign is_ST[i] = (rs[i].inst_info.fu == $unit::FU_STORE);
	  assign is_MUL[i] = (rs[i].inst_info.fu == $unit::FU_MULT);
	  assign is_BR[i] = ((rs[i].inst_info.fu == $unit::FU_UNCOND_BRANCH) | (
		  rs[i].inst_info.fu == $unit::FU_COND_BRANCH));
	  assign is_HALT[i] = (rs[i].inst_info.fu == $unit::FU_HALT);
	  assign instr_ready[i] = ((rs[i].busy & rs[i].t_info.t1.ready) & 
		  rs[i].t_info.t2.ready);
	  assign ALU_instr_ready[i] = (instr_ready[i] & is_ALU[i]);
	  assign LD_instr_ready[i] = ((instr_ready[i] & is_LD[i]) & 
		  LD_age_ready[rs[i].age]);
	  assign ST_instr_ready[i] = (instr_ready[i] & is_ST[i]);
	  assign MUL_instr_ready[i] = (instr_ready[i] & is_MUL[i]);
	  assign BR_instr_ready[i] = (instr_ready[i] & is_BR[i]);
	end

	for (i = 0; (i < 5); i = (i + 1)) begin : gen_ALU 

	  assign ALU_packets[i] = (fu_in.ALU_free_spaces[i] ? 
		  ALU_packets_temp[i] : 0);
	  assign ALU_gnt_indi[i] = (fu_in.ALU_free_spaces[i] ? 
		  ALU_gnt_indi_temp[i] : 0);
	end

	for (i = 0; (i < 3); i = (i + 1)) begin : gen_LD 

	  assign LD_packets[i] = (fu_in.LD_free_spaces[i] ? LD_packets_temp[i] :
		  0);
	  assign LD_gnt_indi[i] = (fu_in.LD_free_spaces[i] ? LD_gnt_indi_temp[i]
		  : 0);
	end

	for (i = 0; (i < 4); i = (i + 1)) begin : gen_ST 

	  assign ST_packets[i] = (fu_in.ST_free_spaces[i] ? ST_packets_temp[i] :
		  0);
	  assign ST_gnt_indi[i] = (fu_in.ST_free_spaces[i] ? ST_gnt_indi_temp[i]
		  : 0);
	end

	for (i = 0; (i < 3); i = (i + 1)) begin : gen_MUL 

	  assign MUL_packets[i] = (fu_in.MUL_free_spaces[i] ? 
		  MUL_packets_temp[i] : 0);
	  assign MUL_gnt_indi[i] = (fu_in.MUL_free_spaces[i] ? 
		  MUL_gnt_indi_temp[i] : 0);
	end

	for (i = 0; (i < 1); i = (i + 1)) begin : gen_BR 

	  assign BR_packets[i] = (fu_in.BR_free_spaces[i] ? BR_packets_temp[i] :
		  0);
	  assign BR_gnt_indi[i] = (fu_in.BR_free_spaces[i] ? BR_gnt_indi_temp[i]
		  : 0);
	end
endmodule

`timescale 1 ns / 100 ps
module brcond(rs1, rs2, func, cond);
	input			rs1;

	$unit::word_t		rs1;
	input			rs2;
	$unit::word_t		rs2;
	input	[2:0]		func;
	output			cond;
	logic			cond;
	logic signed
		[(32 - 1):0]	signed_rs1;
	logic signed
		[(32 - 1):0]	signed_rs2;

	assign signed_rs1 = rs1;
	assign signed_rs2 = rs2;

	always_comb  begin
	  cond = 0;
	  case (func)
	    3'b0:
	      cond = (signed_rs1 == signed_rs2);
	    3'b1:
	      cond = (signed_rs1 != signed_rs2);
	    3'b100:
	      cond = (signed_rs1 < signed_rs2);
	    3'b101:
	      cond = (signed_rs1 >= signed_rs2);
	    3'b110:
	      cond = (rs1 < rs2);
	    3'b111:
	      cond = (rs1 >= rs2);
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module select_info(state, fu, NPC, branch_result, target, branch_valid,
	branch_target, branch_taken);
	input			state;

	$unit::state_t		state;
	input			fu;
	$unit::RS_FU		fu;
	input			NPC;
	$unit::word_t		NPC;
	input			branch_result;
	logic			branch_result;
	input			target;
	$unit::word_t		target;
	output			branch_valid;
	logic			branch_valid;
	output			branch_target;
	$unit::word_t		branch_target;
	output			branch_taken;
	logic			branch_taken;

	always_comb  begin
	  branch_valid = ((state == $unit::BUSY) ? 1'b1 : 1'b0);
	  if (fu == $unit::FU_COND_BRANCH) begin
	    branch_taken = branch_result;
	    branch_target = (branch_result ? target : NPC);
	  end
	  else
	    begin
	      branch_taken = 1'b1;
	      branch_target = target;
	    end
	end
endmodule

`timescale 1 ns / 100 ps
module bru(clock, reset, insn, issue_ready, req, grant, branch_info, bp_out,
	tag_a, tag_b, value_a, value_b);
	input			clock;
	input			reset;
	input			insn;

	$unit::RS_ENTRY		insn;
	output			issue_ready;
	logic			issue_ready;
	output			req;
	$unit::FU_COMPLETE_PACKET
				req;
	input			grant;
	logic			grant;
	output			branch_info;
	$unit::BRANCH_ROB_PACKET
				branch_info;
	output			bp_out;
	$unit::BRANCH_PACKET	bp_out;
	output			tag_a;
	output			tag_b;
	$unit::preg_tag_t	tag_a;
	$unit::preg_tag_t	tag_b;
	input			value_a;
	input			value_b;
	$unit::word_t		value_a;
	$unit::word_t		value_b;
	$unit::word_t		opa;
	$unit::word_t		opb;
	$unit::word_t		next_opa;
	$unit::word_t		next_opb;
	$unit::word_t		target;
	logic			target_done;
	$unit::state_t		state;
	$unit::state_t		next_state;
	$unit::RS_ENTRY		insn_reg;
	$unit::RS_ENTRY		next_insn_reg;
	logic			branch_result;
	evaluation the_right_honourable(
		.insn				(insn_reg), .tag_a, .tag_b, 
		.value_a, .value_b, 
		.operand_a			(next_opa), 
		.operand_b			(next_opb));
	brcond the_tsar(
		.rs1				(value_a), 
		.rs2				(value_b), 
		.func				
		(insn_reg.inst_info.inst.b.funct3), 
		.cond				(branch_result));
	adder target_adder(
		.opa				(next_opa), 
		.opb				(next_opb), 
		.result				(target), 
		.done				(target_done));
	select_info the_king(.state, 
		.fu				(insn_reg.inst_info.fu), 
		.NPC				(insn_reg.inst_info.NPC), 
		.branch_result, .target, 
		.branch_valid			(branch_info.valid), 
		.branch_target			(branch_info.branch_target), 
		.branch_taken			(branch_info.branch_taken));

	assign bp_out.valid = branch_info.valid;
	assign bp_out.taken = branch_info.branch_taken;
	assign bp_out.branch_target = branch_info.branch_target;
	assign bp_out.branch_address = insn_reg.inst_info.PC;

	always_comb  begin
	  if (state == $unit::IDLE) begin
	    next_insn_reg = insn;
	  end
	  else begin
	    next_insn_reg = insn_reg;
	  end
	  case (state)
	    $unit::IDLE:
	      next_state = (insn.busy ? $unit::BUSY : $unit::IDLE);
	    $unit::BUSY:
	      next_state = ((insn_reg.inst_info.fu == $unit::FU_UNCOND_BRANCH) ?
		      $unit::COMPLETE : $unit::IDLE);
	    $unit::COMPLETE:
	      next_state = (grant ? $unit::IDLE : $unit::COMPLETE);
	    default:
	      next_state = $unit::IDLE;
	  endcase
	  branch_info.rob_num = insn_reg.rob_num;
	  branch_info.is_cond_branch = ((insn_reg.inst_info.fu == 
		  $unit::FU_COND_BRANCH) ? 1'b1 : 1'b0);
	  issue_ready = (state == $unit::IDLE);
	  if (state == $unit::COMPLETE) begin
	    req.valid = 1'b1;
	    req.complete_priority = {2 {1'b1}};
	    req.t_idx = insn_reg.t_info.t_idx;
	  end
	  else
	    begin
	      req.valid = 1'b0;
	      req.complete_priority = {2 {1'b0}};
	      req.t_idx = 0;
	    end
	  req.rob_num = insn_reg.rob_num;
	  req.value = insn_reg.inst_info.NPC;
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    state <= #(1) $unit::IDLE;
	  end
	  else
	    begin
	      state <= #(1) next_state;
	    end
	  insn_reg <= #(1) next_insn_reg;
	  opa <= #(1) next_opa;
	  opb <= #(1) next_opb;
	end
endmodule

`timescale 1 ns / 100 ps
module ldu(clock, reset, insn, issue_ready, req, grant, tag_a, tag_b, value_a,
	value_b, load_out, load_in);
	input			clock;
	input			reset;
	input			insn;

	$unit::RS_ENTRY		insn;
	output			issue_ready;
	logic			issue_ready;
	output			req;
	$unit::FU_COMPLETE_PACKET
				req;
	input			grant;
	logic			grant;
	output			tag_a;
	output			tag_b;
	$unit::preg_tag_t	tag_a;
	$unit::preg_tag_t	tag_b;
	input			value_a;
	input			value_b;
	$unit::word_t		value_a;
	$unit::word_t		value_b;
	output			load_out;
	$unit::FU_LSQ_LD_PACKET	load_out;
	input			load_in;
	$unit::LSQ_LD_PACKET_OUT
				load_in;
	$unit::word_t		opa;
	$unit::word_t		next_opa;
	$unit::word_t		opb;
	$unit::word_t		next_opb;
	$unit::word_t		addr;
	$unit::word_t		value;
	$unit::word_t		aligned_value;
	$unit::word_t		extended_value;
	$unit::RS_ENTRY		saved_insn;
	$unit::RS_ENTRY		next_insn;
	$unit::byte_tag_t	valid_byte;
	$unit::byte_tag_t	lowest_valid_byte;
	logic	[1:0]		shift_width;
	$unit::state_t		state;
	$unit::state_t		next_state;
	$unit::LSQ_LD_PACKET_OUT
				load_in_reg;
	evaluation evaluator(
		.insn				(next_insn), .tag_a, .tag_b, 
		.value_a, .value_b, 
		.operand_a			(next_opa), 
		.operand_b			(next_opb));

	assign addr = (opa + opb);
	assign lowest_valid_byte = (addr[1] ? (addr[0] ? 4'h8 : 4'h4) : (addr[0]
		? 4'h2 : 4'b1));
	assign shift_width = (addr[1] ? (addr[0] ? 2'h3 : 2'h2) : (addr[0] ? 
		2'b1 : 2'b0));
	assign load_out.valid = (saved_insn.busy & (addr < 16'hffff));
	assign load_out.address = addr;
	assign load_out.age = saved_insn.age;
	assign load_out.valid_byte = valid_byte;
	assign issue_ready = (state == $unit::IDLE);

	always_ff @(posedge clock) begin
	  if (reset) begin
	    state <= #(1) $unit::IDLE;
	    saved_insn <= #(1) '0;
	    opa <= #(1) '0;
	    opb <= #(1) '0;
	    value <= #(1) '0;
	    load_in_reg <= #(1) '0;
	  end
	  else
	    begin
	      state <= #(1) next_state;
	      opa <= #(1) next_opa;
	      opb <= #(1) next_opb;
	      saved_insn <= #(1) next_insn;
	      value <= #(1) value_b;
	      if (load_in.valid && (state == $unit::BUSY)) begin
		load_in_reg <= #(1) load_in;
	      end
	      else
		begin
		  load_in_reg.valid <= #(1) 1'b0;
		end
	    end
	end
	always_comb  begin
	  aligned_value = '0;
	  if (addr < 16'hffff) begin
	    case (shift_width)
	      2'b0: begin
		aligned_value = load_in_reg.value[(32 - 1):0];
	      end
	      2'b1: begin
		aligned_value = {8'b0, load_in_reg.value[(32 - 1):8]};
	      end
	      2'h2: begin
		aligned_value = {16'b0, load_in_reg.value[(32 - 1):16]};
	      end
	      2'h3: begin
		aligned_value = {24'b0, load_in_reg.value[(32 - 1):24]};
	      end
	    endcase
	  end
	  casez (saved_insn.inst_info.inst)
	    {{12 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0000011}: begin
	      valid_byte = 4'hf;
	    end
	    {{12 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0000011}, {{12
		    {1'bz}}, {5 {1'bz}}, 3'b100, {5 {1'bz}}, 7'b0000011}: begin
	      valid_byte = lowest_valid_byte;
	    end
	    {{12 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0000011}, {{12
		    {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b0000011}: begin
	      valid_byte = (lowest_valid_byte | (lowest_valid_byte << 1));
	    end
	    default: begin
	      valid_byte = 4'b0;
	    end
	  endcase
	  casez (saved_insn.inst_info.inst)
	    {{12 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0000011}:
	      extended_value = aligned_value;
	    {{12 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b0000011}:
	      extended_value = {16'b0, aligned_value[15:0]};
	    {{12 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0000011}:
	      extended_value = {{16 {aligned_value[15]}}, aligned_value[15:0]};
	    {{12 {1'bz}}, {5 {1'bz}}, 3'b100, {5 {1'bz}}, 7'b0000011}:
	      extended_value = {24'b0, aligned_value[7:0]};
	    {{12 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0000011}:
	      extended_value = {{24 {aligned_value[7]}}, aligned_value[7:0]};
	    default:
	      extended_value = 32'b0;
	  endcase
	  case (state)
	    $unit::IDLE:
	      next_state = (insn.busy ? ((addr > 16'hffff) ? $unit::IDLE : 
		      $unit::BUSY) : $unit::IDLE);
	    $unit::BUSY:
	      next_state = (load_in.valid ? $unit::COMPLETE : $unit::BUSY);
	    $unit::COMPLETE:
	      next_state = (grant ? $unit::IDLE : $unit::COMPLETE);
	    default:
	      next_state = $unit::IDLE;
	  endcase
	  next_insn = saved_insn;
	  if (state == $unit::IDLE) begin
	    next_insn = insn;
	  end
	  req = '0;
	  if ((state == $unit::COMPLETE) | (addr > 16'hffff)) begin
	    req.valid = 1'b1;
	    req.complete_priority = {2 {1'b1}};
	    req.rob_num = saved_insn.rob_num;
	    req.value = extended_value;
	    req.t_idx = saved_insn.t_info.t_idx;
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module shifter(opa, opb, func, result, done);
	input			opa;

	$unit::word_t		opa;
	input			opb;
	$unit::word_t		opb;
	input			func;
	$unit::ALU_FUNC		func;
	output			result;
	$unit::word_t		result;
	output			done;
	wire signed
		[(32 - 1):0]	signed_opa;

	assign done = 1'b1;
	assign signed_opa = opa;

	always_comb  begin
	  case (func)
	    $unit::ALU_SRL:
	      result = (opa >> opb[4:0]);
	    $unit::ALU_SLL:
	      result = (opa << opb[4:0]);
	    $unit::ALU_SRA:
	      result = (signed_opa >>> opb[4:0]);
	    default:
	      result = 32'hfacebeec;
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module stu(clock, reset, insn, issue_ready, rob_out, tag_a, tag_b, value_a,
	value_b, store_out);
	input			clock;
	input			reset;
	input			insn;

	$unit::RS_ENTRY		insn;
	output			issue_ready;
	logic			issue_ready;
	output			rob_out;
	$unit::ST_ROB_PACKET	rob_out;
	output			tag_a;
	output			tag_b;
	$unit::preg_tag_t	tag_a;
	$unit::preg_tag_t	tag_b;
	input			value_a;
	input			value_b;
	$unit::word_t		value_a;
	$unit::word_t		value_b;
	output			store_out;
	$unit::FU_LSQ_ST_PACKET	store_out;
	$unit::word_t		opa;
	$unit::word_t		next_opa;
	$unit::word_t		opb;
	$unit::word_t		next_opb;
	$unit::word_t		addr;
	$unit::word_t		value;
	$unit::RS_ENTRY		saved_insn;
	$unit::byte_tag_t	valid_byte;
	$unit::byte_tag_t	lowest_valid_byte;
	logic	[1:0]		shift_width;
	evaluation evaluator(.insn, .tag_a, .tag_b, .value_a, .value_b, 
		.operand_a			(next_opa), 
		.operand_b			(next_opb));

	assign addr = (opa + opb);
	assign lowest_valid_byte = (addr[1] ? (addr[0] ? 4'h8 : 4'h4) : (addr[0]
		? 4'h2 : 4'b1));
	assign shift_width = (addr[1] ? (addr[0] ? 2'h3 : 2'h2) : (addr[0] ? 
		2'b1 : 2'b0));
	assign store_out.valid = saved_insn.busy;
	assign store_out.address = addr;
	assign store_out.valid_byte = valid_byte;
	assign store_out.age = saved_insn.age;
	assign rob_out.valid = store_out.valid;
	assign rob_out.rob_num = saved_insn.rob_num;
	assign issue_ready = 1'b1;

	always_ff @(posedge clock) begin
	  if (reset) begin
	    saved_insn <= #(1) '0;
	    opa <= #(1) '0;
	    opb <= #(1) '0;
	    value <= #(1) '0;
	  end
	  else
	    begin
	      opa <= #(1) next_opa;
	      opb <= #(1) next_opb;
	      saved_insn <= #(1) insn;
	      value <= #(1) value_b;
	    end
	end
	always_comb  begin
	  store_out.value = '0;
	  case (shift_width)
	    2'h3: begin
	      store_out.value = {value[7:0], {24 {1'b0}}};
	    end
	    2'h2: begin
	      store_out.value = {value[15:0], {16 {1'b0}}};
	    end
	    2'b1: begin
	      store_out.value = {value[23:0], {8 {1'b0}}};
	    end
	    2'b0: begin
	      store_out.value = value;
	    end
	  endcase
	  casez (saved_insn.inst_info.inst)
	    {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0100011}
		    :
	      valid_byte = 4'hf;
	    {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0100011}:
	      valid_byte = lowest_valid_byte;
	    {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0100011}:
	      valid_byte = (lowest_valid_byte | (lowest_valid_byte << 1));
	    default:
	      valid_byte = 4'b0;
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module evaluation(insn, tag_a, tag_b, value_a, value_b, operand_a, operand_b);
	input			insn;

	$unit::RS_ENTRY		insn;
	output			tag_a;
	output			tag_b;
	$unit::preg_tag_t	tag_a;
	$unit::preg_tag_t	tag_b;
	input			value_a;
	input			value_b;
	$unit::word_t		value_a;
	$unit::word_t		value_b;
	output			operand_a;
	output			operand_b;
	$unit::word_t		operand_a;
	$unit::word_t		operand_b;

	assign tag_a = insn.t_info.t1.t_idx;
	assign tag_b = insn.t_info.t2.t_idx;

	always_comb  begin
	  operand_a = 32'hdeadfbac;
	  case (insn.inst_info.opa_select)
	    $unit::OPA_IS_RS1:
	      operand_a = value_a;
	    $unit::OPA_IS_NPC:
	      operand_a = insn.inst_info.NPC;
	    $unit::OPA_IS_PC:
	      operand_a = insn.inst_info.PC;
	    $unit::OPA_IS_ZERO:
	      operand_a = 0;
	  endcase
	end
	always_comb  begin
	  operand_b = 32'hfacefeed;
	  case (insn.inst_info.opb_select)
	    $unit::OPB_IS_RS2:
	      operand_b = value_b;
	    $unit::OPB_IS_I_IMM:
	      operand_b = {{21 {insn.inst_info.inst[31]}},
		      insn.inst_info.inst[30:20]};
	    $unit::OPB_IS_S_IMM:
	      operand_b = {{21 {insn.inst_info.inst[31]}},
		      insn.inst_info.inst[30:25], insn.inst_info.inst[11:7]};
	    $unit::OPB_IS_B_IMM:
	      operand_b = {{20 {insn.inst_info.inst[31]}},
		      insn.inst_info.inst[7], insn.inst_info.inst[30:25],
		      insn.inst_info.inst[11:8], {1'b0}};
	    $unit::OPB_IS_U_IMM:
	      operand_b = {insn.inst_info.inst[31:12], {12 {1'b0}}};
	    $unit::OPB_IS_J_IMM:
	      operand_b = {{12 {insn.inst_info.inst[31]}},
		      insn.inst_info.inst[19:12], insn.inst_info.inst[20],
		      insn.inst_info.inst[30:21], {1'b0}};
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module comparator(opa, opb, func, result, done);
	input	[(32 - 1):0]	opa;
	input	[(32 - 1):0]	opb;
	input			func;

	$unit::ALU_FUNC		func;
	output	[(32 - 1):0]	result;
	logic	[(32 - 1):0]	result;
	output			done;
	wire signed
		[(32 - 1):0]	signed_opa;
	wire signed
		[(32 - 1):0]	signed_opb;

	assign done = 1'b1;
	assign signed_opa = opa;
	assign signed_opb = opb;

	always_comb  begin
	  case (func)
	    $unit::ALU_SLT:
	      result = (signed_opa < signed_opb);
	    $unit::ALU_SLTU:
	      result = (opa < opb);
	    default:
	      result = 32'hfacebeec;
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module alu(clock, reset, insn, issue_ready, req, grant, tag_a, tag_b, value_a,
	value_b);
	input			clock;
	input			reset;
	input			insn;

	$unit::RS_ENTRY		insn;
	output			issue_ready;
	logic			issue_ready;
	output			req;
	$unit::FU_COMPLETE_PACKET
				req;
	input			grant;
	logic			grant;
	output			tag_a;
	output			tag_b;
	$unit::preg_tag_t	tag_a;
	$unit::preg_tag_t	tag_b;
	input			value_a;
	input			value_b;
	$unit::word_t		value_a;
	$unit::word_t		value_b;
	$unit::ALU_FUNC		func;
	$unit::ALU_FUNC		next_func;
	$unit::word_t		opa;
	$unit::word_t		opb;
	$unit::word_t		adder_opb;
	$unit::word_t		next_opa;
	$unit::word_t		next_opb;
	$unit::state_t		state;
	$unit::state_t		next_state;
	logic			running;
	logic			done;
	logic			adder_done;
	logic			comp_done;
	logic			shifter_done;
	$unit::word_t		result;
	$unit::word_t		next_result;
	$unit::word_t		adder_result;
	$unit::word_t		comp_result;
	$unit::word_t		shifter_result;
	$unit::preg_tag_t	dest_tag;
	$unit::preg_tag_t	next_dest_tag;
	$unit::rob_tag_t	rob_num;
	$unit::rob_tag_t	next_rob_num;
	$unit::RS_ENTRY		insn_reg;
	$unit::RS_ENTRY		next_insn_reg;
	evaluation evaluator(
		.insn				(next_insn_reg), .tag_a, .tag_b,
		.value_a, .value_b, 
		.operand_a			(next_opa), 
		.operand_b			(next_opb));
	comparator comp(
		.opa				(opa), 
		.opb				(opb), 
		.func				(func), 
		.result				(comp_result), 
		.done				(comp_done));
	adder add(
		.opa				(opa), 
		.opb				(adder_opb), 
		.result				(adder_result), 
		.done				(adder_done));
	shifter shift(
		.opa				(opa), 
		.opb				(opb), 
		.func				(func), 
		.result				(shifter_result), 
		.done				(shifter_done));

	always_comb  begin
	  if (state == $unit::IDLE) begin
	    next_insn_reg = insn;
	  end
	  else begin
	    next_insn_reg = insn_reg;
	  end
	  next_func = func;
	  next_dest_tag = dest_tag;
	  next_rob_num = rob_num;
	  if (state == $unit::IDLE) begin
	    next_func = insn.inst_info.alu_func;
	    next_dest_tag = insn.t_info.t_idx;
	    next_rob_num = insn.rob_num;
	  end
	  adder_opb = ((func == $unit::ALU_SUB) ? ((~opb) + 1'b1) : opb);
	  next_result = 32'hfacebeec;
	  done = 1'b1;
	  case (func)
	    $unit::ALU_ADD, $unit::ALU_SUB:
	      {next_result, done} = {adder_result, adder_done};
	    $unit::ALU_SLT, $unit::ALU_SLTU:
	      {next_result, done} = {comp_result, comp_done};
	    $unit::ALU_AND:
	      next_result = (opa & opb);
	    $unit::ALU_OR:
	      next_result = (opa | opb);
	    $unit::ALU_XOR:
	      next_result = (opa ^ opb);
	    $unit::ALU_SRL, $unit::ALU_SLL, $unit::ALU_SRA:
	      {next_result, done} = {shifter_result, shifter_done};
	    default:
	      {next_result, done} = {32'b0, 1'b0};
	  endcase
	  case (state)
	    $unit::IDLE:
	      next_state = (insn.busy ? $unit::BUSY : $unit::IDLE);
	    $unit::BUSY:
	      next_state = (done ? $unit::COMPLETE : $unit::BUSY);
	    $unit::COMPLETE:
	      next_state = (grant ? $unit::IDLE : $unit::COMPLETE);
	    default:
	      next_state = $unit::IDLE;
	  endcase
	  running = (state == $unit::BUSY);
	  issue_ready = (state == $unit::IDLE);
	  if (state == $unit::COMPLETE) begin
	    req.valid = 1'b1;
	    req.complete_priority = {2 {1'b1}};
	  end
	  else
	    begin
	      req.valid = 1'b0;
	      req.complete_priority = {2 {1'b0}};
	    end
	  req.rob_num = rob_num;
	  req.value = result;
	  req.t_idx = dest_tag;
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    state <= #(1) $unit::IDLE;
	  end
	  else
	    begin
	      state <= #(1) next_state;
	    end
	  opa <= #(1) next_opa;
	  opb <= #(1) next_opb;
	  result <= #(1) next_result;
	  dest_tag <= #(1) next_dest_tag;
	  rob_num <= #(1) next_rob_num;
	  func <= #(1) next_func;
	  insn_reg <= #(1) next_insn_reg;
	end
endmodule

`timescale 1 ns / 100 ps
module adder(opa, opb, result, done);
	input	[(32 - 1):0]	opa;
	input	[(32 - 1):0]	opb;
	output	[(32 - 1):0]	result;

	logic	[(32 - 1):0]	result;
	output			done;

	assign done = 1'b1;
	assign result = (opa + opb);
endmodule

`timescale 1 ns / 100 ps
module mult(clock, reset, insn, issue_ready, req, grant, tag_a, tag_b, value_a,
	value_b);
	input			clock;
	input			reset;
	input			insn;

	$unit::RS_ENTRY		insn;
	output			issue_ready;
	logic			issue_ready;
	output			req;
	$unit::FU_COMPLETE_PACKET
				req;
	input			grant;
	logic			grant;
	output			tag_a;
	output			tag_b;
	$unit::preg_tag_t	tag_a;
	$unit::preg_tag_t	tag_b;
	input			value_a;
	input			value_b;
	$unit::word_t		value_a;
	$unit::word_t		value_b;
	$unit::word_t		mcand;
	$unit::word_t		mplier;
	$unit::word_t		next_mcand;
	$unit::word_t		next_mplier;
	$unit::FU_COMPLETE_PACKET
				entry_in;
	$unit::FU_COMPLETE_PACKET[4:0]
				entries;
	$unit::ALU_FUNC[4:0]	funcs;
	$unit::ALU_FUNC		func_in;
	logic			sign_a;
	logic			sign_b;
	logic			en;
	logic	[((2 * 32) - 1):0]
				mcand_out;
	logic	[((2 * 32) - 1):0]
				mplier_out;
	logic	[((2 * 32) - 1):0]
				mcand_in;
	logic	[((2 * 32) - 1):0]
				mplier_in;
	logic[4:0][((2 * 32) - 1):0]
				internal_mcands;
	logic[4:0][((2 * 32) - 1):0]
				internal_mpliers;
	logic[4:0][((2 * 32) - 1):0]
				internal_products;
	evaluation evaluator(.insn, .tag_a, .tag_b, .value_a, .value_b, 
		.operand_a			(next_mcand), 
		.operand_b			(next_mplier));
	genvar 			i;

	assign sign_a = (((funcs[0] == $unit::ALU_MUL) | (funcs[0] == 
		$unit::ALU_MULH)) | (funcs[0] == $unit::ALU_MULHSU));
	assign sign_b = ((funcs[0] == $unit::ALU_MUL) | (funcs[0] == 
		$unit::ALU_MULH));
	assign mcand_in = (sign_a ? {{32 {mcand[(32 - 1)]}}, mcand} : {{32
		{1'b0}}, mcand});
	assign mplier_in = (sign_b ? {{32 {mplier[(32 - 1)]}}, mplier} : {{32
		{1'b0}}, mplier});
	assign internal_mcands[0] = mcand_in;
	assign internal_mpliers[0] = mplier_in;
	assign internal_products[0] = 'b0;

	always_comb  begin
	  entry_in.valid = insn.busy;
	  entry_in.complete_priority = (insn.busy ? {2 {1'b1}} : {2 {1'b0}});
	  entry_in.rob_num = insn.rob_num;
	  entry_in.value = 32'b0;
	  entry_in.t_idx = insn.t_info.t_idx;
	  func_in = insn.inst_info.alu_func;
	  issue_ready = (~entries[0].valid);
	  req = entries[4];
	  req.value = ((funcs[4] == $unit::ALU_MUL) ? internal_products[4][(32 -
		  1):0] : internal_products[4][((2 * 32) - 1):32]);
	  en = ((~req.valid) | grant);
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    mcand <= #(1) '0;
	    mplier <= #(1) '0;
	    funcs[0] <= #(1) $unit::ALU_ADD;
	    entries[0] <= #(1) '0;
	  end
	  else
	    begin
	      mcand <= #(1) next_mcand;
	      mplier <= #(1) next_mplier;
	      funcs[0] <= #(1) func_in;
	      entries[0] <= #(1) entry_in;
	    end
	end

	for (i = 0; (i < 4); i = (i + 1)) begin : mstage 
	  mult_stage ms(.clock, .reset, .en, 
		.entry_in			(entries[i]), 
		.entry_out			(entries[(i + 1)]), 
		.func_in			(funcs[i]), 
		.func_out			(funcs[(i + 1)]), 
		.product_in			(internal_products[i]), 
		.mplier_in			(internal_mpliers[i]), 
		.mcand_in			(internal_mcands[i]), 
		.product_out			(internal_products[(i + 1)]), 
		.mplier_out			(internal_mpliers[(i + 1)]), 
		.mcand_out			(internal_mcands[(i + 1)]));
	end
endmodule

`timescale 1 ns / 100 ps
module mult_stage(clock, reset, en, entry_in, entry_out, func_in, func_out,
	mplier_in, mcand_in, product_in, mplier_out, mcand_out, product_out);
	input			clock;
	input			reset;
	input			en;
	input			entry_in;

	$unit::FU_COMPLETE_PACKET
				entry_in;
	output			entry_out;
	$unit::FU_COMPLETE_PACKET
				entry_out;
	input			func_in;
	$unit::ALU_FUNC		func_in;
	output			func_out;
	$unit::ALU_FUNC		func_out;
	input	[((2 * 32) - 1):0]
				mplier_in;
	input	[((2 * 32) - 1):0]
				mcand_in;
	input	[((2 * 32) - 1):0]
				product_in;
	output	[((2 * 32) - 1):0]
				mplier_out;
	output	[((2 * 32) - 1):0]
				mcand_out;
	logic	[((2 * 32) - 1):0]
				mplier_out;
	logic	[((2 * 32) - 1):0]
				mcand_out;
	output	[((2 * 32) - 1):0]
				product_out;
	logic	[((2 * 32) - 1):0]
				product_out;
	parameter		NUM_BITS	= ((2 * 32) / 4);
	logic	[((2 * 32) - 1):0]
				prod_in_reg;
	logic	[((2 * 32) - 1):0]
				partial_prod;
	logic	[((2 * 32) - 1):0]
				next_partial_product;
	logic	[((2 * 32) - 1):0]
				partial_prod_unsigned;
	logic	[((2 * 32) - 1):0]
				next_mplier;
	logic	[((2 * 32) - 1):0]
				next_mcand;

	assign product_out = (prod_in_reg + partial_prod);
	assign next_partial_product = (mplier_in[(NUM_BITS - 1):0] * mcand_in);
	assign next_mplier = {{NUM_BITS {1'b0}}, mplier_in[((2 * 32) -
		1):NUM_BITS]};
	assign next_mcand = {mcand_in[(((2 * 32) - 1) - NUM_BITS):0], {NUM_BITS
		{1'b0}}};

	always_ff @(posedge clock) begin
	  if (reset) begin
	    entry_out.valid <= #(1) 1'b0;
	    entry_out.complete_priority <= #(1) {2 {1'b0}};
	  end
	  else if (en) begin
	    entry_out <= #(1) entry_in;
	    prod_in_reg <= #(1) product_in;
	    partial_prod <= #(1) next_partial_product;
	    mplier_out <= #(1) next_mplier;
	    mcand_out <= #(1) next_mcand;
	    func_out <= #(1) func_in;
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module mshr(clock, reset, ld_in, st_in, mem_req, mem_in, ld_out, already_req,
	mshr_out, mshr_empty, mem_req_success);
	input			clock;
	input			reset;
	input[(3 - 1):0]	ld_in;

	$unit::LSQ_DCACHE_LD_PACKET[(3 - 1):0]
				ld_in;
	input			st_in;
	$unit::DCACHE_CTRL_ST_PACKET
				st_in;
	input			mem_req;
	$unit::MSHR_DCACHE_MEM_PACKET
				mem_req;
	input			mem_in;
	$unit::MEM_DCACHE_PACKET
				mem_in;
	output[(3 - 1):0]	ld_out;
	$unit::DCACHE_ENTRY[(3 - 1):0]
				ld_out;
	output			already_req;
	$unit::DCACHE_MEMREQ_SIGNAL
				already_req;
	output			mshr_out;
	$unit::DCACHE_ENTRY	mshr_out;
	output			mshr_empty;
	logic			mshr_empty;
	output			mem_req_success;
	logic			mem_req_success;
	logic	[(15 - 1):0]	mshr_valid;
	logic	[(15 - 1):0]	mshr_valid_next;
	logic	[(15 - 1):0]	mshr_issued;
	logic	[(15 - 1):0]	mshr_issued_next;
	$unit::MSHR_ENTRY[(15 - 1):0]
				mshr;
	$unit::MSHR_ENTRY[(15 - 1):0]
				mshr_next;
	$unit::memory_tag_t	mshr_valid_index;
	logic	[(15 - 1):0]	onehot_valid;
	$unit::memory_line_t	test1;
	$unit::memory_line_t	test2;

	assign onehot_valid = ((-mshr_valid) & mshr_valid);
	assign mshr_empty = (~(|mshr_issued));
	assign mshr_out.dirty = mshr[mshr_valid_index].dirty;
	assign mshr_out.valid = ((onehot_valid == 'b0) ? 1'b0 : 1'b1);
	assign mshr_out.addr = mshr[mshr_valid_index].addr;
	assign mshr_out.data = mshr[mshr_valid_index].data;

	always_comb  begin
	  case (onehot_valid)
	    15'b1:
	      mshr_valid_index = 'b0;
	    15'h0002:
	      mshr_valid_index = 'b1;
	    15'h0004:
	      mshr_valid_index = 'h00000002;
	    15'h0008:
	      mshr_valid_index = 'h00000003;
	    15'h0010:
	      mshr_valid_index = 'h00000004;
	    15'h0020:
	      mshr_valid_index = 'h00000005;
	    15'h0040:
	      mshr_valid_index = 'h00000006;
	    15'h0080:
	      mshr_valid_index = 'h00000007;
	    15'h0100:
	      mshr_valid_index = 'h00000008;
	    15'h0200:
	      mshr_valid_index = 'h00000009;
	    15'h0400:
	      mshr_valid_index = 'h0000000a;
	    15'h0800:
	      mshr_valid_index = 'h0000000b;
	    15'h1000:
	      mshr_valid_index = 'h0000000c;
	    15'h2000:
	      mshr_valid_index = 'h0000000d;
	    15'h4000:
	      mshr_valid_index = 'h0000000e;
	    default:
	      mshr_valid_index = 'b0;
	  endcase
	end
	always_comb  begin
	  mshr_next = mshr;
	  mshr_valid_next = mshr_valid;
	  mshr_issued_next = mshr_issued;
	  already_req = 0;
	  ld_out = 0;
	  mem_req_success = 1'b0;
	  if (mshr_out.valid) begin
	    mshr_valid_next[mshr_valid_index] = 1'b0;
	    mshr_issued_next[mshr_valid_index] = 1'b0;
	  end
	  if ((mem_req.valid && (mem_req.command == $unit::BUS_STORE)) && (
		  mem_in.response != '0)) begin
	    mem_req_success = 1'b1;
	  end
	  else if ((mem_req.valid && (mem_req.command == $unit::BUS_LOAD)) && (
		  mem_in.response != '0)) begin
	    mem_req_success = 1'b1;
	    mshr_next[(mem_in.response - 1)].addr = (mem_req.addr.addr & {{(32 -
		    3) {1'b1}}, {3 {1'b0}}});
	    mshr_valid_next[(mem_in.response - 1)] = 1'b0;
	    mshr_issued_next[(mem_in.response - 1)] = 1'b1;
	    mshr_next[(mem_in.response - 1)].mask = mem_req.mask;
	    mshr_next[(mem_in.response - 1)].w_data = mem_req.w_data;
	  end
	  begin

	    automatic int	j;
	    for (j = 0; (j < 15); j += 1) begin
	      if ((st_in.valid && mshr_issued[j]) && (mshr[j].addr.v_addr.tag ==
		      st_in.address.v_addr.tag)) begin
		already_req.st = 1'b1;
		mshr_next[j].mask = (mshr_next[j].mask & st_in.mask);
		mshr_next[j].w_data = ((mshr_next[j].w_data & st_in.mask) + 
			st_in.data);
	      end
	    end
	  end
	  if ((mem_in.tag != 0) && mshr_issued[(mem_in.tag - 1)]) begin
	    test1 = (mem_in.data & mshr_next[(mem_in.tag - 1)].mask);
	    test2 = (test1 + mshr_next[(mem_in.tag - 1)].w_data);
	    mshr_next[(mem_in.tag - 1)].data.mem = ((mem_in.data & 
		    mshr_next[(mem_in.tag - 1)].mask) + mshr_next[(mem_in.tag -
		    1)].w_data);
	    mshr_next[(mem_in.tag - 1)].dirty = (mem_in.data != 
		    mshr_next[(mem_in.tag - 1)].data.mem);
	    mshr_valid_next[(mem_in.tag - 1)] = 1'b1;
	  end
	  begin

	    automatic int	j;
	    for (j = 0; (j < 15); j += 1) begin
	      begin

		automatic int	i;
		for (i = 0; (i < 3); i += 1) begin
		  if ((ld_in[i].valid && mshr_valid[j]) && (
			  mshr[j].addr.v_addr.tag == ld_in[i].address.v_addr.tag
			  )) begin
		    ld_out[i].addr = ld_in[i].address;
		    ld_out[i].data = mshr[j].data;
		    ld_out[i].valid = 1'b1;
		    ld_out[i].dirty = mshr[j].dirty;
		  end
		  else if ((ld_in[i].valid && mshr_issued[j]) && (
			  mshr[j].addr.v_addr.tag == ld_in[i].address.v_addr.tag
			  )) begin
		    already_req.ld[i] = 1'b1;
		  end
		end
	      end
	    end
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    mshr <= #(1) 0;
	    mshr_valid <= #(1) 0;
	    mshr_issued <= #(1) 0;
	  end
	  else
	    begin
	      mshr <= #(1) mshr_next;
	      mshr_valid <= #(1) mshr_valid_next;
	      mshr_issued <= #(1) mshr_issued_next;
	    end
	end
endmodule

`timescale 1 ns / 100 ps
module post_retirement(clock, reset, st_in_origin, ld_in, st_taken, ld_out,
	st_out, space_out);
	input			clock;
	input			reset;
	input[(3 - 1):0]	st_in_origin;

	$unit::LSQ_PRB_ST_PACKET[(3 - 1):0]
				st_in_origin;
	input[(3 - 1):0]	ld_in;
	$unit::FU_LSQ_LD_PACKET[(3 - 1):0]
				ld_in;
	input			st_taken;
	$unit::ST_STATE		st_taken;
	output[(3 - 1):0]	ld_out;
	$unit::LSQ_LD_PACKET_OUT[(3 - 1):0]
				ld_out;
	output			st_out;
	$unit::PRB_DCACHE_ST_PACKET
				st_out;
	output			space_out;
	$unit::ss_size_t	space_out;
	$unit::prb_tag_t	head;
	$unit::prb_tag_t	tail;
	$unit::prb_tag_t	head_increase;
	$unit::prb_tag_t	tail_increase;
	$unit::PRB_ENTRY[((64 / 4) - 1):0]
				prb;
	$unit::PRB_ENTRY[((64 / 4) - 1):0]
				next_prb;
	$unit::prb_size_t	space_local;
	$unit::prb_size_t	size;
	logic	[(3 - 1):0]	st_tag_found;
	$unit::LSQ_PRB_ST_PACKET[(3 - 1):0]
				st_in;
	logic	[(3 - 1):0]	st_overlap;

	assign space_local = ((64 / 4) - (size + 3));
	assign space_out = ((space_local > 3) ? 3 : space_local);

	always_comb  begin
	  st_in = '0;
	  st_overlap = '0;
	  st_in[0] = st_in_origin[0];
	  begin

	    automatic int	i;
	    for (i = 1; (i < 3); i += 1) begin
	      if (st_in_origin[i].valid) begin
		begin

		  automatic int	j;
		  for (j = 0; (j < (3 - 1)); j += 1) begin
		    if (st_in[j].valid & (st_in_origin[i].address.prb_addr.tag 
			    == st_in[j].address.prb_addr.tag)) begin
		      st_overlap[i] = 1'b1;
		      if (st_in_origin[i].valid_byte[0]) begin
			st_in[j].value[7:0] = st_in_origin[i].value[7:0];
		      end
		      if (st_in_origin[i].valid_byte[1]) begin
			st_in[j].value[15:8] = st_in_origin[i].value[15:8];
		      end
		      if (st_in_origin[i].valid_byte[2]) begin
			st_in[j].value[23:16] = st_in_origin[i].value[23:16];
		      end
		      if (st_in_origin[i].valid_byte[3]) begin
			st_in[j].value[31:24] = st_in_origin[i].value[31:24];
		      end
		      st_in[j].valid_byte = (st_in[j].valid_byte | 
			      st_in_origin[i].valid_byte);
		    end
		  end
		end
		if (~st_overlap[i]) begin
		  st_in[i] = st_in_origin[i];
		end
	      end
	    end
	  end
	end
	always_comb  begin
	  tail_increase = 0;
	  next_prb = prb;
	  st_tag_found = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      if (st_in[i].valid) begin
		next_prb[(tail + tail_increase)].valid = 1'b1;
		next_prb[(tail + tail_increase)].tag = 
			st_in[i].address.prb_addr.tag;
		next_prb[(tail + tail_increase)].value = st_in[i].value;
		next_prb[(tail + tail_increase)].valid_byte = 
			st_in[i].valid_byte;
		tail_increase = (tail_increase + 1);
	      end
	    end
	  end
	  ld_out = '0;
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      if (ld_in[i].valid) begin
		begin

		  automatic int	j;
		  for (j = 0; (j < (64 / 4)); j += 1) begin
		    if (prb[j].valid & (prb[j].tag == 
			    ld_in[i].address.prb_addr.tag)) begin
		      ld_out[i].valid = 1'b1;
		      ld_out[i].value = prb[j].value;
		      ld_out[i].valid_byte = prb[j].valid_byte;
		    end
		  end
		end
	      end
	    end
	  end
	  head_increase = 0;
	  if (prb[head].valid) begin
	    st_out.valid = prb[head].valid;
	    st_out.address.addr = {prb[head].tag, {$clog2(4) {1'b0}}};
	    st_out.valid_byte = prb[head].valid_byte;
	    st_out.value = prb[head].value;
	  end
	  else
	    begin
	      st_out = '0;
	    end
	  if (prb[head].valid) begin
	    if (st_taken == $unit::DONE) begin
	      head_increase = 1'b1;
	      next_prb[head].valid = 1'b0;
	    end
	  end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    head <= #(1) 0;
	    tail <= #(1) 0;
	    prb <= #(1) 0;
	    size <= #(1) 0;
	  end
	  else
	    begin
	      head <= #(1) (head + head_increase);
	      tail <= #(1) (tail + tail_increase);
	      prb <= #(1) next_prb;
	      size <= #(1) ((size + tail_increase) - head_increase);
	    end
	end
endmodule

`timescale 1 ns / 100 ps
module dcache_data(clock, reset, ld_in, st_in, mshr_in, dump_en, st_out, ld_out,
	dump_dmem_done, evict);
	input			clock;
	input			reset;
	input[(3 - 1):0]	ld_in;

	$unit::LSQ_DCACHE_LD_PACKET[(3 - 1):0]
				ld_in;
	input			st_in;
	$unit::DCACHE_CTRL_ST_PACKET
				st_in;
	input			mshr_in;
	$unit::DCACHE_ENTRY	mshr_in;
	input			dump_en;
	output			st_out;
	logic			st_out;
	output[(3 - 1):0]	ld_out;
	$unit::DCACHE_ENTRY[(3 - 1):0]
				ld_out;
	output			dump_dmem_done;
	logic			dump_dmem_done;
	output			evict;
	$unit::DCACHE_ENTRY	evict;
	$unit::DCACHE_ENTRY[((32 / 4) - 1):0][(4 - 1):0]
				cache;
	$unit::DCACHE_ENTRY[((32 / 4) - 1):0][(4 - 1):0]
				cache_next;
	logic[((32 / 4) - 1):0][(4 - 1):0][($clog2(4) - 1):0]
				lru;
	logic[((32 / 4) - 1):0][(4 - 1):0][($clog2(4) - 1):0]
				lru_next;
	logic[((32 / 4) - 1):0][(4 - 1):0][($clog2(4) - 1):0]
				lru_new;
	logic[((32 / 4) - 1):0][(4 - 1):0][($clog2(4) - 1):0]
				lru_old;
	$unit::dcache_index_t[(3 - 1):0]
				ld_in_index;
	$unit::dcache_index_t	mshr_in_index;
	$unit::dcache_index_t	st_in_index;
	logic[((32 / 4) - 1):0][(4 - 1):0]
				updated;
	logic	[($clog2((32 / 4)) - 1):0]
				dump_index;
	logic	[($clog2((32 / 4)) - 1):0]
				dump_index_next;
	logic	[($clog2(4) - 1):0]
				dump_way;
	logic	[($clog2(4) - 1):0]
				dump_way_next;
	logic	[(4 - 1):0]	st_hit_check;
	genvar 			k;
	genvar 			i;

	assign mshr_in_index = mshr_in.addr.d_addr.index;
	assign st_in_index = st_in.address.d_addr.index;
	assign dump_dmem_done = (((dump_index == ((32 / 4) - 1)) && (dump_way ==
		(4 - 1))) ? 1'b1 : 1'b0);
	assign st_out = ((st_hit_check == 4'b0) ? 1'b0 : 1'b1);

	always_comb  begin
	  cache_next = cache;
	  ld_out = 0;
	  lru_next = lru;
	  evict = 0;
	  if (dump_en) begin
	    evict = cache[dump_index][dump_way];
	    cache_next[dump_index][dump_way].valid = 0;
	    dump_way_next = (((dump_index == ((32 / 4) - 1)) && (dump_way == (4 
		    - 1))) ? dump_way : (dump_way + 1));
	    dump_index_next = ((dump_index == ((32 / 4) - 1)) ? dump_index : ((
		    dump_way_next == 4'b0) ? (dump_index + 1) : dump_index));
	  end
	  else
	    begin
	      dump_way_next = dump_way;
	      dump_index_next = dump_index;
	      updated = '0;
	      begin

		automatic int	j;
		for (j = 0; (j < 4); j += 1) begin
		  begin

		    automatic int
				i;
		    for (i = 0; (i < 3); i += 1) begin
		      if ((ld_in[i].valid && cache[ld_in_index[i]][j].valid) && 
			      (cache[ld_in_index[i]][j].addr.d_addr.tag == 
			      ld_in[i].address.d_addr.tag)) begin
			ld_out[i] = cache[ld_in_index[i]][j];
			updated[ld_in_index[i]][j] = 1'b1;
		      end
		    end
		  end
		  if ((st_in.valid && cache[st_in_index][j].valid) && (
			  cache[st_in_index][j].addr.d_addr.tag == 
			  st_in.address.d_addr.tag)) begin
		    cache_next[st_in_index][j].valid = st_in.valid;
		    cache_next[st_in_index][j].data.mem = ((
			    cache_next[st_in_index][j].data.mem & st_in.mask) + 
			    st_in.data);
		    cache_next[st_in_index][j].dirty = ((
			    cache_next[st_in_index][j].dirty | (
			    cache_next[st_in_index][j].data != 
			    cache[st_in_index][j].data)) ? 1'b1 : 1'b0);
		    updated[st_in_index][j] = 1'b1;
		  end
		end
	      end
	      if (mshr_in.valid) begin
		evict = cache_next[mshr_in_index][lru[mshr_in_index][0]];
		cache_next[mshr_in_index][lru[mshr_in_index][0]] = mshr_in;
		updated[mshr_in_index][lru[mshr_in_index][0]] = 1'b1;
	      end
	      lru_new = '0;
	      lru_old = '0;
	      begin

		automatic int	i;
		for (i = 0; (i < (32 / 4)); i += 1) begin
		  begin

		    automatic int
				j;
		    for (j = 0; (j < 4); j += 1) begin
		      if (updated[i][j]) begin
			lru_new[i] = {lru[i][j], lru_new[i][(4 - 1):1]};
		      end
		      else begin
			lru_old[i] = {lru_old[i][(4 - 2):0], lru[i][j]};
		      end
		    end
		  end
		end
	      end
	      lru_next = (lru_old | lru_new);
	    end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    cache <= #(1) 0;
	    lru <= #(1) {(32 / 4) {2'b0, 2'b1, 2'h2, 2'h3}};
	    dump_index <= #(1) 0;
	    dump_way <= #(1) 0;
	  end
	  else
	    begin
	      cache <= #(1) cache_next;
	      lru <= #(1) lru_next;
	      dump_index <= #(1) dump_index_next;
	      dump_way <= #(1) dump_way_next;
	    end
	end

	for (k = 0; (k < 3); k = (k + 1)) begin : set_index 

	  assign ld_in_index[k] = ld_in[k].address.d_addr.index;
	end

	for (i = 0; (i < 4); i = (i + 1)) begin : search_st_hit 

	  assign st_hit_check[i] = ((st_in.valid && cache[st_in_index][i].valid)
		  && (cache[st_in_index][i].addr.d_addr.tag == 
		  st_in.address.d_addr.tag));
	end
endmodule

`timescale 1 ns / 100 ps
module victim_cache(clock, reset, ld_in, st_in, dcache_in, dcache_exchange,
	dump_en, mem_w_req, ld_out, st_hit, st_out, dump_done);
	input			clock;
	input			reset;
	input[(3 - 1):0]	ld_in;

	$unit::LSQ_DCACHE_LD_PACKET[(3 - 1):0]
				ld_in;
	input			st_in;
	$unit::PRB_DCACHE_ST_PACKET
				st_in;
	input			dcache_in;
	$unit::DCACHE_ENTRY	dcache_in;
	input[(3 - 1):0]	dcache_exchange;
	$unit::DCACHE_ENTRY[(3 - 1):0]
				dcache_exchange;
	input			dump_en;
	output			mem_w_req;
	$unit::DCACHE_MEM_PACKET
				mem_w_req;
	output[(3 - 1):0]	ld_out;
	$unit::DCACHE_ENTRY[(3 - 1):0]
				ld_out;
	output			st_hit;
	logic			st_hit;
	output			st_out;
	$unit::DCACHE_ENTRY	st_out;
	output			dump_done;
	logic			dump_done;
	$unit::DCACHE_ENTRY[(4 - 1):0]
				vc;
	$unit::DCACHE_ENTRY[(4 - 1):0]
				vc_next;
	$unit::lru_t[(4 - 1):0]	lru;
	$unit::lru_t[(4 - 1):0]	lru_next;
	logic			updated;
	logic	[($clog2(4) - 1):0]
				dump_idx;
	logic	[($clog2(4) - 1):0]
				dump_idx_next;
	logic	[(4 - 1):0]	vc_valid;
	logic	[(4 - 1):0]	vc_valid_next;
	$unit::lru_t[(4 - 1):0]	lru_1;
	$unit::lru_t[(4 - 1):0]	lru_2;
	$unit::lru_t[(4 - 1):0]	lru_3;
	$unit::lru_t[(4 - 1):0]	test;
	logic[(3 - 1):0][($clog2(4) - 1):0]
				ex_idx;
	$unit::DCACHE_ENTRY	vc_out;

	assign dump_done = ((dump_idx == (4 - 1)) ? 1'b1 : 1'b0);

	always_comb  begin
	  dump_idx_next = dump_idx;
	  vc_next = vc;
	  vc_valid_next = vc_valid;
	  if (dump_en && (~dcache_in.valid)) begin
	    lru_next = {lru[0], lru[(4 - 1):1]};
	    dump_idx_next = (dump_done ? (4 - 1) : (dump_idx + 1));
	    vc_next[lru[0]].valid = 1'b0;
	  end
	  else
	    begin
	      begin

		automatic int	i;
		for (i = 0; (i < 4); i += 1) begin
		  vc_next[i].valid = vc[i].valid;
		end
	      end
	      lru_next = lru;
	      ex_idx = 0;
	      vc_next[lru[0]].valid = ((vc[lru[0]].valid | dcache_in.valid) | 
		      dcache_exchange[0].valid);
	      vc_next[lru[1]].valid = (vc[lru[1]].valid | 
		      dcache_exchange[1].valid);
	      lru_1 = lru_next;
	      begin

		automatic int	i;
		for (i = 0; (i < 3); i += 1) begin
		  ld_out[i] = 0;
		  updated = 1'b0;
		  begin

		    automatic int
				j;
		    for (j = 0; (j < 4); j += 1) begin
		      if ((ld_in[i].valid && vc[j].valid) && (
			      ld_in[i].address.v_addr.tag == 
			      vc[j].addr.v_addr.tag)) begin
			ld_out[i] = vc[j];
			vc_next[j].valid = 0;
			begin

			  automatic int
				k;
			  for (k = 0; (k < 4); k += 1) begin
			    if ((lru_next[0] == j) && (!updated)) begin
			      updated = 1'b1;
			      ex_idx[i] = j;
			    end
			    else if (updated) begin
			      lru_next = {lru_next[1], lru_next[(4 - 1):2],
				      lru_next[0]};
			    end
			    else begin
			      lru_next = {lru_next[0], lru_next[(4 - 1):1]};
			    end
			  end
			end
		      end
		    end
		  end
		end
	      end
	    end
	  lru_2 = lru_next;
	  updated = 1'b0;
	  st_out = 0;
	  st_hit = 1'b0;
	  begin

	    automatic int	j;
	    for (j = 0; (j < 4); j += 1) begin
	      if ((st_in.valid && vc[j].valid) && (st_in.address.v_addr.tag == 
		      vc[j].addr.v_addr.tag)) begin
		st_hit = 1'b1;
		if ((~ld_out[0].valid) && (~ld_out[1].valid)) begin
		  st_out = vc[j];
		  vc_next[j].valid = 0;
		  begin

		    automatic int
				k;
		    for (k = 0; (k < 4); k += 1) begin
		      if ((lru_next[0] == j) && (!updated)) begin
			updated = 1'b1;
			ex_idx[0] = j;
		      end
		      else if (updated) begin
			lru_next = {lru_next[1], lru_next[(4 - 1):2],
				lru_next[0]};
		      end
		      else begin
			lru_next = {lru_next[0], lru_next[(4 - 1):1]};
		      end
		    end
		  end
		end
	      end
	    end
	  end
	  test = lru_next;
	  if (dcache_in.valid) begin
	    vc_next[lru_next[0]].addr = dcache_in.addr;
	    vc_next[lru_next[0]].data = dcache_in.data;
	    vc_next[lru_next[0]].dirty = dcache_in.dirty;
	    vc_next[lru_next[0]].valid = dcache_in.valid;
	    lru_next = {lru_next[0], lru_next[(4 - 1):1]};
	  end
	  begin

	    automatic int	i;
	    for (i = 0; (i < 3); i += 1) begin
	      vc_next[lru_next[0]].addr = (dcache_exchange[i].valid ? 
		      dcache_exchange[i].addr : vc[lru_next[0]].addr);
	      vc_next[lru_next[0]].data = (dcache_exchange[i].valid ? 
		      dcache_exchange[i].data : vc[lru_next[0]].data);
	      vc_next[lru_next[0]].dirty = (dcache_exchange[i].valid ? 
		      dcache_exchange[i].dirty : vc[lru_next[0]].dirty);
	      vc_next[lru_next[0]].valid = (dcache_exchange[i].valid ? 
		      dcache_exchange[i].valid : vc_next[lru_next[0]].valid);
	      lru_next = (dcache_exchange[i].valid ? {lru_next[0], lru_next[(4 -
		      1):1]} : lru_next);
	    end
	  end
	  begin

	    automatic int	i;
	    for (i = 4; (i >= 0); i -= 1) begin
	      if ((~(dcache_exchange[0].valid && (lru[i] == ex_idx[0]))) && (~(
		      dcache_exchange[1].valid && (lru[i] == ex_idx[1])))) begin
		vc_out = vc[lru[i]];
	      end
	    end
	  end
	  lru_3 = lru_next;
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    vc <= #(1) 0;
	    lru <= #(1) {2'b0, 2'b1, 2'h2, 2'h3};
	    mem_w_req.valid <= #(1) 0;
	    mem_w_req.command <= #(1) $unit::BUS_NONE;
	    mem_w_req.addr <= #(1) 0;
	    mem_w_req.data <= #(1) 0;
	    dump_idx <= #(1) 0;
	  end
	  else
	    begin
	      vc <= #(1) vc_next;
	      lru <= #(1) lru_next;
	      mem_w_req.valid <= #(1) (((dcache_in.valid || dump_en) & 
		      vc_out.valid) & vc_out.dirty);
	      mem_w_req.command <= #(1) $unit::BUS_STORE;
	      mem_w_req.addr <= #(1) vc_out.addr;
	      mem_w_req.data <= #(1) vc_out.data;
	      dump_idx <= #(1) dump_idx_next;
	    end
	end
endmodule

`timescale 1 ns / 100 ps
module dcache(clock, reset, ld_fu_in, st_fu_in, mem_in, halt_retire, st_state,
	ld_out, mem_req, halt);
	input			clock;
	input			reset;
	input[(3 - 1):0]	ld_fu_in;

	$unit::LSQ_DCACHE_LD_PACKET[(3 - 1):0]
				ld_fu_in;
	input			st_fu_in;
	$unit::PRB_DCACHE_ST_PACKET
				st_fu_in;
	input			mem_in;
	$unit::MEM_DCACHE_PACKET
				mem_in;
	input			halt_retire;
	output			st_state;
	$unit::ST_STATE		st_state;
	output[(3 - 1):0]	ld_out;
	$unit::DCACHE_LSQ_LD_PACKET[(3 - 1):0]
				ld_out;
	output			mem_req;
	$unit::DCACHE_MEM_PACKET
				mem_req;
	output			halt;
	$unit::DCACHE_ENTRY	mshr_out;
	$unit::DCACHE_ENTRY	evict;
	$unit::DCACHE_ENTRY	dcache_mshr_in;
	$unit::DCACHE_ENTRY[(3 - 1):0]
				exchange_dmem_out;
	$unit::DCACHE_ENTRY[(3 - 1):0]
				exchange_dmem_in;
	$unit::DCACHE_ENTRY[(3 - 1):0]
				ld_dmem_out;
	$unit::DCACHE_ENTRY[(3 - 1):0]
				ld_mshr_out;
	logic			st_out;
	logic			mem_req_success;
	logic	[(3 - 1):0]	ld_in_block;
	$unit::DCACHE_MEMREQ_SIGNAL
				already_req;
	logic			dump;
	logic			dump_next;
	logic			mshr_empty;
	logic			halt_rob;
	logic			halt_rob_next;
	logic			dump_dmem_done;
	$unit::LSQ_DCACHE_LD_PACKET[(3 - 1):0]
				ld_in;
	$unit::DCACHE_CTRL_ST_PACKET
				st_in;
	logic[(3 - 1):0][(3 - 1):0]
				duplicated_ld;
	logic[(3 - 1):0][($clog2((3 + 1)) - 1):0]
				ld_idx_array;
	$unit::DCACHE_MEMREQ_SIGNAL
				test1;
	$unit::DCACHE_MEMREQ_SIGNAL
				test2;
	$unit::DCACHE_MEMREQ_SIGNAL
				test3;
	$unit::MSHR_DCACHE_MEM_PACKET
				mem_w_dreq;
	$unit::MSHR_DCACHE_MEM_PACKET
				mem_r_dreq;
	$unit::MSHR_DCACHE_MEM_PACKET
				mem_dreq;
	$unit::MSHR_DCACHE_MEM_PACKET
				mem_dreq_next;
	$unit::DCACHE_MEMREQ_SIGNAL
				mem_req_signal;
	genvar 			k;
	dcache_data dmem(
		.clock				(clock), 
		.reset				(reset), 
		.ld_in				(ld_in), 
		.st_in				(st_in), 
		.mshr_in			(dcache_mshr_in), 
		.dump_en			(dump), 
		.st_out				(st_out), 
		.ld_out				(ld_dmem_out), 
		.dump_dmem_done			(dump_dmem_done), 
		.evict				(evict));
	mshr mshr_(
		.clock				(clock), 
		.reset				(reset), 
		.ld_in				(ld_in), 
		.st_in				(st_in), 
		.mem_req			(mem_dreq), 
		.mem_in				(mem_in), 
		.ld_out				(ld_mshr_out), 
		.already_req			(already_req), 
		.mshr_out			(mshr_out), 
		.mshr_empty			(mshr_empty), 
		.mem_req_success		(mem_req_success));

	assign ld_in = ld_fu_in;
	assign st_in.valid = st_fu_in.valid;
	assign st_in.address = st_fu_in.address;
	assign st_in.mask[7:0] = (((~st_fu_in.address.addr[(3 - 1)]) && 
		st_fu_in.valid_byte[0]) ? 8'b0 : 8'hff);
	assign st_in.mask[15:8] = (((~st_fu_in.address.addr[(3 - 1)]) && 
		st_fu_in.valid_byte[1]) ? 8'b0 : 8'hff);
	assign st_in.mask[23:16] = (((~st_fu_in.address.addr[(3 - 1)]) && 
		st_fu_in.valid_byte[2]) ? 8'b0 : 8'hff);
	assign st_in.mask[31:24] = (((~st_fu_in.address.addr[(3 - 1)]) && 
		st_fu_in.valid_byte[3]) ? 8'b0 : 8'hff);
	assign st_in.mask[39:32] = ((st_fu_in.address.addr[(3 - 1)] && 
		st_fu_in.valid_byte[0]) ? 8'b0 : 8'hff);
	assign st_in.mask[47:40] = ((st_fu_in.address.addr[(3 - 1)] && 
		st_fu_in.valid_byte[1]) ? 8'b0 : 8'hff);
	assign st_in.mask[55:48] = ((st_fu_in.address.addr[(3 - 1)] && 
		st_fu_in.valid_byte[2]) ? 8'b0 : 8'hff);
	assign st_in.mask[63:56] = ((st_fu_in.address.addr[(3 - 1)] && 
		st_fu_in.valid_byte[3]) ? 8'b0 : 8'hff);
	assign st_in.data[7:0] = (((~st_fu_in.address.addr[(3 - 1)]) && 
		st_fu_in.valid_byte[0]) ? st_fu_in.value[7:0] : 8'b0);
	assign st_in.data[15:8] = (((~st_fu_in.address.addr[(3 - 1)]) && 
		st_fu_in.valid_byte[1]) ? st_fu_in.value[15:8] : 8'b0);
	assign st_in.data[23:16] = (((~st_fu_in.address.addr[(3 - 1)]) && 
		st_fu_in.valid_byte[2]) ? st_fu_in.value[23:16] : 8'b0);
	assign st_in.data[31:24] = (((~st_fu_in.address.addr[(3 - 1)]) && 
		st_fu_in.valid_byte[3]) ? st_fu_in.value[31:24] : 8'b0);
	assign st_in.data[39:32] = ((st_fu_in.address.addr[(3 - 1)] && 
		st_fu_in.valid_byte[0]) ? st_fu_in.value[7:0] : 8'b0);
	assign st_in.data[47:40] = ((st_fu_in.address.addr[(3 - 1)] && 
		st_fu_in.valid_byte[1]) ? st_fu_in.value[15:8] : 8'b0);
	assign st_in.data[55:48] = ((st_fu_in.address.addr[(3 - 1)] && 
		st_fu_in.valid_byte[2]) ? st_fu_in.value[23:16] : 8'b0);
	assign st_in.data[63:56] = ((st_fu_in.address.addr[(3 - 1)] && 
		st_fu_in.valid_byte[3]) ? st_fu_in.value[31:24] : 8'b0);
	assign st_state = ((st_out || (already_req.st || mem_req_signal.st)) ? 
		$unit::DONE : $unit::NONE);
	assign mem_req.valid = mem_dreq.valid;
	assign mem_req.command = mem_dreq.command;
	assign mem_req.addr = mem_dreq.addr;
	assign mem_req.data = mem_dreq.data;
	assign mem_dreq_next = ((mem_req.valid && (~mem_req_success)) ? mem_dreq
		: (mem_w_dreq.valid ? mem_w_dreq : (mem_r_dreq.valid ? 
		mem_r_dreq : {1'b0, $unit::BUS_NONE, 32'b0, 64'b0})));
	assign dcache_mshr_in.valid = mshr_out.valid;
	assign dcache_mshr_in.addr = mshr_out.addr;
	assign dcache_mshr_in.data.mem = ((~st_in.valid) ? mshr_out.data.mem : (
		(mshr_out.addr.v_addr.tag == st_in.address.v_addr.tag) ? ((
		mshr_out.data.mem & st_in.mask) + st_in.data) : 
		mshr_out.data.mem));
	assign dcache_mshr_in.dirty = (((mshr_out.data.mem == 
		dcache_mshr_in.data.mem) && st_in.valid) ? mshr_out.dirty : 1'b1
		);
	assign dump_next = (dump | (((halt_rob && (~st_in.valid)) && mshr_empty)
		&& (~mem_req.valid)));
	assign halt_rob_next = (halt_rob | halt_retire);
	assign mem_w_dreq.valid = (evict.valid && evict.dirty);
	assign mem_w_dreq.command = $unit::BUS_STORE;
	assign mem_w_dreq.addr = evict.addr;
	assign mem_w_dreq.data = evict.data;
	assign halt = (dump && dump_dmem_done);

	always_comb  begin
	  mem_r_dreq = '0;
	  if ((mem_req.valid && (~mem_req_success)) || mem_w_dreq.valid) begin
	    mem_req_signal = 'b0;
	  end
	  else
	    begin
	      mem_req_signal = 'b0;
	      if (((st_in.valid && (~already_req.st)) && (~st_out)) && ((
		      st_in.address.v_addr.tag != mem_req.addr.v_addr.tag) || (~
		      mem_req.valid))) begin
		mem_req_signal = 'b1;
		mem_r_dreq.valid = 1'b1;
		mem_r_dreq.command = $unit::BUS_LOAD;
		mem_r_dreq.addr.m_addr.tag = st_in.address.m_addr.tag;
		mem_r_dreq.mask = st_in.mask;
		mem_r_dreq.w_data = st_in.data;
	      end
	      begin

		automatic int	i;
		for (i = 0; (i < 3); i += 1) begin
		  if (((ld_in[i].valid & (~already_req.ld[i])) && (~
			  ld_out[i].valid)) && ((ld_in[i].address.v_addr.tag != 
			  mem_req.addr.v_addr.tag) || (~mem_req.valid))) begin
		    mem_req_signal = ('b1 << (i + 1));
		    mem_r_dreq.valid = 1'b1;
		    mem_r_dreq.command = $unit::BUS_LOAD;
		    mem_r_dreq.addr.m_addr.tag = ld_in[i].address.m_addr.tag;
		    mem_r_dreq.mask = 64'hffffffffffffffff;
		    mem_r_dreq.w_data = 64'b0;
		  end
		end
	      end
	    end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    dump <= #(1) 1'b0;
	    halt_rob <= #(1) 1'b0;
	    mem_dreq <= #(1) '0;
	  end
	  else
	    begin
	      dump <= #(1) dump_next;
	      halt_rob <= #(1) halt_rob_next;
	      mem_dreq <= #(1) mem_dreq_next;
	    end
	end

	for (k = 0; (k < 3); k = (k + 1)) begin : assign_ld_out 

	  assign ld_in_block[k] = ld_fu_in[k].address.w_addr.tag[0];
	  assign ld_out[k] = (ld_dmem_out[k].valid ? {ld_dmem_out[k].valid,
		  ld_dmem_out[k].data.block[ld_in_block[k]]} : (
		  ld_mshr_out[k].valid ? {ld_mshr_out[k].valid,
		  ld_mshr_out[k].data.block[ld_in_block[k]]} : {1'b0, 32'b0}));
	end
endmodule

// END: VCS tokens