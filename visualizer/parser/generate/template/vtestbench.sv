/////////////////////////////////////////////////////////////////////////
//                                                                     //
//                                                                     //
//   Modulename :  visual_testbench.v                                  //
//                                                                     //
//  Description :  Testbench module for the verisimple pipeline        //
//                   for the visual debugger                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

extern void init();
extern void waitforresponse();

module testbench();

  logic        clock;
	logic        reset;
	logic [31:0] clock_count;
	logic [31:0] instr_count;
	int          wb_fileno;

	logic [1:0]  proc2mem_command;
	logic [`XLEN-1:0] proc2mem_addr;
	logic [63:0] proc2mem_data;
	logic  [3:0] mem2proc_response;
	logic [63:0] mem2proc_data;
	logic  [3:0] mem2proc_tag;
`ifndef CACHE_MODE
	MEM_SIZE     proc2mem_size;
`endif

	logic 		 pipeline_halt;
	logic		   halt;
  ss_size_t pipeline_completed_insts;

	// Instantiate the Pipeline
	`DUT(pipeline) core(
		// Inputs
		.clock             (clock),
		.reset             (reset),
		.mem2proc_response (mem2proc_response),
		.mem2proc_data     (mem2proc_data),
		.mem2proc_tag      (mem2proc_tag),


		// Outputs
`ifndef CACHE_MODE
		.proc2mem_size     (proc2mem_size),
`endif
		.proc2mem_command  (proc2mem_command),
		.proc2mem_addr     (proc2mem_addr),
		.proc2mem_data     (proc2mem_data),
		.halt_retire			 (pipeline_halt),
		.halt							 (halt),
		.pipeline_completed_insts (pipeline_completed_insts)
	);

	// Instantiate the Data Memory
	mem memory (
		// Inputs
		.clk               (clock),
		.proc2mem_command  (proc2mem_command),
		.proc2mem_addr     (proc2mem_addr),
		.proc2mem_data     (proc2mem_data),
`ifndef CACHE_MODE
		.proc2mem_size     (proc2mem_size),
`endif

		// Outputs

		.mem2proc_response (mem2proc_response),
		.mem2proc_data     (mem2proc_data),
		.mem2proc_tag      (mem2proc_tag)
	);

	logic reset_end;

  // Generate System Clock
  always
  begin
    #(`VERILOG_CLOCK_PERIOD/2.0);
    clock = ~clock;
  end

	always @(posedge clock) begin
		if(reset) begin
			clock_count <= `SD 0;
			instr_count <= `SD 0;
		end else begin
			clock_count <= `SD (clock_count + 1);
			instr_count <= `SD (instr_count + pipeline_completed_insts);
		end
	end

  // Show contents of a range of Unified Memory, in both hex and decimal
  task show_mem_with_decimal;
    input [31:0] start_addr;
    input [31:0] end_addr;
    begin
      for(int k = start_addr; k < end_addr; k = k+1) begin
        if (memory.unified_memory[k] != 0) begin
          $display("MEM mem_%h 64 %h", k*8, memory.unified_memory[k]);
        end
      end
    end
  endtask  // task show_mem_with_decimal

  always @(negedge clock) begin
		if (halt) begin
			$finish;
		end
		if (clock_count > 5000) begin
			$finish;
		end
  end 

  initial
  begin
    clock = 0;
    reset = 0;
		reset_end = 1'b0;
    init();

    // Pulse the reset signal
    reset = 1'b1;
    @(posedge clock);
    @(posedge clock);

    // Read program contents into memory array
    $readmemh("program.mem", memory.unified_memory);

    @(posedge clock);
    @(posedge clock);
    `SD;
    // This reset is at an odd time to avoid the pos & neg clock edges
    reset = 1'b0;
		reset_end = 1'b1;
  end

  // This block is where we dump all of the signals that we care about to
  // the visual debugger.  Notice this happens at *every* clock edge.
  always @(clock) begin
    #2;
		if (reset_end) begin
			$display("TIME _ _ %h", clock_count);

			{% for item in variable %}
					{% if item.add_syn_guard %}
`ifndef SYN
					{% endif %}
			$display("VAR {{item.hierarchy}} {{item.width}} %h", {{item.hierarchy}});
					{% if item.add_syn_guard %}
`endif
					{% endif %}
			{% endfor %}

			show_mem_with_decimal(0, `MEM_64BIT_LINES);

			// must come last
			$display("break");

			// This is a blocking call to allow the debugger to control when we
			// advance the simulation
			waitforresponse();
		end
  end
endmodule
