`timescale 1ns/100ps

{% for item in cfunc_def %}
{{item}}
{% endfor %}

{% if has_main %}

typedef struct packed {
    {% for item in main_in %}
    {{item}}
    {% endfor %}
} {{main_name}}_IN_P;

typedef struct packed {
    {% for item in main_out %}
    {{item}}
    {% endfor %}
} {{main_name}}_OUT_P;

{{main_in_cfunc}}

{{main_out_cfunc}}

import "DPI-C" context function void read_{{main_name}}_TB_IN({{main_name}}_IN_P i);
import "DPI-C" context function void read_{{main_name}}_TB_OUT({{main_name}}_OUT_P i);
import "DPI-C" context function void step_{{main_name}}_TB();
import "DPI-C" context function int check_{{main_name}}_TB();
import "DPI-C" context function void print_{{main_name}}_TB();

{% endif %}


module testbench;

    logic clock, reset;
    int check;

    always begin
        #5;
        clock = ~clock;
    end

    task exit_on_error;
        #1;
        $display("@@@Failed at time %f", $time);
        $finish;
    endtask

    {% if has_main %}

    {{main_name}}_IN_P in;
    {{main_name}}_OUT_P out;

    task clear_in;
        in = '0;
    endtask


    {{main_name}} {{main_name}}_0 (
        {% if has_clock %}
        .clock(clock),
        .reset(reset),
        {% endif %}

        {% for name in main_in_name %}
        .{{name.name}}(in.{{name.name}}){{name.comma}}
        {% endfor %}

        {% for name in main_out_name %}
        .{{name.name}}(out.{{name.name}}){{name.comma}}
        {% endfor %}
    );

    {% endif %}

    {% for item in vfunc_def %}
    {{item}}
    {% endfor %}


    initial begin
        clock = 1'b0;
        reset = 1'b0;
    
        {% if has_main %}
        clear_in();
        {% endif %}

        @(negedge clock)
        @(negedge clock)
        reset = 1'b1;
        @(negedge clock)
        @(negedge clock)
        reset = 1'b0;
        @(negedge clock)
        
        {% if has_main %}
        #1 read_{{main_name}}_TB_IN(in);
        #1 step_{{main_name}}_TB();
        #1 print_{{main_name}}_TB();
        #1 read_{{main_name}}_TB_OUT(out);
        #1 check = check_{{main_name}}_TB();
        if (!check)
            exit_on_error();
        {% endif %}


        $finish;
    end

endmodule