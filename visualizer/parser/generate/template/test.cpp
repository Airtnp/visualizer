#include "svffi.h"
#include <iostream>

// TODO: add color
template <typename T, typename U>
bool print_assert(size_t indent, const char* s, const char* m, T cpp, U verilog) {
    if (cpp != verilog) {
        for (size_t i = 0; i < indent; ++i)
            std::cout << '\t';
        std::cout << "[Incorrect] " << s << "." << m << ": " << cpp << "(cpp)" << " vs " << verilog << "(verilog)\n";
    }
    return cpp == verilog;
}

template <typename T, typename U>
void print_info(size_t indent, const char* s, const char* m, T cpp, U verilog) {
    for (size_t i = 0; i < indent; ++i)
        std::cout << '\t';
    std::cout << "[Info] " << s << "." << m << ": " << cpp << "(cpp)" << " vs " << verilog << "(verilog)\n";
}

void print_header(size_t indent, const char* s, const char* m) {
    for (size_t i = 0; i < indent; ++i)
        std::cout << '\t';
    std::cout << "[Enter] " << s << "." << m << '\n';
}


using INST = PackedLogicArray<32>;

{% for item in gen %}

{{item}}

{% endfor %}

{% if has_main %}

struct {{main_name}}_TB {
    {{main_name}}_IN_P in;
    {{main_name}}_OUT_P out;

    {{main_name}}_TB () {}
    
    bool check(size_t indent=0) {
        bool success = true;
        print_header(indent, "{{main_name}}_TB", "out");
        success &= out.check(indent + 1);
        return success;
    }

    void print(size_t indent=0) {
        print_header(indent, "{{main_name}}_TB", "in");
        in.print(indent + 1);
        print_header(indent, "{{main_name}}_TB", "out");
        out.print(indent + 1);
    }

    void step() {}
};

{{main_name}}_TB {{main_name}}_TB_instance;

extern "C" void read_{{main_name}}_TB_IN(svOpenArrayHandle h) {
    read({{main_name}}_TB_instance.in, h);
}

extern "C" void read_{{main_name}}_TB_OUT(svOpenArrayHandle h) {
    read({{main_name}}_TB_instance.out, h);
}

extern "C" void print_{{main_name}}_TB(svOpenArrayHandle h) {
    {{main_name}}_TB_instance.print();
}

extern "C" void step_{{main_name}}_TB(svOpenArrayHandle h) {
    {{main_name}}_TB_instance.step();
}

extern "C" int check_{{main_name}}_TB(svOpenArrayHandle h) {
    return {{main_name}}_TB_instance.check();
}

{% endif %}