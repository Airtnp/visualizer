"""
Generate testbench for collecting information
"""
import sys
import os
from copy import deepcopy
from jinja2 import Environment, FileSystemLoader
from visualizer.parser.verilog.parse import parse
from visualizer.parser.verilog.preprocess import preprocess
from visualizer.parser.verilog.analyze import get_variable, get_struct
from visualizer.parser.verilog.expr import SVFFIStruct, PackedStructArray, SVFFIUnion, PackedLogicArray, Logic

sys.setrecursionlimit(500)
dir_path = os.path.dirname(os.path.realpath(__file__))


env = Environment(
    loader=FileSystemLoader(os.path.join(dir_path, 'template'))
)


def gen_test(gen, gen_struct, scope, vinfo, entry=None):
    vcontext = {}
    vcontext['cfunc_def'] = []
    vcontext['has_main'] = True if entry else False
    vcontext['vfunc_def'] = []

    ccontext = {}
    ccontext['gen'] = []
    ccontext['has_main'] = True if entry else False

    for g in gen:
        ccontext['gen'].append(g.to_str())

    for g in gen_struct:
        vcontext['cfunc_def'].append(g.to_vfn_header())
        vcontext['vfunc_def'].append(g.to_vfn_task())

    in_member = []
    out_member = []
    in_name = []
    out_name = []
    has_clock = False

    if entry:
        vcontext['main_name'] = entry
        ccontext['main_name'] = entry
        for _, v in vinfo[entry]['variable'].items():
            if v['name'] == 'clock':
                has_clock = True
            if v['direction'] == 'input' and v['name'] != 'clock' and v['name'] != 'reset':
                in_name.append({
                    'name': v['name'], 'comma': ','
                })
                if v['type'] == 'logic':
                    if v['packed_dim'] == []:
                        in_member.append(Logic(''))
                    else:
                        r = v['packed_dim'][0].left.expr.value
                        in_member.append(PackedLogicArray('', r + 1))
                else:
                    tyname = v['type']
                    if v['type'].startswith('$unit::'):
                        tyname = tyname[7:]
                    t = deepcopy(scope[tyname])
                    if v['packed_dim'] == []:
                        in_member.append(t)
                    else:
                        r = v['packed_dim'][0].left.expr.value
                        in_member.append(PackedStructArray('', t, r + 1))
                in_member[-1].m_name = v['name']
            if v['direction'] == 'output':
                out_name.append({
                    'name': v['name'], 'comma': ','
                })
                if v['type'] == 'logic':
                    if v['packed_dim'] == []:
                        out_member.append(Logic(v['name']))
                    else:
                        r = v['packed_dim'][0].left.expr.value
                        out_member.append(PackedLogicArray('', r + 1))
                else:
                    tyname = v['type']
                    if v['type'].startswith('$unit::'):
                        tyname = tyname[7:]
                    t = deepcopy(scope[tyname])
                    if v['packed_dim'] == []:
                        out_member.append(t)
                    else:
                        r = v['packed_dim'][0].left.expr.value
                        out_member.append(PackedStructArray('', t, r + 1))
                out_member[-1].m_name = v['name']
        out_name[-1]['comma'] = ''
        vcontext['main_in_name'] = in_name
        vcontext['main_out_name'] = out_name
        vcontext['main_in'] = []
        vcontext['main_out'] = []
        for m in in_member:
            vcontext['main_in'].append(
                m.to_vtype() + ' ' + m.m_name + ';'
            )
        for m in out_member:
            vcontext['main_out'].append(
                m.to_vtype() + ' ' + m.m_name + ';'
            )

        in_g = SVFFIStruct(entry + '_IN_P', True, *in_member)
        out_g = SVFFIStruct(entry + '_OUT_P', True, *out_member)
        ccontext['gen'].append(
            in_g.to_str()
        )
        ccontext['gen'].append(
            out_g.to_str()
        )
        vcontext['main_in_cfunc'] = in_g.to_vfn_header()
        vcontext['main_out_cfunc'] = out_g.to_vfn_header()
        


    cppfile = env.get_template('test.cpp').render(ccontext)
    vfile = env.get_template('testbench.sv').render(vcontext)

    return cppfile, vfile


def collect_struct_member(entry, direction, s, parents):
    variables = []
    if isinstance(s, SVFFIStruct):
        for m in s.members:
            variables.extend(collect_struct_member(
                entry, direction,
                m, parents + [s.m_name]
            ))
    elif isinstance(s, PackedStructArray):
        for i in range(s.n):
            variables.extend(collect_struct_member(
                entry, direction,
                s.ty, parents + [s.m_name + '[{}]'.format(i)]
            ))
    elif isinstance(s, SVFFIUnion):
        # HACK: INST specialized
        variables.append({
            'module': entry,
            'width': 32,
            'hierarchy': '.'.join(parents + [s.m_name]),
            'add_syn_guard': direction == 'module'
        })
    else:
        # TODO: only 1 dim support now.
        variables.append({
            'module': entry,
            'width': str(int(s.n)),
            'hierarchy': '.'.join(parents + [s.m_name]),
            'add_syn_guard': direction == 'module'
        })
        # HACK: for -> SVFFIAlias
        if variables[-1]['hierarchy'][-1] == '.':
            variables[-1]['hierarchy'] = variables[-1]['hierarchy'][:-1]
    return variables


def gen_vtb_helper(entry_ins, entry, scope, vinfo, parents):
    variables = []
    parents.append(entry_ins)
    for _, v in vinfo[entry]['variable'].items():
        if v['inner']:
            continue
        next_entry = v['name']
        if not v['inloop'] or v['label'] == []:
            v['repeat'] = 1
        for i in range(v['repeat']):
            if v['inloop'] and v['label'] != []:
                next_entry = '.'.join(v['label']) + '[{}].'.format(i) + v['name']
            if 'is_module' in v.keys():
                # FIXME: ignore module with parameter overwritten now.
                # Hopefully, only selectors
                if v['param_map'] != {}:
                    continue
                # TODO: only support 1 dim now
                n = 1
                if next_entry in parents or v['name'] in parents:
                    continue
                if v['packed_dim'] != []:
                    if len(v['packed_dim']) != 1:
                        continue
                    r = v['packed_dim'][0]
                    r.left.expr.eval()
                    r.right.expr.eval()
                    n = int(r.left.expr.value - r.right.expr.value + 1)
                for j in range(n):
                    next_entry_module = next_entry
                    if v['packed_dim'] != []:
                        next_entry_module = next_entry + '[{}]'.format(j)
                    if next_entry_module in parents:
                        continue
                    if v['is_module']:
                        # ignore recursive
                        variables.extend(gen_vtb_helper(
                            next_entry_module,
                            v['type'],
                            scope,
                            vinfo,
                            parents[:]
                        ))
                    else:
                        type_name = v['type']
                        if type_name.startswith('$unit::'):
                            type_name = type_name[7:]
                        if type_name == 'genvar':
                            continue
                        ty = deepcopy(scope[type_name])
                        ty.m_name = next_entry_module
                        variables.extend(collect_struct_member(
                            entry, v['direction'],
                            ty, parents[:]
                        ))
            else:
                width = []
                if v['packed_dim'] == []:
                    width = '1'
                else:
                    for w in v['packed_dim']:
                        width.append(str(int(w.left.expr.value - w.right.expr.value + 1)))
                variables.append({
                    'module': entry,
                    'width': '_'.join(width),
                    'hierarchy': '.'.join(parents + [next_entry]),
                    'add_syn_guard': v['direction'] == 'module'
                })
    return variables


def gen_vtb(entry_ins, entry, scope, vinfo):
    variables = gen_vtb_helper(entry_ins, entry, scope, vinfo, [])
    return env.get_template('vtestbench.sv').render({
        'variable': variables
    }), variables


if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        text = preprocess(f.readlines())
        scan, info = parse(text)
        vinfo = get_variable(info)
        scope, gen, gen_struct = get_struct(info)
        scope['integer'] = PackedLogicArray('integer', 32)
        name = 'pipeline' if len(sys.argv) < 3 else sys.argv[2]
        import ipdb;
        ipdb.set_trace()
        vtb, variables = gen_vtb('core', name, scope, vinfo)
        cppfile, vfile = gen_test(gen, gen_struct, scope, vinfo, name)
        with open('./test.cpp', 'w+') as c:
            c.write(cppfile)
        with open('./testbench.sv', 'w+') as v:
            v.write(vfile)
        with open('./vtestbench.sv', 'w+') as v:
            v.write(vtb)
