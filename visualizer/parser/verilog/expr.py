"""
Describe expressions.
"""
from math import log2, floor
from visualizer.parser.verilog.svast import Expr


Expr.vexpr = property(lambda self: self.expr)


class SymBase:
    def __init__(self):
        self.value = 0
        self.variable = {}
        self.unknown = True
        self.symbolic = True
    def __repr__(self):
        return self.__class__.__name__ + '(' + repr(self.__dict__) + ')'
    def eval(self):
        pass
    @property
    def concrete(self):
        return not self.unknown and not self.symbolic
    @property
    def vexpr(self):
        return self


class SymValue(SymBase):
    def __init__(self, value, is_native = False):
        super().__init__()
        self.svalue = value
        self.is_native = is_native
        self.unknown = self.svalue.has_unknown if not is_native else False
        self.symbolic = False
    def eval(self):
        if not self.unknown:
            if self.is_native:
                self.value = self.svalue
            else:
                self.value = self.svalue.value


STrue = SymValue(1, True)
SFalse = SymValue(0, True)


class Sym(SymBase):
    def __init__(self, name, module):
        super().__init__()
        self.name = name
        self.hi = name.split('.')
        self.mod = module
    def eval(self):
        # FIXME: parameter might be overwritten
        if False and self.name in self.mod.local.keys():
            self.value = self.mod.local[self.name]
            self.is_unknown = False
            self.symbolic = False


class UnaryOp(SymBase):
    def __init__(self, sym):
        super().__init__()
        self.sym = sym.vexpr


class BitNot(UnaryOp):
    pass


class Neg(UnaryOp):
    pass


class Pos(UnaryOp):
    pass


class Not(UnaryOp):
    pass


class PostAdd(UnaryOp):
    pass


class PostNeg(UnaryOp):
    pass


class AndBit(UnaryOp):
    pass


class OrBit(UnaryOp):
    pass


class Log2(UnaryOp):
    def eval(self):
        self.sym.eval()
        self.unknown = self.sym.unknown
        self.symbolic = self.sym.symbolic
        if self.sym.concrete:
            self.value = int(floor(log2(self.sym.value)))


class BinOp(SymBase):
    def __init__(self, lsym, rsym):
        super().__init__()
        self.lsym = lsym.vexpr
        self.rsym = rsym.vexpr
    def eval_bin(self, fn):
        self.lsym.eval()
        self.rsym.eval()
        self.unknown = self.lsym.unknown or self.rsym.unknown
        self.symbolic = self.lsym.symbolic or self.rsym.symbolic
        if self.lsym.concrete and self.rsym.concrete:
            self.value = fn(self.lsym.value, self.rsym.value)


class Add(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x + y)


class Sub(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x - y)


class Mul(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x * y)


class Div(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x / y)


class Pow(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x ** y)


class Mod(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x % y)


class And(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x and y)


class Or(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x or y)


class BitAnd(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x & y)


class BitOr(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x | y)


class NAnd(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: ~(x & y))


class NOr(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: ~(x | y))


class Xor(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x ^ y)


class XNor(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: ~(x ^ y))


class LLShift(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x << y)


class ALShift(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x << y)


class LRShift(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: (x % 0x100000000) >> y)


class ARShift(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x >> y)


class GE(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x >= y)


class GT(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x > y)


class LE(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x <= y)


class LT(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x < y)


class EQ(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x == y)


class NE(BinOp):
    def eval(self):
        self.eval_bin(lambda x, y: x != y)


class Dot(BinOp):
    pass


class TernaryOp(SymBase):
    def __init__(self, cond, lsym, rsym):
        super().__init__()
        self.cond = cond
        self.lsym = lsym
        self.rsym = rsym


class SymConcat(SymBase):
    def __init__(self, *args):
        super().__init__()
        self.syms = args


class Repeat(SymBase):
    def __init__(self, num, expr):
        super().__init__()
        self.num = num
        self.expr = expr


class Slice(SymBase):
    def __init__(self, expr, slice_):
        super().__init__()
        self.expr = expr
        self.slice = slice_


class Cast(SymBase):
    def __init__(self, type_, expr):
        super().__init__()
        self.type = type_
        self.expr = expr


class SVFFIBase:
    pass


class Logic(SVFFIBase):
    def __init__(self, name):
        self.name = name
        self.m_name = ''
        self.n = 1
        self.need_dup = True
    def to_str(self):
        return 'Logic {};\n'.format(self.name)
    def to_type(self):
        return 'Logic'
    def to_ctype(self):
        return 'bool'
    def to_value(self):
        return '.as_bool()'
    def to_vtype(self):
        return 'logic'


class SVFFIAlias(SVFFIBase):
    def __init__(self, name, ty):
        self.name = name
        self.ty = ty
        self.m_name = ''
    def to_str(self):
        return 'using {} = {};\n'.format(self.name, self.ty.to_type())
    def to_vtype(self):
        return self.name


class PackedLogicArray(SVFFIBase):
    def __init__(self, name, n):
        self.name = name
        self.n = n
        self.m_name = ''
        self.need_dup = True
        self.is_alias = False
        self.alias_name = ''
    def to_str(self):
        return 'PackedLogicArray<{}> {};\n'.format(self.n, self.name)
    def to_type(self):
        return 'PackedLogicArray<{}>'.format(self.n)
    def to_ctype(self):
        if self.n <= 64:
            return 'uint64_t'
        else:
            return 'uint64_t[{}]'.format(self.n / 64)
    def to_value(self):
        if self.n <= 32:
            return '.as_int()'
        elif self.n <= 64:
            return '.as_long()'
        else:
            return '.error()'
    def to_vtype(self):
        if self.is_alias:
            return self.alias_name
        return 'logic [{}:0]'.format(self.n - 1)


class PackedStructArray(SVFFIBase):
    def __init__(self, name, ty, n):
        self.name = name
        self.ty = ty
        self.n = n
        self.m_name = ''
        self.need_dup = False
    def to_str(self):
        return 'PackedStructArray<{}, {}> {};\n'.format(self.ty.to_type(), self.n, self.name)
    def to_type(self):
        return 'PackedStructArray<{}, {}>'.format(self.ty.to_type(), self.n)
    def to_vtype(self):
        return '{} [{}:0]'.format(self.ty.to_vtype(), self.n - 1)


# TODO: rewrite this to jinja2 template.
class SVFFIStruct(SVFFIBase):
    def __init__(self, name, is_packed, *args):
        self.name = name
        self.is_packed = 'true' if is_packed else 'false'
        self.members = args
        self.m_name = ''
        self.need_dup = False
    def to_vfn_header(self):
        if self.name.startswith('struct__'):
            return ''
        f_str = """
import "DPI-C" context function void read_{0}({0} i);
import "DPI-C" context function void check_{0}({0} i);
import "DPI-C" context function void step_{0}({0} i);
import "DPI-C" context function void print_{0}({0} i);
""".format(self.name)
        return f_str

    def to_vfn_task(self):
        if self.name.startswith('struct__'):
            return ''
        for m in self.members:
            # TODO: struct member is not supported.
            # Maybe change this to '0?
            if isinstance(m, SVFFIStruct) or isinstance(m, PackedStructArray):
                return ''

        f_str = """
\ttask fill_{};
\t\toutput {} p;
\t\tinput longint {};\n""".format(
            self.name, self.name, 
            ', '.join(list(map(lambda s: s + '_', self.names)))
        )
        # TODO: not support struct type
        for m in self.members:
            f_str += '\t\t' + 'p.{} = {};\n'.format(m.m_name, m.m_name + '_')
        f_str += '\tendtask\n'

        f_str += """
\ttask clear_{};
\t\toutput {} p;\n""".format(
            self.name, self.name, 
        )
        # TODO: not support struct type
        for m in self.members:
            f_str += '\t\t' + 'p.{} = 0;\n'.format(m.m_name)
        f_str += '\tendtask\n'

        return f_str
                      
    def to_cfn(self):
        f_str = """
{0} {0}_instance;

extern "C" void read_{0} (svOpenArrayHandle h) {{
    read({0}_instance, h);
}}

extern "C" int check_{0} () {{
    {0}_instance.check();
}}

extern "C" void step_{0} () {{
    {0}_instance.step();
}}

extern "C" void print_{0} () {{
    {0}_instance.print();
}}
""".format(self.name)
        return f_str

    def repr_str(self, str):
        return "\"{}\"".format(str)

    def to_str(self):
        s_str = ''
        header = 'struct {} {{\n'.format(self.name)
        s_str += header
        self.names = []
        for m in self.members:
            s_str += '\t' + m.to_type() + ' {};\n'.format(m.m_name)
            self.names.append(m.m_name)
            if m.need_dup:
                s_str += '\t' + m.to_ctype() + ' {}_;\n'.format(m.m_name)

        s_str += '\tvoid step() {\n\n\t}'
        s_str += """
\tbool check(size_t indent=0) {
\t\tbool success = true;\n"""
        for m in self.members:
            if isinstance(m, PackedStructArray):
                for i in range(m.n):
                    s_str += '\t\tprint_header(indent, {}, {});\n'.format(self.repr_str(self.name), self.repr_str(m.m_name + '[{}]'.format(i)))
                    s_str += '\t\tsuccess &= {}[{}].check(indent + 1);\n'.format(m.m_name, i)
            elif isinstance(m, SVFFIStruct):
                s_str += '\t\tprint_header(indent, {}, {});\n'.format(
                    self.repr_str(self.name), self.repr_str(m.m_name))
                s_str += "\t\tsuccess &= {}.check(indent + 1);\n".format(m.m_name)
            else:
                s_str += '\t\tsuccess &= print_assert(indent, {}, {}, {}, {});\n'.format(
                    self.repr_str(self.name),
                    self.repr_str(m.m_name),
                    m.m_name + '_', m.m_name + m.to_value())
        s_str += '\t\treturn success;\n'
        s_str += '\t}'

        s_str += """
\tvoid print(size_t indent=0) {
"""
        for m in self.members:
            if isinstance(m, PackedStructArray):
                for i in range(m.n):
                    s_str += '\t\tprint_header(indent, {}, {});\n'.format(
                        self.repr_str(self.name), self.repr_str(m.m_name + '[{}]'.format(i)))
                    s_str += '\t\t{}[{}].print(indent + 1);\n'.format(m.m_name, i)
            elif isinstance(m, SVFFIStruct):
                s_str += '\t\tprint_header(indent, {}, {});\n'.format(
                    self.repr_str(self.name), self.repr_str(m.m_name))
                s_str += "\t\t{}.print(indent + 1);\n".format(m.m_name)
            else:
                s_str += '\t\tprint_info(indent, {}, {}, {}, {});\n'.format(
                    self.repr_str(self.name),
                    self.repr_str(m.m_name),
                    m.m_name + '_', m.m_name + m.to_value())
        s_str += '\t}\n'

        end = '};\n'
        reflect = 'SN_REFLECTION({}, {}, {});\n'.format(
            self.name, self.is_packed, ', '.join(self.names))
        s_str += end
        s_str += reflect
        s_str += self.to_cfn()
        return s_str
    def to_type(self):
        return self.name
    def to_vtype(self):
        return self.name


class SVFFIUnion(SVFFIBase):
    def __init__(self, name):
        self.name = name
        self.m_name = ''
        self.need_dup = True
    def to_str(self):
        return ''
    def to_type(self):
        return self.name
    def to_ctype(self):
        return 'uint32_t'
    def to_value(self):
        return '.as_int()'
    def to_vtype(self):
        return self.name
