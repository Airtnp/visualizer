"""
Analyze the info from parsing.
"""
import sys
from copy import deepcopy
from collections import defaultdict, OrderedDict
from visualizer.parser.verilog.parse import parse
from visualizer.parser.verilog.preprocess import preprocess
from visualizer.parser.verilog.svast import (
    TypeExpr, StructExpr, EnumExpr,
    UnionExpr, TypedefStmt
)
from visualizer.parser.verilog.expr import (
    Logic, SVFFIAlias, PackedLogicArray, PackedStructArray,
    SVFFIStruct, SVFFIUnion
)


def eval_type(ty, scope):
    t = None
    if isinstance(ty, TypeExpr) or isinstance(ty, EnumExpr):
        # Always Logic here
        if ty.type == 'logic':
            t = Logic('')
        else:
            t = deepcopy(scope[ty.type.node])
        if ty.range:
            ty.range.left.expr.eval()
            r = ty.range.left.expr.value
            if ty.type == 'logic':
                t = PackedLogicArray('', r + 1)
            else:
                t = PackedStructArray('', t, r + 1)
    elif isinstance(ty, StructExpr):
        is_packed = ty.packed
        members = []
        for name, mty in ty.member.items():
            members.append(eval_type(mty, scope))
            members[-1].m_name = name
        t = SVFFIStruct('', is_packed, *members)
    else:
        # Do nothing to union now.
        pass
    return t
    

def get_struct(info):
    scope = OrderedDict()
    gen = []
    gen_struct = []
    for s in info['TypedefStmt']:
        name = s.name
        ty = s.type
        if isinstance(ty, TypeExpr):
            aty = eval_type(ty, scope)
            scope[name] = aty
            aty.is_alias = True
            aty.alias_name = name
            gen.append(SVFFIAlias(name, aty))
        elif isinstance(ty, StructExpr):
            aty = eval_type(ty, scope)
            scope[name] = aty
            aty.name = name
            gen.append(aty)
            gen_struct.append(aty)
        elif isinstance(ty, EnumExpr):
            aty = eval_type(ty, scope) 
            scope[name] = aty
            gen.append(SVFFIAlias(name, aty))
        elif isinstance(ty, UnionExpr):
            scope[name] = SVFFIUnion(name)
    return scope, gen, gen_struct


def get_variable(info):
    vinfo = defaultdict(
        lambda: {
            'parameter': defaultdict(dict),
            'variable': defaultdict(dict),
        }
    )
    for pblock in info['ParameterBlock']:
        for k, v in pblock.params.items():
            vinfo[pblock.module]['parameter'][k] = {
                'name': k,
                'value': v
            }
    for p in info['ParameterStmt']:
        vinfo[p.module]['parameter'][p.name] = {
            'name': p.name,
            'value': p.value,
        }
    for a in info['ArgumentDecl'] + info['VariableSingleDecl']:
        for d in a.packed_dim:
            d.left.expr.eval()
            d.right.expr.eval()
            if not d.left.expr.concrete:
                print("Unable to evaluate", d.left.expr)
            if not d.right.expr.concrete:
                print("Unable to evaluate", d.right.expr)
        ainfo = {
            'name': a.name,
            'direction': a.direction,
            'type': a.type,
            'signed': a.signed,
            'packed_dim': a.packed_dim,
            'unpacked_dim': a.unpacked_dim,
            'value': a.value,
            'inner': False,
            'inloop': False,
        }
        if 'inner' in a.__dict__:
            ainfo['inner'] = True
        if 'inloop' in a.__dict__:
            ainfo['inloop'] = True
            ainfo['repeat'] = a.repeat
        if hasattr(a, 'inloop') and hasattr(a, 'label'):
            ainfo['label'] = a.label
        # variable within module will not have direction
        if ainfo['direction'] is None:
            ainfo['direction'] = 'module'
        for k, v in ainfo.items():
            if k == 'direction':
                if k in vinfo[a.module]['variable'][a.name].keys():
                    if v == 'module' and vinfo[a.module]['variable'][a.name][k] != 'module':
                        continue
            if v is not None:
                vinfo[a.module]['variable'][a.name][k] = v
    for m in info['ModuleUseStmt']:
        if not m.is_assign:
            ainfo = {
                'name': m.name,
                'type': m.type,
                'is_module': not m.is_decl,
                'packed_dim': m.packed_dim,
                'unpacked_dim': m.unpacked_dim,
                'direction': 'module',
                'inner': False,
                'inloop': False,
                'param_map': m.param_map,
                'port_map': m.port_map,
            }
            if 'inner' in m.__dict__:
                ainfo['inner'] = True
            if 'inloop' in m.__dict__:
                ainfo['inloop'] = True
                ainfo['repeat'] = m.repeat
            if hasattr(m, 'inloop') and hasattr(m, 'label'):
                ainfo['label'] = m.label
            for k, v in ainfo.items():
                if k == 'direction':
                    if k in vinfo[m.module]['variable'][m.name].keys():
                        if v == 'module' and vinfo[m.module]['variable'][m.name][k] != 'module':
                            continue
                if v is not None:
                    vinfo[m.module]['variable'][m.name][k] = v
    return vinfo


if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        text = preprocess(f.readlines())
        scan, info = parse(text)
        vinfo = get_variable(info)
        import ipdb;
        ipdb.set_trace()
