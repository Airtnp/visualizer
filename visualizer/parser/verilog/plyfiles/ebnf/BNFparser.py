"""
BNF syntax parser
"""
from visualizer.parser.verilog.plyfiles.meta.parser import ParserGenerator


# Unused, since meta.ast only uses ParserGenerator(None, None)
BNFParser = ParserGenerator
