"""
BNF generator.

BNF -> lexer
BNF -> parser (useless, we only need ParserGenerator)
BNf -> ast
"""
from copy import deepcopy
from visualizer.parser.verilog.plyfiles.ebnf.BNFlexer import (
    collect_ops, collect_terms, BNFLexer, rearrange_ops
)
from visualizer.parser.verilog.plyfiles.ebnf.BNFast import BNFDecl


def escape_ops_no_slash(op):
    escapes = r"""[]{}()*+?.,&|^$#-/'"""
    for e in escapes:
        op = op.replace(e, r'\{}'.format(e))
    return op


def info_from_ebnf(
    text, astnode_name = 'GenAstNode',
    op_name_suggestion = {},
    base_indent = '    ', special_kw_token_dict = {}
):
    """
    Special Keyword Token Dict denotes
        identifier -> IDENTIFIER
    where
        IDENTIFIER is defined in lexer as special rule
        identifier is defined in ebnf and its rule is like 0-9 A-Z, not actual EBNF
    """
    op_dict = collect_ops(text, op_name_suggestion)

    rules = ')|('.join(op_dict.keys())
    rules = '(' + rules + ')'
    bnf_oprule = {
        rules: ['BNFOP']
    }

    lexer = BNFLexer(bnf_oprule, {}, [])
    tokens, ids, nonterms, terms, precs = lexer.run(text)

    kw_dict = collect_terms(op_dict, terms, {})
    kw_dict.update(special_kw_token_dict)

    lexer = BNFLexer(bnf_oprule, {}, [])
    lexer.build()

    global BNFDecl
    BNFDecl = BNFDecl.rescan()
    BNFDecl.parser.precedence = (
        ('left', 'BNFALT'),
        ('nonassoc', 'BNFOP'),
        ('left', 'BNFOPTL', 'BNFOPTR'),
        ('left', 'BNFSTARL', 'BNFSTARR'),
        ('nonassoc', 'BNFEND', 'BNFDEFINE')
    )

    BNFDecl.set_lexer(lexer.lexer)
    p = BNFDecl.parse(text)
    ast_define = p.generate_ast(astnode_name, op_dict, kw_dict, base_indent)

    return ast_define, p.convert_op_dict(op_dict), kw_dict, precs


def output_ast(ast_define):
    return '\n'.join([d[0] for d in ast_define])


def output_lexer(
    lexer_name,
    op_dict, kw_dict, states = [],
    base_indent = '    ', special_kw_dict = {}):
    """
    Special keyword dict denotes extra key to lexer
        r'regex': 'IDENTIFIER'
    """

    new_op_dict = deepcopy(special_kw_dict)
    new_op_dict.update(op_dict)
    # by design, disallow rearrange here.
    # new_op_dict = rearrange_ops(new_op_dict)

    lexer_header = """from collections import OrderedDict
from visualizer.parser.verilog.plyfiles.rules import *
from visualizer.parser.verilog.plyfiles.meta.lexer import LexerGenerator
from visualizer.parser.verilog.plyfiles.meta.default import *
"""

    lexer_main_1 = """
op_dict = OrderedDict([
"""


    for k, v in new_op_dict.items():
        lexer_main_1 += base_indent
        if isinstance(v, list) and len(v) >= 3 and v[2]:
            lexer_main_1 += '({}, '.format(repr(k))
        else:
            lexer_main_1 += '({}, '.format(repr(escape_ops_no_slash(k)))
        l = deepcopy(v)
        if isinstance(l, list) and len(l) >= 2 and hasattr(l[1], '__call__'):
            l[1] = l[1].__name__
            lexer_main_1 += '[\'{}\', {}]),'.format(l[0], l[1])
        else:
            lexer_main_1 += '{}),'.format(l)
        lexer_main_1 += '\n'

    lexer_main_2 = """
])
kw_dict = {
"""

    special_kw_names = []
    for k, v in special_kw_dict.items():
        if isinstance(v, str):
            special_kw_names.append(v)
        else:
            special_kw_names.append(v[0])

    for k, v in kw_dict.items():
        name = v
        if name in special_kw_names:
            # specially defined A -> B in op_dict
            continue
        lexer_main_2 += base_indent
        lexer_main_2 += '{}'.format(repr(k))
        lexer_main_2 += ': \'{}\','.format(v)
        lexer_main_2 += '\n'

    lexer_main_3 = """
}}
states = {}
{} = lambda: LexerGenerator(op_dict, kw_dict, states)
""".format(states, lexer_name)

    return '\n'.join([lexer_header, lexer_main_1, lexer_main_2, lexer_main_3]), new_op_dict
