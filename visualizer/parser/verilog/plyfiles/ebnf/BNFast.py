"""
BNF syntax AST.
"""
from collections import OrderedDict
from visualizer.parser.verilog.plyfiles.meta.ast import *


class BNFAstNode(AstNode):
    """
    BNFAstNode base.
    Need to define is_base (avoid fields check) and op_map (support str -> op)
    """
    is_base = True
    op_map = {
        r'{': 'BNFSTARL',
        r'}': 'BNFSTARR',
        r'[': 'BNFOPTL',
        r']': 'BNFOPTR',
        r'::=': 'BNFDEFINE',
        r'|': 'BNFALT',
        r';': 'BNFEND',
    }


class BNFIdentifier(BNFAstNode):
    """
    BNFIdentifier ::=
        BNFIDENTIFIER
    """
    fields = Token('BNFIDENTIFIER')


class BNFLiteral(BNFAstNode):
    """
    BNFLiteral ::=
        BNFIDENTIFIER
        | BNFOP
        # | BNFNUMBER
    """
    fields = (
        BNFIdentifier,
        Token('BNFOP'),
        # Token('BNFNUMBER'),        
    )


class BNFExpr(BNFAstNode):
    """
    BNFExpr ::=
        BNFList
        | BNFList BNFALT BNFExpr
    """
    fields = []

class BNFOpt(BNFAstNode):
    """
    BNFOpt ::=
        BNFOPTL BNFExpr BNFOPTR
    """
    fields = ['[', BNFExpr, ']']


class BNFStar(BNFAstNode):
    """
    BNFStar ::=
        BNFSTARL BNFExpr BNFSTARR
    """
    fields = ['{', BNFExpr, '}']


class BNFItem(BNFAstNode):
    """
    BNFItem ::=
        BNFLiteral
        | BNFOpt
        | BNFStar
    """
    fields = (
        BNFLiteral,
        BNFOpt,
        BNFStar,
    )


class BNFItemPrec(BNFAstNode):
    """
    BNFItemPrec ::=
        BNFItem BNFPREC?
    """
    fields = [BNFItem, Optional(Token('BNFPREC'))]


class BNFList(BNFAstNode):
    """
    BNFList ::=
        BNFItemPrec*
    """
    fields = Star(BNFItemPrec)


BNFExpr.fields = Plus(BNFList, postfix='BNFALT')
BNFExpr = BNFExpr.rescan()


class BNFDefine(BNFAstNode):
    """
    BNFDefine ::=
        BNFIDENTIFIER ::= BNFExpr BNFEND
    ;
    """
    fields = [
        Token('BNFIDENTIFIER'),
        Operator('::='),
        BNFExpr,
        Operator(';')
    ]


class BNFDecl(AstNode):
    """
    BNFDecl ::=
        BNFDefine+
    ;
    """
    fields = Plus(BNFDefine)

    def convert_camelcase(self, snake):
        return snake.title().replace('_', '')

    def convert_unquote_escape(self, quote, escape_quote = True):
        if quote == '\\':
            return r'\\'
        if len(quote) >= 3 and quote[0] == '\'' and quote[-1] == '\'':
            quote = quote[1:-1]
        if escape_quote:
            quote = quote.replace('\'', r'\'')
        return quote

    def convert_op_dict(self, op_dict):
        new_op_dict = OrderedDict()
        for attr in op_dict.values():
            new_op_dict[self.convert_unquote_escape(attr[1], escape_quote=False)] = [attr[0]]
        return new_op_dict

    def generate_docstring(self, classname, fields, basenode):
        """
        Nonrecursively generate rules.
        Basically, generate a class inherited to basenode.
        The generated cls is not so useful because of the incomplete child
        defintions.
        """
        dct = {}
        dct['fields'] = fields
        dct['is_rec'] = False
        cls = type(classname, basenode.__mro__, dct)
        return cls.collect_rules(), cls

    def generate_fields(self, node):
        """
        Generate fields from BNF{Expr, Item, List, Literal, Identifier, Star, Opt}
        str will be id, BNFDefine and BNFDecl can't appear here.
        """
        if isinstance(node, BNFExpr):
            if len(node.node) == 0:
                return 'empty'
            elif len(node.node) == 1:
                return self.generate_fields(node.node[0])
            else:
                return Alt(*[self.generate_fields(n) for n in node.node])
        elif isinstance(node, BNFList):
            if len(node.node) == 0:
                return 'empty'
            elif len(node.node) == 1:
                return self.generate_fields(node.node[0])
            else:
                return Concat(*[self.generate_fields(n) for n in node.node])
        elif isinstance(node, BNFItemPrec):
            if node.node[1] is not None:
                prec = node.node[1]
                return Concat(self.generate_fields(node.node[0]), Token('%prec ' + prec[1:]), force_inline=True)
            return self.generate_fields(node.node[0])
        elif isinstance(node, BNFItem):
            return self.generate_fields(node.node)
        elif isinstance(node, BNFLiteral):
            return self.generate_fields(node.node)
        elif isinstance(node, BNFIdentifier):
            if node.node == '_':
                return Operator('_')
            if node.node not in self.kw_dict.keys():
                self.dependency[self.current_rawname].append(node.node)
                return NonTerm(self.convert_camelcase(node.node))
            # Else, it's a terminal and should not be touched.
            return Token(self.kw_dict[node.node])
        elif isinstance(node, BNFStar):
            return Star(self.generate_fields(node.node[1]))
        elif isinstance(node, BNFOpt):
            return Optional(self.generate_fields(node.node[1]))
        elif isinstance(node, str):
            real_node = self.convert_unquote_escape(node)            
            if node in list(map(lambda s: s[1], self.op_dict.values())):
                return Operator(real_node)
            # Should be impossible
            import ipdb; ipdb.set_trace()
        else:
            # Should be impossible
            import ipdb; ipdb.set_trace()

    def generate_ast(self, basenode_name, op_dict, kw_dict, base_indent = '\t'):
        """
        From EBNF syntax to Python AstNodes,
        This can be generated by runtime, but to file can be more clear.
        BaseNodeName should be user-defined.
        """

        header = """from visualizer.parser.verilog.plyfiles.meta.ast import *
"""

        self.op_dict = op_dict
        self.kw_dict = kw_dict
        self.scanned_rule = {}
        self.dependency = {}

        basenode_template_header = """
class {}(AstNode):
""" + base_indent + """is_base = True
""" + base_indent + """op_map = """

        basenode_template_end = """
""" + base_indent + """}
"""

        basenode_def = basenode_template_header.format(basenode_name)
        basenode_def += '{\n'
        basenode = type(basenode_name, AstNode.__mro__, {'is_base': True})
        basenode.op_map = {}
        for op, attr in op_dict.items():
            basenode_def += base_indent * 2
            basenode_def += '\'{}\''.format(str(self.convert_unquote_escape(attr[1])))
            basenode_def += ': \'{}\','.format(attr[0])
            basenode_def += '\n'
            basenode.op_map[self.convert_unquote_escape(attr[1])] = attr[0]
        basenode_def += basenode_template_end

        class_template = """
class {}({}):
""" + base_indent + """{}
""" + base_indent + """fields = {}
"""

        class_decl_template = """
class {}({}):
""" + base_indent + """{}
""" + base_indent + """fields = []
"""

        class_def_template = """
{}.fields = {}
# {}.rescan()
"""

        defines = [(header, None), (basenode_def, basenode)]


        for define in self.node:
            rawname = define.node[0]
            classname = self.convert_camelcase(rawname)
            self.dependency[rawname] = []
            self.current_rawname = rawname
            fields = self.generate_fields(define.node[2])

            syntax, cls = self.generate_docstring(classname, fields, basenode)
            docstring = "\"\"\"\n" \
                        + base_indent \
                        + "{}\n" \
                        + base_indent \
                        + "\"\"\""
            sep = '\n' + base_indent
            docstring = docstring.format(
                sep.join(syntax.split('\n'))
            )
            cls.__doc__ = docstring

            self.scanned_rule[rawname] = [False, classname, fields, docstring, cls]
            # For first generation, self-reference dependency will forbid generation
            if all([
                self.scanned_rule.get(dependency, False)
                for dependency in self.dependency[rawname]
            ]) and rawname not in self.dependency[rawname]:
                defines.append((
                    class_template.format(
                        classname, basenode_name, docstring, fields),
                    cls,
                ))
                self.scanned_rule[rawname][0] = True
            else:
                defines.append((
                    class_decl_template.format(
                        classname, basenode_name, docstring
                    ),
                    self.scanned_rule[rawname][4]
                ))

            # A simple n^2 might be enough for scanning rules...
            for name, attrs in self.scanned_rule.items():
                if not attrs[0]:
                    if all([
                        (self.scanned_rule.get(dependency, False)
                        or dependency == name)
                        for dependency in self.dependency[name]
                    ]):
                        defines.append((
                            class_def_template.format(
                                attrs[1],
                                attrs[2],
                                attrs[1],
                            ),
                            attrs[4]
                        ))
                        attrs[0] = True

        return defines
