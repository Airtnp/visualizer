"""
BNF syntax paser using Ply.
Specially, the BNF syntax used by SystemVerilog Annex,
No grouping, string, special sequence
"""
from copy import deepcopy
from collections import OrderedDict
from ply.lex import lex, TOKEN
from visualizer.parser.verilog.plyfiles.meta.lexer import LexerGenerator
from visualizer.parser.verilog.plyfiles.meta.default import (
    default_error_func,
    default_empty_rule,
    default_token_rule
)


def rearrange_ops(ops):
    op_dict = OrderedDict()
    for key in sorted(ops.keys(), key=lambda s: (len(s), s), reverse=True):
        op_dict[key] = ops[key]
    return op_dict


def escape_ops(op):
    escapes = r"""\[]{}()*+?.,&|^$#-/'"""
    for e in escapes:
        op = op.replace(e, r'\{}'.format(e))
    return op


def collect_ops(text, name_map):
    """
    Collect seen operators in given ebnf.
    The problem is, ply lexer parse by regex length,
    so <identifier><sign> will considered as <identifier> <sign>

    Also, it should return a ordered dict ordered by regex length
    It could avoid conflicts like ';'|'|;
    """

    def remove_identifier_start(sep):
        if sep == '_':
            return '_'
        for idx in range(len(sep)):
            c = sep[idx]
            if c.isalnum() or c == '_':
                continue
            else:
                return sep[idx:]
        return ''

    ops = {}
    known_op = [r'{', r'}', r'[', r']', r'|', r'::=', r';']
    
    for sep in text.split():
        sep = remove_identifier_start(sep)
        check_alnum = lambda s: s.isalnum() or s == '_'
        if sep.startswith('%'):
            continue
        if (
            (not all(map(check_alnum, sep)) or sep == '_')
            and not sep in known_op
        ):
            known_op.append(sep)
            opname = ''
            unquote_sep = sep
            if len(sep) >= 3 and sep[0] == '\'' and sep[-1] == '\'':
                unquote_sep = sep[1:-1]
            if unquote_sep in name_map.keys():
                opname = name_map[unquote_sep]
            else:
                ascii_map = ''
                for c in sep:
                    ascii_map += chr(ord(c) // 26 + ord('A'))
                    ascii_map += chr(ord(c) % 26 + ord('A'))
                opname = 'GENTOK_{}'.format(ascii_map)
            escaped_op = escape_ops(sep)
            ops[escaped_op] = [opname, sep]
    op_dict = rearrange_ops(ops)
    return op_dict


def collect_terms(op_dict, terms, name_map):
    term_dict = {}
    known_terms = {}
    check_alnum = lambda s: s.isalnum() or s == '_'
    known_ops = list(map(lambda s: s[1], op_dict.values()))
    for term in terms:
        # dealt within collect_op
        # terminals like $root will considered first as an op
        if not all(map(check_alnum, term)) or term == '_':
            continue
        if term in known_ops:
            continue
        if term in name_map.keys():
            term_dict[term] = name_map
            known_terms[term.upper()] = 0
        else:
            if term.upper() in known_terms.keys():
                known_terms[term.upper()] += 1
                term_dict[term] = 'GENKW_{}{}'.format(
                    term.upper(), known_terms[term.upper()])
            else:
                known_terms[term.upper()] = 0
                term_dict[term] = 'GENKW_{}'.format(term.upper())
    return term_dict


class BNFLexer:
    def __init__(self, op_dict, kw_dict, states, error_func = default_error_func):
        tokens_dict, states = self.override_bnf_symbols(op_dict, states)
        self.lexer = LexerGenerator(tokens_dict, kw_dict, states, error_func)

    @staticmethod
    def bnf_identifier(obj, t):
        obj.bnf_ids.append(t.value)
        t.type = obj.keyword_dict.get(t.value, 'BNFIDENTIFIER')
        return t

    @staticmethod
    def bnf_define(obj, t):
        obj.bnf_nonterm.append(obj.bnf_ids[-1])
        return t

    @staticmethod
    def bnf_prec(obj, t):
        obj.bnf_prec.append(t.value)
        return t

    def override_bnf_symbols(self, op_dict, states):
        tokens_dict = {
            r'\{': ['BNFSTARL'],
            r'\}': ['BNFSTARR'],
            r'\[': ['BNFOPTL'],
            r'\]': ['BNFOPTR'],
            r'::=': ['BNFDEFINE', BNFLexer.bnf_define],
            r'\|': ['BNFALT'],
            r';': ['BNFEND'],
            ' \t': ['ignore'],
            r'(([a-zA-Z_])([a-zA-Z_0-9])*)': ['BNFIDENTIFIER', BNFLexer.bnf_identifier],
            r'(\%[a-zA-Z_]*)': ['BNFPREC', BNFLexer.bnf_prec]
            # r'([0-9]+)': ['BNFNUMBER'],
        }
        tokens_dict.update(op_dict)
        states = states + [['bnf_ids', list], ['bnf_nonterm', list], ['bnf_prec', list]]
        return tokens_dict, states

    def build(self, **kwargs):
        self.lexer.build(**kwargs)

    def input(self, data):
        self.lexer.input(data)

    def token(self):
        return self.lexer.token()

    def run(self, data, **kwargs):
        """
        Actually non-terminal states can be generated from first run,
        then set into second run. No need for oracle keywords.
        """
        self.build()
        self.input(data)
        tokens = []
        while True:
            tok = self.token()
            if not tok:
                break
            tokens.append(tok)
        term = list(set(self.lexer.bnf_ids) - set(self.lexer.bnf_nonterm))
        return (
            tokens, self.lexer.bnf_ids, 
            self.lexer.bnf_nonterm, term,
            self.lexer.bnf_prec
        )

