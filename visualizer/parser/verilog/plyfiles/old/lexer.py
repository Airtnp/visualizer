"""
SystemVerilog Lexer using Ply.
Modify pyverilog.vparser.lexer
"""
from ply.lex import lex, TOKEN


def default_error_func(token, text):
    print('Illegal token {} at {}'.format(
        repr(token.value), (token.lineno, token.lexpos)))
    for line in text[token.lineno-3:token.lineno+4]:
        print('Error: ' + line)


class SVLexerBase:
    def __init__(self, error_func = default_error_func):
        self.error_func = error_func
        self.lexer = None
        self.text = None
    
    def build(self, **kwargs):
        self.lexer = lex(object=self, **kwargs)

    def input(self, data):
        assert(self.lexer is not None)
        self.text = data.split('\n')
        self.lexer.input(data)

    def token(self):
        return self.lexer.token()

    operators = (
        'PLUS', 'MINUS', 'POWER', 'TIMES', 'DIVIDE', 'MOD',
        'NOT', 'OR', 'NOR', 'AND', 'NAND', 'XOR', 'XNOR',
        'LOR', 'LAND', 'LNOT',
        'LSHIFTA', 'RSHIFTA', 'LSHIFT', 'RSHIFT',
        'LT', 'GT', 'LE', 'GE', 'EQ', 'NE', 'EQL', 'NEL',
        'QUESTION', 'ASSIGNEQ',
    )

    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_POWER = r'\*\*'
    t_TIMES = r'\*'
    t_DIVIDE = r'/'
    t_MOD = r'%'

    t_NOT = r'~'
    t_OR = r'\|'
    t_NOR = r'~\|'
    t_AND = r'\&'
    t_NAND = r'~\&'
    t_XOR = r'\^'
    t_XNOR = r'~\^'

    t_LOR = r'\|\|'
    t_LAND = r'\&\&'
    t_LNOT = r'!'

    t_LSHIFTA = r'<<<'
    t_RSHIFTA = r'>>>'
    t_LSHIFT = r'<<'
    t_RSHIFT = r'>>'

    t_LT = r'<'
    t_GT = r'>'
    t_LE = r'<='
    t_GE = r'>='
    t_EQ = r'=='
    t_NE = r'!='
    t_EQL = r'==='
    t_NEL = r'!=='

    t_QUESTION = r'\?'
    t_ASSIGNEQ = r'='

    others = (
        'IDENTIFIER',
        'AT', 'COMMA', 'COLON', 'SEMICOLON', 'DOT',
        'PLUSCOLON', 'MINUSCOLON', 
        'LIT_FLOAT', 'LIT_STR',                                         # NOTE: x support
        'LIT_SIGNED_INT_DEC', 'LIT_SIGNED_INT_HEX',                     # NOTE: x support
        'LIT_SIGNED_INT_OCT', 'LIT_SIGNED_INT_BIN',                     # NOTE: x support
        'LIT_INT_DEC', 'LIT_INT_HEX', 
        'LIT_INT_OCT', 'LIT_INT_BIN',
        'LPAREN', 'RPAREN', 'LBRACKET', 'RBRACKET',
        'LBRACE', 'RBRACE', 'DELAY', 
        'DOLLAR',                                                       # NOTE: x support
    )

    t_AT = r'@'
    t_COMMA = r','
    t_COLON = r':'
    t_SEMICOLON = r';'
    t_DOT = r'\.'
    t_PLUSCOLON = r'\+:'
    t_MINUSCOLON = r'-:'

    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_LBRACKET = r'\['
    t_RBRACKET = r'\]'
    t_LBRACE = r'\{'
    t_RBRACE = r'\}'
    t_DELAY = r'\#'
    t_DOLLAR = r'\$'

    skipped = (
        'BLOCKCOMMENT', 'LINECOMMENT', 'DIRECTIVE',
    )

    t_ignore = " \t"
    
    # NOTE: x support, include `define, `include in .v is not allowed
    directive = r"(?m)^\`.*?\n"

    linecomment = r"//.*?\n"
    blockcomment = r"/\*(.|\n)?\*/"
    
    @TOKEN(directive)
    def t_DIRECTIVE(self, token):
        if isinstance(self.directive, list):
            self.directive.append(
                (self.lexer.lineno, token.value)
            )
        token.lexer.lineno += token.value.count('\n')
    
    @TOKEN(linecomment)
    def t_LINECOMMENT(self, token):
        token.lexer.lineno += token.value.count('\n')
    
    @TOKEN(blockcomment)
    def t_BLOCKCOMMENT(self, token):
        token.lexer.lineno += token.value.count('\n')

    bin_number = '[0-9]*\'[bB][0-1xXzZ?][0-1xXzZ?_]*'
    signed_bin_number = '[0-9]*\'[sS][bB][0-1xZzZ?][0-1xXzZ?_]*'
    octal_number = '[0-9]*\'[oO][0-7xXzZ?][0-7xXzZ?_]*'
    signed_octal_number = '[0-9]*\'[sS][oO][0-7xXzZ?][0-7xXzZ?_]*'
    hex_number = '[0-9]*\'[hH][0-9a-fA-FxXzZ?][0-9a-fA-FxXzZ?_]*'
    signed_hex_number = '[0-9]*\'[sS][hH][0-9a-fA-FxXzZ?][0-9a-fA-FxXzZ?_]*'

    decimal_number = '([0-9]*\'[dD][0-9xXzZ?][0-9xXzZ?_]*)|([0-9][0-9_]*)'
    signed_decimal_number = '[0-9]*\'[sS][dD][0-9xXzZ?][0-9xXzZ?_]*'

    exponent_part = r"""([eE][-+]?[0-9]+)"""
    fractional_constant = r"""([0-9]*\.[0-9]+)|([0-9]+\.)"""
    float_number = '((((' \
                    + fractional_constant \
                    + ')' + exponent_part \
                    + '?)|([0-9]+' \
                    + exponent_part \
                    + ')))'

    simple_escape = r"""([a-zA-Z\\?'"])"""
    octal_escape = r"""([0-7]{1,3})"""
    hex_escape = r"""(x[0-9a-fA-F]+)"""
    escape_sequence = r"(\\(" \
                        + simple_escape \
                        + '|' + octal_escape \
                        + '|' + hex_escape \
                        + '))'
    string_char = r'([^"\\\n]|' + escape_sequence + ')'
    string_literal = '"' + string_char + '*"'
    
    identifier = r"(\`?([a-zA-Z_])([a-zA-Z_0-9$])*)|((\\\S)(\S)*)"

    @TOKEN(string_literal)
    def t_LIT_STR(self, t):
        return t

    @TOKEN(float_number)
    def t_LIT_FLOAT(self, t):
        return t

    @TOKEN(signed_bin_number)
    def t_LIT_SIGNED_INT_BIN(self, t):
        return t

    @TOKEN(bin_number)
    def t_LIT_INT_BIN(self, t):
        return t

    @TOKEN(signed_octal_number)
    def t_LIT_SIGNED_INT_OCT(self, t):
        return t

    @TOKEN(octal_number)
    def t_LIT_INT_OCT(self, t):
        return t

    @TOKEN(signed_hex_number)
    def t_LIT_SIGNED_INT_HEX(self, t):
         return t

    @TOKEN(hex_number)
    def t_LIT_INT_HEX(self, t):
         return t

    @TOKEN(signed_decimal_number)
    def t_LIT_SIGNED_INT_DEC(self, t):
        return t

    @TOKEN(decimal_number)
    def t_LIT_INT_DEC(self, t):
        return t

    @TOKEN(identifier)
    def t_IDENTIFIER(self, token):
        # ply parse by file, so workaround is testing self attributes
        if hasattr(self, 'macro'):
            if token.value[0] == '`':
                self.macro.append(
                    (self.lexer.lineno, token.value)
                )
        if hasattr(self, 'kwmap'):
            token.type = self.kwmap.get(token.value, 'IDENTIFIER')
        return token




class SVHeaderLexer(SVLexerBase):

    def __init__(self, error_func = default_error_func):
        super().__init__(error_func)

    keywords = (
        'DEFINE', 'ENUM', 'LOGIC', 'WIRE',
        'STRUCT', 'INPUT', 'OUTPUT'
    )

    tokens = keywords + SVLexerBase.operators + SVLexerBase.others


class SVerilogLexer(SVLexerBase):
    def __init__(self, error_func = default_error_func):
        super().__init__(error_func)
        self.directive = []
        self.macro = []

    keywords = (
        'MODULE', 'ENDMODULE',
        'BEGIN', 'END',
        'GENERATE', 'ENDGENERATE', 'GENVAR',
        'FUNCTION', 'ENDFUNCTION', 'TASK', 'ENDTASK',                   # NOTE: X SUPPORT, ONLY IN TESTBENCH
        'INPUT', 'OUTPUT', 'LOGIC', 'WIRE', 'PARAMETER',
        'INOUT', 'REG', 'INTEGER', 'REAL', 'SIGNED', 'UNSIGNED',        # NOTE: X SUPPORT
        'ASSIGN', 'ALWAYS_FF', 'ALWAYS_COMB', 'POSEDGE', 'NEGEDGE',
        'INITIAL', 'LOCALPARAM', 'SUPPLY0', 'SUPPLY1',                  # NOTE: X SUPPORT
        'TRI', 'TRI0', 'TRI1', 'WAND', 'WOR',                           # NOTE: X SUPPORT
        'BYTE', 'SHORTINT', 'INT', 'LONGINT', 'TIME',                   # NOTE: X SUPPORT
        'IF', 'ELSE', 'FOR', 'WHILE', 'CASE', 'CASEX', 'CASEZ',
        'ENDCASE', 'DEFAULT',
        'WAIT', 'FOREVER', 'DISABLE', 'FORK', 'JOIN',                   # NOTE: x support
    )

    kwmap = {}
    for kw in keywords:
        kwmap[kw.lower()] = kw

    tokens = keywords + SVLexerBase.operators + SVLexerBase.others


def sverilog_lex(text):
    lexer = SVerilogLexer()
    lexer.build()
    lexer.input(text)

    tokens = []
    while True:
        tok = lexer.token()
        if not tok:
            break
        tokens.append(tok)
    return tokens, lexer.directive, lexer.macro


def svheader_lex(text):
    lexer = SVHeaderLexer()
    lexer.build()
    lexer.input(text)

    tokens = []
    while True:
        tok = lexer.token()
        if not tok:
            break
        tokens.append(tok)
    return tokens
