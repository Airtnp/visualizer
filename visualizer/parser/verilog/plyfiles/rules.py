"""
Extra rules.
"""
def integral_literal_rule(obj, t):
    """
    This is shit. Ply gets all functions by sys._getframe(2).f_globals
    But the order is not determined. Then 3'b0 could be LIT_DEC and error
    """
    if '.' in t.value:
        t.type = 'LIT_FLOAT'
    if '\'b' in t.value or '\'B' in t.value:
        t.type = 'LIT_BIN'
    if '\'o' in t.value or '\'O' in t.value:
        t.type = 'LIT_OCT'
    if '\'h' in t.value or '\'H' in t.value:
        t.type = 'LIT_HEX'    
    return t


def define_preprocess_rule(obj, t):
    """
    Without preprocessor, it's hard to define `define RULE Any_Character here,
    delayed to AST.
    """
    return t
