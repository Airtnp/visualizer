"""
Some common default rules.
"""
def default_token_rule(obj, token):
    # token.lexer.lineno += token.value.count('\n')
    return token


def default_identifier_rule(obj, token):
    if hasattr(obj, 'token_dict') and token.value in obj.token_dict.keys():
        token.type = obj.token_dict[token.value][0]
        return token
    if hasattr(obj, 'keyword_dict') and token.value in obj.keyword_dict.keys():
        token.type = obj.keyword_dict[token.value]
        return token
    return token


def default_empty_rule(obj, token):
    token.lexer.lineno += token.value.count('\n')


def default_error_func(token, text, pos):
    lineno = token.lineno
    # lineno = lineno % (len(text) - 1)
    colno = pos[1]
    if pos[1] == 0:
        colno = token.lexpos
    print('Illegal character(s) {} at {}'.format(
        repr(token.value), (lineno, colno)))
    print('ErrorLine(s): ')
    line_min = max(0, lineno - 3)
    line_max = min(lineno + 4, len(text))
    for idx in range(line_min, line_max):
        if idx == lineno - 1:
            print('->\t' + text[idx])
        else:
            print('\t' + text[idx])
    import ipdb; ipdb.set_trace()

