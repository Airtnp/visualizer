"""
Meta AST strcuture.
"""
import re
from sys import stdout
from collections import defaultdict
from visualizer.parser.verilog.plyfiles.meta.parser import ParserGenerator


class AstMeta(type):
    """
    Convert fields to parser rules.
    list => Concat,
    Tuple => Alt,
    str => Operator,
    """

    counter = defaultdict(int)
    first_cap_re = re.compile('(.)([A-Z][a-z]+)')
    all_cap_re = re.compile('([a-z0-9])([A-Z])')
    rules = []

    def __new__(meta, classname, base, attrs):
        basic_blocks = (
            'AstNode', 'Token', 'Operator', 'NonTerm',
            'Concat', 'Alt',
            'Plus', 'Star', 'Optional'
        )
        if classname not in basic_blocks and not attrs.get('is_base', False):
            if 'fields' not in attrs.keys():
                raise Exception("AstNode needs fields attributes")

            fields = attrs['fields']
            parser = ParserGenerator(None, None)
            parser.start = meta.convert_snakecase(classname)

            cls = super(AstMeta, meta).__new__(meta, classname, base, attrs)
            meta.rules = [classname]

            @classmethod
            def rescan(cls, is_start=True, parser=None):
                if is_start:
                    meta.rules = [classname]
                    parser = ParserGenerator(None, None)
                    parser.start = meta.convert_snakecase(classname)
                fields = cls.__dict__['fields']
                is_rec = cls.__dict__.get('is_rec', True)
                new_cls = super(AstMeta, meta).__new__(
                    meta, cls.__name__, cls.__mro__, dict(cls.__dict__))
                if fields == []:
                    new_cls.incomplete = True
                    return cls
                meta.traverse_rulename(
                    meta, fields, parser, new_cls, is_outermost=True, is_rec=is_rec)
                new_cls.parser = parser
                return new_cls
            
            cls.rescan = rescan

            if fields == []:
                cls.incomplete = True
                return cls
            is_rec = attrs.get('is_rec', True)
            meta.traverse_rulename(
                meta, fields, parser, cls, is_outermost=True, is_rec=is_rec)
            cls.parser = parser
            return cls
        return super(AstMeta, meta).__new__(meta, classname, base, attrs)

    def convert_snakecase(camel):
        """
        @ref: https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case
        """
        s = AstMeta.first_cap_re.sub(r'\1_\2', camel)
        return AstMeta.all_cap_re.sub(r'\1_\2', s).lower()

    def temp_rulename(prefix):
        AstMeta.counter[prefix] += 1
        return 'temp_{}_{}'.format(prefix, AstMeta.counter[prefix])

    def traverse_rulename(meta, fields, parser, cls, is_outermost=False, is_rec=True, inline_count=0):
        """
        Traverse fields definition.
        Return rulename.
        """
        def list_rule(fields, parser, force_inline=False):
            new_inline_count = inline_count
            if not is_outermost:
                new_inline_count = inline_count + 1
            rulenames = [
                meta.traverse_rulename(
                    meta,
                    field,
                    parser,
                    cls,
                    is_rec = is_rec,
                    inline_count = new_inline_count,
                ) for field in fields
            ]
            if force_inline or is_outermost or inline_count == 0:
                return ' '.join(rulenames)
            return parser.create_concat_rule(
                meta.temp_rulename('concat'),
                *rulenames,
            )

        def concat_rule(fields, parser):
            return list_rule(fields.args, parser, fields.force_inline)

        def tuple_rule(fields, parser, force_inline=False):
            new_inline_count = inline_count
            if not is_outermost:
                new_inline_count = inline_count + 1
            rulenames = [
                meta.traverse_rulename(
                    meta,
                    field,
                    parser,
                    cls,
                    is_rec = is_rec,
                    inline_count = new_inline_count
                ) for field in fields
            ]
            if force_inline or is_outermost or inline_count == 0:
                return ' \n | '.join(rulenames)
            return parser.create_alt_rule(
                meta.temp_rulename('alt'),
                *rulenames
            )

        def alt_rule(fields, parser):
            return tuple_rule(fields.args, parser, fields.force_inline)

        def token_rule(fields, parser):
            return fields.token

        def nonterm_rule(fields, parser):
            return meta.convert_snakecase(fields.nonterm)

        def operator_rule(fields, parser):
            if not fields.op[0].isalpha():
                fields.op = cls.search_op(fields.op)
            return fields.op

        def str_rule(fields, parser):
            return cls.search_op(fields)

        def optional_rule(fields, parser):
            return parser.create_opt_rule(
                meta.traverse_rulename(
                    meta, fields.node, parser, cls,
                    is_rec = is_rec, inline_count = 1)
            )

        def star_rule(fields, parser):
            return parser.create_star_rule(
                meta.traverse_rulename(
                    meta, fields.node, parser, cls,
                    is_rec = is_rec, inline_count = 1),
                prefix=fields.prefix,
                postfix=fields.postfix
            )

        def plus_rule(fields, parser):
            return parser.create_plus_rule(
                meta.traverse_rulename(
                    meta, fields.node, parser, cls,
                    is_rec = is_rec, inline_count = 0),
                prefix=fields.prefix,
                postfix=fields.postfix
            )

        def node_rule(fields, parser):
            """
            For class Rule(AstNode)
            """
            if fields.__name__ not in meta.rules and is_rec:
                meta.rules.append(fields.__name__)
                fields = fields.rescan(is_start=False, parser=parser)
                return meta.convert_snakecase(fields.__name__)
                # return meta.traverse_rulename(meta, fields.fields, parser, fields, is_outermost=True, is_rec=is_rec)
            else:
                return meta.convert_snakecase(fields.__name__)

        ast_map = defaultdict(lambda: node_rule)
        ast_map.update({
            Token: token_rule,
            NonTerm: nonterm_rule,
            list: list_rule,
            Concat: concat_rule,
            tuple: tuple_rule,
            Alt: alt_rule,
            str: str_rule,
            Operator: operator_rule,
            Optional: optional_rule,
            Star: star_rule,
            Plus: plus_rule,
        })

        rulename = ast_map[type(fields)](fields, parser)

        if is_outermost:
            is_single = True
            if type(fields) == list and len(fields) > 1:
                is_single = False
            if type(fields) == Concat and len(fields.args) > 1:
                is_single = False
            if type(fields) == tuple:
                for f in fields:
                    if type(f) == list and len(f) > 1:
                        is_single = False
                        break
                    if type(f) == Concat and len(f.args) > 1:
                        is_single = False
                        break
            if type(fields) == Alt:
                for f in fields.args:
                    if type(f) == list and len(f) > 1:
                        is_single = False
                        break
                    if type(f) == Concat and len(f.args) > 1:
                        is_single = False
                        break            
            parser.create_equiv_rule(
                meta.convert_snakecase(cls.__name__),
                cls,
                rulename,
                is_single = is_single
            )
            return meta.convert_snakecase(cls.__name__)

        return rulename

class AstNode(metaclass=AstMeta):
    op_map = {}
    
    @classmethod
    def set_opmap(cls, op_map):
        cls.op_map = op_map

    @classmethod
    def search_op(cls, op):
        if op[0].isalpha():
            return op
        if op in cls.op_map.keys():
            return cls.op_map[op]
        raise Exception("Undefined operator: " + op)

    def __init__(self, node):
        setattr(self, 'node', node)

    @classmethod
    def build(cls, lexer_cls = None, *args, **kwargs):
        cls.parser.build(lexer_cls, *args, **kwargs)

    @classmethod
    def set_lexer(cls, lexer):
        cls.parser.set_lexer(lexer)

    @classmethod
    def parse(cls, text, precedence = None, *args, **kwargs):
        return cls.parser.parse(text, precedence, *args, **kwargs)

    @classmethod
    def show_rules(cls):
        for name in cls.parser.__class__.rules[::-1]:
            if name != 'p_error':
                print(cls.parser.__class__.__dict__[name].__doc__)

    @classmethod
    def collect_rules(cls):
        rules = []
        for name in cls.parser.__class__.rules[::-1]:
            if name != 'p_error':
                rules.append(cls.parser.__class__.__dict__[name].__doc__)
        return '\n'.join(rules)

    @staticmethod
    def flatten(node, level=0, mode='default', stop=[]):
        """
        default mode: flatten [expr, [expr, expr]] to [id, expr, expr]
        star mode: flatten [expr, [expr, expr]] to [id, id, id]
        Star mode will be useful for Expr*
        """
        if level == 0 or node.__class__.__name__ in stop:
            return node
        def ap_or_ex(ls, child):
            if isinstance(child, list):
                ls.extend(child)
            else:
                ls.append(child)

        if isinstance(node, list):
            result = []
            if mode == 'default':
                for child in node:                    
                    ap_or_ex(
                        result,
                        AstNode.flatten(
                            child, level - 1,
                            mode = mode, stop = stop))
            else:
                for child in node:
                    if isinstance(child, list):
                        ap_or_ex(
                            result, 
                            AstNode.flatten(
                                child, level, 
                                mode = mode, stop = stop))
                    else:
                        ap_or_ex(
                            result,
                            AstNode.flatten(
                                child, level - 1, 
                                mode = mode, stop = stop))
            return result
        elif isinstance(node, AstNode):
            return AstNode.flatten(node.node, level, mode = mode, stop = stop)

        return node

    @staticmethod
    def select_flatten(node, sel=[], level=0, mode='default', stop=[]):
        res = AstNode.flatten(node, level=level, mode=mode, stop=sel+stop+['str'])
        reslist = []
        if isinstance(res, list):
            for item in res:
                if item.__class__.__name__ in sel:
                    reslist.append(item)
                elif isinstance(item, str) and item in sel:
                    reslist.append(item)
        else:
            if res.__class__.__item__ in sel:
                reslist.append(res)
            elif isinstance(res, str) and res in sel:
                reslist.append(res)
        return reslist

    def flat(self, level=0, mode='default', stop=[]):
        return self.flatten(self.node, level, mode, stop)

    def select(self, sel=[], level=-1, mode='default', stop=[]):
        return self.select_flatten(self.node, sel, level, mode, stop)

    def has(self, sel, level=-1, mode='default', stop=[]):
        return len(self.select([sel], level, mode, stop)) != 0

    def show_traverse(self, indent=0):
        indent_base = '  '
        indent_text = indent_base * indent
        stdout.write(indent_text + self.__class__.__name__ + ': \n')
        
        def show_default(node, indent):
            if node is not None:
                stdout.write(indent_base * indent + repr(node))
            else:
                stdout.write(indent_base * indent + '\'empty\'')

        def show_list(node, indent):
            stdout.write(indent_base * (indent - 1) + '[\n')
            for child in node:
                if isinstance(child, list):
                    show_list(child, indent + 1)
                elif isinstance(child, AstNode):
                    show_ast(child, indent)
                else:
                    show_default(child, indent)
                stdout.write('\n')
            stdout.write(indent_base * (indent - 1) + ']')

        def show_ast(node, indent):
            node.show_traverse(indent)

        if isinstance(self.node, list):
            show_list(self.node, indent + 1)
        elif isinstance(self.node, AstNode):
            show_ast(self.node, indent + 1)
        else:
            show_default(self.node, indent + 1)


class Token(AstNode):
    def __init__(self, token):
        self.token = token

    def __repr__(self):
        return 'Token(\'{}\')'.format(self.token)


class NonTerm(AstNode):
    """
    NonTerm is only used within generate_fields,
    to support repr(x) without quote
    """
    def __init__(self, nonterm):
        self.nonterm = nonterm

    def __repr__(self):
        return str(self.nonterm)


class Concat(AstNode):
    def __init__(self, *args, force_inline=False):
        self.args = args
        self.force_inline = force_inline

    def __repr__(self):
        if len(self.args) == 0:
            return 'empty'
        elif len(self.args) == 1:
            return repr(self.args[0])
        else:
            if self.force_inline:
                extra_force = ', force_inline=True'
                return 'Concat(' + ', '.join([repr(x) for x in self.args]) + extra_force + ')'
            return '[' + ', '.join([repr(x) for x in self.args]) + ']'


class Alt(AstNode):
    def __init__(self, *args, force_inline=False):
        self.args = args
        self.force_inline = force_inline

    def __repr__(self):
        if len(self.args) == 0:
            return 'empty'
        elif len(self.args) == 1:
            return repr(self.args[0])
        else:
            if self.force_inline:
                extra_force = ', force_inline=True'
                return 'Alt(' + ', '.join([repr(x) for x in self.args]) + extra_force + ')'
            return '(' + ', '.join([repr(x) for x in self.args]) + ')'


class Operator(AstNode):    
    def __init__(self, op):
        self.op = op

    def __repr__(self):
        return 'Operator(\'{}\')'.format(self.op)


class Optional(AstNode):
    def __init__(self, node):
        self.node = node

    def __repr__(self):
        return 'Optional({})'.format(repr(self.node))


class Star(AstNode):
    def __init__(self, node, prefix='', postfix=''):
        self.node = node
        self.prefix = prefix
        self.postfix = postfix

    def __repr__(self):
        extra_fix = ''
        if self.prefix != '':
            extra_fix += ", prefix=\'{}\'".format(self.prefix)
        if self.postfix != '':
            extra_fix += ", postfix=\'{}\'".format(self.postfix)
        return 'Star({}{})'.format(
            repr(self.node), extra_fix)


class Plus(AstNode):
    def __init__(self, node, prefix='', postfix=''):
        self.node = node
        self.prefix = prefix
        self.postfix = postfix

    def __repr__(self):
        extra_fix = ''
        if self.prefix != '':
            extra_fix += ", prefix=\'{}\'".format(self.prefix)
        if self.postfix != '':
            extra_fix += ", postfix=\'{}\'".format(self.postfix)
        return 'Plus({}{})'.format(
            repr(self.node), extra_fix)
