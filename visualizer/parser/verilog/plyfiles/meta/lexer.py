"""
Meta lexer generator.
"""
from collections import OrderedDict
from ply.lex import lex, TOKEN
from visualizer.parser.verilog.plyfiles.meta.default import (
    default_error_func,
    default_empty_rule,
    default_token_rule
)


class LexerGenerator:
    counter = 0

    def __new__(cls, *args, **kwargs):
        is_rec = kwargs.pop('is_rec', False)
        if is_rec:
            return super(LexerGenerator, cls).__new__(cls)
        classname = 'Lexer_{}'.format(LexerGenerator.counter)
        LexerGenerator.counter += 1
        new_cls = type(classname, cls.__mro__, dict(cls.__dict__))
        # Manually add **kwargs then is_rec is allowed.
        return new_cls(*args, is_rec=True, **kwargs)

    def __init__(self, token_dict, keyword_dict, states, error_func = default_error_func, **kwargs):
        self.error_func = error_func
        self.lexer = None
        self.text = None
        self.data = None
        self.token_dict = token_dict
        self.keyword_dict = keyword_dict
        self.tokens_map = {}
        self.xstates = states
        self.extract_from_dict()
        self.lineno = 0

    def create_func_wrapper(self, token_defname, token, func):
        def anonymous(obj, t):
            return func(obj, t)
        anonymous.__name__ = token_defname
        anonymous.__doc__ = func.__doc__
        anonymous = TOKEN(token)(anonymous)
        return anonymous

    def extract_from_dict(self):
        self.tokens = list(map(lambda s: s[0], self.token_dict.values())) + \
                            list(self.keyword_dict.values())
        if 'ignore' in self.tokens:
            self.tokens.remove('ignore')
        for token in self.token_dict.keys():
            attrs = self.token_dict[token]
            token_defname = 't_{}'.format(attrs[0])
            self.tokens_map[token] = attrs[0]
            if len(attrs) == 1 or not hasattr(attrs[1], '__call__'):
                # t_Token
                setattr(self.__class__, token_defname, token)
            else:
                # Token(...) def
                # create new function so TOKEN setting regex won't overwrite
                anonymous = self.create_func_wrapper(token_defname, token, attrs[1])
                setattr(self.__class__, token_defname, anonymous)
        # keyword should all be dealt within identifier rules
        '''
        for keyword, token in self.keyword_dict.items():
            if isinstance(token, str):
                token_defname = 't_{}'.format(token)
                self.tokens_map[keyword] = token
                setattr(self.__class__, token_defname, keyword)
            else:
                attrs = token   
                token_defname = 't_{}'.format(attrs[0])
                self.tokens_map[keyword] = attrs[0]
                if len(attrs) == 1 or not hasattr(attrs[1], '__call__'):
                    # t_Token
                    setattr(self.__class__, token_defname, token)
                elif len(attrs) == 2:
                    # Token(...) def
                    setattr(self.__class__, token_defname, TOKEN(token)(attrs[1]))
        '''
        # [(name, type)...]
        for state in self.xstates:
            setattr(self, state[0], state[1]())
    
    def build(self, **kwargs):
        self.lexer = lex(object=self, **kwargs)

    def input(self, data):
        assert(self.lexer is not None)
        self.text = data.split('\n')
        self.data = data
        self.lexer.input(data)

    def token(self):
        return self.lexer.token()

    def lexer_pos(self, token):
        line_start = self.data.rfind('\n', 0, token.lexpos) + 1
        colno = (token.lexpos - line_start) + 1
        return (token.lineno, colno)

    def t_newline(self, token):
        r'\n+'
        token.lexer.lineno += token.value.count('\n')

    def t_error(self, token):
        self.error_func(token, self.text, self.lexer_pos(token))

    def run(self, data, **kwargs):
        self.build()
        self.input(data)
        tokens = []
        while True:
            tok = self.token()
            if not tok:
                break
            tokens.append(tok)
        return tokens
