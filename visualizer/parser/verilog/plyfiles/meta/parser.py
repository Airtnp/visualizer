"""
Meta parser generator.

TODO: add anonymous feature
    This can be done by return a lambda of str -> ()
"""
from ply.yacc import yacc
from visualizer.parser.verilog.plyfiles.meta.default import (
    default_error_func,
)


class SetAttrMeta(type):
    def __new__(meta, classname, bases, attrs):
        return super().__new__(meta, classname, bases, attrs)

    def __setattr__(cls, attribute, value):
        if attribute.startswith('p_'):
            cls.rules.append(attribute)
        return super().__setattr__(attribute, value)


class ParserGenerator(metaclass=SetAttrMeta):
    counter = 0
    rules = []

    def __new__(cls, *args, **kwargs):
        ParserGenerator.rules = []
        is_rec = kwargs.pop('is_rec', False)
        if is_rec:
            return super(ParserGenerator, cls).__new__(cls)
        classname = 'Parser_{}'.format(ParserGenerator.counter)
        ParserGenerator.counter += 1
        new_cls = type(classname, cls.__mro__, dict(cls.__dict__))
        # Manually add **kwargs then is_rec is allowed.
        return new_cls(*args, is_rec=True, **kwargs)

    def __init__(self, lexer_cls, precedence, error_func = default_error_func, **kwargs):
        self.error_func = error_func
        self.lexer_cls = lexer_cls
        self.precedence = precedence
        if lexer_cls is not None:
            self.tokens = self.lexer_cls.tokens
        self.parser = None
        self.text = None
        self.lexer = None
    
    def build(self, lexer_cls = None, *args, **kwargs):
        if lexer_cls is not None:
            self.lexer_cls = lexer_cls
            self.tokens = self.lexer_cls.tokens
        self.lexer = self.lexer_cls(*args, **kwargs)

    def set_lexer(self, lexer):
        self.lexer_cls = lexer.__class__
        self.tokens = lexer.tokens
        self.lexer = lexer

    def parse(self, text, precedence = None, build_options = {}, *args, **kwargs):
        if precedence is not None:
            self.precedence = precedence
        self.parser = yacc(module=self, **build_options)
        self.text = text.split('\n')
        return self.parser.parse(text, lexer=self.lexer, *args, **kwargs)

    def p_empty(self, p):
        'empty :'
        pass

    def p_error(self, p):
        self.error_func(p, self.text, (p.lineno, 0))

    def create_opt_rule(self, rulename, prefix='', postfix=''):
        """
        Generate <rulename>_opt from given rulename (rule?)
        @ref: https://groups.google.com/forum/#!topic/ply-hack/xEiny7zmzDo
        return type should be Optional[T] (default in python)
        Actually, it converts normal BNF -> parser combinator??
        """
        optname = rulename + '_opt'
        def optrule(self, p):
            p[0] = p[1]
        
        optrule.__doc__ = "{} : empty \n | {} {} {}".format(
            optname, 
            prefix, rulename, postfix,
        )
        optrule.__name__ = "p_{}".format(optname)
        setattr(self.__class__, optrule.__name__, optrule)
        return optname

    def create_star_rule(self, rulename, prefix='', postfix=''):
        """
        Generate <rulename>_star from given rulename (rule*)
        return type should be List[T].
        """
        starname = rulename + '_star'
        idx = int(prefix != '') + int(postfix != '') + 2
        def starrule(self, p):
            if p[1] is None:
                p[0] = []
            else:
                p[0] = [p[1]] + p[idx]
        
        starrule.__doc__ = "{} : empty \n | {} {} {} {}".format(
            starname, 
            prefix, rulename, postfix,
            starname, 
        )
        starrule.__name__ = "p_{}".format(starname)
        setattr(self.__class__, starrule.__name__, starrule)
        return starname

    def create_plus_rule(self, rulename, prefix='', postfix=''):
        """
        Generate <rulename>_plus from given rulename (rule+)
        return type should be List[T]
        """
        plusname = rulename + '_plus'
        idx = int(prefix != '') + int(postfix != '') + 2
        def plusrule(self, p):
            if len(p) == 2:
                p[0] = [p[1]]
            else:
                p[0] = [p[1]] + p[idx]
        
        plusrule.__doc__ = "{} : {} \n | {} {} {} {}".format(
            plusname, rulename,
            prefix, rulename, postfix,
            plusname, 
        )
        plusrule.__name__ = "p_{}".format(plusname)
        setattr(self.__class__, plusrule.__name__, plusrule)
        return plusname

    def create_alt_rule(self, altname, *rulenames):
        """
        Generate <rulename> | ... | <rulename> from given rulenames (A|B|...)
        return type should be Union[T1, ..., Tn]
        NOTE: Tn could be list
        """
        def altrule(self, p):
            p[0] = p[1:]
        
        altrule.__doc__ = "{} : {}".format(altname, ' \n | '.join(rulenames))
        altrule.__name__ = "p_{}".format(altname)
        setattr(self.__class__, altrule.__name__, altrule)
        return altname

    def create_concat_rule(self, concatname, *rulenames):
        """
        Generate <rulename> ... <rulename> from given rulenames (AB...)
        return type should be AstType(T1, ..., Tn)
        """
        def concatrule(self, p):
            p[0] = p[1:]
        
        concatrule.__doc__ = "{} : {}".format(concatname, ' '.join(rulenames))
        concatrule.__name__ = "p_{}".format(concatname)
        setattr(self.__class__, concatrule.__name__, concatrule)
        return concatname

    def create_equiv_rule(self, equivname, asttype, rulename, is_single = False):
        """
        Generate <name> from <rulename>
        NOTE: it should be outermost_name : ... so we just get everything
        """
        def equivrule(self, p):
            if is_single:
                p[0] = asttype(p[1])
            else:
                p[0] = asttype(p[1:])
        
        equivrule.__doc__ = "{} : {}".format(equivname, rulename)
        equivrule.__name__ = "p_{}".format(equivname)
        setattr(self.__class__, equivrule.__name__, equivrule)
        return equivname
