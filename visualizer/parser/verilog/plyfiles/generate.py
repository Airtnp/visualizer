"""
Generate SystemVerilog ast/lexer.

The SystemVerilog EBNF files is modified to have pre-defined primitives
    identifier: ...
And remove [ \t\n] from Any_ASCII_Characters
"""
from os.path import dirname, abspath
from collections import OrderedDict
from visualizer.parser.verilog.plyfiles.rules import *
from visualizer.parser.verilog.plyfiles.ebnf.BNFgenerator import (
    info_from_ebnf, output_ast, output_lexer
)
from visualizer.parser.verilog.plyfiles.meta.default import (
    default_empty_rule, default_identifier_rule, default_token_rule
)

__path__ = abspath(dirname(__file__))

ebnf_path = __path__ + '/./BNFSVMin.ebnf'
ast_path = __path__ + '/../svast.py'
lexer_path = __path__ + '/../lexer.py'

special_kw_token_dict = {
    'IDENTIFIER': 'IDENTIFIER',
    'BLOCKCOMMENT': 'BLOCKCOMMENT',
    'LINECOMMENT': 'LINECOMMENT',
    'decimal_number': 'LIT_DEC',
    'binary_number': 'LIT_BIN',
    'octal_number': 'LIT_OCT',
    'hex_number': 'LIT_HEX',
    'float_number': 'LIT_FLOAT',
}


special_kw_dict = OrderedDict([
    (r"//.*?\n", ['LINECOMMENT', default_empty_rule, True]),
    (r"/\*(.|\n)?\*/", ['BLOCKCOMMENT', default_empty_rule, True]),
    (r"((\$unit\:\:)?\`?([a-zA-Z_])([a-zA-Z_0-9$])*)", ['IDENTIFIER', default_identifier_rule, True]),
    (r"(([0-9]*\'[dDbBoOhH][0-9a-fA-FxXzZ?][0-9a-fA-FxXzZ?_\.]*)|([0-9][0-9_.]*))", ['LIT_DEC', integral_literal_rule, True]),
    (' \t', ['ignore']),
])

op_name_map = {
    r'+': 'PLUS',
    r'-': 'MINUS',
    r'**': 'POWER',
    r'*': 'TIMES',
    r'/': 'DIVIDE',
    r'%': 'MOD',
    r'~': 'OPNOT',
    r'|': 'OR',
    r'~|': 'NOR',
    r'&': 'OPAND',
    r'~&': 'OPNAND',
    r'^': 'OPXOR',
    r'~^': 'OPXNOR',
    r'||': 'LOR',
    r'&&': 'LAND',
    r'!': 'LNOT',
    r'<<<': 'LSHIFTA',
    r'>>>': 'RSHIFTA',
    r'<<': 'LSHIFT',
    r'>>': 'RSHIFT',
    r'<': 'LT',
    r'>': 'GT',
    r'<=': 'LE',
    r'>=': 'GE',
    r'==': 'EQ',
    r'!=': 'NE',
    r'===': 'EQL',
    r'!==': 'NEL',
    r'?': 'QUESTION',
    r'=': 'ASSIGNEQ',
    r'@': 'AT',
    r',': 'COMMA',
    r':': 'COLON',
    r';': 'SEMICOLON',
    r'.': 'DOT',
    r'+:': 'PLUSCOLON',
    r'-:': 'MINUSCOLON',
    r'(': 'LPAREN',
    r')': 'RPAREN',
    r'[': 'LBRACKET',
    r']': 'RBRACKET',
    r'{': 'LBRACE',
    r'}': 'RBRACE',
    r'#': 'DELAY',
    r'$': 'DOLLAR',
    r"'": 'QUOTE',
    r'"': 'DOUBLEQUOTE',
    r'_': 'UNDERSCORE',
}


if __name__ == '__main__':
    with open(ebnf_path, 'r') as ebnf:
        text = ''.join(ebnf.readlines())
        ast_define, op_dict, kw_dict, precs = info_from_ebnf(
            text, 'SVAstNode',
            op_name_map, special_kw_token_dict=special_kw_token_dict
        )
        ast_text = output_ast(ast_define)
        lexer_text, new_op_dict = output_lexer(
            'SVLexer',
            op_dict, kw_dict,
            special_kw_dict=special_kw_dict
        )
    with open(ast_path, 'w+') as ast:
        ast.write(ast_text)
    with open(lexer_path, 'w+') as lexer:
        lexer.write(lexer_text)
    
