
`noportcoerce
`noinline
`timescale 1 ns / 100 ps
	localparam		RV32_instr_width_gp
						= 32;
	localparam		RV32_reg_data_width_gp
						= 32;
	localparam		RV32_reg_addr_width_gp
						= 5;
	localparam		RV32_shamt_width_gp
						= 5;
	localparam		RV32_opcode_width_gp
						= 7;
	localparam		RV32_funct3_width_gp
						= 3;
	localparam		RV32_funct7_width_gp
						= 7;
	localparam		RV32_Iimm_width_gp
						= 12;
	localparam		RV32_Simm_width_gp
						= 12;
	localparam		RV32_Bimm_width_gp
						= 12;
	localparam		RV32_Uimm_width_gp
						= 20;
	localparam		RV32_Jimm_width_gp
						= 20;

	typedef enum logic
		[3:0] {
		INST_ADDR_MISALIGN = 4'b0,
		INST_ACCESS_FAULT = 4'b1,
		ILLEGAL_INST = 4'h2,
		BREAKPOINT = 4'h3,
		LOAD_ADDR_MISALIGN = 4'h4,
		LOAD_ACCESS_FAULT = 4'h5,
		STORE_ADDR_MISALIGN = 4'h6,
		STORE_ACCESS_FAULT = 4'h7,
		ECALL_U_MODE = 4'h8,
		ECALL_S_MODE = 4'h9,
		NO_ERROR = 4'ha,
		ECALL_M_MODE = 4'hb,
		INST_PAGE_FAULT = 4'hc,
		LOAD_PAGE_FAULT = 4'hd,
		HALTED_ON_WFI = 4'he,
		STORE_PAGE_FAULT = 4'hf
	} EXCEPTION_CODE;

	typedef enum logic
		[2:0] {
		OPA_IS_RS1 = 3'b0,
		OPA_IS_NPC = 3'b1,
		OPA_IS_PC = 3'h2,
		OPA_IS_ZERO = 3'h3,
		OPA_IS_EX_MEM = 3'h4,
		OPA_IS_MEM_WB = 3'h5
	} ALU_OPA_SELECT;

	typedef enum logic
		[3:0] {
		OPB_IS_RS2 = 4'b0,
		OPB_IS_I_IMM = 4'b1,
		OPB_IS_S_IMM = 4'h2,
		OPB_IS_B_IMM = 4'h3,
		OPB_IS_U_IMM = 4'h4,
		OPB_IS_J_IMM = 4'h5,
		OPB_IS_EX_MEM = 4'h6,
		OPB_IS_MEM_WB = 4'h7
	} ALU_OPB_SELECT;

	typedef enum logic
		[1:0] {
		DEST_RD = 2'b0,
		DEST_NONE = 2'b1
	} DEST_REG_SEL;

	typedef enum logic
		[4:0] {
		ALU_ADD = 5'b0,
		ALU_SUB = 5'b1,
		ALU_SLT = 5'h02,
		ALU_SLTU = 5'h03,
		ALU_AND = 5'h04,
		ALU_OR = 5'h05,
		ALU_XOR = 5'h06,
		ALU_SLL = 5'h07,
		ALU_SRL = 5'h08,
		ALU_SRA = 5'h09,
		ALU_MUL = 5'h0a,
		ALU_MULH = 5'h0b,
		ALU_MULHSU = 5'h0c,
		ALU_MULHU = 5'h0d,
		ALU_DIV = 5'h0e,
		ALU_DIVU = 5'h0f,
		ALU_REM = 5'h10,
		ALU_REMU = 5'h11
	} ALU_FUNC;

	typedef enum logic
		[1:0] {
		BUS_NONE = 2'b0,
		BUS_LOAD = 2'b1,
		BUS_STORE = 2'h2
	} BUS_COMMAND;

	typedef enum logic
		[1:0] {
		BYTE = 2'b0,
		HALF = 2'b1,
		WORD = 2'h2,
		DOUBLE = 2'h3
	} MEM_SIZE;

	typedef struct packed {
	logic	[6:0]		funct7;
	logic	[4:0]		rs2;
	logic	[4:0]		rs1;
	logic	[2:0]		funct3;
	logic	[4:0]		rd;
	logic	[6:0]		opcode;
	} struct__0000;

	typedef struct packed {
	logic	[11:0]		imm;
	logic	[4:0]		rs1;
	logic	[2:0]		funct3;
	logic	[4:0]		rd;
	logic	[6:0]		opcode;
	} struct__0001;

	typedef struct packed {
	logic	[6:0]		off;
	logic	[4:0]		rs2;
	logic	[4:0]		rs1;
	logic	[2:0]		funct3;
	logic	[4:0]		set;
	logic	[6:0]		opcode;
	} struct__0002;

	typedef struct packed {
	logic			of;
	logic	[5:0]		s;
	logic	[4:0]		rs2;
	logic	[4:0]		rs1;
	logic	[2:0]		funct3;
	logic	[3:0]		et;
	logic			f;
	logic	[6:0]		opcode;
	} struct__0003;

	typedef struct packed {
	logic	[19:0]		imm;
	logic	[4:0]		rd;
	logic	[6:0]		opcode;
	} struct__0004;

	typedef struct packed {
	logic			of;
	logic	[9:0]		et;
	logic			s;
	logic	[7:0]		f;
	logic	[4:0]		rd;
	logic	[6:0]		opcode;
	} struct__0005;

	typedef union packed {
	logic	[31:0]		inst;
	struct__0000		r;
	struct__0001		i;
	struct__0002		s;
	struct__0003		b;
	struct__0004		u;
	struct__0005		j;
	} INST;

	typedef struct packed {
	logic			valid;
	INST			inst;
	logic	[(32 - 1):0]	NPC;
	logic	[(32 - 1):0]	PC;
	} IF_ID_PACKET;

	typedef struct packed {
	logic	[(32 - 1):0]	NPC;
	logic	[(32 - 1):0]	PC;
	logic	[(32 - 1):0]	rs1_value;
	logic	[(32 - 1):0]	rs2_value;
	ALU_OPA_SELECT		opa_select;
	ALU_OPB_SELECT		opb_select;
	INST			inst;
	logic	[4:0]		dest_reg_idx;
	ALU_FUNC		alu_func;
	logic			rd_mem;
	logic			wr_mem;
	logic			cond_branch;
	logic			uncond_branch;
	logic			halt;
	logic			illegal;
	logic			csr_op;
	logic			valid;
	} ID_EX_PACKET;

	typedef struct packed {
	logic	[(32 - 1):0]	alu_result;
	logic	[(32 - 1):0]	NPC;
	logic			take_branch;
	logic	[(32 - 1):0]	rs2_value;
	logic			rd_mem;
	logic			wr_mem;
	logic	[4:0]		dest_reg_idx;
	logic			halt;
	logic			illegal;
	logic			csr_op;
	logic			valid;
	logic	[2:0]		mem_size;
	} EX_MEM_PACKET;

`portcoerce
`inline
`timescale 1 ns / 100 ps
module alu(opa, opb, func, result);
	input	[(32 - 1):0]	opa;
	input	[(32 - 1):0]	opb;
	input			func;

	$unit::ALU_FUNC		func;
	output	[(32 - 1):0]	result;
	logic	[(32 - 1):0]	result;
	wire signed
		[(32 - 1):0]	signed_opa;
	wire signed
		[(32 - 1):0]	signed_opb;
	wire signed
		[((2 * 32) - 1):0]
				signed_mul;
	wire signed
		[((2 * 32) - 1):0]
				mixed_mul;
	wire	[((2 * 32) - 1):0]
				unsigned_mul;

	assign signed_opa = opa;
	assign signed_opb = opb;
	assign signed_mul = (signed_opa * signed_opb);
	assign unsigned_mul = (opa * opb);
	assign mixed_mul = (signed_opa * opb);

	always_comb  begin
	  case (func)
	    $unit::ALU_ADD:
	      result = (opa + opb);
	    $unit::ALU_SUB:
	      result = (opa - opb);
	    $unit::ALU_AND:
	      result = (opa & opb);
	    $unit::ALU_SLT:
	      result = (signed_opa < signed_opb);
	    $unit::ALU_SLTU:
	      result = (opa < opb);
	    $unit::ALU_OR:
	      result = (opa | opb);
	    $unit::ALU_XOR:
	      result = (opa ^ opb);
	    $unit::ALU_SRL:
	      result = (opa >> opb[4:0]);
	    $unit::ALU_SLL:
	      result = (opa << opb[4:0]);
	    $unit::ALU_SRA:
	      result = (signed_opa >>> opb[4:0]);
	    $unit::ALU_MUL:
	      result = signed_mul[(32 - 1):0];
	    $unit::ALU_MULH:
	      result = signed_mul[((2 * 32) - 1):32];
	    $unit::ALU_MULHSU:
	      result = mixed_mul[((2 * 32) - 1):32];
	    $unit::ALU_MULHU:
	      result = unsigned_mul[((2 * 32) - 1):32];
	    default:
	      result = 32'hfacebeec;
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module brcond(rs1, rs2, func, cond);
	input	[(32 - 1):0]	rs1;
	input	[(32 - 1):0]	rs2;
	input	[2:0]		func;
	output			cond;

	logic			cond;
	logic signed
		[(32 - 1):0]	signed_rs1;
	logic signed
		[(32 - 1):0]	signed_rs2;

	assign signed_rs1 = rs1;
	assign signed_rs2 = rs2;

	always_comb  begin
	  cond = 0;
	  case (func)
	    3'b0:
	      cond = (signed_rs1 == signed_rs2);
	    3'b1:
	      cond = (signed_rs1 != signed_rs2);
	    3'b100:
	      cond = (signed_rs1 < signed_rs2);
	    3'b101:
	      cond = (signed_rs1 >= signed_rs2);
	    3'b110:
	      cond = (rs1 < rs2);
	    3'b111:
	      cond = (rs1 >= rs2);
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module ex_stage(clock, reset, id_ex_packet_in, ex_mem_packet_in,
	mem_wb_dest_reg_idx_in, mem_wb_result_in, ex_packet_out);
	input			clock;
	input			reset;
	input			id_ex_packet_in;

	$unit::ID_EX_PACKET	id_ex_packet_in;
	input			ex_mem_packet_in;
	$unit::EX_MEM_PACKET	ex_mem_packet_in;
	input	[4:0]		mem_wb_dest_reg_idx_in;
	input	[(32 - 1):0]	mem_wb_result_in;
	output			ex_packet_out;
	$unit::EX_MEM_PACKET	ex_packet_out;
	logic	[(32 - 1):0]	opa_mux_out;
	logic	[(32 - 1):0]	opb_mux_out;
	logic			brcond_result;
	logic	[(32 - 1):0]	new_rs1_value;
	$unit::ALU_OPA_SELECT	forward_opa_select;
	$unit::ALU_OPB_SELECT	forward_opb_select;
	wire			is_opa_valid_forward = ((
		id_ex_packet_in.opa_select == $unit::OPA_IS_RS1) && (
		id_ex_packet_in.inst.r.rs1 != 5'b0));
	wire			is_opb_valid_forward = ((
		id_ex_packet_in.opb_select == $unit::OPB_IS_RS2) && (
		id_ex_packet_in.inst.r.rs2 != 5'b0));
	wire			is_new_rs1_required = ((
		id_ex_packet_in.inst.r.rs1 != 5'b0) && 
		id_ex_packet_in.cond_branch);
	wire			is_new_rs2_required = ((
		id_ex_packet_in.inst.r.rs2 != 5'b0) && (id_ex_packet_in.wr_mem 
		|| id_ex_packet_in.cond_branch));
	alu alu_0(
		.opa				(opa_mux_out), 
		.opb				(opb_mux_out), 
		.func				(id_ex_packet_in.alu_func), 
		.result				(ex_packet_out.alu_result));
	brcond brcond(
		.rs1				(new_rs1_value), 
		.rs2				(ex_packet_out.rs2_value), 
		.func				(id_ex_packet_in.inst.b.funct3),
		.cond				(brcond_result));

	assign ex_packet_out.NPC = id_ex_packet_in.NPC;
	assign ex_packet_out.rd_mem = id_ex_packet_in.rd_mem;
	assign ex_packet_out.wr_mem = id_ex_packet_in.wr_mem;
	assign ex_packet_out.dest_reg_idx = id_ex_packet_in.dest_reg_idx;
	assign ex_packet_out.halt = id_ex_packet_in.halt;
	assign ex_packet_out.illegal = id_ex_packet_in.illegal;
	assign ex_packet_out.csr_op = id_ex_packet_in.csr_op;
	assign ex_packet_out.valid = id_ex_packet_in.valid;
	assign ex_packet_out.mem_size = id_ex_packet_in.inst.r.funct3;
	assign ex_packet_out.take_branch = (id_ex_packet_in.uncond_branch | (
		id_ex_packet_in.cond_branch & brcond_result));

	always_comb  begin
	  forward_opa_select = id_ex_packet_in.opa_select;
	  forward_opb_select = id_ex_packet_in.opb_select;
	  if (is_opa_valid_forward) begin
	    if (id_ex_packet_in.inst.r.rs1 == ex_mem_packet_in.dest_reg_idx)
		    begin
	      forward_opa_select = $unit::OPA_IS_EX_MEM;
	    end
	    else if (id_ex_packet_in.inst.r.rs1 == mem_wb_dest_reg_idx_in) begin
	      forward_opa_select = $unit::OPA_IS_MEM_WB;
	    end
	  end
	  if (is_opb_valid_forward) begin
	    if (id_ex_packet_in.inst.r.rs2 == ex_mem_packet_in.dest_reg_idx)
		    begin
	      forward_opb_select = $unit::OPB_IS_EX_MEM;
	    end
	    else if (id_ex_packet_in.inst.r.rs2 == mem_wb_dest_reg_idx_in) begin
	      forward_opb_select = $unit::OPB_IS_MEM_WB;
	    end
	  end
	end
	always_comb  begin
	  new_rs1_value = id_ex_packet_in.rs1_value;
	  if (is_new_rs1_required) begin
	    if (id_ex_packet_in.inst.r.rs1 == ex_mem_packet_in.dest_reg_idx)
		    begin
	      new_rs1_value = ex_mem_packet_in.alu_result;
	    end
	    else if (id_ex_packet_in.inst.r.rs1 == mem_wb_dest_reg_idx_in) begin
	      new_rs1_value = mem_wb_result_in;
	    end
	  end
	end
	always_comb  begin
	  ex_packet_out.rs2_value = id_ex_packet_in.rs2_value;
	  if (is_new_rs2_required) begin
	    if (id_ex_packet_in.inst.r.rs2 == ex_mem_packet_in.dest_reg_idx)
		    begin
	      ex_packet_out.rs2_value = ex_mem_packet_in.alu_result;
	    end
	    else if (id_ex_packet_in.inst.r.rs2 == mem_wb_dest_reg_idx_in) begin
	      ex_packet_out.rs2_value = mem_wb_result_in;
	    end
	  end
	end
	always_comb  begin
	  opa_mux_out = 32'hdeadfbac;
	  case (forward_opa_select)
	    $unit::OPA_IS_RS1:
	      opa_mux_out = id_ex_packet_in.rs1_value;
	    $unit::OPA_IS_NPC:
	      opa_mux_out = id_ex_packet_in.NPC;
	    $unit::OPA_IS_PC:
	      opa_mux_out = id_ex_packet_in.PC;
	    $unit::OPA_IS_ZERO:
	      opa_mux_out = 0;
	    $unit::OPA_IS_EX_MEM:
	      opa_mux_out = ex_mem_packet_in.alu_result;
	    $unit::OPA_IS_MEM_WB:
	      opa_mux_out = mem_wb_result_in;
	  endcase
	end
	always_comb  begin
	  opb_mux_out = 32'hfacefeed;
	  case (forward_opb_select)
	    $unit::OPB_IS_RS2:
	      opb_mux_out = id_ex_packet_in.rs2_value;
	    $unit::OPB_IS_I_IMM:
	      opb_mux_out = {{21 {id_ex_packet_in.inst[31]}},
		      id_ex_packet_in.inst[30:20]};
	    $unit::OPB_IS_S_IMM:
	      opb_mux_out = {{21 {id_ex_packet_in.inst[31]}},
		      id_ex_packet_in.inst[30:25], id_ex_packet_in.inst[11:7]};
	    $unit::OPB_IS_B_IMM:
	      opb_mux_out = {{20 {id_ex_packet_in.inst[31]}},
		      id_ex_packet_in.inst[7], id_ex_packet_in.inst[30:25],
		      id_ex_packet_in.inst[11:8], {1'b0}};
	    $unit::OPB_IS_U_IMM:
	      opb_mux_out = {id_ex_packet_in.inst[31:12], {12 {1'b0}}};
	    $unit::OPB_IS_J_IMM:
	      opb_mux_out = {{12 {id_ex_packet_in.inst[31]}},
		      id_ex_packet_in.inst[19:12], id_ex_packet_in.inst[20],
		      id_ex_packet_in.inst[30:21], {1'b0}};
	    $unit::OPB_IS_EX_MEM:
	      opb_mux_out = ex_mem_packet_in.alu_result;
	    $unit::OPB_IS_MEM_WB:
	      opb_mux_out = mem_wb_result_in;
	  endcase
	end
endmodule

`timescale 1 ns / 100 ps
module decoder(if_packet, opa_select, opb_select, dest_reg, alu_func, rd_mem,
	wr_mem, cond_branch, uncond_branch, csr_op, halt, illegal, valid_inst);
	input			if_packet;

	$unit::IF_ID_PACKET	if_packet;
	output			opa_select;
	$unit::ALU_OPA_SELECT	opa_select;
	output			opb_select;
	$unit::ALU_OPB_SELECT	opb_select;
	output			dest_reg;
	$unit::DEST_REG_SEL	dest_reg;
	output			alu_func;
	$unit::ALU_FUNC		alu_func;
	output			rd_mem;
	output			wr_mem;
	output			cond_branch;
	output			uncond_branch;
	logic			rd_mem;
	logic			wr_mem;
	logic			cond_branch;
	logic			uncond_branch;
	output			csr_op;
	logic			csr_op;
	output			halt;
	logic			halt;
	output			illegal;
	logic			illegal;
	output			valid_inst;
	logic			valid_inst;
	$unit::INST		inst;
	logic			valid_inst_in;

	assign inst = if_packet.inst;
	assign valid_inst_in = if_packet.valid;
	assign valid_inst = (valid_inst_in & (~illegal));

	always_comb  begin
	  opa_select = $unit::OPA_IS_RS1;
	  opb_select = $unit::OPB_IS_RS2;
	  alu_func = $unit::ALU_ADD;
	  dest_reg = $unit::DEST_NONE;
	  csr_op = 1'b0;
	  rd_mem = 1'b0;
	  wr_mem = 1'b0;
	  cond_branch = 1'b0;
	  uncond_branch = 1'b0;
	  halt = 1'b0;
	  illegal = 1'b0;
	  if (valid_inst_in) begin
	    casez (inst)
	      {{20 {1'bz}}, {5 {1'bz}}, 7'b0110111}: begin
		dest_reg = $unit::DEST_RD;
		opa_select = $unit::OPA_IS_ZERO;
		opb_select = $unit::OPB_IS_U_IMM;
	      end
	      {{20 {1'bz}}, {5 {1'bz}}, 7'b0010111}: begin
		dest_reg = $unit::DEST_RD;
		opa_select = $unit::OPA_IS_PC;
		opb_select = $unit::OPB_IS_U_IMM;
	      end
	      {{20 {1'bz}}, {5 {1'bz}}, 7'b1101111}: begin
		dest_reg = $unit::DEST_RD;
		opa_select = $unit::OPA_IS_PC;
		opb_select = $unit::OPB_IS_J_IMM;
		uncond_branch = 1'b1;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b1100111}: begin
		dest_reg = $unit::DEST_RD;
		opa_select = $unit::OPA_IS_RS1;
		opb_select = $unit::OPB_IS_I_IMM;
		uncond_branch = 1'b1;
	      end
	      {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}},
		      7'b1100011}, {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5
		      {1'bz}}, 7'b1100011}, {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}},
		      3'b100, {5 {1'bz}}, 7'b1100011}, {{7 {1'bz}}, {5 {1'bz}},
		      {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b1100011}, {{7 {1'bz}},
		      {5 {1'bz}}, {5 {1'bz}}, 3'b110, {5 {1'bz}}, 7'b1100011}, 
		      {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b111, {5 {1'bz}},
		      7'b1100011}: begin
		opa_select = $unit::OPA_IS_PC;
		opb_select = $unit::OPB_IS_B_IMM;
		cond_branch = 1'b1;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0000011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0000011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0000011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b100, {5 {1'bz}}, 7'b0000011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b0000011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		rd_mem = 1'b1;
	      end
	      {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}},
		      7'b0100011}, {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5
		      {1'bz}}, 7'b0100011}, {{7 {1'bz}}, {5 {1'bz}}, {5 {1'bz}},
		      3'b010, {5 {1'bz}}, 7'b0100011}: begin
		opb_select = $unit::OPB_IS_S_IMM;
		wr_mem = 1'b1;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SLT;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b011, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SLTU;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b111, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_AND;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b110, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_OR;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b100, {5 {1'bz}}, 7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_XOR;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0010011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SLL;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b0010011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SRL;
	      end
	      {7'b0100000, {5 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}},
		      7'b0010011}: begin
		dest_reg = $unit::DEST_RD;
		opb_select = $unit::OPB_IS_I_IMM;
		alu_func = $unit::ALU_SRA;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
	      end
	      {7'b0100000, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0110011}
		      : begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SUB;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SLT;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b011, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SLTU;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b111, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_AND;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b110, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_OR;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b100, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_XOR;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SLL;
	      end
	      {7'b0, {5 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SRL;
	      end
	      {7'b0100000, {5 {1'bz}}, {5 {1'bz}}, 3'b101, {5 {1'bz}},
		      7'b0110011}: begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_SRA;
	      end
	      {7'b1, {5 {1'bz}}, {5 {1'bz}}, 3'b0, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_MUL;
	      end
	      {7'b1, {5 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_MULH;
	      end
	      {7'b1, {5 {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_MULHSU;
	      end
	      {7'b1, {5 {1'bz}}, {5 {1'bz}}, 3'b011, {5 {1'bz}}, 7'b0110011}: 
		      begin
		dest_reg = $unit::DEST_RD;
		alu_func = $unit::ALU_MULHU;
	      end
	      {{12 {1'bz}}, {5 {1'bz}}, 3'b1, {5 {1'bz}}, 7'b1110011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b010, {5 {1'bz}}, 7'b1110011}, {{12
		      {1'bz}}, {5 {1'bz}}, 3'b011, {5 {1'bz}}, 7'b1110011}: 
		      begin
		csr_op = 1'b1;
	      end
	      {12'b000100000101, 13'b0, 7'b1110011}: begin
		halt = 1'b1;
	      end
	      default:
		illegal = 1'b1;
	    endcase
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module id_stage(clock, reset, wb_reg_wr_en_out, wb_reg_wr_idx_out,
	wb_reg_wr_data_out, if_id_packet_in, id_ex_packet_in, id_hazard_out,
	id_packet_out);
	input			clock;
	input			reset;
	input			wb_reg_wr_en_out;
	input	[4:0]		wb_reg_wr_idx_out;
	input	[(32 - 1):0]	wb_reg_wr_data_out;
	input			if_id_packet_in;

	$unit::IF_ID_PACKET	if_id_packet_in;
	input			id_ex_packet_in;
	$unit::ID_EX_PACKET	id_ex_packet_in;
	output			id_hazard_out;
	logic			id_hazard_out;
	output			id_packet_out;
	$unit::ID_EX_PACKET	id_packet_out;
	$unit::DEST_REG_SEL	dest_reg_select;
	regfile regf_0(
		.rda_idx			(if_id_packet_in.inst.r.rs1), 
		.rda_out			(id_packet_out.rs1_value), 
		.rdb_idx			(if_id_packet_in.inst.r.rs2), 
		.rdb_out			(id_packet_out.rs2_value), 
		.wr_clk				(clock), 
		.wr_en				(wb_reg_wr_en_out), 
		.wr_idx				(wb_reg_wr_idx_out), 
		.wr_data			(wb_reg_wr_data_out));
	decoder decoder_0(
		.if_packet			(if_id_packet_in), 
		.opa_select			(id_packet_out.opa_select), 
		.opb_select			(id_packet_out.opb_select), 
		.alu_func			(id_packet_out.alu_func), 
		.dest_reg			(dest_reg_select), 
		.rd_mem				(id_packet_out.rd_mem), 
		.wr_mem				(id_packet_out.wr_mem), 
		.cond_branch			(id_packet_out.cond_branch), 
		.uncond_branch			(id_packet_out.uncond_branch), 
		.csr_op				(id_packet_out.csr_op), 
		.halt				(id_packet_out.halt), 
		.illegal			(id_packet_out.illegal), 
		.valid_inst			(id_packet_out.valid));
	wire			next_is_valid_load = (id_ex_packet_in.rd_mem & 
		id_ex_packet_in.valid);

	assign id_packet_out.inst = if_id_packet_in.inst;
	assign id_packet_out.NPC = if_id_packet_in.NPC;
	assign id_packet_out.PC = if_id_packet_in.PC;

	always_comb  begin
	  case (dest_reg_select)
	    $unit::DEST_RD:
	      id_packet_out.dest_reg_idx = if_id_packet_in.inst.r.rd;
	    $unit::DEST_NONE:
	      id_packet_out.dest_reg_idx = 5'b0;
	    default:
	      id_packet_out.dest_reg_idx = 5'b0;
	  endcase
	end
	always_comb  begin
	  id_hazard_out = 1'b0;
	  if (next_is_valid_load) begin
	    if (((id_packet_out.opa_select == $unit::OPA_IS_RS1) && (
		    id_packet_out.inst.r.rs1 != 5'b0)) && (
		    id_packet_out.inst.r.rs1 == id_ex_packet_in.inst.r.rd)) 
		    begin
	      id_hazard_out = 1'b1;
	    end
	    if (((id_packet_out.opb_select == $unit::OPB_IS_RS2) && (
		    id_packet_out.inst.r.rs2 != 5'b0)) && (
		    id_packet_out.inst.r.rs2 == id_ex_packet_in.inst.r.rd)) 
		    begin
	      id_hazard_out = 1'b1;
	    end
	    if (((id_packet_out.wr_mem || id_packet_out.cond_branch) && (
		    id_packet_out.inst.r.rs2 != 5'b0)) && (
		    id_packet_out.inst.r.rs2 == id_ex_packet_in.inst.r.rd)) 
		    begin
	      id_hazard_out = 1'b1;
	    end
	    if ((id_packet_out.cond_branch && (id_packet_out.inst.r.rs1 != 5'b0)
		    ) && (id_packet_out.inst.r.rs1 == id_ex_packet_in.inst.r.rd)
		    ) begin
	      id_hazard_out = 1'b1;
	    end
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module if_stage(clock, reset, mem_wb_valid_inst, ex_mem_take_branch,
	ex_mem_target_pc, Imem2proc_data, id_hazard_in, mem_data_hazard_in,
	proc2Imem_addr, if_packet_out);
	input			clock;
	input			reset;
	input			mem_wb_valid_inst;
	input			ex_mem_take_branch;
	input	[(32 - 1):0]	ex_mem_target_pc;
	input	[63:0]		Imem2proc_data;
	input			id_hazard_in;
	input			mem_data_hazard_in;
	output	[(32 - 1):0]	proc2Imem_addr;

	logic	[(32 - 1):0]	proc2Imem_addr;
	output			if_packet_out;
	$unit::IF_ID_PACKET	if_packet_out;
	logic	[(32 - 1):0]	PC_reg;
	logic	[(32 - 1):0]	PC_plus_4;
	logic	[(32 - 1):0]	next_PC;
	logic			PC_enable;

	assign proc2Imem_addr = {PC_reg[(32 - 1):3], 3'b0};
	assign if_packet_out.inst = (PC_reg[2] ? Imem2proc_data[63:32] : 
		Imem2proc_data[31:0]);
	assign PC_plus_4 = (PC_reg + 4);
	assign next_PC = (ex_mem_take_branch ? ex_mem_target_pc : PC_plus_4);
	assign PC_enable = ((if_packet_out.valid & (!id_hazard_in)) | 
		ex_mem_take_branch);
	assign if_packet_out.valid = (!mem_data_hazard_in);
	assign if_packet_out.NPC = PC_plus_4;
	assign if_packet_out.PC = PC_reg;

	always_ff @(posedge clock) begin
	  if (reset) begin
	    PC_reg <= #(1) 0;
	  end
	  else if (PC_enable) begin
	    PC_reg <= #(1) next_PC;
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module mem_stage(clock, reset, ex_mem_packet_in, Dmem2proc_data, mem_result_out,
	proc2Dmem_command, proc2Dmem_size, proc2Dmem_addr, proc2Dmem_data);
	input			clock;
	input			reset;
	input			ex_mem_packet_in;

	$unit::EX_MEM_PACKET	ex_mem_packet_in;
	input	[(32 - 1):0]	Dmem2proc_data;
	output	[(32 - 1):0]	mem_result_out;
	logic	[(32 - 1):0]	mem_result_out;
	output	[1:0]		proc2Dmem_command;
	logic	[1:0]		proc2Dmem_command;
	output			proc2Dmem_size;
	$unit::MEM_SIZE		proc2Dmem_size;
	output	[(32 - 1):0]	proc2Dmem_addr;
	logic	[(32 - 1):0]	proc2Dmem_addr;
	output	[(32 - 1):0]	proc2Dmem_data;
	logic	[(32 - 1):0]	proc2Dmem_data;

	assign proc2Dmem_command = (ex_mem_packet_in.wr_mem ? $unit::BUS_STORE :
		(ex_mem_packet_in.rd_mem ? $unit::BUS_LOAD : $unit::BUS_NONE));
	assign proc2Dmem_size = $unit::MEM_SIZE'(ex_mem_packet_in.mem_size[1:0])
		;
	assign proc2Dmem_data = ex_mem_packet_in.rs2_value;
	assign proc2Dmem_addr = ex_mem_packet_in.alu_result;

	vcs_assert_noname_4: assert property(@(negedge clock) (((32 == 32) && 
		ex_mem_packet_in.rd_mem) |-> (proc2Dmem_size != $unit::DOUBLE)))
		;


	always_comb  begin
	  mem_result_out = ex_mem_packet_in.alu_result;
	  if (ex_mem_packet_in.rd_mem) begin
	    if (~ex_mem_packet_in.mem_size[2]) begin
	      if (ex_mem_packet_in.mem_size[1:0] == 2'b0) begin
		mem_result_out = {{(32 - 8) {Dmem2proc_data[7]}},
			Dmem2proc_data[7:0]};
	      end
	      else if (ex_mem_packet_in.mem_size[1:0] == 2'b1) begin
		mem_result_out = {{(32 - 16) {Dmem2proc_data[15]}},
			Dmem2proc_data[15:0]};
	      end
	      else begin
		mem_result_out = Dmem2proc_data;
	      end
	    end
	    else
	      begin
		if (ex_mem_packet_in.mem_size[1:0] == 2'b0) begin
		  mem_result_out = {{(32 - 8) {1'b0}}, Dmem2proc_data[7:0]};
		end
		else if (ex_mem_packet_in.mem_size[1:0] == 2'b1) begin
		  mem_result_out = {{(32 - 16) {1'b0}}, Dmem2proc_data[15:0]};
		end
		else begin
		  mem_result_out = Dmem2proc_data;
		end
	      end
	  end
	end
endmodule

`timescale 1 ns / 100 ps
module pipeline(clock, reset, mem2proc_response, mem2proc_data, mem2proc_tag,
	proc2mem_command, proc2mem_addr, proc2mem_data, proc2mem_size,
	pipeline_completed_insts, pipeline_error_status, pipeline_commit_wr_idx,
	pipeline_commit_wr_data, pipeline_commit_wr_en, pipeline_commit_NPC,
	if_NPC_out, if_IR_out, if_valid_inst_out, if_id_NPC, if_id_IR,
	if_id_valid_inst, id_ex_NPC, id_ex_IR, id_ex_valid_inst, ex_mem_NPC,
	ex_mem_IR, ex_mem_valid_inst, mem_wb_NPC, mem_wb_IR, mem_wb_valid_inst);
	input			clock;
	input			reset;
	input	[3:0]		mem2proc_response;
	input	[63:0]		mem2proc_data;
	input	[3:0]		mem2proc_tag;
	output	[1:0]		proc2mem_command;

	logic	[1:0]		proc2mem_command;
	output	[(32 - 1):0]	proc2mem_addr;
	logic	[(32 - 1):0]	proc2mem_addr;
	output	[63:0]		proc2mem_data;
	logic	[63:0]		proc2mem_data;
	output			proc2mem_size;
	$unit::MEM_SIZE		proc2mem_size;
	output	[3:0]		pipeline_completed_insts;
	logic	[3:0]		pipeline_completed_insts;
	output			pipeline_error_status;
	$unit::EXCEPTION_CODE	pipeline_error_status;
	output	[4:0]		pipeline_commit_wr_idx;
	logic	[4:0]		pipeline_commit_wr_idx;
	output	[(32 - 1):0]	pipeline_commit_wr_data;
	logic	[(32 - 1):0]	pipeline_commit_wr_data;
	output			pipeline_commit_wr_en;
	logic			pipeline_commit_wr_en;
	output	[(32 - 1):0]	pipeline_commit_NPC;
	logic	[(32 - 1):0]	pipeline_commit_NPC;
	output	[(32 - 1):0]	if_NPC_out;
	logic	[(32 - 1):0]	if_NPC_out;
	output	[31:0]		if_IR_out;
	logic	[31:0]		if_IR_out;
	output			if_valid_inst_out;
	logic			if_valid_inst_out;
	output	[(32 - 1):0]	if_id_NPC;
	logic	[(32 - 1):0]	if_id_NPC;
	output	[31:0]		if_id_IR;
	logic	[31:0]		if_id_IR;
	output			if_id_valid_inst;
	logic			if_id_valid_inst;
	output	[(32 - 1):0]	id_ex_NPC;
	logic	[(32 - 1):0]	id_ex_NPC;
	output	[31:0]		id_ex_IR;
	logic	[31:0]		id_ex_IR;
	output			id_ex_valid_inst;
	logic			id_ex_valid_inst;
	output	[(32 - 1):0]	ex_mem_NPC;
	logic	[(32 - 1):0]	ex_mem_NPC;
	output	[31:0]		ex_mem_IR;
	logic	[31:0]		ex_mem_IR;
	output			ex_mem_valid_inst;
	logic			ex_mem_valid_inst;
	output	[(32 - 1):0]	mem_wb_NPC;
	logic	[(32 - 1):0]	mem_wb_NPC;
	output	[31:0]		mem_wb_IR;
	logic	[31:0]		mem_wb_IR;
	output			mem_wb_valid_inst;
	logic			mem_wb_valid_inst;
	logic			if_id_enable;
	logic			id_ex_enable;
	logic			ex_mem_enable;
	logic			mem_wb_enable;
	logic	[(32 - 1):0]	proc2Imem_addr;
	$unit::IF_ID_PACKET	if_packet;
	$unit::IF_ID_PACKET	if_id_packet;
	$unit::ID_EX_PACKET	id_packet;
	logic			id_hazard;
	$unit::ID_EX_PACKET	id_ex_packet;
	$unit::EX_MEM_PACKET	ex_packet;
	$unit::EX_MEM_PACKET	ex_mem_packet;
	logic	[(32 - 1):0]	mem_result_out;
	logic	[(32 - 1):0]	proc2Dmem_addr;
	logic	[(32 - 1):0]	proc2Dmem_data;
	logic	[1:0]		proc2Dmem_command;
	$unit::MEM_SIZE		proc2Dmem_size;
	logic			mem_wb_halt;
	logic			mem_wb_illegal;
	logic	[4:0]		mem_wb_dest_reg_idx;
	logic	[(32 - 1):0]	mem_wb_result;
	logic			mem_wb_take_branch;
	logic	[(32 - 1):0]	wb_reg_wr_data_out;
	logic	[4:0]		wb_reg_wr_idx_out;
	logic			wb_reg_wr_en_out;
	if_stage if_stage_0(
		.clock				(clock), 
		.reset				(reset), 
		.mem_wb_valid_inst		(mem_wb_valid_inst), 
		.ex_mem_take_branch		(ex_mem_packet.take_branch), 
		.ex_mem_target_pc		(ex_mem_packet.alu_result), 
		.Imem2proc_data			(mem2proc_data), 
		.id_hazard_in			(id_hazard), 
		.mem_data_hazard_in		(mem_data_hazard), 
		.proc2Imem_addr			(proc2Imem_addr), 
		.if_packet_out			(if_packet));
	id_stage id_stage_0(
		.clock				(clock), 
		.reset				(reset), 
		.if_id_packet_in		(if_id_packet), 
		.wb_reg_wr_en_out		(wb_reg_wr_en_out), 
		.wb_reg_wr_idx_out		(wb_reg_wr_idx_out), 
		.wb_reg_wr_data_out		(wb_reg_wr_data_out), 
		.id_ex_packet_in		(id_ex_packet), 
		.id_hazard_out			(id_hazard), 
		.id_packet_out			(id_packet));
	ex_stage ex_stage_0(
		.clock				(clock), 
		.reset				(reset), 
		.id_ex_packet_in		(id_ex_packet), 
		.ex_mem_packet_in		(ex_mem_packet), 
		.mem_wb_dest_reg_idx_in		(mem_wb_dest_reg_idx), 
		.mem_wb_result_in		(mem_wb_result), 
		.ex_packet_out			(ex_packet));
	mem_stage mem_stage_0(
		.clock				(clock), 
		.reset				(reset), 
		.ex_mem_packet_in		(ex_mem_packet), 
		.Dmem2proc_data			(mem2proc_data[(32 - 1):0]), 
		.mem_result_out			(mem_result_out), 
		.proc2Dmem_command		(proc2Dmem_command), 
		.proc2Dmem_size			(proc2Dmem_size), 
		.proc2Dmem_addr			(proc2Dmem_addr), 
		.proc2Dmem_data			(proc2Dmem_data));
	wb_stage wb_stage_0(
		.clock				(clock), 
		.reset				(reset), 
		.mem_wb_NPC			(mem_wb_NPC), 
		.mem_wb_result			(mem_wb_result), 
		.mem_wb_dest_reg_idx		(mem_wb_dest_reg_idx), 
		.mem_wb_take_branch		(mem_wb_take_branch), 
		.mem_wb_valid_inst		(mem_wb_valid_inst), 
		.reg_wr_data_out		(wb_reg_wr_data_out), 
		.reg_wr_idx_out			(wb_reg_wr_idx_out), 
		.reg_wr_en_out			(wb_reg_wr_en_out));

	assign pipeline_completed_insts = {3'b0, mem_wb_valid_inst};
	assign pipeline_error_status = (mem_wb_illegal ? $unit::ILLEGAL_INST : (
		mem_wb_halt ? $unit::HALTED_ON_WFI : ((mem2proc_response == 4'b0
		) ? $unit::LOAD_ACCESS_FAULT : $unit::NO_ERROR)));
	assign pipeline_commit_wr_idx = wb_reg_wr_idx_out;
	assign pipeline_commit_wr_data = wb_reg_wr_data_out;
	assign pipeline_commit_wr_en = wb_reg_wr_en_out;
	assign pipeline_commit_NPC = mem_wb_NPC;
	assign proc2mem_command = ((proc2Dmem_command == $unit::BUS_NONE) ? 
		$unit::BUS_LOAD : proc2Dmem_command);
	assign proc2mem_addr = ((proc2Dmem_command == $unit::BUS_NONE) ? 
		proc2Imem_addr : proc2Dmem_addr);
	assign proc2mem_size = ((proc2Dmem_command == $unit::BUS_NONE) ? 
		$unit::DOUBLE : proc2Dmem_size);
	assign proc2mem_data = {32'b0, proc2Dmem_data};
	assign mem_data_hazard = ((ex_mem_packet.rd_mem | ex_mem_packet.wr_mem) 
		& ex_mem_packet.valid);
	assign if_NPC_out = if_packet.NPC;
	assign if_IR_out = if_packet.inst;
	assign if_valid_inst_out = if_packet.valid;
	assign if_id_NPC = if_id_packet.NPC;
	assign if_id_IR = if_id_packet.inst;
	assign if_id_valid_inst = if_id_packet.valid;
	assign if_id_enable = (~id_hazard);
	assign id_ex_NPC = id_ex_packet.NPC;
	assign id_ex_IR = id_ex_packet.inst;
	assign id_ex_valid_inst = id_ex_packet.valid;
	assign id_ex_enable = 1'b1;
	assign ex_mem_NPC = ex_mem_packet.NPC;
	assign ex_mem_valid_inst = ex_mem_packet.valid;
	assign ex_mem_enable = 1'b1;
	assign mem_wb_enable = 1'b1;

	always_ff @(posedge clock) begin
	  if (reset | ex_mem_packet.take_branch) begin
	    if_id_packet.inst <= #(1) 32'h00000013;
	    if_id_packet.valid <= #(1) 1'b0;
	    if_id_packet.NPC <= #(1) 0;
	    if_id_packet.PC <= #(1) 0;
	  end
	  else
	    begin
	      if (if_id_enable) begin
		if_id_packet <= #(1) if_packet;
	      end
	    end
	end
	always_ff @(posedge clock) begin
	  if ((reset | ex_mem_packet.take_branch) | id_hazard) begin
	    id_ex_packet <= #(1) '{{32 {1'b0}}, {32 {1'b0}}, {32 {1'b0}}, {32
		    {1'b0}}, $unit::OPA_IS_RS1, $unit::OPB_IS_RS2, 32'h00000013,
		    5'b0, $unit::ALU_ADD, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0,
		    1'b0, 1'b0};
	  end
	  else
	    begin
	      if (id_ex_enable) begin
		id_ex_packet <= #(1) id_packet;
	      end
	    end
	end
	always_ff @(posedge clock) begin
	  if (reset | ex_mem_packet.take_branch) begin
	    ex_mem_IR <= #(1) 32'h00000013;
	    ex_mem_packet <= #(1) 0;
	  end
	  else
	    begin
	      if (ex_mem_enable) begin
		ex_mem_IR <= #(1) id_ex_IR;
		ex_mem_packet <= #(1) ex_packet;
	      end
	    end
	end
	always_ff @(posedge clock) begin
	  if (reset) begin
	    mem_wb_NPC <= #(1) 0;
	    mem_wb_IR <= #(1) 32'h00000013;
	    mem_wb_halt <= #(1) 0;
	    mem_wb_illegal <= #(1) 0;
	    mem_wb_valid_inst <= #(1) 0;
	    mem_wb_dest_reg_idx <= #(1) 5'b0;
	    mem_wb_take_branch <= #(1) 0;
	    mem_wb_result <= #(1) 0;
	  end
	  else
	    begin
	      if (mem_wb_enable) begin
		mem_wb_NPC <= #(1) ex_mem_packet.NPC;
		mem_wb_IR <= #(1) ex_mem_IR;
		mem_wb_halt <= #(1) ex_mem_packet.halt;
		mem_wb_illegal <= #(1) ex_mem_packet.illegal;
		mem_wb_valid_inst <= #(1) ex_mem_packet.valid;
		mem_wb_dest_reg_idx <= #(1) ex_mem_packet.dest_reg_idx;
		mem_wb_take_branch <= #(1) ex_mem_packet.take_branch;
		mem_wb_result <= #(1) mem_result_out;
	      end
	    end
	end
endmodule

`timescale 1 ns / 100 ps
module regfile(rda_idx, rdb_idx, wr_idx, wr_data, wr_en, wr_clk, rda_out,
	rdb_out);
	input	[4:0]		rda_idx;
	input	[4:0]		rdb_idx;
	input	[4:0]		wr_idx;
	input	[(32 - 1):0]	wr_data;
	input			wr_en;
	input			wr_clk;
	output	[(32 - 1):0]	rda_out;
	output	[(32 - 1):0]	rdb_out;

	logic	[(32 - 1):0]	rda_out;
	logic	[(32 - 1):0]	rdb_out;
	logic[31:0][(32 - 1):0]	registers;
	wire	[(32 - 1):0]	rda_reg = registers[rda_idx];
	wire	[(32 - 1):0]	rdb_reg = registers[rdb_idx];

	always_comb  if (rda_idx == 5'b0) begin
	  rda_out = 0;
	end
	else if (wr_en && (wr_idx == rda_idx)) begin
	  rda_out = wr_data;
	end
	else begin
	  rda_out = rda_reg;
	end
	always_comb  if (rdb_idx == 5'b0) begin
	  rdb_out = 0;
	end
	else if (wr_en && (wr_idx == rdb_idx)) begin
	  rdb_out = wr_data;
	end
	else begin
	  rdb_out = rdb_reg;
	end
	always_ff @(posedge wr_clk) if (wr_en) begin
	  registers[wr_idx] <= #(1) wr_data;
	end
endmodule

`timescale 1 ns / 100 ps
module wb_stage(clock, reset, mem_wb_NPC, mem_wb_result, mem_wb_take_branch,
	mem_wb_dest_reg_idx, mem_wb_valid_inst, reg_wr_data_out, reg_wr_idx_out,
	reg_wr_en_out);
	input			clock;
	input			reset;
	input	[(32 - 1):0]	mem_wb_NPC;
	input	[(32 - 1):0]	mem_wb_result;
	input			mem_wb_take_branch;
	input	[4:0]		mem_wb_dest_reg_idx;
	input			mem_wb_valid_inst;
	output	[(32 - 1):0]	reg_wr_data_out;

	logic	[(32 - 1):0]	reg_wr_data_out;
	output	[4:0]		reg_wr_idx_out;
	logic	[4:0]		reg_wr_idx_out;
	output			reg_wr_en_out;
	logic			reg_wr_en_out;
	wire	[(32 - 1):0]	result_mux;

	assign result_mux = (mem_wb_take_branch ? mem_wb_NPC : mem_wb_result);
	assign reg_wr_en_out = (mem_wb_dest_reg_idx != 5'b0);
	assign reg_wr_idx_out = mem_wb_dest_reg_idx;
	assign reg_wr_data_out = result_mux;
endmodule

// END: VCS tokens