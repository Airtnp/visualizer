from visualizer.parser.verilog.plyfiles.meta.ast import *


class SVAstNode(AstNode):
    is_base = True
    op_map = {
        '`noportcoerce': 'GENTOK_DSEGEHEIEHEKEMDVEHDXEKDVDX',
        '`portcoerce': 'GENTOK_DSEIEHEKEMDVEHDXEKDVDX',
        '`noinline': 'GENTOK_DSEGEHEBEGEEEBEGDX',
        '||': 'LOR',
        '`inline': 'GENTOK_DSEBEGEEEBEGDX',
        '`define': 'GENTOK_DSDWDXDYEBEGDX',
        '~|': 'NOR',
        '|=': 'GENTOK_BNEUCJBN',
        '$clog2': 'GENTOK_BKDVEEEHDZBY',
        '}': 'RBRACE',
        '|': 'OR',
        '{': 'LBRACE',
        ']': 'RBRACKET',
        '[': 'LBRACKET',
        ')': 'RPAREN',
        '(': 'LPAREN',
        '%=': 'GENTOK_BNBLCJBN',
        ';': 'SEMICOLON',
        '%': 'MOD',
        '--': 'GENTOK_BTBT',
        '++': 'GENTOK_BRBR',
        '**': 'POWER',
        '&&': 'LAND',
        '>>>=': 'GENTOK_CKCKCKCJ',
        '=?=': 'GENTOK_CJCLCJ',
        '<<<=': 'GENTOK_CICICICJ',
        '!?=': 'GENTOK_BHCLCJ',
        '~^': 'OPXNOR',
        '~&': 'OPNAND',
        '^~': 'GENTOK_DQEW',
        '^=': 'GENTOK_DQCJ',
        '/=': 'GENTOK_BVCJ',
        '-=': 'GENTOK_BTCJ',
        '-:': 'MINUSCOLON',
        '+=': 'GENTOK_BRCJ',
        '+:': 'PLUSCOLON',
        '*=': 'GENTOK_BQCJ',
        '\'0': 'GENTOK_BNBW',
        '>>>': 'RSHIFTA',
        '>>=': 'GENTOK_CKCKCJ',
        '===': 'EQL',
        '<<=': 'GENTOK_CICICJ',
        '<<<': 'LSHIFTA',
        ':/': 'GENTOK_CGBV',
        '!==': 'NEL',
        '^': 'OPXOR',
        '?': 'QUESTION',
        '/': 'DIVIDE',
        '.': 'DOT',
        '-': 'MINUS',
        ',': 'COMMA',
        '+': 'PLUS',
        '*': 'TIMES',
        ')': 'RPAREN',
        '(': 'LPAREN',
        '\'': 'QUOTE',
        '&': 'OPAND',
        '#': 'DELAY',
        '>>': 'RSHIFT',
        '>=': 'GE',
        '==': 'EQ',
        '<=': 'LE',
        '<<': 'LSHIFT',
        ':=': 'GENTOK_CGCJ',
        '!=': 'NE',
        '~': 'OPNOT',
        '@': 'AT',
        '>': 'GT',
        '=': 'ASSIGNEQ',
        '<': 'LT',
        ':': 'COLON',
        '!': 'LNOT',

    }


class IntegralNumber(SVAstNode):
    """
    integral_number : LIT_DEC 
     | LIT_OCT 
     | LIT_BIN 
     | LIT_HEX
    """
    fields = (Token('LIT_DEC'), Token('LIT_OCT'), Token('LIT_BIN'), Token('LIT_HEX'))


class PortDirection(SVAstNode):
    """
    port_direction : GENKW_INPUT 
     | GENKW_OUTPUT
    """
    fields = (Token('GENKW_INPUT'), Token('GENKW_OUTPUT'))


class RangeSign(SVAstNode):
    """
    range_sign : COLON 
     | MINUSCOLON 
     | PLUSCOLON
    """
    fields = (Operator('COLON'), Operator('MINUSCOLON'), Operator('PLUSCOLON'))


class Range(SVAstNode):
    """
    range : LBRACKET expr range_sign expr RBRACKET 
     | LBRACKET expr RBRACKET
    """
    fields = []


class VariableType(SVAstNode):
    """
    variable_type : GENKW_WIRE GENKW_SIGNED_opt 
     | GENKW_LOGIC GENKW_SIGNED_opt 
     | GENKW_WAND GENKW_SIGNED_opt 
     | GENKW_WOR GENKW_SIGNED_opt
    GENKW_SIGNED_opt : empty 
     |  GENKW_SIGNED 
    GENKW_SIGNED_opt : empty 
     |  GENKW_SIGNED 
    GENKW_SIGNED_opt : empty 
     |  GENKW_SIGNED 
    GENKW_SIGNED_opt : empty 
     |  GENKW_SIGNED 
    """
    fields = ([Token('GENKW_WIRE'), Optional(Token('GENKW_SIGNED'))], [Token('GENKW_LOGIC'), Optional(Token('GENKW_SIGNED'))], [Token('GENKW_WAND'), Optional(Token('GENKW_SIGNED'))], [Token('GENKW_WOR'), Optional(Token('GENKW_SIGNED'))])


class ParameterDecl(SVAstNode):
    """
    parameter_decl : GENKW_PARAMETER identifier ASSIGNEQ expr
    """
    fields = []


class ParameterCall(SVAstNode):
    """
    parameter_call : DOT identifier LPAREN expr RPAREN 
     | identifier
    """
    fields = []


class ParameterList(SVAstNode):
    """
    parameter_list : temp_concat_1_star
    temp_concat_1_star : empty 
     |  temp_concat_1  temp_concat_1_star
    temp_concat_1 : parameter_decl SEMICOLON
    """
    fields = Star([ParameterDecl, Operator('SEMICOLON')])


class ParameterCallList(SVAstNode):
    """
    parameter_call_list : temp_concat_2_star
    temp_concat_2_star : empty 
     |  temp_concat_2  temp_concat_2_star
    temp_concat_2 : COMMA parameter_call
    """
    fields = Star([Operator('COMMA'), ParameterCall])


class ParameterBlock(SVAstNode):
    """
    parameter_block : DELAY LPAREN parameter_list RPAREN
    """
    fields = [Operator('DELAY'), Operator('LPAREN'), ParameterList, Operator('RPAREN')]


class ParameterCallBlock(SVAstNode):
    """
    parameter_call_block : DELAY LPAREN parameter_call parameter_call_list RPAREN
    """
    fields = [Operator('DELAY'), Operator('LPAREN'), ParameterCall, ParameterCallList, Operator('RPAREN')]


class PortArgumentType(SVAstNode):
    """
    port_argument_type : GENKW_LOGIC 
     | GENKW_WIRE 
     | GENKW_WAND 
     | GENKW_WOR 
     | identifier
    """
    fields = []


class PortArgumentTypeDecl(SVAstNode):
    """
    port_argument_type_decl : range_star identifier
    range_star : empty 
     |  range  range_star
    """
    fields = []


class PortArgumentDecl(SVAstNode):
    """
    port_argument_decl : range_star identifier 
     | port_argument_type port_argument_type_decl
    range_star : empty 
     |  range  range_star
    """
    fields = []


class ArgumentDecl(SVAstNode):
    """
    argument_decl : identifier 
     | port_direction port_argument_decl
    """
    fields = []


class ArgumentList(SVAstNode):
    """
    argument_list : temp_concat_4_opt
    temp_concat_4_opt : empty 
     |  temp_concat_4 
    temp_concat_4 : argument_decl temp_concat_3_star
    temp_concat_3_star : empty 
     |  temp_concat_3  temp_concat_3_star
    temp_concat_3 : COMMA argument_decl
    """
    fields = Optional([ArgumentDecl, Star([Operator('COMMA'), ArgumentDecl])])


class ArgumentBlock(SVAstNode):
    """
    argument_block : LPAREN argument_list RPAREN
    """
    fields = [Operator('LPAREN'), ArgumentList, Operator('RPAREN')]


class VariableEqualDeclStmt(SVAstNode):
    """
    variable_equal_decl_stmt : ASSIGNEQ expr
    """
    fields = []


class VariableSingleDecl(SVAstNode):
    """
    variable_single_decl : range_star identifier range_star variable_equal_decl_stmt_opt
    variable_equal_decl_stmt_opt : empty 
     |  variable_equal_decl_stmt 
    range_star : empty 
     |  range  range_star
    range_star : empty 
     |  range  range_star
    """
    fields = []


class VariableTypeDeclStmt(SVAstNode):
    """
    variable_type_decl_stmt : variable_single_decl temp_concat_5_star
    temp_concat_5_star : empty 
     |  temp_concat_5  temp_concat_5_star
    temp_concat_5 : COMMA variable_single_decl
    """
    fields = [VariableSingleDecl, Star([Operator('COMMA'), VariableSingleDecl])]


class VariableDeclStmt(SVAstNode):
    """
    variable_decl_stmt : variable_type variable_type_decl_stmt SEMICOLON 
     | port_direction variable_type_opt variable_type_decl_stmt SEMICOLON
    variable_type_opt : empty 
     |  variable_type 
    """
    fields = ([VariableType, VariableTypeDeclStmt, Operator('SEMICOLON')], [PortDirection, Optional(VariableType), VariableTypeDeclStmt, Operator('SEMICOLON')])


class AssignmentStmt(SVAstNode):
    """
    assignment_stmt : GENKW_ASSIGN blockassign_stmt
    """
    fields = []


class ParameterKeyword(SVAstNode):
    """
    parameter_keyword : GENKW_LOCALPARAM 
     | GENKW_PARAMETER
    """
    fields = (Token('GENKW_LOCALPARAM'), Token('GENKW_PARAMETER'))


class ParameterStmt(SVAstNode):
    """
    parameter_stmt : parameter_keyword identifier ASSIGNEQ expr SEMICOLON
    """
    fields = []


class BlockassignSym(SVAstNode):
    """
    blockassign_sym : ASSIGNEQ 
     | GENTOK_BRCJ 
     | GENTOK_BTCJ 
     | GENTOK_BQCJ 
     | GENTOK_BVCJ 
     | GENTOK_BNBLCJBN 
     | GENTOK_DQCJ 
     | GENTOK_BNEUCJBN 
     | GENTOK_CICICJ 
     | GENTOK_CKCKCJ 
     | GENTOK_CICICICJ 
     | GENTOK_CKCKCKCJ 
     | GENTOK_CGCJ 
     | GENTOK_CGBV
    """
    fields = (Operator('ASSIGNEQ'), Operator('GENTOK_BRCJ'), Operator('GENTOK_BTCJ'), Operator('GENTOK_BQCJ'), Operator('GENTOK_BVCJ'), Operator('GENTOK_BNBLCJBN'), Operator('GENTOK_DQCJ'), Operator('GENTOK_BNEUCJBN'), Operator('GENTOK_CICICJ'), Operator('GENTOK_CKCKCJ'), Operator('GENTOK_CICICICJ'), Operator('GENTOK_CKCKCKCJ'), Operator('GENTOK_CGCJ'), Operator('GENTOK_CGBV'))


class BlockassignStmt(SVAstNode):
    """
    blockassign_stmt : expr blockassign_sym expr SEMICOLON
    """
    fields = []


AssignmentStmt.fields = [Token('GENKW_ASSIGN'), BlockassignStmt]
# AssignmentStmt.rescan()


class Delay(SVAstNode):
    """
    delay : DELAY integral_number 
     | DELAY LPAREN integral_number RPAREN
    """
    fields = ([Operator('DELAY'), IntegralNumber], [Operator('DELAY'), Operator('LPAREN'), IntegralNumber, Operator('RPAREN')])


class NonblockassignStmt(SVAstNode):
    """
    nonblockassign_stmt : expr LE delay_opt expr SEMICOLON %prec LEASSIGN
    delay_opt : empty 
     |  delay 
    """
    fields = []


class TypeName(SVAstNode):
    """
    type_name : GENKW_LOGIC 
     | GENKW_WIRE 
     | GENKW_WAND 
     | GENKW_WOR 
     | identifier
    """
    fields = []


class TypeExpr(SVAstNode):
    """
    type_expr : type_name 
     | type_name range
    """
    fields = (TypeName, [TypeName, Range])


class EnumItem(SVAstNode):
    """
    enum_item : identifier temp_concat_6_opt
    temp_concat_6_opt : empty 
     |  temp_concat_6 
    temp_concat_6 : ASSIGNEQ expr
    """
    fields = []


class EnumList(SVAstNode):
    """
    enum_list : enum_item temp_concat_7_star
    temp_concat_7_star : empty 
     |  temp_concat_7  temp_concat_7_star
    temp_concat_7 : COMMA enum_item
    """
    fields = [EnumItem, Star([Operator('COMMA'), EnumItem])]


class EnumExpr(SVAstNode):
    """
    enum_expr : GENKW_ENUM type_expr LBRACE enum_list RBRACE
    """
    fields = [Token('GENKW_ENUM'), TypeExpr, Operator('LBRACE'), EnumList, Operator('RBRACE')]


class StructItem(SVAstNode):
    """
    struct_item : type_expr identifier
    """
    fields = []


class StructList(SVAstNode):
    """
    struct_list : temp_concat_8_star
    temp_concat_8_star : empty 
     |  temp_concat_8  temp_concat_8_star
    temp_concat_8 : struct_item SEMICOLON
    """
    fields = Star([StructItem, Operator('SEMICOLON')])


class StructExpr(SVAstNode):
    """
    struct_expr : GENKW_STRUCT GENKW_PACKED_opt LBRACE struct_list RBRACE
    GENKW_PACKED_opt : empty 
     |  GENKW_PACKED 
    """
    fields = [Token('GENKW_STRUCT'), Optional(Token('GENKW_PACKED')), Operator('LBRACE'), StructList, Operator('RBRACE')]


class UnionExpr(SVAstNode):
    """
    union_expr : GENKW_UNION GENKW_PACKED_opt LBRACE struct_list RBRACE
    GENKW_PACKED_opt : empty 
     |  GENKW_PACKED 
    """
    fields = [Token('GENKW_UNION'), Optional(Token('GENKW_PACKED')), Operator('LBRACE'), StructList, Operator('RBRACE')]


class EnumStmt(SVAstNode):
    """
    enum_stmt : enum_expr identifier variable_equal_decl_stmt_opt SEMICOLON
    variable_equal_decl_stmt_opt : empty 
     |  variable_equal_decl_stmt 
    """
    fields = []


class StructStmt(SVAstNode):
    """
    struct_stmt : struct_expr identifier variable_equal_decl_stmt_opt SEMICOLON
    variable_equal_decl_stmt_opt : empty 
     |  variable_equal_decl_stmt 
    """
    fields = []


class UnionStmt(SVAstNode):
    """
    union_stmt : union_expr identifier variable_equal_decl_stmt_opt SEMICOLON
    variable_equal_decl_stmt_opt : empty 
     |  variable_equal_decl_stmt 
    """
    fields = []


class CastExpr(SVAstNode):
    """
    cast_expr : identifier QUOTE LPAREN expr RPAREN
    """
    fields = []


class PackExpr(SVAstNode):
    """
    pack_expr : QUOTE LBRACE expr temp_concat_9_star RBRACE
    temp_concat_9_star : empty 
     |  temp_concat_9  temp_concat_9_star
    temp_concat_9 : COMMA expr
    """
    fields = []


class Concatentation(SVAstNode):
    """
    concatentation : LBRACE expr temp_concat_10_star RBRACE
    temp_concat_10_star : empty 
     |  temp_concat_10  temp_concat_10_star
    temp_concat_10 : COMMA expr
    """
    fields = []


class MultiConcatentation(SVAstNode):
    """
    multi_concatentation : LBRACE expr LBRACE expr temp_concat_11_star RBRACE RBRACE
    temp_concat_11_star : empty 
     |  temp_concat_11  temp_concat_11_star
    temp_concat_11 : COMMA expr
    """
    fields = []


class Expr(SVAstNode):
    """
    expr : integral_number 
     | identifier 
     | LIT_FLOAT 
     | expr DOT expr 
     | LPAREN expr RPAREN 
     | expr range 
     | expr PLUS expr 
     | expr POWER expr 
     | expr MINUS expr 
     | expr TIMES expr 
     | expr DIVIDE expr 
     | expr MOD expr 
     | expr LAND expr 
     | expr LOR expr 
     | expr OPAND expr 
     | expr OPNAND expr 
     | expr OR expr 
     | expr NOR expr 
     | expr OPXOR expr 
     | expr OPXNOR expr 
     | expr LSHIFTA expr 
     | expr LSHIFT expr 
     | expr RSHIFTA expr 
     | expr RSHIFT expr 
     | expr GE expr 
     | expr GT expr 
     | expr LT expr 
     | expr EQ expr 
     | expr EQL expr 
     | expr NE expr 
     | expr NEL expr 
     | expr GENTOK_CJCLCJ expr 
     | expr GENTOK_BHCLCJ expr 
     | expr GENTOK_BRBR 
     | expr GENTOK_BTBT 
     | LNOT expr 
     | MINUS expr %prec UNARYMINUS 
     | PLUS expr %prec UNARYPLUS 
     | OPNOT expr 
     | OPAND expr %prec UNARYAND 
     | OR expr %prec UNARYOR 
     | OPXOR expr %prec UNARYXOR 
     | GENTOK_DQEW expr %prec UNARYXNOR 
     | expr QUESTION expr COLON expr %prec COND 
     | concatentation 
     | multi_concatentation 
     | struct_expr 
     | enum_expr 
     | union_expr 
     | cast_expr 
     | pack_expr 
     | GENTOK_BKDVEEEHDZBY LPAREN expr RPAREN 
     | GENTOK_BNBW
    """
    fields = []


NonblockassignStmt.fields = [Expr, Operator('LE'), Optional(Delay), Expr, Concat(Operator('SEMICOLON'), Token('%prec LEASSIGN'), force_inline=True)]
# NonblockassignStmt.rescan()


Concatentation.fields = [Operator('LBRACE'), Expr, Star([Operator('COMMA'), Expr]), Operator('RBRACE')]
# Concatentation.rescan()


MultiConcatentation.fields = [Operator('LBRACE'), Expr, Operator('LBRACE'), Expr, Star([Operator('COMMA'), Expr]), Operator('RBRACE'), Operator('RBRACE')]
# MultiConcatentation.rescan()


VariableEqualDeclStmt.fields = [Operator('ASSIGNEQ'), Expr]
# VariableEqualDeclStmt.rescan()


BlockassignStmt.fields = [Expr, BlockassignSym, Expr, Operator('SEMICOLON')]
# BlockassignStmt.rescan()


PackExpr.fields = [Operator('QUOTE'), Operator('LBRACE'), Expr, Star([Operator('COMMA'), Expr]), Operator('RBRACE')]
# PackExpr.rescan()


Range.fields = ([Operator('LBRACKET'), Expr, RangeSign, Expr, Operator('RBRACKET')], [Operator('LBRACKET'), Expr, Operator('RBRACKET')])
# Range.rescan()


class IfStmt(SVAstNode):
    """
    if_stmt : GENKW_IF LPAREN expr RPAREN inner_stmt %prec THEN 
     | GENKW_IF LPAREN expr RPAREN inner_stmt GENKW_ELSE inner_stmt
    """
    fields = []


class ForAssExpr(SVAstNode):
    """
    for_ass_expr : expr 
     | expr ASSIGNEQ expr 
     | expr GENTOK_BRCJ expr 
     | expr GENTOK_BTCJ expr
    """
    fields = (Expr, [Expr, Operator('ASSIGNEQ'), Expr], [Expr, Operator('GENTOK_BRCJ'), Expr], [Expr, Operator('GENTOK_BTCJ'), Expr])


class ForStmt(SVAstNode):
    """
    for_stmt : GENKW_FOR LPAREN for_ass_expr SEMICOLON expr SEMICOLON for_ass_expr RPAREN for_inner_stmt
    """
    fields = []


class GenerateStmt(SVAstNode):
    """
    generate_stmt : GENKW_GENERATE stmt GENKW_ENDGENERATE
    """
    fields = []


class CaseKeyword(SVAstNode):
    """
    case_keyword : GENKW_CASE 
     | GENKW_CASEX 
     | GENKW_CASEZ
    """
    fields = (Token('GENKW_CASE'), Token('GENKW_CASEX'), Token('GENKW_CASEZ'))


class CaseLabel(SVAstNode):
    """
    case_label : expr temp_concat_12_star 
     | GENKW_DEFAULT
    temp_concat_12_star : empty 
     |  temp_concat_12  temp_concat_12_star
    temp_concat_12 : COMMA expr
    """
    fields = ([Expr, Star([Operator('COMMA'), Expr])], Token('GENKW_DEFAULT'))


class CaseLabelStmt(SVAstNode):
    """
    case_label_stmt : case_label COLON inner_stmt
    """
    fields = []


class CaseStmt(SVAstNode):
    """
    case_stmt : GENKW_PRIORITY_opt case_keyword LPAREN expr RPAREN case_label_stmt case_label_stmt_star GENKW_ENDCASE
    case_label_stmt_star : empty 
     |  case_label_stmt  case_label_stmt_star
    GENKW_PRIORITY_opt : empty 
     |  GENKW_PRIORITY 
    """
    fields = [Optional(Token('GENKW_PRIORITY')), CaseKeyword, Operator('LPAREN'), Expr, Operator('RPAREN'), CaseLabelStmt, Star(CaseLabelStmt), Token('GENKW_ENDCASE')]


class AlwaysCombStmt(SVAstNode):
    """
    always_comb_stmt : GENKW_ALWAYS_COMB inner_stmt
    """
    fields = []


class EdgeDirection(SVAstNode):
    """
    edge_direction : GENKW_POSEDGE 
     | GENKW_NEGEDGE
    """
    fields = (Token('GENKW_POSEDGE'), Token('GENKW_NEGEDGE'))


class AlwaysFfStmt(SVAstNode):
    """
    always_ff_stmt : GENKW_ALWAYS_FF AT LPAREN edge_direction identifier RPAREN inner_stmt
    """
    fields = []


class ModuleArgumentDecl(SVAstNode):
    """
    module_argument_decl : DOT identifier temp_concat_13_opt 
     | expr
    temp_concat_13_opt : empty 
     |  temp_concat_13 
    temp_concat_13 : LPAREN expr RPAREN
    """
    fields = []


class ModuleArgumentList(SVAstNode):
    """
    module_argument_list : temp_concat_15_opt
    temp_concat_15_opt : empty 
     |  temp_concat_15 
    temp_concat_15 : module_argument_decl temp_concat_14_star
    temp_concat_14_star : empty 
     |  temp_concat_14  temp_concat_14_star
    temp_concat_14 : COMMA module_argument_decl
    """
    fields = Optional([ModuleArgumentDecl, Star([Operator('COMMA'), ModuleArgumentDecl])])


class ModuleUseStmt(SVAstNode):
    """
    module_use_stmt : identifier identifier range_star LPAREN module_argument_list RPAREN SEMICOLON 
     | identifier parameter_call_block identifier range_star LPAREN module_argument_list RPAREN SEMICOLON 
     | expr identifier variable_equal_decl_stmt_opt SEMICOLON 
     | identifier identifier variable_equal_decl_stmt_opt SEMICOLON
    variable_equal_decl_stmt_opt : empty 
     |  variable_equal_decl_stmt 
    variable_equal_decl_stmt_opt : empty 
     |  variable_equal_decl_stmt 
    range_star : empty 
     |  range  range_star
    range_star : empty 
     |  range  range_star
    """
    fields = []


class AutomaticVarStmt(SVAstNode):
    """
    automatic_var_stmt : GENKW_AUTOMATIC type_expr identifier SEMICOLON
    """
    fields = []


class OuterStmtBlock(SVAstNode):
    """
    outer_stmt_block : GENKW_BEGIN temp_concat_16_opt outer_stmt_star GENKW_END
    outer_stmt_star : empty 
     |  outer_stmt  outer_stmt_star
    temp_concat_16_opt : empty 
     |  temp_concat_16 
    temp_concat_16 : COLON identifier
    """
    fields = []


class InnerStmtBlock(SVAstNode):
    """
    inner_stmt_block : GENKW_BEGIN temp_concat_17_opt inner_stmt_star GENKW_END
    inner_stmt_star : empty 
     |  inner_stmt  inner_stmt_star
    temp_concat_17_opt : empty 
     |  temp_concat_17 
    temp_concat_17 : COLON identifier
    """
    fields = []


class OuterStmt(SVAstNode):
    """
    outer_stmt : expr_opt SEMICOLON 
     | variable_decl_stmt 
     | assignment_stmt 
     | parameter_stmt 
     | struct_stmt 
     | enum_stmt 
     | union_stmt 
     | module_use_stmt 
     | always_comb_stmt 
     | always_ff_stmt 
     | outer_stmt_block
    expr_opt : empty 
     |  expr 
    """
    fields = ([Optional(Expr), Operator('SEMICOLON')], VariableDeclStmt, AssignmentStmt, ParameterStmt, StructStmt, EnumStmt, UnionStmt, ModuleUseStmt, AlwaysCombStmt, AlwaysFfStmt, OuterStmtBlock)


class InnerStmt(SVAstNode):
    """
    inner_stmt : expr_opt SEMICOLON 
     | blockassign_stmt 
     | nonblockassign_stmt %prec NONBLOCK 
     | variable_decl_stmt 
     | module_use_stmt 
     | if_stmt 
     | assignment_stmt 
     | for_stmt 
     | case_stmt 
     | always_comb_stmt 
     | generate_stmt 
     | automatic_var_stmt 
     | localparam_stmt 
     | inner_stmt_block
    expr_opt : empty 
     |  expr 
    """
    fields = []


IfStmt.fields = ([Token('GENKW_IF'), Operator('LPAREN'), Expr, Operator('RPAREN'), Concat(InnerStmt, Token('%prec THEN'), force_inline=True)], [Token('GENKW_IF'), Operator('LPAREN'), Expr, Operator('RPAREN'), InnerStmt, Token('GENKW_ELSE'), InnerStmt])
# IfStmt.rescan()


AlwaysCombStmt.fields = [Token('GENKW_ALWAYS_COMB'), InnerStmt]
# AlwaysCombStmt.rescan()


CaseLabelStmt.fields = [CaseLabel, Operator('COLON'), InnerStmt]
# CaseLabelStmt.rescan()


class ForInnerStmtBlock(SVAstNode):
    """
    for_inner_stmt_block : GENKW_BEGIN temp_concat_18_opt for_inner_stmt_star GENKW_END
    for_inner_stmt_star : empty 
     |  for_inner_stmt  for_inner_stmt_star
    temp_concat_18_opt : empty 
     |  temp_concat_18 
    temp_concat_18 : COLON identifier
    """
    fields = []


class ForInnerStmt(SVAstNode):
    """
    for_inner_stmt : assignment_stmt 
     | blockassign_stmt 
     | nonblockassign_stmt %prec NONBLOCK 
     | variable_decl_stmt 
     | module_use_stmt 
     | if_stmt 
     | case_stmt 
     | for_stmt 
     | automatic_var_stmt 
     | for_inner_stmt_block
    """
    fields = (AssignmentStmt, BlockassignStmt, Concat(NonblockassignStmt, Token('%prec NONBLOCK'), force_inline=True), VariableDeclStmt, ModuleUseStmt, IfStmt, CaseStmt, ForStmt, AutomaticVarStmt, ForInnerStmtBlock)


ForStmt.fields = [Token('GENKW_FOR'), Operator('LPAREN'), ForAssExpr, Operator('SEMICOLON'), Expr, Operator('SEMICOLON'), ForAssExpr, Operator('RPAREN'), ForInnerStmt]
# ForStmt.rescan()


class Stmt(SVAstNode):
    """
    stmt : outer_stmt 
     | inner_stmt
    """
    fields = (OuterStmt, InnerStmt)


GenerateStmt.fields = [Token('GENKW_GENERATE'), Stmt, Token('GENKW_ENDGENERATE')]
# GenerateStmt.rescan()


class Identifier(SVAstNode):
    """
    identifier : IDENTIFIER
    """
    fields = Token('IDENTIFIER')


TypeName.fields = (Token('GENKW_LOGIC'), Token('GENKW_WIRE'), Token('GENKW_WAND'), Token('GENKW_WOR'), Identifier)
# TypeName.rescan()


ParameterDecl.fields = [Token('GENKW_PARAMETER'), Identifier, Operator('ASSIGNEQ'), Expr]
# ParameterDecl.rescan()


ArgumentDecl.fields = (Identifier, [PortDirection, PortArgumentDecl])
# ArgumentDecl.rescan()


EnumItem.fields = [Identifier, Optional([Operator('ASSIGNEQ'), Expr])]
# EnumItem.rescan()


PortArgumentType.fields = (Token('GENKW_LOGIC'), Token('GENKW_WIRE'), Token('GENKW_WAND'), Token('GENKW_WOR'), Identifier)
# PortArgumentType.rescan()


OuterStmtBlock.fields = [Token('GENKW_BEGIN'), Optional([Operator('COLON'), Identifier]), Star(OuterStmt), Token('GENKW_END')]
# OuterStmtBlock.rescan()


AlwaysFfStmt.fields = [Token('GENKW_ALWAYS_FF'), Operator('AT'), Operator('LPAREN'), EdgeDirection, Identifier, Operator('RPAREN'), InnerStmt]
# AlwaysFfStmt.rescan()


StructStmt.fields = [StructExpr, Identifier, Optional(VariableEqualDeclStmt), Operator('SEMICOLON')]
# StructStmt.rescan()


ForInnerStmtBlock.fields = [Token('GENKW_BEGIN'), Optional([Operator('COLON'), Identifier]), Star(ForInnerStmt), Token('GENKW_END')]
# ForInnerStmtBlock.rescan()


ParameterCall.fields = ([Operator('DOT'), Identifier, Operator('LPAREN'), Expr, Operator('RPAREN')], Identifier)
# ParameterCall.rescan()


PortArgumentDecl.fields = ([Star(Range), Identifier], [PortArgumentType, PortArgumentTypeDecl])
# PortArgumentDecl.rescan()


UnionStmt.fields = [UnionExpr, Identifier, Optional(VariableEqualDeclStmt), Operator('SEMICOLON')]
# UnionStmt.rescan()


ModuleUseStmt.fields = ([Identifier, Identifier, Star(Range), Operator('LPAREN'), ModuleArgumentList, Operator('RPAREN'), Operator('SEMICOLON')], [Identifier, ParameterCallBlock, Identifier, Star(Range), Operator('LPAREN'), ModuleArgumentList, Operator('RPAREN'), Operator('SEMICOLON')], [Expr, Identifier, Optional(VariableEqualDeclStmt), Operator('SEMICOLON')], [Identifier, Identifier, Optional(VariableEqualDeclStmt), Operator('SEMICOLON')])
# ModuleUseStmt.rescan()


InnerStmtBlock.fields = [Token('GENKW_BEGIN'), Optional([Operator('COLON'), Identifier]), Star(InnerStmt), Token('GENKW_END')]
# InnerStmtBlock.rescan()


AutomaticVarStmt.fields = [Token('GENKW_AUTOMATIC'), TypeExpr, Identifier, Operator('SEMICOLON')]
# AutomaticVarStmt.rescan()


Expr.fields = (IntegralNumber, Identifier, Token('LIT_FLOAT'), [Expr, Operator('DOT'), Expr], [Operator('LPAREN'), Expr, Operator('RPAREN')], [Expr, Range], [Expr, Operator('PLUS'), Expr], [Expr, Operator('POWER'), Expr], [Expr, Operator('MINUS'), Expr], [Expr, Operator('TIMES'), Expr], [Expr, Operator('DIVIDE'), Expr], [Expr, Operator('MOD'), Expr], [Expr, Operator('LAND'), Expr], [Expr, Operator('LOR'), Expr], [Expr, Operator('OPAND'), Expr], [Expr, Operator('OPNAND'), Expr], [Expr, Operator('OR'), Expr], [Expr, Operator('NOR'), Expr], [Expr, Operator('OPXOR'), Expr], [Expr, Operator('OPXNOR'), Expr], [Expr, Operator('LSHIFTA'), Expr], [Expr, Operator('LSHIFT'), Expr], [Expr, Operator('RSHIFTA'), Expr], [Expr, Operator('RSHIFT'), Expr], [Expr, Operator('GE'), Expr], [Expr, Operator('GT'), Expr], [Expr, Operator('LT'), Expr], [Expr, Operator('EQ'), Expr], [Expr, Operator('EQL'), Expr], [Expr, Operator('NE'), Expr], [Expr, Operator('NEL'), Expr], [Expr, Operator('GENTOK_CJCLCJ'), Expr], [Expr, Operator('GENTOK_BHCLCJ'), Expr], [Expr, Operator('GENTOK_BRBR')], [Expr, Operator('GENTOK_BTBT')], [Operator('LNOT'), Expr], [Operator('MINUS'), Concat(Expr, Token('%prec UNARYMINUS'), force_inline=True)], [Operator('PLUS'), Concat(Expr, Token('%prec UNARYPLUS'), force_inline=True)], [Operator('OPNOT'), Expr], [Operator('OPAND'), Concat(Expr, Token('%prec UNARYAND'), force_inline=True)], [Operator('OR'), Concat(Expr, Token('%prec UNARYOR'), force_inline=True)], [Operator('OPXOR'), Concat(Expr, Token('%prec UNARYXOR'), force_inline=True)], [Operator('GENTOK_DQEW'), Concat(Expr, Token('%prec UNARYXNOR'), force_inline=True)], [Expr, Operator('QUESTION'), Expr, Operator('COLON'), Concat(Expr, Token('%prec COND'), force_inline=True)], Concatentation, MultiConcatentation, StructExpr, EnumExpr, UnionExpr, CastExpr, PackExpr, [Operator('GENTOK_BKDVEEEHDZBY'), Operator('LPAREN'), Expr, Operator('RPAREN')], Operator('GENTOK_BNBW'))
# Expr.rescan()


ParameterStmt.fields = [ParameterKeyword, Identifier, Operator('ASSIGNEQ'), Expr, Operator('SEMICOLON')]
# ParameterStmt.rescan()


ModuleArgumentDecl.fields = ([Operator('DOT'), Identifier, Optional([Operator('LPAREN'), Expr, Operator('RPAREN')])], Expr)
# ModuleArgumentDecl.rescan()


PortArgumentTypeDecl.fields = [Star(Range), Identifier]
# PortArgumentTypeDecl.rescan()


CastExpr.fields = [Identifier, Operator('QUOTE'), Operator('LPAREN'), Expr, Operator('RPAREN')]
# CastExpr.rescan()


VariableSingleDecl.fields = [Star(Range), Identifier, Star(Range), Optional(VariableEqualDeclStmt)]
# VariableSingleDecl.rescan()


EnumStmt.fields = [EnumExpr, Identifier, Optional(VariableEqualDeclStmt), Operator('SEMICOLON')]
# EnumStmt.rescan()


StructItem.fields = [TypeExpr, Identifier]
# StructItem.rescan()


class ModuleDecl(SVAstNode):
    """
    module_decl : GENKW_MODULE identifier parameter_block_opt argument_block SEMICOLON stmt_star GENKW_ENDMODULE
    stmt_star : empty 
     |  stmt  stmt_star
    parameter_block_opt : empty 
     |  parameter_block 
    """
    fields = [Token('GENKW_MODULE'), Identifier, Optional(ParameterBlock), ArgumentBlock, Operator('SEMICOLON'), Star(Stmt), Token('GENKW_ENDMODULE')]


class Directive(SVAstNode):
    """
    directive : GENTOK_DSEGEHEIEHEKEMDVEHDXEKDVDX 
     | GENTOK_DSEGEHEBEGEEEBEGDX 
     | GENTOK_DSEIEHEKEMDVEHDXEKDVDX 
     | GENTOK_DSEBEGEEEBEGDX
    """
    fields = (Operator('GENTOK_DSEGEHEIEHEKEMDVEHDXEKDVDX'), Operator('GENTOK_DSEGEHEBEGEEEBEGDX'), Operator('GENTOK_DSEIEHEKEMDVEHDXEKDVDX'), Operator('GENTOK_DSEBEGEEEBEGDX'))


class LocalparamStmt(SVAstNode):
    """
    localparam_stmt : GENKW_LOCALPARAM identifier ASSIGNEQ expr SEMICOLON
    """
    fields = [Token('GENKW_LOCALPARAM'), Identifier, Operator('ASSIGNEQ'), Expr, Operator('SEMICOLON')]


InnerStmt.fields = ([Optional(Expr), Operator('SEMICOLON')], BlockassignStmt, Concat(NonblockassignStmt, Token('%prec NONBLOCK'), force_inline=True), VariableDeclStmt, ModuleUseStmt, IfStmt, AssignmentStmt, ForStmt, CaseStmt, AlwaysCombStmt, GenerateStmt, AutomaticVarStmt, LocalparamStmt, InnerStmtBlock)
# InnerStmt.rescan()


class TypedefType(SVAstNode):
    """
    typedef_type : type_expr 
     | struct_expr 
     | enum_expr 
     | union_expr
    """
    fields = (TypeExpr, StructExpr, EnumExpr, UnionExpr)


class TypedefStmt(SVAstNode):
    """
    typedef_stmt : GENKW_TYPEDEF typedef_type identifier SEMICOLON
    """
    fields = [Token('GENKW_TYPEDEF'), TypedefType, Identifier, Operator('SEMICOLON')]


class DefineStmt(SVAstNode):
    """
    define_stmt : GENTOK_DSDWDXDYEBEGDX identifier expr_star
    expr_star : empty 
     |  expr  expr_star
    """
    fields = [Operator('GENTOK_DSDWDXDYEBEGDX'), Identifier, Star(Expr)]


class Verilog(SVAstNode):
    """
    verilog : verilog_item_star
    verilog_item_star : empty 
     |  verilog_item  verilog_item_star
    """
    fields = []


class VerilogItem(SVAstNode):
    """
    verilog_item : module_decl 
     | directive 
     | define_stmt 
     | typedef_stmt 
     | localparam_stmt 
     | BLOCKCOMMENT 
     | LINECOMMENT
    """
    fields = (ModuleDecl, Directive, DefineStmt, TypedefStmt, LocalparamStmt, Token('BLOCKCOMMENT'), Token('LINECOMMENT'))


Verilog.fields = Star(VerilogItem)
# Verilog.rescan()
