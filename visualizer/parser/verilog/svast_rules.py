"""
Extra rules for SystemVerilog Ast.
Avoid overwrite.
"""
from collections import OrderedDict
from visualizer.parser.verilog.lexer import op_dict, kw_dict
from visualizer.parser.verilog.svast import *
from visualizer.parser.verilog.expr import *


def analysis(node, collect = None, extras = {}):
    if isinstance(node, AstNode):
        node.analysis(collect, extras)
    elif isinstance(node, list):
        for child in node:
            analysis(child, collect, extras)
    else:
        pass
    if collect is not None:
        collect[node.__class__.__name__].append(node)


SVAstNode.analysis = lambda self, collect = None, extras = {}: analysis(self.node, collect, extras)


def define_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.flatten(self.node, level = -1)
    self.name = flat[1]
    self.expr = ''.join(flat[2:])


DefineStmt.analysis = define_analysis


def typedef_analysis(self, collect = None, extras = {}):
    # typedef TypedefType Identifier ;
    self.__dict__.update(extras)
    flat = self.node
    self.name = flat[2].node
    analysis(flat[1], collect, extras)
    self.type = flat[1].node


TypedefStmt.analysis = typedef_analysis


def enum_expr_analysis(self, collect = None, extras = {}):
    # enum TypeExpr { EnumList } 
    self.__dict__.update(extras)
    self.type = None
    self.range = None
    self.enum = OrderedDict()
    # EnumItem+
    flat = self.select(sel=['TypeExpr', 'EnumItem'], level = -1)
    for item in flat:
        if isinstance(item, TypeExpr):
            analysis(item, collect, extras)
            self.type = item.type
            self.range = item.range
        if isinstance(item, EnumItem):        
            identifier, optexpr = item.node
            if optexpr is not None:
                expr = optexpr[1]
                analysis(expr, collect, extras)
                self.enum[identifier.node] = expr
            else:
                self.enum[identifier.node] = 'default'


EnumExpr.analysis = enum_expr_analysis


def struct_expr_analysis(self, collect = None, extras = {}):
    # struct { struct_list }
    self.__dict__.update(extras)
    self.packed = self.has('packed')
    self.member = OrderedDict()
    mem_list = self.select(['StructItem'])
    for item in mem_list:
        ty, identifier = item.node
        analysis(ty, collect, extras)
        self.member[identifier.node] = ty


StructExpr.analysis = struct_expr_analysis
UnionExpr.analysis = struct_expr_analysis


def integral_number_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    number = self.node
    self.radix = 10
    self.width = None
    self.value = None
    self.has_unknown = False
    if '\'' in number:
        if 'h' in number or 'H' in number:
            self.radix = 16
            width, _, value = number.lower().partition('\'h')
        elif 'd' in number or 'D' in number:
            self.radix = 10
            width, _, value = number.lower().partition('\'d')
        elif 'o' in number or 'O' in number:
            self.radix = 8
            width, _, value = number.lower().partition('\'o')
        elif 'b' in number or 'B' in number:
            self.radix = 2
            width, _, value = number.lower().partition('\'b')
        self.width = width
        if 'z' in value or '?' in value or 'x' in value:
            self.value = value
            self.has_unknown = True
        else:
            self.value = int(value, base=self.radix)
    else:
        # raw integer
        self.value = int(number, 10)


IntegralNumber.analysis = integral_number_analysis


def range_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['Expr'], level = -1)
    if len(flat) == 1:
        analysis(flat[0], collect, extras)
        self.left = flat[0]
    else:
        analysis(flat[0], collect, extras)
        self.left = flat[0]
        analysis(flat[1], collect, extras)
        self.right = flat[1]


Range.analysis = range_analysis


def parameter_block_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['ParameterDecl'])
    self.params = {}
    for item in flat:
        name = item.node[1].node
        analysis(item.node[3], collect, extras)
        value = item.node[3]
        self.params[name] = value


ParameterBlock.analysis = parameter_block_analysis


def parameter_call_block_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['ParameterCall'])
    self.params = {}
    for item in flat:
        if len(item.node) == 1:
            name = item.node[0].node
            self.params[name] = Sym(name, self.modins)
            continue            
        name = item.node[1].node
        analysis(item.node[3], collect, extras)
        value = item.node[3]
        self.params[name] = value


ParameterCallBlock.analysis = parameter_call_block_analysis


def argument_decl_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['Range', 'str'])
    self.name = ''
    self.direction = None
    if self.has('input'):
        self.direction = 'input'
    elif self.has('output'):
        self.direction = 'output'
    self.type = None
    if self.has('logic'):
        self.type = 'logic'
    elif self.has('wire'):
        self.type = 'wire'
    self.signed = self.has('signed')
    self.packed_dim = []
    self.unpacked_dim = []
    self.value = None

    has_meet_id = False
    for item in flat:
        if isinstance(item, str):
            # Here we only handle wire/logic type variable decls.
            # The rest is handled in Module_use_stmt
            if item in kw_dict.keys():
                continue
            has_meet_id = True
            assert self.name == ''
            self.name = item
        elif isinstance(item, Range):
            analysis(item, collect, extras)
            if has_meet_id:
                self.unpacked_dim.append(item)
            else:
                self.packed_dim.append(item)


ArgumentDecl.analysis = argument_decl_analysis


def variable_single_decl_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['Range', 'str', 'Expr'])
    # User-defined type decl is dealt within ModuleOrId
    self.name = ''
    self.packed_dim = []
    self.unpacked_dim = []

    self.value = None

    has_meet_id = False
    has_meet_eq = False
    for item in flat:
        if isinstance(item, str):
            if item == '=':
                has_meet_eq = True
                continue
            if self.name != '':
                import ipdb; ipdb.set_trace()
            self.name = item
            has_meet_id = True
        elif isinstance(item, Range):
            analysis(item, collect, extras)
            if has_meet_id:
                self.unpacked_dim.append(item)
            else:
                self.packed_dim.append(item)
        else:
            analysis(item, collect, extras)
            self.value = item


VariableSingleDecl.analysis = variable_single_decl_analysis


def variable_decl_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['VariableSingleDecl', 'str'])
    self.direction = None
    if self.has('input'):
        self.direction = 'input'
    elif self.has('output'):
        self.direction = 'output'
    self.type = 'logic'
    if self.has('logic'):
        self.type = 'logic'
    elif self.has('wire'):
        self.type = 'wire'
    self.signed = self.has('signed')
    self.vars = []
    for item in flat:
        if isinstance(item, VariableSingleDecl):
            analysis(item, collect, extras)
            item.direction = self.direction
            item.type = self.type
            item.signed = self.signed
            self.vars.append(item)


VariableDeclStmt.analysis = variable_decl_stmt_analysis


def parameter_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    self.name = self.node[1].node
    analysis(self.node[3], collect, extras)
    self.value = self.node[3]
    self.value.expr.eval();
    self.modins.local[self.name] = self.value.expr.value


ParameterStmt.analysis = parameter_stmt_analysis


def assignment_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    analysis(self.node[1], collect, extras)
    self.assign = self.node[1]
    self.assign.is_inner = False


AssignmentStmt.analysis = assignment_stmt_analysis


def blockassign_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    analysis(self.node[0], collect, extras)
    # When assign -> block, should be false, otherwise true
    self.is_inner = False
    self.lhs = self.node[0]
    self.op = self.node[1].node
    analysis(self.node[2], collect, extras)
    self.rhs = self.node[2]


BlockassignStmt.analysis = blockassign_stmt_analysis


def nonblockassign_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    analysis(self.node[0], collect, extras)
    self.is_inner = True
    self.lhs = self.node[0]
    analysis(self.node[3], collect, extras)
    self.rhs = self.node[3]
    self.delay = 'default'
    delay = self.node[2]
    if delay is not None:
        if len(delay.node) == 1:
            self.delay = delay.node
        elif len(delay.node) == 2:
            analysis(delay.node[1], collect, extras)
            self.delay = delay.node[1].value
        else:
            analysis(delay.node[2], collect, extras)
            self.delay = delay.node[2].value


NonblockassignStmt.analysis = nonblockassign_stmt_analysis


def type_expr_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    self.type = self.node[0].node
    self.range = None
    if len(self.node) > 1:
        analysis(self.node[1], collect, extras)
        self.range = self.node[1]


TypeExpr.analysis = type_expr_analysis


def if_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['InnerStmt', 'Expr'])
    cond = flat[0]
    analysis(cond, collect, extras)
    old_cond = extras.get('cond', STrue)
    analysis(flat[1], collect, {**extras, 'cond': And(old_cond, cond)})
    if len(flat) == 3:
        analysis(flat[2], collect, {**extras, 'cond': And(old_cond, Not(cond))})


IfStmt.analysis = if_stmt_analysis


def for_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    init, step = self.select(['ForAssExpr'], level=1)
    # TODO: check cond
    # must be (i < ...)
    cond = self.node[4]
    num = cond.node[1].node[2]
    analysis(num, collect, extras)
    num.expr.eval()
    self.num = num.expr.value
    stmt = self.select(['ForInnerStmt'])
    analysis(stmt[0], collect, {
        **extras,
        'inloop': True,
        'repeat': self.num
    })


ForStmt.analysis = for_stmt_analysis


def stmt_block_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    if 'label' in extras.keys():
        label = extras['label']
    else:
        label = []
    if self.node[1] is not None:
        label.append(self.node[1][1].node)
    for stmt in self.node[2]:
        analysis(stmt, collect, extras = {
            **extras,
            'label': label
        })


InnerStmtBlock.analysis = stmt_block_analysis
OuterStmtBlock.analysis = stmt_block_analysis
ForInnerStmtBlock.analysis = stmt_block_analysis


def case_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['Expr', 'CaseLabelStmt'])
    inspect = flat[0]
    analysis(inspect, collect, extras)
    non_default = SFalse
    for item in flat[1:]:
        # Assume default the last item....
        analysis(item, collect, extras={**extras, 'inspect': inspect, 'non_default': non_default})
        non_default = Or(non_default, item.cond)


CaseStmt.analysis = case_stmt_analysis


def case_label_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    flat = self.select(['CaseLabel', 'InnerStmt'])
    analysis(flat[0], collect, extras)
    old_cond = extras.get('cond', STrue)
    non_default = extras['non_default']
    inspect = extras['inspect']
    if flat[0].is_default:
        analysis(
            flat[1], collect,
            extras={
                **extras,
                'cond': And(old_cond, Not(EQ(inspect, non_default)))
            }
        )
        self.cond = Not(EQ(inspect, non_default))
    else:
        analysis(
            flat[1], collect,
            extras={
                **extras,
                'cond': And(old_cond, EQ(inspect, flat[0].case_label))
            }
        )
        self.cond = EQ(inspect, flat[0].case_label)


CaseLabelStmt.analysis = case_label_stmt_analysis


def case_label_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    self.is_default = False
    init_label = SFalse
    flat = self.select(['Expr', 'str'])
    for item in flat:
        if isinstance(item, str):
            self.is_default = True
            break
        analysis(item, collect, extras)
        init_label = Or(init_label, item)
    self.case_label = init_label


CaseLabel.analysis = case_label_analysis


def always_comb_stmt(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    item = self.node[1]
    analysis(item, collect, extras={
        **extras,
        'delay': 0,
        'inner': True,
    })


AlwaysCombStmt.analysis = always_comb_stmt


def always_ff_stmt(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    item = self.node[-1]
    # Always posedge clock....
    analysis(item, collect, extras={
        **extras,
        'delay': 1,
        'inner': True,
    })


AlwaysFfStmt.analysis = always_ff_stmt


def expr_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    self.expr = None
    if len(self.node) == 1:
        item = self.node[0]
        if isinstance(item, IntegralNumber):
            analysis(item, collect, extras)
            self.expr = SymValue(item)
        elif isinstance(item, Concatentation) or isinstance(item, PackExpr):
            flat = item.select(['Expr'])
            for e in flat:
                analysis(e, collect, extras)
            self.expr = SymConcat(*flat)
        elif isinstance(item, MultiConcatentation):
            flat = item.select(['Expr'])
            for e in flat:
                analysis(e, collect, extras)
            self.expr = Repeat(flat[0], flat[1])
        elif isinstance(item, CastExpr):
            analysis(item.node[3], collect, extras)
            self.expr = Cast(Sym(item.node[0].node), item.node[3])
        elif isinstance(item, Identifier):
            # FIXME: actually parameter can be overwritten outer, only consider default value
            self.expr = Sym(item.node, self.modins)
        elif item == '\'0':
            self.expr = SFalse
        else:
            # Should be impossible, union/struct/enum expr
            import ipdb; ipdb.set_trace()
            analysis(item, collect, extras)
            self.expr = item
    elif len(self.node) == 2:
        item1 = self.node[0]
        item2 = self.node[1]
        if isinstance(self.node[0], Identifier):
            flat = self.select(['str'])
            self.expr = Sym(''.join(flat))
        elif item1 == '!':
            analysis(item2, collect, extras)
            self.expr = Not(item2)
        elif item1 == '~':
            analysis(item2, collect, extras)
            self.expr = BitNot(item2)
        elif item1 == '+':
            analysis(item2, collect, extras)
            self.expr = Pos(item2)
        elif item1 == '-':
            analysis(item2, collect, extras)
            self.expr = Neg(item2)
        elif item1 == '|':
            analysis(item2, collect, extras)
            self.expr = OrBit(item2)
        elif item1 == '&':
            analysis(item2, collect, extras)
            self.expr = AndBit(item2)
        elif item2 == '++':
            analysis(item1, collect, extras)
            self.expr = Add(item1, 1)
        elif item2 == '--':
            analysis(item1, collect, extras)
            self.expr = Sub(item1, 1)
        elif isinstance(item2, Range):
            analysis(item1, collect, extras)
            analysis(item2, collect, extras)
            self.expr = Slice(item1, item2)
        else:
            # Should be other conditions, won't support now.
            import ipdb; ipdb.set_trace()
    elif len(self.node) == 3:
        if self.node[0] == '(':
            analysis(self.node[1], collect, extras)
            self.expr = self.node[1].expr
        elif isinstance(self.node[1], str):
            op = self.node[1]
            analysis(self.node[0], collect, extras)
            analysis(self.node[2], collect, extras)
            if op == '+':
                self.expr = Add(self.node[0], self.node[2])
            elif op == '.':
                self.expr = Dot(self.node[0], self.node[2])
            elif op == '-':
                self.expr = Sub(self.node[0], self.node[2])
            elif op == '**':
                self.expr = Pow(self.node[0], self.node[2])
            elif op == '*':
                self.expr = Mul(self.node[0], self.node[2])
            elif op == '/':
                self.expr = Div(self.node[0], self.node[2])
            elif op == '%':
                self.expr = Mod(self.node[0], self.node[2])
            elif op == '&':
                self.expr = BitAnd(self.node[0], self.node[2])
            elif op == '&&':
                self.expr = And(self.node[0], self.node[2])
            elif op == '|':
                self.expr = BitOr(self.node[0], self.node[2])
            elif op == '||':
                self.expr = Or(self.node[0], self.node[2])
            elif op == '~&':
                self.expr = NAnd(self.node[0], self.node[2])
            elif op == '~|':
                self.expr = NOr(self.node[0], self.node[2])
            elif op == '^':
                self.expr = Xor(self.node[0], self.node[2])
            elif op == '~^':
                self.expr = XNor(self.node[0], self.node[2])
            elif op == '<<<':
                self.expr = ALShift(self.node[0], self.node[2])
            elif op == '<<':
                self.expr = LLShift(self.node[0], self.node[2])
            elif op == '>>>':
                self.expr = ARShift(self.node[0], self.node[2])
            elif op == '>>':
                self.expr = LRShift(self.node[0], self.node[2])
            elif op == '>=':
                self.expr = GE(self.node[0], self.node[2])
            elif op == '<=':
                self.expr = LE(self.node[0], self.node[2])
            elif op == '>':
                self.expr = GT(self.node[0], self.node[2])
            elif op == '<':
                self.expr = LT(self.node[0], self.node[2])
            elif op == '==':
                self.expr = EQ(self.node[0], self.node[2])
            elif op == '===':
                self.expr = EQ(self.node[0], self.node[2])
            elif op == '!=':
                self.expr = NE(self.node[0], self.node[2])
            elif op == '!==':
                self.expr = NE(self.node[0], self.node[2])
            else:
                # Unsupported operators
                import ipdb; ipdb.set_trace()
        else:
            print(self.node)
            import ipdb; ipdb.set_trace()
            # Unknown reason
            raise Exception("What")
    elif len(self.node) == 4:
        # $clog2.
        analysis(self.node[2], collect, extras)
        self.expr = Log2(self.node[2])
    elif len(self.node) == 5:
        if self.node[1] == '?':
            analysis(self.node[0], collect, extras)
            analysis(self.node[2], collect, extras)
            analysis(self.node[4], collect, extras)
            self.expr = TernaryOp(self.node[0], self.node[2], self.node[4])
        else:
            import ipdb; ipdb.set_trace()
    else:
        import ipdb; ipdb.set_trace()


Expr.analysis = expr_analysis


def module_use_stmt_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    self.is_decl = True
    self.port_map = {}
    self.param_map = {}
    self.packed_dim = []
    self.unpacked_dim = []
    self.is_assign = False
    flat = self.select(['ParameterCallBlock', 'ModuleArgumentList'], level=1)
    if len(flat) != 0:
        flat = self.select(['ParameterCallBlock', 'ModuleArgumentList', 'Expr', 'Identifier', 'Range'], level=1)
        self.is_decl = False
        self.type = self.node[0].node
        if self.has('ParameterCallBlock'):
            self.name = self.node[2].node
        else:
            self.name = self.node[1].node
        for item in flat:
            if isinstance(item, ParameterCallBlock):
                analysis(item, collect, extras)
                self.param_map = self.node[1].params
            elif isinstance(item, ModuleArgumentList):
                analysis(item, collect, extras)
                self.port_map = item.port_map
            elif isinstance(item, Range):
                analysis(item, collect, extras)
                self.packed_dim.append(item)
    else:
        if self.has('BlockassignSym'):
            self.is_assign = True
            lhs = self.node[0:2]
            lhs.extend(self.node[2])
            rhs = self.node[4]
            lhs_expr = Expr(lhs)
            rhs_expr = rhs
            op = self.node[3]
            expr = BlockassignStmt([lhs_expr, op, rhs_expr])
            analysis(expr, collect, extras)
            return
        flat = self.select(['Identifier', 'Range'])
        self.type = flat[0].node
        for item in flat[1:]:
            if isinstance(item, Range):
                analysis(item, collect, extras)
                self.packed_dim.append(item)
            elif isinstance(item, Identifier):
                self.name = item.node


ModuleUseStmt.analysis = module_use_stmt_analysis


def module_argument_list_analysis(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    self.port_map = {}
    flat = self.select(['Identifier', 'Expr'])
    last_name = None
    for item in flat:
        if isinstance(item, Identifier):
            self.port_map[item.node] = item.node
            last_name = item.node
        else:
            analysis(item, collect, extras)
            if last_name is not None:
                self.port_map[last_name] = item
                last_name = None
            else:
                # TODO: analyze module argument.
                pass


ModuleArgumentList.analysis = module_argument_list_analysis


def module_decl(self, collect = None, extras = {}):
    self.__dict__.update(extras)
    self.name = self.node[1].node
    self.local = {}
    for item in self.node:
        analysis(item, collect, {
            **extras,
            'module': self.name,
            'modins': self,
        })


ModuleDecl.analysis = module_decl


