






import "DPI-C" context function void read_ROB_ENTRY(ROB_ENTRY i);


import "DPI-C" context function void read_BRANCH_PACKET(BRANCH_PACKET i);


import "DPI-C" context function void read_DECODE_MT_PACKET(DECODE_MT_PACKET i);


import "DPI-C" context function void read_DECODE_RS_PACKET(DECODE_RS_PACKET i);


import "DPI-C" context function void read_ROB_MT_PACKET(ROB_MT_PACKET i);


import "DPI-C" context function void read_MT_ROB_PACKET(MT_ROB_PACKET i);


import "DPI-C" context function void read_MT_ENTRY(MT_ENTRY i);


import "DPI-C" context function void read_MT_RS_PACKET(MT_RS_PACKET i);


import "DPI-C" context function void read_CDB_PACKET(CDB_PACKET i);


import "DPI-C" context function void read_RS_ENTRY(RS_ENTRY i);


import "DPI-C" context function void read_IF_DECODE_PACKET(IF_DECODE_PACKET i);


import "DPI-C" context function void read_DECODE_ROB_PACKET(DECODE_ROB_PACKET i);


import "DPI-C" context function void read_FU_ISSUE_PACKET(FU_ISSUE_PACKET i);

module testbench;

    always begin
        #20;
        clock = ~clock;
    end

    task exit_on_error;
        #1;
        $display("@@@Failed at time %f", $time);
        $finish;
    endtask







	task fill_ROB_ENTRY;
		output ROB_ENTRY p;
		input longint told_idx_, t_idx_, dest_reg_idx_, complete_, branch_, branch_target_, rollback_, halt_;
		p.told_idx = told_idx_;
		p.t_idx = t_idx_;
		p.dest_reg_idx = dest_reg_idx_;
		p.complete = complete_;
		p.branch = branch_;
		p.branch_target = branch_target_;
		p.rollback = rollback_;
		p.halt = halt_;
	endtask

	task clear_ROB_ENTRY;
		output ROB_ENTRY p;
		p.told_idx = 0;
		p.t_idx = 0;
		p.dest_reg_idx = 0;
		p.complete = 0;
		p.branch = 0;
		p.branch_target = 0;
		p.rollback = 0;
		p.halt = 0;
	endtask


	task fill_BRANCH_PACKET;
		output BRANCH_PACKET p;
		input longint valid_, branch_target_, branch_address_;
		p.valid = valid_;
		p.branch_target = branch_target_;
		p.branch_address = branch_address_;
	endtask

	task clear_BRANCH_PACKET;
		output BRANCH_PACKET p;
		p.valid = 0;
		p.branch_target = 0;
		p.branch_address = 0;
	endtask


	task fill_DECODE_MT_PACKET;
		output DECODE_MT_PACKET p;
		input longint valid_, rs1_idx_, rs2_idx_, dest_reg_idx_;
		p.valid = valid_;
		p.rs1_idx = rs1_idx_;
		p.rs2_idx = rs2_idx_;
		p.dest_reg_idx = dest_reg_idx_;
	endtask

	task clear_DECODE_MT_PACKET;
		output DECODE_MT_PACKET p;
		p.valid = 0;
		p.rs1_idx = 0;
		p.rs2_idx = 0;
		p.dest_reg_idx = 0;
	endtask


	task fill_DECODE_RS_PACKET;
		output DECODE_RS_PACKET p;
		input longint NPC_, PC_, opa_select_, opb_select_, inst_, alu_func_, fu_;
		p.NPC = NPC_;
		p.PC = PC_;
		p.opa_select = opa_select_;
		p.opb_select = opb_select_;
		p.inst = inst_;
		p.alu_func = alu_func_;
		p.fu = fu_;
	endtask

	task clear_DECODE_RS_PACKET;
		output DECODE_RS_PACKET p;
		p.NPC = 0;
		p.PC = 0;
		p.opa_select = 0;
		p.opb_select = 0;
		p.inst = 0;
		p.alu_func = 0;
		p.fu = 0;
	endtask


	task fill_ROB_MT_PACKET;
		output ROB_MT_PACKET p;
		input longint valid_, retire_told_idx_, retire_t_idx_, dest_reg_idx_;
		p.valid = valid_;
		p.retire_told_idx = retire_told_idx_;
		p.retire_t_idx = retire_t_idx_;
		p.dest_reg_idx = dest_reg_idx_;
	endtask

	task clear_ROB_MT_PACKET;
		output ROB_MT_PACKET p;
		p.valid = 0;
		p.retire_told_idx = 0;
		p.retire_t_idx = 0;
		p.dest_reg_idx = 0;
	endtask


	task fill_MT_ROB_PACKET;
		output MT_ROB_PACKET p;
		input longint told_idx_, t_idx_, dest_reg_idx_;
		p.told_idx = told_idx_;
		p.t_idx = t_idx_;
		p.dest_reg_idx = dest_reg_idx_;
	endtask

	task clear_MT_ROB_PACKET;
		output MT_ROB_PACKET p;
		p.told_idx = 0;
		p.t_idx = 0;
		p.dest_reg_idx = 0;
	endtask


	task fill_MT_ENTRY;
		output MT_ENTRY p;
		input longint t_idx_, ready_;
		p.t_idx = t_idx_;
		p.ready = ready_;
	endtask

	task clear_MT_ENTRY;
		output MT_ENTRY p;
		p.t_idx = 0;
		p.ready = 0;
	endtask



	task fill_CDB_PACKET;
		output CDB_PACKET p;
		input longint valid_, rob_num_, t_idx_, branch_target_;
		p.valid = valid_;
		p.rob_num = rob_num_;
		p.t_idx = t_idx_;
		p.branch_target = branch_target_;
	endtask

	task clear_CDB_PACKET;
		output CDB_PACKET p;
		p.valid = 0;
		p.rob_num = 0;
		p.t_idx = 0;
		p.branch_target = 0;
	endtask



	task fill_IF_DECODE_PACKET;
		output IF_DECODE_PACKET p;
		input longint valid_, inst_, NPC_, PC_, branch_address_;
		p.valid = valid_;
		p.inst = inst_;
		p.NPC = NPC_;
		p.PC = PC_;
		p.branch_address = branch_address_;
	endtask

	task clear_IF_DECODE_PACKET;
		output IF_DECODE_PACKET p;
		p.valid = 0;
		p.inst = 0;
		p.NPC = 0;
		p.PC = 0;
		p.branch_address = 0;
	endtask



	task fill_FU_ISSUE_PACKET;
		output FU_ISSUE_PACKET p;
		input longint ALU_free_spaces_, ST_free_spaces_, LD_free_spaces_, MUL_free_spaces_;
		p.ALU_free_spaces = ALU_free_spaces_;
		p.ST_free_spaces = ST_free_spaces_;
		p.LD_free_spaces = LD_free_spaces_;
		p.MUL_free_spaces = MUL_free_spaces_;
	endtask

	task clear_FU_ISSUE_PACKET;
		output FU_ISSUE_PACKET p;
		p.ALU_free_spaces = 0;
		p.ST_free_spaces = 0;
		p.LD_free_spaces = 0;
		p.MUL_free_spaces = 0;
	endtask

endmodule