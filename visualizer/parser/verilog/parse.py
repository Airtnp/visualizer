"""
Parse .vh/.v files.

string_literal with space will be considered as no space
"""
# pylint: disable=W0614, W0612, E1101
import re
from collections import defaultdict
from visualizer.parser.verilog.svast_rules import *
from visualizer.parser.verilog.svast import Verilog
from visualizer.parser.verilog.lexer import SVLexer, op_dict
from visualizer.parser.verilog.plyfiles.ebnf.BNFlexer import escape_ops


precedence = [
    ('nonassoc', 'THEN'),
    ('nonassoc', 'GENKW_ELSE'),
    ('left', '{', '}'),
    ('nonassoc', '=', '+=', '-=', '*=', '/=', '%=', '^=', '|=', '<<=', '>>=', '<<<=', '>>>=', ':=', ':/', 'NONBLOCK'),
    # ('right', '->'),
    ('right', 'COND'),
    ('left', '||'),
    ('left', '&&'),
    ('left', '|'),
    ('left', '^', '~^'),
    ('left', '&'),
    ('left', '==', '!=', '===', '!==', '=?=', '!?='),
    ('left', '<', '<=', '>', '>='),
    ('left', '<<', '>>', '<<<', '>>>'),
    ('left', '+', '-'),
    ('left', '*', '/', '%'),
    ('left', '**'),
    ('right', 'UNARYPLUS', 'UNARYMINUS', '!', '~', 'UNARYAND', '~&', 'UNARYOR', '~|', 'UNARYXOR', 'UNARYXNOR', '++', '--'),
    ('left', '(', ')', '[', ']', '.'),
    ('nonassoc', 'LEASSIGN')
]


new_precedence = []
for i in range(len(precedence)):
    new_pred = [precedence[i][0]]
    for j in range(1, len(precedence[i])):
        if not precedence[i][j][0].isalpha():
            new_pred.append(op_dict[escape_ops(precedence[i][j])][0])
        else:
            new_pred.append(precedence[i][j])
    if len(new_pred) != 1:
        new_precedence.append(tuple(new_pred))

precedence = tuple(new_precedence)


def parse(text):
    lexer = SVLexer()
    lexer.build()
    
    tokens = lexer.run(text)
    
    lexer = SVLexer()
    lexer.build()

    global Verilog
    Scan = Verilog

    Scan = Scan.rescan()
    Scan.parser.precedence = precedence
    Scan.set_lexer(lexer)
    scan = Scan.parse(text)
    info = defaultdict(list)
    scan.analysis(info)
    return scan, info
