"""
Preprocess using vcs Xman
"""
import os
from pathlib import Path
from subprocess import call
from visualizer import ROOT


PWD = os.path.dirname(os.path.abspath(__file__))


def preprocess(lines):
    new_lines = []
    meet_assert = False
    for l in lines:
        if '`timescale' in l.strip():
            continue
        if l.strip().startswith("vcs_assert"):
            meet_assert = True
        if not meet_assert:
            new_lines.append(l)
        else:
            if l.strip().startswith(';'):
                meet_assert = False
    text = ''.join(new_lines)
    text += '\n'
    text = text.replace('logic[', 'logic [')
    return text


def call_preprocess(testbench, simfiles):
    retcode = call([
        'vcs',
        '-V', '-sverilog',
        '+vc', '-Mupdate',
        '-line', '-full64',
        '+vcs+vcdpluson', '-debug_pp',
        '-Xman=4', ' ',
        ' '.join(testbench + simfiles),
    ])
    assert retcode == 0
    with open('./tokens.v') as f:
        text = preprocess(f.readlines())
        return text


def collect_sv(root_path):
    header = []
    source = []
    # No dependency found here...
    for f in Path(root_path).glob("**/*.v"):
        source.append(f)
    for f in Path(root_path).glob("**/*.sv"):
        source.append(f)
    for f in Path(root_path).glob("**/*.vh"):
        header.append(f)
    for f in Path(root_path).glob("**/*.svh"):
        header.append(f)
    return header, source

