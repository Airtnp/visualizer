"""
Define RISC-V 32I reg/mem model
"""
def reg_name(idx):
    if idx == 0:
        return "zero"
    elif idx == 1:
        return "ra"
    elif idx == 2:
        return "sp"
    elif idx == 3:
        return "gp"
    elif idx == 4:
        return "tp"
    elif idx == 5:
        return "t0"
    elif idx == 6:
        return "t1"
    elif idx == 7:
        return "t2"
    elif idx == 8:
        return "s0"
    elif idx == 9:
        return "s1"
    elif 10 <= idx <= 17:
        return "a{}".format(idx - 10)
    elif 18 <= idx <= 27:
        return "s{}".format(idx - 16)
    else:
        return "t{}".format(idx - 25)


class Reg:
    def __init__(self):
        self.reg_v = [0] * 32
    def __getitem__(self, idx):
        return self.reg_v[idx]
    def __setitem__(self, idx, value):
        if idx != 0:
            self.reg_v[idx] = value


class Model:
    REG_WRITE = 0
    MEM_LOAD = 1
    MEM_STORE = 2
    PC_WRITE = 3
    PC_WRITE_COND_SUCC = 4
    PC_WRITE_COND_FAIL = 5
    HALT = 6
    def __init__(self):
        self.mem = [0] * (64 * 1024)
        self.reg = Reg()
        self.pc_history = [0]
        self.status = self.REG_WRITE

    @property
    def pc(self):
        return self.pc_history[-1]

    @pc.setter
    def pc(self, value):
        self.pc_history.append(value)