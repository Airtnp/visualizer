"""
Define RISC-V 32I instructions
"""
from collections import defaultdict
from visualizer.model.riscv.model import Model, reg_name


def sign32(n):
    n = n & 0xffffffff
    return n | (-(n & 0x80000000))


class InstType:
    def __init__(self, func):
        self.func = func
        self.rd = None
        self.rs1 = None
        self.rs2 = None
        self.uimm = None
        self.signed_imm = False
        self.imm = None
        self.opcode = self.func.__rvopcode__
        self.funct3 = self.func.__funct3__
        self.funct7 = self.func.__funct7__
        self.funct12 = self.func.__funct12__

    def __call__(self, model):
        return self.func(self, model)

    def to_str(self, convert = False):
        convt = lambda x: str(x)
        if convert:
            convt = lambda x: reg_name(x)
        frag = [self.func.__rvname__ + '   ']
        tag_count = 0
        if self.rd is not None:
            frag.append(convt(self.rd))
        else:
            tag_count += 1
        if self.rs1 is not None:
            frag.append(convt(self.rs1))
        else:
            tag_count += 1
        if self.rs2 is not None:
            frag.append(convt(self.rs2))
        else:
            tag_count += 1
        if self.imm is not None:
            imm = self.imm
            if self.func.__rvname__ == "lui" or self.func.__rvname__ == "auipc":
                imm >>= 12
            frag.append(str(sign32(imm)))
        else:
            tag_count += 1
        frag.append('\t'*tag_count)
        return '\t'.join(frag)

    def decode(self, mcode):
        pass

    def encode(self):
        pass

    def is_nop(self):
        return False

    def is_call(self):
        return False

    def is_ret(self):
        return False


class RType(InstType):
    def decode(self, mcode):
        self.rd = (mcode >> 7) & 0x1F
        self.rs1 = (mcode >> 15) & 0x1F
        self.rs2 = (mcode >> 20) & 0x1F
    
    def encode(self):
        x = self.opcode | (self.funct3 << 12) | (self.funct7 << 25)
        x |= (self.rd << 7) | (self.rs1 << 15) | (self.rs2 << 20)
        return x


class IType(InstType):
    def decode(self, mcode):
        self.signed_imm = (mcode >> 31) & 0x1
        self.rd = (mcode >> 7) & 0x1F
        self.rs1 = (mcode >> 15) & 0x1F
        self.uimm = (mcode >> 20) & 0x7FF
        self.imm = self.uimm
        if self.signed_imm:
            self.imm = (((1 << 22) - 1) << 11) | self.uimm

    def encode(self):
        x = self.opcode | (self.funct3 << 12)
        x |= (self.rd << 7) | (self.rs1 << 15) | (self.uimm << 20)
        x |= (self.signed_imm << 31)
        return x

    def is_nop(self):
        is_nop_ = (self.rd == 0) and (self.rs1 == 0)
        is_nop_ &= (self.imm == 0) and (self.func.__rvname__ == "addi")
        return is_nop_

    def is_call(self):
        # NOTE: the risc-v specs says jalr ra x6 offset, but gcc says no
        # jalr ra ra offset
        if self.func.__rvname__ == "jalr":
            return (self.rd == 1) and (self.rs1 == 1)
        else:
            return False

    def is_ret(self):
        # jalr zero ra 0
        if self.func.__rvname__ == "jalr":
            return (self.rd == 0) and (self.rs1 == 1) and (self.imm == 0)
        else:
            return False



class ISType(IType):
    """
    For SRA SRL SLL
    """
    def decode(self, mcode):
        self.rd = (mcode >> 7) & 0x1F
        self.rs1 = (mcode >> 15) & 0x1F
        self.uimm = (mcode >> 20) & 0x1F # shamt
        self.imm = self.uimm
    
    def encode(self):
        x = self.opcode | (self.funct3 << 12) | (self.funct7 << 25)
        x |= (self.rd << 7) | (self.rs1 << 15) | (self.uimm << 20)
        return x


class SType(InstType):
    def decode(self, mcode):
        self.signed_imm = (mcode >> 31) & 0x1
        self.rs1 = (mcode >> 15) & 0x1F
        self.rs2 = (mcode >> 20) & 0x1F
        self.uimm = ((mcode >> 7) & 0x1F) | (((mcode >> 25) & 0x3F) << 5)
        self.imm = self.uimm
        if self.signed_imm:
            self.imm = (((1 << 22) - 1) << 11) | self.uimm

    def encode(self):
        x = self.opcode | (self.funct3 << 12)
        x |= (self.rs1 << 15) | (self.rs2 << 20)
        x |= (((self.uimm >> 5) & 0x3F) << 25) | ((self.uimm & 0x1F) << 7)
        x |= (self.signed_imm << 31)
        return x


class BType(InstType):
    def decode(self, mcode):
        self.signed_imm = (mcode >> 31) & 0x1
        self.rs1 = (mcode >> 15) & 0x1F
        self.rs2 = (mcode >> 20) & 0x1F
        self.uimm = (mcode >> 8) & 0xF
        self.uimm |= ((mcode >> 25) & 0x3F) << 4
        self.uimm |= ((mcode >> 7) & 0x1) << 10
        self.uimm <<= 1
        self.imm = self.uimm
        if self.signed_imm:
            self.imm = (((1 << 21) - 1) << 12) | self.uimm

    def encode(self):
        x = self.opcode | (self.funct3 << 12)
        x |= (self.rs1 << 15) | (self.rs2 << 20)
        x |= ((self.uimm >> 11) & 0x1) << 7
        x |= ((self.uimm >> 1) & 0xF) << 8
        x |= ((self.uimm >> 5) & 0x3F) << 25
        x |= ((self.uimm >> 12) & 0x1) << 31
        return x


class UType(InstType):
    def decode(self, mcode):
        self.rd = (mcode >> 7) & 0x1F
        self.uimm = (mcode >> 12) & 0xFFFFF
        self.imm = self.uimm

    def encode(self):
        x = self.opcode | (self.rd << 7) | (self.uimm << 12)
        return x


class JType(InstType):
    def decode(self, mcode):
        self.signed_imm = (mcode >> 31) & 0x1
        self.rd = (mcode >> 7) & 0x1F
        self.uimm = (mcode >> 21) & 0x3FF
        self.uimm |= ((mcode >> 20) & 0x1) << 10
        self.uimm |= ((mcode >> 12) & 0xFF) << 11
        self.uimm <<= 1
        self.imm = self.uimm
        if self.signed_imm:
            self.imm = (((1 << 13) - 1) << 20) | self.uimm

    def encode(self):
        x = self.opcode | (self.rd << 7)
        x |= ((self.uimm >> 12) & 0xFF) << 12
        x |= ((self.uimm >> 11) & 0x1) << 20
        x |= ((self.uimm >> 1) & 0x3FF) << 21
        x |= ((self.uimm >> 20) & 0x1) << 31
        x |= (self.signed_imm << 31)
        return x


class PType(InstType):
    def decode(self, mcode):
        self.xrd = (mcode >> 7) & 0x1F
        self.xrs1 = (mcode >> 15) & 0x1F
        # only use wfi, so
        self.rd = None
        self.rs1 = None
    
    def encode(self):
        x = self.opcode | (self.xrd << 7)
        x |= (self.funct3 << 12) | (self.xrs1 << 15)
        x |= (self.funct12 << 20)
        return x


class InstSet:
    FUNC_SET = {}
    DECODE_SET = {}

    def decode(self, mcode):
        opcode = mcode & 0x7F
        funct3 = (mcode >> 12) & 0x7
        funct7 = mcode >> 25
        funct12 = mcode >> 20
        ainst = self.DECODE_SET[opcode]
        func = ainst
        if isinstance(ainst, dict):
            ainst = ainst[funct3]
            func = ainst
            if isinstance(ainst, dict):
                if opcode == 0x73:
                    func = ainst[funct12]
                else:
                    func = ainst[funct7]
        inst = func.__rvtype__(func)
        inst.decode(mcode)
        return inst

    def encode(self, name, rd, rs1, rs2, imm):
        func = self.FUNC_SET[name]
        inst = func.__rvtype__(func)
        inst.rd = rd
        inst.rs1 = rs1
        inst.rs2 = rs2
        inst.imm = imm
        inst.signed_imm = imm < 0
        inst.uimm = imm
        return inst.encode()


def inst(name, rvtype, opcode, funct3 = None, funct7 = None, funct12 = None):
    def collector(func):
        func.__rvname__ = name
        func.__rvtype__ = rvtype
        func.__rvopcode__ = opcode
        func.__funct3__ = funct3
        func.__funct7__ = funct7
        func.__funct12__ = funct12
        InstSet.FUNC_SET[name] = func
        if opcode not in InstSet.DECODE_SET.keys():
            InstSet.DECODE_SET[opcode] = {}
        if funct3 is not None:
            if funct3 not in InstSet.DECODE_SET[opcode].keys():
                InstSet.DECODE_SET[opcode][funct3] = {}
            if funct7 is not None:
                InstSet.DECODE_SET[opcode][funct3][funct7] = func
            elif funct12 is not None:
                InstSet.DECODE_SET[opcode][funct3][funct12] = func
            else:
                InstSet.DECODE_SET[opcode][funct3] = func
        else:
            InstSet.DECODE_SET[opcode] = func
    return collector


@inst("lui", UType, 0x37)
def LUI(inst, model):
    model.reg[inst.rd] = inst.imm << 12


@inst("auipc", UType, 0x17)
def AUIPC(inst, model):
    model.reg[inst.rd] = model.pc + (inst.imm << 12)


@inst("jal", JType, 0x6F)
def JAL(inst, model):
    model.reg[inst.rd] = model.pc + 4
    model.pc = inst.imm
    model.status = Model.PC_WRITE


@inst("jalr", IType, 0x67, 0)
def JALR(inst, model):
    oldpc = model.pc
    model.pc = model.reg[inst.rs1] + inst.imm
    model.reg[inst.rd] = oldpc + 4
    model.status = Model.PC_WRITE


@inst("beq", BType, 0x63, 0)
def BEQ(inst, model):
    if model.reg[inst.rs1] == model.reg[inst.rs2]:
        model.pc = model.pc + inst.imm
        model.status = Model.PC_WRITE_COND_SUCC
    else:
        model.status = Model.PC_WRITE_COND_FAIL


@inst("bne", BType, 0x63, 1)
def BNE(inst, model):
    if model.reg[inst.rs1] != model.reg[inst.rs2]:
        model.pc = model.pc + inst.imm
        model.status = Model.PC_WRITE_COND_SUCC
    else:
        model.status = Model.PC_WRITE_COND_FAIL


@inst("blt", BType, 0x63, 4)
def BLT(inst, model):
    if sign32(model.reg[inst.rs1]) < sign32(model.reg[inst.rs2]):
        model.pc = model.pc + inst.imm
        model.status = Model.PC_WRITE_COND_SUCC
    else:
        model.status = Model.PC_WRITE_COND_FAIL


@inst("bge", BType, 0x63, 5)
def BGE(inst, model):
    if sign32(model.reg[inst.rs1]) >= sign32(model.reg[inst.rs2]):
        model.pc = model.pc + inst.imm
        model.status = Model.PC_WRITE_COND_SUCC
    else:
        model.status = Model.PC_WRITE_COND_FAIL


@inst("bltu", BType, 0x63, 6)
def BLTU(inst, model):
    if model.reg[inst.rs1] < model.reg[inst.rs2]:
        model.pc = model.pc + inst.imm
        model.status = Model.PC_WRITE_COND_SUCC
    else:
        model.status = Model.PC_WRITE_COND_FAIL


@inst("bgeu", BType, 0x63, 7)
def BGEU(inst, model):
    if model.reg[inst.rs1] >= model.reg[inst.rs2]:
        model.pc = model.pc + inst.imm
        model.status = Model.PC_WRITE_COND_SUCC
    else:
        model.status = Model.PC_WRITE_COND_FAIL


@inst("lb", IType, 0x03, 0)
def LB(inst, model):
    mem = model.mem[model.reg[inst.rs1] + inst.imm]
    if mem & 0x80:
        mem |= 0xFFFFFF00
    model.reg[inst.rd] = mem
    model.status = Model.MEM_LOAD


@inst("lh", IType, 0x03, 1)
def LH(inst, model):
    mem = model.mem[model.reg[inst.rs1] + inst.imm]
    mem |= (model.mem[model.reg[inst.rs1] + inst.imm + 1] << 8)
    if mem & 0x8000:
        mem |= 0xFFFF0000
    model.reg[inst.rd] = mem
    model.status = Model.MEM_LOAD


@inst("lw", IType, 0x03, 2)
def LW(inst, model):
    mem = model.mem[model.reg[inst.rs1] + inst.imm]
    mem |= (model.mem[model.reg[inst.rs1] + inst.imm + 1] << 8)
    mem |= (model.mem[model.reg[inst.rs1] + inst.imm + 2] << 16)
    mem |= (model.mem[model.reg[inst.rs1] + inst.imm + 3] << 24)
    model.reg[inst.rd] = mem
    model.status = Model.MEM_LOAD


@inst("lbu", IType, 0x03, 4)
def LBU(inst, model):
    mem = model.mem[model.reg[inst.rs1] + inst.imm]
    model.reg[inst.rd] = mem
    model.status = Model.MEM_LOAD


@inst("lhu", IType, 0x03, 5)
def LHU(inst, model):
    mem = model.mem[model.reg[inst.rs1] + inst.imm]
    mem |= (model.mem[model.reg[inst.rs1] + inst.imm + 1] << 8)
    model.reg[inst.rd] = mem
    model.status = Model.MEM_LOAD


@inst("sb", SType, 0x23, 0)
def SB(inst, model):
    model.mem[model.reg[inst.rs1] + inst.imm] = model.reg[inst.rs2] & 0xFF
    model.status = Model.MEM_STORE


@inst("sh", SType, 0x23, 1)
def SH(inst, model):
    model.mem[model.reg[inst.rs1] + inst.imm] = model.reg[inst.rs2] & 0xFF
    model.mem[model.reg[inst.rs1] + inst.imm + 1] = (model.reg[inst.rs2] & 0xFF00) >> 8
    model.status = Model.MEM_STORE


@inst("sw", SType, 0x23, 2)
def SW(inst, model):
    model.mem[model.reg[inst.rs1] + inst.imm] = model.reg[inst.rs2] & 0xFF
    model.mem[model.reg[inst.rs1] + inst.imm + 1] = (model.reg[inst.rs2] & 0xFF00) >> 8
    model.mem[model.reg[inst.rs1] + inst.imm + 2] = (model.reg[inst.rs2] & 0xFF0000) >> 16
    model.mem[model.reg[inst.rs1] + inst.imm + 3] = (model.reg[inst.rs2] & 0xFF000000) >> 24
    model.status = Model.MEM_STORE


@inst("addi", IType, 0x13, 0)
def ADDI(inst, model):
    model.reg[inst.rd] = sign32(model.reg[inst.rs1]) + inst.imm


@inst("slti", IType, 0x13, 2)
def SLTI(inst, model):
    model.reg[inst.rd] = (sign32(model.reg[inst.rs1]) < inst.imm)


@inst("sltiu", IType, 0x13, 3)
def SLTIU(inst, model):
    model.reg[inst.rd] = (model.reg[inst.rs1] < inst.imm)


@inst("xori", IType, 0x13, 4)
def XORI(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] ^ inst.imm


@inst("ori", IType, 0x13, 6)
def ORI(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] | inst.imm


@inst("andi", IType, 0x13, 7)
def ANDI(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] & inst.imm


@inst("slli", ISType, 0x13, 1, 0x00)
def SLLI(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] << inst.imm


@inst("srli", ISType, 0x13, 5, 0x00)
def SRLI(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] >> inst.imm


@inst("srai", ISType, 0x13, 5, 0x20)
def SRAI(inst, model):
    model.reg[inst.rd] = sign32(model.reg[inst.rs1]) >> inst.imm


@inst("add", RType, 0x33, 0, 0x00)
def ADD(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] + model.reg[inst.rs2]


@inst("sub", RType, 0x33, 0, 0x20)
def SUB(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] - model.reg[inst.rs2]


@inst("sll", RType, 0x33, 1, 0x00)
def SLL(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] << (model.reg[inst.rs2] & 0x1F)


@inst("slt", RType, 0x33, 2, 0x00)
def SLT(inst, model):
    model.reg[inst.rd] = sign32(model.reg[inst.rs1]) < sign32(model.reg[inst.rs2])


@inst("sltu", RType, 0x33, 3, 0x00)
def SLTU(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] < model.reg[inst.rs2]


@inst("xor", RType, 0x33, 4, 0x00)
def XOR(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] ^ model.reg[inst.rs2]


@inst("srl", RType, 0x33, 5, 0x00)
def SRL(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] >> (model.reg[inst.rs2] & 0x1F)


@inst("sra", RType, 0x33, 5, 0x20)
def SRA(inst, model):
    model.reg[inst.rd] = sign32(model.reg[inst.rs1]) >> (model.reg[inst.rs2] & 0x1F)


@inst("or", RType, 0x33, 6, 0x00)
def OR(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] | model.reg[inst.rs2]


@inst("and", RType, 0x33, 7, 0x00)
def AND(inst, model):
    model.reg[inst.rd] = model.reg[inst.rs1] & model.reg[inst.rs2]


@inst("mul", RType, 0x33, 0, 0x01)
def MUL(inst, model):
    model.reg[inst.rd] = (model.reg[inst.rs1] * model.reg[inst.rs2]) & 0xFFFF


@inst("mulh", RType, 0x33, 1, 0x01)
def MULH(inst, model):
    model.reg[inst.rd] = ((sign32(model.reg[inst.rs1]) * sign32(model.reg[inst.rs2])) >> 32) & 0xFFFF


@inst("mulhu", RType, 0x33, 3, 0x01)
def MULHU(inst, model):
    model.reg[inst.rd] = ((model.reg[inst.rs1] * model.reg[inst.rs2]) >> 32) & 0xFFFF


@inst("mulhsu", RType, 0x33, 2, 0x01)
def MULHSU(inst, model):
    model.reg[inst.rd] = ((sign32(model.reg[inst.rs1]) * model.reg[inst.rs2]) >> 32) & 0xFFFF


@inst("wfi", PType, 0x73, 0, None, 0x105)
def WFI(inst, model):
    model.status = Model.HALT


if __name__ == "__main__":
    from pprint import pprint
    pprint(InstSet.DECODE_SET)
    isa = InstSet()
    mcode = isa.encode("mul", 10, 12, 13, 0x1)
    inst_2 = isa.decode(mcode)
    model = Model()
    model.reg[12] = 2
    model.reg[13] = 2
    inst_2(model)
