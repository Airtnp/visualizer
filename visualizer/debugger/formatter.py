"""
Define self-formatter
"""
from pygments.formatters import HtmlFormatter


class VFormatter(HtmlFormatter):
    """
    @ref: https://stackoverflow.com/questions/39603381/using-more-than-one-highlight-color-in-pygments
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.highlight_groups = kwargs.get('highlight_groups', [])

    def wrap(self, source, outfile):
        return self._wrap_code(source)

    def _wrap_code(self, source):
        _prefix = ''
        if self.cssclass is not None:
            _prefix += '<div class="highlight">'
        if self.filename is not None:
            _prefix += '<span class="filename">{}</span>'.format(self.filename)
        yield 0, _prefix + '<pre>'

        for count, s in enumerate(source):
            is_highlight, content = s
            if is_highlight:
                for highlight_group in self.highlight_groups:
                    col, lines = highlight_group
                    if count in lines:
                        content = '<span style="background-color:{}">{}</span>'.format(col, content)                        
            yield is_highlight, content

        # close open things
        _postfix = ''
        if self.cssclass is not None:
            _postfix += '</div>'
        yield 0, '</pre>' + _postfix