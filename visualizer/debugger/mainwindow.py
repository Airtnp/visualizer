#!/usr/bin/env python
"""
Mainwindow of Debugger
"""
import sys
import os
import threading
import logging
import time
import pygments
import socket
from collections import OrderedDict, defaultdict
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtWidgets import QMainWindow, QApplication
from visualizer.debugger.storage import VarMap, MemoryMap, Value, Sim
from visualizer.debugger.ui.visualizer_ui import Ui_Visualizer
from visualizer.debugger.subwindow import ModuleTreeView
from visualizer.debugger.formatter import VFormatter
from visualizer.model.instruction import InstSet


code_template = """
<html>
<head>
<style>
{}
</style>
</head>
<body>
{}
</body>
</html>
"""


logging.basicConfig(format="%(asctime)s: [%(levelname)s] %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class RecvThread(QThread):
    signal = pyqtSignal('PyQt_PyObject')
    def __init__(self, recv_h, send_h, conn):
        super().__init__()
        self.recv_handle = recv_h
        self.send_handle = send_h
        self.conn = conn
        self.keep_recv = True

    def ack(self, result):
        logger.debug('ACK')
        self.send_handle.write('A'.encode('utf-8'))

    def run(self):
        while self.keep_recv:
            msg = ''
            data = ''
            # data = self.recv_handle.read(4096)
            has_data = True
            try:
                data = self.conn.recv(4096)
            except (BlockingIOError, socket.error):
                has_data = False
            while has_data and data is not None and data != '':
                msg += data.decode()
                # data = self.recv_handle.read(4096)
                try:
                    data = self.conn.recv(4096)
                except (BlockingIOError, socket.error):
                    has_data = False
            if msg != '':
                self.signal.emit(msg)
            else:
                time.sleep(0.05)
        logger.info('Recv End')


"""
TODO:
    visualizer install manual
    ldu stu, lsq, ... annotated
    multiple tables (id, rs, rob, mt, lsq, prb, regfile) i/dcache?
    statistics: bp accuracy & CPI
    watch variable for ipython (approximation?)
    expr parsing
    callgraph implementation
    right click expand as module
    mark red changing variable
    variable history viewer
    [optional] new lexer
"""

class Visualizer(QMainWindow, Ui_Visualizer):
    # signals must be class variable
    # https://stackoverflow.com/questions/2970312/pyqt4-qtcore-pyqtsignal-object-has-no-attribute-connect
    recv_ack = pyqtSignal('PyQt_PyObject')
    def __init__(self, read_fd, write_fd, conn):
        super().__init__()
        self.setupUi(self)
        self.resize(2240, 1560)
        self.regList.core = self
        self.regList.set_header()
        self.memList.core = self
        self.memList.set_header()
        self.varList.core = self
        self.varList.set_header()
        self.kernel.core = self
        self.recv_handle = os.fdopen(read_fd, 'rb', 0)
        self.send_handle = os.fdopen(write_fd, 'wb', 0)
        self.conn = conn
        self.recv_t = RecvThread(self.recv_handle, self.send_handle, self.conn)
        self.recv_t.signal.connect(self.process_message)
        self.recv_ack.connect(self.recv_t.ack)
        self.var = VarMap(self)
        self.mem = MemoryMap(self, 64 * 1024 // 8)
        self.sim = None
        self.max_time = -1
        self.cur_time = -1
        self.recv_t.start()
        self.has_reset = False
        self.get_break = False
        self.superscalar = 3
        self.posedge = True
        self.time_caused_by_next = False
        self.max_cur_time = -1

        self.code_view_last_line = 0

        self.subs = []
        # Weird default argument problem...
        self.next_button.clicked.connect(self.next_click)
        self.back_button.clicked.connect(self.back_click)
        self.print_button.clicked.connect(self.print_core)

    def collect_hilines(self):
        hi_lines = defaultdict(list)
        for i in range(self.superscalar):
            base = "core.if_stage0.decode_out[{}].".format(i)
            valid = self.var.get_hier(base + 'valid').value_
            pc = self.var.get_hier(base + 'PC')
            if valid:
                hi_lines['if'].append(pc.value_ // 4)
            base = "core.id_stage0.pass_if_id_packet[{}].".format(i)
            valid = self.var.get_hier(base + 'valid').value_
            if valid:
                pc = self.var.get_hier(base + 'PC')
                hi_lines['id'].append(pc.value_ // 4)
            base = "core.rob_0.decode_in[{}].".format(i)
            valid = self.var.get_hier(base + 'valid').value_
            if valid:
                pc = self.var.get_hier('core.rs_0.decode_in[{}].PC'.format(i))
                hi_lines['dispatch'].append(pc.value_ // 4)
        for i in range(5):
            base = 'core.alu_0[{}].next_insn_reg.'.format(i)
            valid = self.var.get_hier(base + 'busy').value_
            if valid:
                pc = self.var.get_hier(base + 'inst_info.PC')
                hi_lines['alu_{}'.format(i)].append(pc.value_ // 4)
        for i in range(3):
            # Only shows in one cycle..., no way to track inside
            base = 'core.mul_0[{}].insn.'.format(i)
            valid = self.var.get_hier(base + 'busy').value_
            if valid:
                pc = self.var.get_hier(base + 'inst_info.PC')
                hi_lines['mul_{}'.format(i)].append(pc.value_ // 4)
        for i in range(3):
            # Only shows in one cycle..., no way to track inside
            base = 'core.ldu_0[{}].saved_insn.'.format(i)
            valid = self.var.get_hier(base + 'busy').value_
            if valid:
                pc = self.var.get_hier(base + 'inst_info.PC')
                hi_lines['ldu_{}'.format(i)].append(pc.value_ // 4)
        for i in range(4):
            # Only shows in one cycle..., no way to track inside
            base = 'core.stu_0[{}].saved_insn.'.format(i)
            valid = self.var.get_hier(base + 'busy').value_
            if valid:
                pc = self.var.get_hier(base + 'inst_info.PC')
                hi_lines['stu_{}'.format(i)].append(pc.value_ // 4)
        base = 'core.her_majesty.next_insn_reg.'
        valid = self.var.get_hier(base + 'busy').value_
        if valid:
            pc = self.var.get_hier(base + 'inst_info.PC')
            hi_lines['bru'].append(pc.value_ // 4)
        return hi_lines
        
    def set_assembly(self):
        hi_lines = self.collect_hilines()
        # insts = list(filter(lambda x: not x.is_nop(), self.sim.insts))
        inst_map = self.sim.inst_map
        pcs = list(sorted(inst_map.keys()))
        lines = OrderedDict([(i // 4, hex(i) + "\t" + inst_map[i].to_str(True)) for i in pcs])
        back_map = {}
        for i in range(len(pcs)):
            pc = pcs[i]
            back_map[pc // 4] = i
            if inst_map[pc].is_nop():
                lines[pc // 4] = hex(pc) + "\t" + "nop"
        lexer = pygments.lexers.GasLexer()
        hi_color = {
            'if': '#FFA07A',
            'id': '#FFB6C1',
            'dispatch': '#FF6347',
            # 'issue': '#EEE8AA',
            'alu_0': '#BA55D3',
            'alu_1': '#9370DB',
            'alu_2': '#80ced6',
            'alu_3': '#fefbd8',
            'alu_4': '#618685',
            'mul_0': '#8B008B',
            'mul_1': '#800080',
            'mul_2': '#bccad6',
            'ldu_0': '#cfe0e8',
            'ldu_1': '#b7d7e8',
            'ldu_2': '#d9ad7c',
            'stu_0': '#674d3c',
            'stu_1': '#622569',
            'stu_2': '#f2e394',
            'stu_3': '#d96459',
            'bru': '#7B68EE',
            # TODO: ldu, stu
        }
        hgroup = []
        total = 0
        ntotal = 0
        for k, hlines in hi_lines.items():
            blines = []
            for pc in hlines:
                lines[pc] += '\t\t◁' + k
                total += back_map[pc]
                ntotal += 1
                blines.append(back_map[pc])
            hgroup.append((hi_color[k], blines))
        format_opts = dict(
            # linenos="table",
            linespans="line",
            # linenostep=4,
            # linenostart=0,
            highlight_groups=hgroup,
        )
        formatter = VFormatter(**format_opts)
        css = formatter.get_style_defs(".highlights")
        source = pygments.format(
            lexer.get_tokens("\n".join(lines.values())), formatter
        )
        self.code_view.setHtml(code_template.format(css, source))
        if total != 0 and ntotal != 0:
            self.code_view_last_line = max(0, int(total / ntotal) - 5)
        self.code_view.scrollToAnchor("line-{}".format(self.code_view_last_line))

    def init_sim(self):
        insts = {}
        for i in range(self.mem.limit):
            mline = self.mem[i] # 64 bits
            if mline.empty():
                continue
            vline = mline.value(0).value_
            inst1 = vline & 0xFFFFFFFF
            inst2 = (vline >> 32) & 0xFFFFFFFF
            try:
                insts[i * 8] = InstSet().decode(inst1)
            except:
                pass
            try:
                insts[i * 8 + 4] = InstSet().decode(inst2)
            except:
                pass
        self.sim = Sim(self, insts)

    def update_time(self, update=True):
        self.cyclesLabel.setText("Cycles: {}".format(self.cur_time))
        if update:
            self.regList.update_time()
            self.memList.update_time()
            self.varList.update_time()
            self.set_assembly()
            for w in self.subs:
                w.update_time()
        if self.posedge and self.time_caused_by_next and self.cur_time > self.max_cur_time:
            self.sim.update_time()
        self.max_cur_time = max(self.cur_time, self.max_cur_time)

    def print_core(self):
        stage = ModuleTreeView(self, 'core')
        self.subs.append(stage)
        stage.show()

    def back_click(self):
        self.back(True)

    def back(self, update=True):
        self.cur_time = max(self.cur_time - 1, 0)
        if self.has_reset:
            self.time_caused_by_next = False
            self.update_time(update)

    def next_click(self):
        self.next(True)

    def next(self, update=True):
        # The processing might be slow and can't pass ACK then.
        oldtime = self.cur_time
        if self.cur_time >= self.max_time:
            if self.get_break:
                self.recv_ack.emit(True)
                self.get_break = False
            if update:
                time.sleep(0.02)
        self.cur_time = min(self.cur_time + 1, self.max_time)
        if self.has_reset:
            self.time_caused_by_next = True
            self.update_time(update)
        if oldtime != self.cur_time:
            return 1
        return 0

    def setup_ipython(self, app, window):
        shell = self.kernel.kernel_client.kernel.shell
        shell.magic("clear")
        user_ns = shell.user_ns
        user_ns["app"] = app
        user_ns["window"] = self
        user_ns["core"] = self
        user_ns["var"] = self.var
        config = shell.config
        config.TerminalIPythonApp.display_banner = ""
        from visualizer.debugger import ipython_extension
        shell.extension_manager.load_extension(ipython_extension.__name__)

    def process_message(self, msg):
        lines = msg.split('\n')
        for l in lines:
            logging.debug('|'+l+'|')
            if l == 'break':
                self.get_break = True
            if l == 'break' or l == '':
                continue
            # $finish called from...
            if l == 'finish' or l.startswith('$finish'):
                self.recv_t.keep_recv = False
                logger.info('Finish')
                break
            # Handle myth Time _ _       X
            ty, hierarchy_name, width, value = list(filter(None, l.split(' ')))
            if ty == 'TIME':
                if 'x' in value or 'X' in value:
                    self.has_reset = False
                else:
                    self.has_reset = True
                    self.posedge = not self.posedge                        
                    self.max_time = int(value, 16)
                    logger.info('Message Cycle: ' + str(self.max_time))
            elif ty == 'VAR':
                width = list(map(lambda s: int(s), width.split('_')))
                if self.has_reset:
                    self.var.add_var(self.max_time, hierarchy_name, width, Value(value))
            elif ty == 'MEM':
                # mem_xxx
                addr = int(hierarchy_name[4:], 16)
                if self.has_reset:
                    self.mem.add_mem(self.max_time, addr, Value(value))
        if self.get_break and self.max_time == 0:
            self.init_sim()
            self.set_assembly()

    def closeEvent(self, event):
        self.recv_t.quit()
        event.accept()


if __name__ == '__main__':
    read_fd = int(sys.argv[1])
    write_fd = int(sys.argv[2])
    port = int(sys.argv[3])
    print(port)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 16 * 1024 * 1024)
    s.bind(('localhost', port))
    s.listen(5)
    conn, addr = s.accept()
    conn.setblocking(False)

    app = QApplication(sys.argv)
    v = Visualizer(read_fd, write_fd, conn)
    v.show()
    v.setup_ipython(app, v)
    sys.exit(app.exec_())