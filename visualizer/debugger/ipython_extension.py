import imp
import logging
import os
import os.path
import sys
import time
import threading
from pathlib import Path
from shlex import split as shsplit
from pprint import pprint

from IPython import get_ipython
from IPython.core.interactiveshell import InteractiveShell
from IPython.core.magic import Magics, line_magic, magics_class
from PyQt5 import QtWidgets
from PyQt5.QtCore import QThread, pyqtSignal


def op_restrict(low = 0, high = 65536):
    def comp(actual, given):
        return low <= actual <= high
    return comp


def op_eq(actual, given):
    return actual == given


def args(*param_names, **kwargs):
    def func_wrapper(func):
        name = kwargs.pop("name", func.__name__)
        comp = kwargs.pop("comp", op_eq)
        info = kwargs.pop("usage", None)

        def recv_args(inst, query):
            param = shsplit(query)
            if not comp(len(param), len(param_names)):
                if not info:
                    print("USAGE: {} {}".format(name, "".join(param_names)))
                else:
                    print("USAGE: {}".format(info))
                return
            func(inst, query)

        # __wrapped__ is coming from functools
        recv_args.__name__ = func.__wrapped__.__name__
        recv_args.__doc__ = func.__wrapped__.__doc__
        return recv_args

    return func_wrapper


class RContThread(QThread):
    signal = pyqtSignal('PyQt_PyObject')
    def __init__(self, num, core, breakpoints):
        super().__init__()
        self.core = core
        self.breakpoints = breakpoints
        self.num = num

    def run(self):
        has_activated = False
        counter = 0
        if self.num != '':
            num = int(self.num)
        else:
            num = 0xFFFFFFFFF
        while not has_activated and counter < num:
            counter += 1
            for b in self.breakpoints:
                if b.activated:
                    b.activated = False
                    has_activated = True
                    break
            if has_activated:
                break
            self.core.back(False)
            self.signal.emit(counter)
            if self.core.cur_time == 0:
                break
            time.sleep(0.05)


class ContThread(QThread):
    signal = pyqtSignal('PyQt_PyObject')
    def __init__(self, num, core, breakpoints):
        super().__init__()
        self.core = core
        self.breakpoints = breakpoints
        self.num = num

    def run(self):
        has_activated = False
        counter = 0
        if self.num != '':
            num = int(self.num)
        else:
            num = 0xFFFFFFFFF
        while not has_activated and counter < num:
            for b in self.breakpoints:
                if b.activated:
                    b.activated = False
                    has_activated = True
                    break
            if has_activated:
                break
            step = self.core.next(False)
            counter += step
            if step != 1:
                time.sleep(0.05)
            else:
                self.signal.emit(counter)


class Breakpoint:
    def __init__(self, expr):
        self.enabled = True
        self.activated = False
        self.expr = expr

    def __str__(self):
        enable_str = "(enabled)" if self.enabled else "(disabled)"
        return str(self.expr) + " " + enable_str


@magics_class
class VisMagics(Magics):
    def __init__(self, shell):
        self.core = None
        if shell is not None:
            self.user_ns = shell.user_ns
            self.user_ns["breakpoints"] = []
        else:
            # happens during initialisation of ipython
            self.user_ns = None
        self.shell = shell
        super().__init__(shell)

    @property
    def app(self):
        return self.user_ns["app"]

    @line_magic("show_table")
    def show_table(self, query):
        pass


    @line_magic("watch")
    def watch_var(self, query):
        pass

    @line_magic("diswatch")
    def diswatch_var(self, query):
        pass

    @line_magic("break")
    def break_on(self, query):
        pass

    @args("num")
    @line_magic("disable")
    def enable_b(self, num):
        self.user_ns["breakpoints"][int(num)].enabled = True

    @args("num")
    @line_magic("disable")
    def disable_b(self, num):
        self.user_ns["breakpoints"][int(num)].enabled = False

    @line_magic("rcont")
    def run_rev_continue(self, num):
        self.rcont_t = RContThread(num, self.user_ns["core"], self.user_ns["breakpoints"])
        self.rcont_t.signal.connect(self.run_cont_update)
        self.rcont_t.start()

    @line_magic("rn")
    def run_back(self, query):
        self.user_ns["core"].back()

    @line_magic("n")
    def run_next(self, query):
        self.user_ns["core"].next()

    @line_magic("cont")
    def run_continue(self, num):
        self.cont_t = ContThread(num, self.user_ns["core"], self.user_ns["breakpoints"])
        self.cont_t.signal.connect(self.run_cont_update)
        self.cont_t.start()

    def run_cont_update(self, num):
        self.user_ns["core"].update_time()

    @line_magic("p")
    def p_value(self, key):
        if key in self.user_ns["var"].var.keys():
            pprint(self.user_ns["var"].var[key])
        else:
            all_name = self.listall(key)
            for k in all_name:
                print("\t{} = {},\n".format(
                    k,
                    self.user_ns["var"][k]
                ))

    @line_magic("print")
    def print_value(self, key):
        self.p_value(key)
    
    @args("varname", name="listvar")
    @line_magic("listvar")
    def listvar(self, varname):
        all_name = self.listall(varname)
        for k in all_name:
            print(k)

    def listall(self, varname):
        all_name = []
        for k in self.user_ns["var"].var.keys():
            if varname in k:
                all_name.append(k)
        return all_name


# get_ipython will be magically set by ipython
ip = get_ipython()
hase_magics = VisMagics(ip)
ip.register_magics(hase_magics)