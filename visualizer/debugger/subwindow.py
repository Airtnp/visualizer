"""
Define subwindows of debugger
"""

import logging
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QTreeWidgetItem, QDialog, QAction, QMenu, QTableWidgetItem, QHeaderView
from visualizer.debugger.ui.stage_ui import Ui_Module
from visualizer.debugger.ui.viewtable import QMenuTable
from visualizer.model.instruction import InstSet


logging.basicConfig(format="%(asctime)s: [%(levelname)s] %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ModuleTreeView(QDialog, Ui_Module):
    def __init__(self, core, upper):
        super().__init__()
        self.setupUi(self)
        self.resize(1024, 768)
        self.core = core
        self.upper = upper
        self.varTree.clear()
        self.stageLabel.setText(upper)
        self.expand_item = []
        self.select_item = []
        self.construct()
        self.varTree.setContextMenuPolicy(Qt.CustomContextMenu)
        self.varTree.customContextMenuRequested.connect(self.contextMenuEvent)
        self.setWindowTitle(upper)

    @property
    def time(self):
        return self.core.cur_time
    
    def closeEvent(self, event):
        self.core.subs.remove(self)
        event.accept()

    def create_item(self, module_name, module, parent, expand = [], select = []):
        parent.append(module_name)
        if not isinstance(module, dict):
            item = QTreeWidgetItem([module_name, str(module.value(self.time))])
            item.value = module.value(self.time)
            if '.'.join(parent) in select:
                item.setSelected(True)
                self.select_item.append(item)
            return item
        item = QTreeWidgetItem([module_name, ''])
        if '.'.join(parent) in expand:
            item.setExpanded(True)
            self.expand_item.append(item)
        if '.'.join(parent) in select:
            item.setSelected(True)
            self.select_item.append(item)        
        for k, v in module.items():
            ch = self.create_item(k, v, parent[:], expand, select)
            item.addChild(ch)
        return item

    def update_time(self):
        self.expand_item = []
        self.select_item = []
        expand = []
        select = []
        for i in range(self.varTree.topLevelItemCount()):
            expand1, select1 = self.collect_expand(self.varTree.topLevelItem(i), [])
            expand += expand1
            select += select1
        self.varTree.clear()
        self.construct(expand, select)

    def collect_expand(self, item, parent):
        if item.childCount() == 0:
            select_item = ['.'.join(parent + [item.text(0)])] if item.isSelected() else []
            return [], select_item
        expand_item = []
        select_item = []
        cur_text = item.text(0)
        parent.append(cur_text)
        if item.isExpanded():
            expand_item.append('.'.join(parent))
        if item.isSelected():
            select_item.append('.'.join(parent))
        for i in range(item.childCount()):
            expand1, select1 = self.collect_expand(item.child(i), parent[:])
            expand_item += expand1
            select_item += select1
        return expand_item, select_item

    def construct(self, expand = [], select = []):
        upper_dict = self.core.var.get_hier(self.upper)
        for k, v in upper_dict.items():
            item = self.create_item(k, v, [], expand, select)
            self.varTree.addTopLevelItem(item)                
            if k in expand:
                item.setExpanded(True)
            if k in select:
                item.setSelected(True)
        for item in self.expand_item:
            item.setExpanded(True)
        for item in self.select_item:
            item.setSelected(True)
        self.varTree.sortItems(0, Qt.AscendingOrder)

    def contextMenuEvent(self, pos):
        item = self.varTree.itemAt(pos)
        if item.childCount() == 0:
            menu = QMenu(self)
            as_resp = QAction("Repr as response", self)
            as_resp.triggered.connect(lambda: self.repr_as_response(item))
            menu.addAction(as_resp)
            as_int = QAction("Repr as int", self)
            as_int.triggered.connect(lambda: self.repr_as_int(item))
            menu.addAction(as_int)
            as_hex = QAction("Repr as hex", self)
            as_hex.triggered.connect(lambda: self.repr_as_hex(item))
            menu.addAction(as_hex)
            as_bin = QAction("Repr as bin", self)
            as_bin.triggered.connect(lambda: self.repr_as_bin(item))
            menu.addAction(as_bin)
            as_inst = QAction("Repr as inst", self)
            as_inst.triggered.connect(lambda: self.repr_as_inst(item))
            menu.addAction(as_inst)
            as_inst_con = QAction("Repr as inst(conventional)", self)
            as_inst_con.triggered.connect(lambda: self.repr_as_inst_con(item))
            menu.addAction(as_inst_con)
            menu.popup(QCursor.pos())

    def repr_as_response(self, item):
        item.setText(1, str(item.value))

    def repr_as_int(self, item):
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            text = str(value.value_)
        item.setText(1, text)

    def repr_as_hex(self, item):
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            text = hex(value.value_)
        item.setText(1, text)

    def repr_as_inst(self, item):
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            try:
                text = str(InstSet().decode(value.value_).to_str())
            except:
                text = "error"
        item.setText(1, text)

    def repr_as_inst_con(self, item):
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            try:
                text = str(InstSet().decode(value.value_).to_str(True))
            except:
                text = "error"
        item.setText(1, text)

    def repr_as_bin(self, item):
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            text = bin(value.value_)
        item.setText(1, text)


# TODO: add UI
class ShowTable(QDialog, QMenuTable):
    def __init__(self, core, base, table_name, size, members, head='', tail=''):
        super().__init__()
        # self.setupUi(self)
        self.core = core
        self.base = base
        self.name = table_name
        self.members = members
        self.head = head
        self.tail = tail
        self.size = size
        self.has_head = head != ''
        self.special_selected = True
        self.special_cols = list(range(2, len(self.members) + 2)) if self.has_head else list(range(1, len(self.members) + 1))

    def set_header(self):
        header_n = 0
        if self.has_head:
            header_n = 2 + len(self.members)
            self.setHorizontalHeaderLabels(["#", "ht"] + self.members)
        else:
            header_n = 1 + len(self.members)
            self.setHorizontalHeaderLabels(["#"] + self.members)
        
        for i in range(header_n):
            self.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

    @property
    def time(self):
        return self.core.cur_time

    def closeEvent(self, event):
        self.core.subs.remove(self)
        event.accept()

    def construct(self):
        self.varTable.setColumnCount(len(self.members) + 1 + int(self.has_head))
        self.varTable.setRowCount(self.size)
        for i in range(self.size):
            self.varTable.setItem(i, 0, QTableWidgetItem(str(i)))
            idx = 1
            if self.has_head:
                head = "{}.{}".format(self.base, self.head)
                head = self.core.var.get_hier(head).value_
                tail = "{}.{}".format(self.base, self.tail)
                tail = self.core.var.get_hier(tail).value_
                idx += 1
                if i == head and i == tail:
                    self.varTable.setItem(i, 1, QTableWidgetItem("ht"))
                elif i == head:
                    self.varTable.setItem(i, 1, QTableWidgetItem("h"))
                elif i == tail:
                    self.varTable.setItem(i, 1, QTableWidgetItem("t"))
                else:
                    self.varTable.setItem(i, 1, QTableWidgetItem(" "))                
            for m in self.members:
                entry = "{}.{}[{}].{}".format(self.base, self.name, i, m)
                entry = self.core.var.get_hier(entry)
                value_item = QTableWidgetItem(str(entry.value_))
                value_item = entry.value(self.time)
                self.varTable.setItem(i, idx, value_item)
                idx += 1