# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'stage.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Module(object):
    def setupUi(self, Module):
        Module.setObjectName("Module")
        Module.resize(386, 408)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Module)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.stageLabel = QtWidgets.QLabel(Module)
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.stageLabel.setFont(font)
        self.stageLabel.setFrameShape(QtWidgets.QFrame.Box)
        self.stageLabel.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.stageLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.stageLabel.setObjectName("stageLabel")
        self.verticalLayout.addWidget(self.stageLabel)
        self.varTree = QtWidgets.QTreeWidget(Module)
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.varTree.setFont(font)
        self.varTree.setObjectName("varTree")
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.varTree.headerItem().setFont(0, font)
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.varTree.headerItem().setFont(1, font)
        self.varTree.header().setDefaultSectionSize(240)
        self.varTree.header().setMinimumSectionSize(24)
        self.varTree.header().setStretchLastSection(True)
        self.verticalLayout.addWidget(self.varTree)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(Module)
        QtCore.QMetaObject.connectSlotsByName(Module)

    def retranslateUi(self, Module):
        _translate = QtCore.QCoreApplication.translate
        Module.setWindowTitle(_translate("Module", "Dialog"))
        self.stageLabel.setText(_translate("Module", "Decode Stage"))
        self.varTree.headerItem().setText(0, _translate("Module", "Variables"))
        self.varTree.headerItem().setText(1, _translate("Module", "Value"))


