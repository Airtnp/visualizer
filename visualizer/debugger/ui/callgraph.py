# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'callgraph.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(842, 480)
        self.listView = QtWidgets.QListView(Dialog)
        self.listView.setGeometry(QtCore.QRect(20, 40, 141, 411))
        self.listView.setObjectName("listView")
        self.graphicsView = QtWidgets.QGraphicsView(Dialog)
        self.graphicsView.setGeometry(QtCore.QRect(170, 200, 651, 251))
        self.graphicsView.setObjectName("graphicsView")
        self.textBrowser = QtWidgets.QTextBrowser(Dialog)
        self.textBrowser.setGeometry(QtCore.QRect(170, 40, 651, 121))
        self.textBrowser.setObjectName("textBrowser")
        self.memListLabel = QtWidgets.QLabel(Dialog)
        self.memListLabel.setGeometry(QtCore.QRect(30, 10, 121, 21))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.memListLabel.setFont(font)
        self.memListLabel.setFrameShape(QtWidgets.QFrame.Box)
        self.memListLabel.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.memListLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.memListLabel.setObjectName("memListLabel")
        self.memListLabel_2 = QtWidgets.QLabel(Dialog)
        self.memListLabel_2.setGeometry(QtCore.QRect(170, 10, 121, 21))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.memListLabel_2.setFont(font)
        self.memListLabel_2.setFrameShape(QtWidgets.QFrame.Box)
        self.memListLabel_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.memListLabel_2.setAlignment(QtCore.Qt.AlignCenter)
        self.memListLabel_2.setObjectName("memListLabel_2")
        self.memListLabel_3 = QtWidgets.QLabel(Dialog)
        self.memListLabel_3.setGeometry(QtCore.QRect(170, 170, 121, 21))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.memListLabel_3.setFont(font)
        self.memListLabel_3.setFrameShape(QtWidgets.QFrame.Box)
        self.memListLabel_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.memListLabel_3.setAlignment(QtCore.Qt.AlignCenter)
        self.memListLabel_3.setObjectName("memListLabel_3")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.memListLabel.setText(_translate("Dialog", "Call Stack"))
        self.memListLabel_2.setText(_translate("Dialog", "Program"))
        self.memListLabel_3.setText(_translate("Dialog", "Call Graph"))


