"""
Define view table
"""
from PyQt5.QtGui import QContextMenuEvent, QCursor
from PyQt5.QtWidgets import QAction, QMenu, QTableWidget, QTableWidgetItem, QHeaderView
from visualizer.model.model import reg_name
from visualizer.model.instruction import InstSet


class QMenuTable(QTableWidget):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.special_selected = True
        self.special_cols = [1]
    
    def contextMenuEvent(self, event):
        col = self.columnAt(event.pos().x())
        row = self.rowAt(event.pos().y())
        if not self.special_selected or col in self.special_cols:
            menu = QMenu(self)
            as_resp = QAction("Repr as response", self)
            as_resp.triggered.connect(lambda: self.repr_as_response(row, col))
            menu.addAction(as_resp)
            as_int = QAction("Repr as int", self)
            as_int.triggered.connect(lambda: self.repr_as_int(row, col))
            menu.addAction(as_int)
            as_hex = QAction("Repr as hex", self)
            as_hex.triggered.connect(lambda: self.repr_as_hex(row, col))
            menu.addAction(as_hex)
            as_bin = QAction("Repr as bin", self)
            as_bin.triggered.connect(lambda: self.repr_as_bin(row, col))
            menu.addAction(as_bin)
            as_inst = QAction("Repr as inst", self)
            as_inst.triggered.connect(lambda: self.repr_as_inst(row, col))
            menu.addAction(as_inst)
            as_inst_con = QAction("Repr as inst(conventional)", self)
            as_inst_con.triggered.connect(lambda: self.repr_as_inst_con(row, col))
            menu.addAction(as_inst_con)
            menu.popup(QCursor.pos())

    def repr_as_response(self, row, col):
        item = self.item(row, col)
        item.setText(str(item.value))

    def repr_as_int(self, row, col):
        item = self.item(row, col)
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            text = str(value.value_)
        item.setText(text)

    def repr_as_hex(self, row, col):
        item = self.item(row, col)
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            text = hex(value.value_)
        item.setText(text)

    def repr_as_inst(self, row, col):
        item = self.item(row, col)
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            try:
                text = str(InstSet().decode(value.value_).to_str())
            except:
                text = "error"
        item.setText(text)

    def repr_as_inst_con(self, row, col):
        item = self.item(row, col)
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            try:
                text = str(InstSet().decode(value.value_).to_str(True))
            except:
                text = "error"
        item.setText(text)

    def repr_as_bin(self, row, col):
        item = self.item(row, col)
        value = item.value
        text = "unknown"
        if not value.has_unknown:
            text = bin(value.value_)
        item.setText(text)


class RegTableWidget(QMenuTable):
    def __init__(self, parent = None):
        super(RegTableWidget, self).__init__(parent)
        self.reg_size = 32

    def set_header(self):
        self.setHorizontalHeaderLabels(["Register Name", "Value"])
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)

    @property
    def time(self):
        return self.core.cur_time

    def update_time(self):
        self.clear()
        self.set_header()
        self.setRowCount(self.reg_size)
        self.construct()

    def reg_value(self, idx):
        at_str = "core.mt_0.at[{}]".format(idx)
        preg_idx = self.core.var.get_hier(at_str).value(self.time).value_
        pr_str = "core.rf_stage_0.registers[{}]".format(preg_idx)
        return self.core.var.get_hier(pr_str).value(self.time)

    def construct(self):
        for i in range(self.reg_size):
            value_item = QTableWidgetItem(str(self.reg_value(i)))
            value_item.value = self.reg_value(i)
            self.setItem(i, 0, QTableWidgetItem(reg_name(i)))
            self.setItem(i, 1, value_item)


class VarTableWidget(QMenuTable):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.expr = []

    def set_header(self):
        self.setHorizontalHeaderLabels(["Expression", "Value"])
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)

    @property
    def time(self):
        return self.core.cur_time

    def add_expr(self, expr):
        self.expr.append(expr)

    def remove_expr(self, expr):
        self.expr.remove(expr)

    def update_time(self):
        self.clear()
        self.set_header()
        self.setRowCount(len(self.expr))
        self.construct()

    def construct(self):
        for i in range(len(self.expr)):
            expr = self.expr[i]
            value_item = QTableWidgetItem(str(expr.value_))
            value_item.value = expr.value_
            self.setItem(i, 0, QTableWidgetItem(str(expr)))
            self.setItem(i, 1, value_item)


class MemTableWidget(QMenuTable):
    def __init__(self, parent = None):
        super().__init__(parent)

    def set_header(self):
        self.setHorizontalHeaderLabels(["Address", "Value"])
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)

    @property
    def time(self):
        return self.core.cur_time

    def update_time(self):
        self.clear()
        self.set_header()
        self.construct()

    def construct(self):
        items = []
        for i in range(self.core.mem.limit):
            sec = self.core.mem.mem_section[i]
            if sec.logtime != [] or (len(sec.logtime) == 1 and sec.value_history[0].value_ == 0):
                value_item = QTableWidgetItem(str(sec.value(self.time)))
                value_item.value = sec.value(self.time)
                items.append([
                    QTableWidgetItem(hex(i * 8)),
                    value_item
                ])
        self.setRowCount(len(items))
        for i in range(len(items)):
            self.setItem(i, 0, items[i][0])
            self.setItem(i, 1, items[i][1])

