import os
os.environ['QT_API'] = 'pyqt5'

from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtSvg 
from qtconsole.rich_jupyter_widget import RichJupyterWidget
from qtconsole.inprocess import QtInProcessKernelManager


def get_app_qt5(*args, **kwargs):
    """Create a new qt5 app or return an existing one."""
    app = QtWidgets.QApplication.instance()
    if app is None:
        if not args:
            args = ([''],)
        app = QtWidgets.QApplication(*args, **kwargs)
    return app


class QJupyterWidget(RichJupyterWidget):
    """
    REF: https://stackoverflow.com/questions/29421936/cant-quit-pyqt5-application-with-embedded-ipython-qtconsole
    """
    def __init__(self, parent, customBanner = '', *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.banner = customBanner
        self.kernel_manager = kernel_manager = QtInProcessKernelManager()
        kernel_manager.start_kernel()
        kernel_manager.kernel.gui = 'qt'
        self.kernel_client = kernel_client = self._kernel_manager.client()
        kernel_client.start_channels()

        def stop():
            kernel_client.stop_channels()
            kernel_manager.shutdown_kernel()
            get_app_qt5().exit()            
        self.exit_requested.connect(stop)

    def pushVariables(self,variableDict):
        self.kernel_manager.kernel.shell.push(variableDict)

    def clearTerminal(self):
        self._control.clear()    

    def printText(self,text):
        self._append_plain_text(text)        

    def executeCommand(self,command):
        self._execute(command,False)

