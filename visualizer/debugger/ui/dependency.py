# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dependency.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(839, 508)
        self.graphicsView = QtWidgets.QGraphicsView(Dialog)
        self.graphicsView.setGeometry(QtCore.QRect(20, 290, 801, 201))
        self.graphicsView.setObjectName("graphicsView")
        self.memListLabel_2 = QtWidgets.QLabel(Dialog)
        self.memListLabel_2.setGeometry(QtCore.QRect(20, 260, 141, 21))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.memListLabel_2.setFont(font)
        self.memListLabel_2.setFrameShape(QtWidgets.QFrame.Box)
        self.memListLabel_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.memListLabel_2.setAlignment(QtCore.Qt.AlignCenter)
        self.memListLabel_2.setObjectName("memListLabel_2")
        self.graphicsView_2 = QtWidgets.QGraphicsView(Dialog)
        self.graphicsView_2.setGeometry(QtCore.QRect(20, 50, 791, 201))
        self.graphicsView_2.setObjectName("graphicsView_2")
        self.memListLabel_3 = QtWidgets.QLabel(Dialog)
        self.memListLabel_3.setGeometry(QtCore.QRect(20, 20, 121, 21))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(14)
        self.memListLabel_3.setFont(font)
        self.memListLabel_3.setFrameShape(QtWidgets.QFrame.Box)
        self.memListLabel_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.memListLabel_3.setAlignment(QtCore.Qt.AlignCenter)
        self.memListLabel_3.setObjectName("memListLabel_3")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.memListLabel_2.setText(_translate("Dialog", "Data flow"))
        self.memListLabel_3.setText(_translate("Dialog", "Control flow"))


