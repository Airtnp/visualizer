#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>              /* Obtain O_* constant definitions */
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

using namespace std;
int parent2child[2];
int child2parent[2];


enum PIPE_FILE_DESCRIPTERS {
    READ_FD = 0, WRITE_FD = 1
};

extern "C" void init() {
    // TODO: redo this.
    const char *target = "/home/lrxiao/EECS470/visualizer/visualizer/debugger/mainwindow.py";
    srand(time(NULL));
    size_t port = rand() % 32768 + 10000;

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in servaddr;
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);

    if (pipe2(parent2child, O_NONBLOCK) < 0) {
        perror("Can't make pipe");
        exit(1);
    }

    if (pipe2(child2parent, O_NONBLOCK) < 0) {
        perror("Can't make pipe");
        exit(1);
    }
    
    fcntl(parent2child[WRITE_FD], F_SETPIPE_SZ, 1 * 1024 * 1024);
    
    pid_t pid = fork();
    int retcode;
    char arg1[20];
    char arg2[20];
    char arg3[20];
    snprintf(arg1, sizeof(arg1), "%d", parent2child[READ_FD]);
    snprintf(arg2, sizeof(arg2), "%d", child2parent[WRITE_FD]);
    snprintf(arg3, sizeof(arg3), "%d", port);
    switch (pid) {
        case -1:
            perror("Can't fork");
            exit(1);
            break;

        case 0: /* Child */
            close(parent2child[WRITE_FD]);
            close(child2parent[READ_FD]);

            // redirect child stdin to ParentRead
            // dup2(parent2child[READ_FD], STDIN_FILENO);
            // redierct child stdout to ChildWrite
            // dup2(child2parent[WRITE_FD], STDOUT_FILENO);

            // close(parent2child[READ_FD]);
            // close(child2parent[WRITE_FD]);

            retcode = execlp(target, target, arg1, arg2, arg3, (char *) NULL);
            if (retcode) {
                perror("Can't execute target");
                exit(1);
            }
            exit(0);
            break;

        default: /* Parent */
            close(parent2child[READ_FD]);
            close(child2parent[WRITE_FD]);
            // redirect parent stdout to ParentWrite
            // dup2(parent2child[WRITE_FD], 1);
            close(parent2child[WRITE_FD]);
            dup2(sockfd, 1);
            while(connect(sockfd, (sockaddr*)&servaddr, sizeof(sockaddr_in)))
                ;
            break;
    }
}


extern "C" void waitforresponse() {
    char c = 0;
    // fprintf(stderr, "Wait for response\n");
    while (c != 'A') {
        ssize_t bytes = read(child2parent[READ_FD], &c, 1);
        while (bytes == 0) {
            bytes = read(child2parent[READ_FD], &c, 1);
        }
        // fprintf(stderr, "%c", c);
    }
}