"""
Define variable/memory/register history representation
"""
from bisect import bisect_right
from visualizer.model.model import Model


class Value:
    def __init__(self, value):
        self.value = value
        self.has_unknown = False
        if 'x' in value or 'X' in value:
            self.has_unknown = True
        else:
            self.value_ = int(value, 16)

    def __eq__(self, rhs):
        if self.has_unknown != rhs.has_unknown:
            return False
        return self.value == rhs.value

    def __str__(self):
        return self.value


class HistoryValue:
    def __init__(self):
        self.value_history = {}
        self.logtime = []

    def empty(self):
        return self.logtime == []

    def add_value(self, time, value):
        if self.logtime != [] and self.logtime[-1] == time:
                return
        if self.logtime == [] or self.value_history[self.logtime[-1]] != value:
            self.logtime.append(time)
            self.value_history[time] = value

    def value(self, time):
        idx = bisect_right(self.logtime, time)
        if idx == 0:
            idx = 1
        return self.value_history[self.logtime[idx - 1]]


class Variable(HistoryValue):
    @staticmethod
    def from_hierarchy_name(scope, hierarchy_name, width, core):
        hierarchy = hierarchy_name.split('.')
        v = Variable(hierarchy, width)
        v.core = core
        scope[hierarchy_name] = v
        child = v
        for i in range(len(hierarchy) - 1, -1):
            name = '.'.join(hierarchy[:i])
            if name not in scope.keys():
                pv = Variable(hierarchy[:i])
                scope[name] = pv
            if not scope[name].has_child(child):
                scope[name].add_child(scope[name])
            child = scope[name]

    def __init__(self, hierarchy, width=None):
        super().__init__()
        self.hierarchy = hierarchy
        self.name = self.hierarchy[-1]
        self.is_leaf = True if width else False
        self.width = width
        self.child = []
        self.core = None

    @property
    def value_(self):
        if not self.value(self.core.cur_time).has_unknown:
            return self.value(self.core.cur_time).value_
        else:
            return '?'

    def has_child(self, child):
        for c in self.child:
            if child.name == c.name:
                return True
        return False

    def add_child(self, var):
        self.child.append(var)

    def find_child(self, hier):
        if hier == []:
            return [self]
        for c in self.child:
            if c.name == hier[0]:
                if c.find_child(hier[1:]):
                    return [self, c.find_child(hier[1:])]
        return []

    def __repr__(self):
        if self.child == []:
            return str(self.value_)
        else:
            return self.child


class VarMap:
    def __init__(self, core):
        self.var = {}
        self.hier_var = {}
        self.core = core

    def add_var(self, time, hierarchy_name, width, value):
        if hierarchy_name not in self.var.keys():
            Variable.from_hierarchy_name(self.var, hierarchy_name, width, self.core)
        hierarchy = hierarchy_name.split('.')
        upper = self.hier_var
        for i in range(len(hierarchy)-1):
            if hierarchy[i] not in upper.keys():
                upper[hierarchy[i]] = {}
            upper = upper[hierarchy[i]]
        self.var[hierarchy_name].add_value(time, value)
        self.var[hierarchy_name].core = self.core
        upper[hierarchy[-1]] = self.var[hierarchy_name]

    def get_hier(self, key):
        hier = key.split('.')
        upper = self.hier_var
        for k in hier:
            upper = upper[k]
        return upper

    def __getitem__(self, key):
        return self.var[key]
    
    def __setitem__(self, key, value):
        self.var[key] = value


class Memory(HistoryValue):
    def __init__(self, address, width = 8):
        super().__init__()
        self.address = address
        # NOTE: only single dim allowed here.
        self.width = width
        self.core = None

    @property
    def value_(self):
        return self.value(self.core.cur_time).value_

    @staticmethod
    def merge(*mems):
        address = mems[0].address
        width = mems[0].width
        time = mems[0].time
        hv = mems[0].value_history
        for i in range(1, len(mems)):
            if address + width != mems[i].address:
                return None
            new_hv = mems[0].value_history
            if time < mems[i].time:
                for j in range(mems[i].time - time):
                    hv.append(hv[-1])
            elif time > mems[i].time:
                for j in range(time - mems[i].time):
                    new_hv.append(new_hv[-1])
            time = max(time, mems[i].time)
            for j in range(time):
                assert not hv[j].has_unknown
                assert not new_hv[j].has_unknown
                hv[j].value = (hv[j].value << mems[i].width) + new_hv[j].value
            width += mems[i].width
        mem = Memory(address, width)
        mem.time = time
        mem.value_history = hv
        return mem

class MemoryMap:
    def __init__(self, core, mem_limit):
        self.limit = mem_limit
        self.mem_section = []
        self.core = core
        for i in range(self.limit):
            self.mem_section.append(Memory(i * 8, 64))
            self.mem_section[-1].add_value(-1, Value("0"))

    def add_mem(self, time, addr, value):
        self.mem_section[addr // 8].add_value(time, value)
        self.mem_section[addr // 8].core = self.core

    def __getitem__(self, key):
        self.mem_section[key].core = self.core
        return self.mem_section[key]


class Register(HistoryValue):
    def __init__(self, prn):
        self.prn = prn


class RegList:
    def __init__(self, n):
        self.reg = []
        self.reg_map = []
        for i in range(n):
            self.reg.append(Register(i))
        for i in range(32):
            self.reg_map[i] = i
    
    def __getitem__(self, key):
        return self.reg[key]


class Sim:
    def __init__(self, core, insts):
        self.core = core
        self.insts = insts.values()
        self.inst_map = insts
        self.model = Model()
        self.is_halt = False
        self.predict = [ [] for _ in range(len(self.insts)) ]
        self.init_mem()
    
    @property
    def time(self):
        return self.core.cur_time

    def init_mem(self):
        for k, v in self.inst_map.items():
            x = v.encode()
            for j in range(4):
                self.model.mem[k * 4 + j] = (x >> (8 * j)) & 0xFF

    def flush_predict(self):
        self.predict = [ [] for _ in range(len(self.insts)) ]

    def add_predict(self, addr, target):
        self.predict[addr // 4].append(target)

    def update_time(self):
        halt = self.core.var.get_hier("core.halt")
        rollback_retire = self.core.var.get_hier("core.rob_0.rollback_retire")
        if halt.value_ or rollback_retire.value_:
            self.step_until()
            self.edge = True
            reg_map = [0] * 32
            for i in range(32):
                reg_idx = self.core.var.get_hier("core.mt_0.at_next[{}]".format(i))
                reg_idx = reg_idx.value_
                reg_v = self.core.var.get_hier("core.rf_stage_0.registers_next[{}]".format(reg_idx))
                reg_v = reg_v.value_
                reg_map[i] = reg_v
            for i in range(32):
                if reg_map[i] != self.model.reg[i]:
                    print("Incorrect at PC: {}".format(self.model.pc))
                    print("Reg {} | Sim: {} vs Verilog: {}".format(i, reg_map[i], self.model.reg[i]))
            for i in range(self.core.mem.limit):
                mmem = 0
                for j in range(7, -1, -1):
                    mmem <<= 8
                    mmem += self.model.mem[i * 8 + j]
                vmem = self.core.mem[i].value_
                if mmem != vmem:
                    print("Incorrect at PC: {}".format(self.model.pc))
                    print("Mem {} | Sim: {} vs Verilog: {}".format(hex(i * 8), mmem, vmem))
        for i in range(3):
            valid = self.core.var.get_hier("core.rob_0.decode_in[{}].valid".format(i))
            if valid.value(self.time).value_:
                fu = self.core.var.get_hier("core.rs_0.decode_in[{}].fu".format(i)).value_
                if fu == 4 or fu == 5:
                    target = self.core.var.get_hier(
                        "core.rob_0.decode_in[{}].branch_info.branch_target".format(i))
                    pc = self.core.var.get_hier(
                        "core.rs_0.decode_in[{}].PC".format(i))
                    self.add_predict(pc.value_, target.value_)

    def step_once(self):
        self.model.status = Model.REG_WRITE
        inst = self.insts[self.model.pc // 4]
        inst(self.model)

    def step_until(self):
        while True:
            is_cond_branch = False
            is_jump = False
            oldpc = self.model.pc
            cond_predict = self.model.pc + 4
            self.step_once()
            if self.model.status == Model.HALT:
                self.is_halt = True
                break
            elif self.model.status == Model.PC_WRITE:
                is_cond_branch = True
                is_jump = True
                cond_predict = self.model.pc
            elif self.model.status == Model.PC_WRITE_COND_SUCC:
                is_cond_branch = True
                is_jump = True
                cond_predict = self.model.pc
            elif self.model.status == Model.PC_WRITE_COND_FAIL:
                is_cond_branch = True
            if not is_jump:
                self.model.pc += 4
            if is_cond_branch:
                assert(len(self.predict[oldpc // 4]))
                predict_cond = self.predict[oldpc // 4][0]
                self.predict[oldpc // 4] = self.predict[oldpc // 4][1:]
                if cond_predict != predict_cond:
                    break
