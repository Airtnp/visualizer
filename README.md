# Visualizer

A visual debugger of Verilog simulation especially for CPUs in Python.

## Requirements

python >= 3.6 (unsatisified in CAEN, but install pyenv instead)
setuptools >= 47.0
pip >= 19.0

## TODO List

- [x] parser-verilog: Implement Verilog parser for module input/output logic/wire connection.
- [x] parser-generate: Implement Verilog testbench generating & C file generating, to receive I/O bypassing static compiling problem.
- [ ] parser-program: Implement GAS parser for label and decoding (just ISA)
- [ ] model: Implement ISA model (Alpha or RISC-V), including register, memory, cache, instructions.
- [x] debugger: Implement basic debugger interface, maybe PyQt5 + gdb remote server. This will show enough information, including register/memory/cache/variable/watchpoint/assert values and graphs(waves), callstack, history tracking (reverse debugging)
- [ ] analysis: Implement dataflow + controlflow analysis

## Solutions

- How to do eval function?
- - track all variables names in parsing
- How to overcome static compiling?
- - track all varaibles
- How to reverse debugging?
- - do COW like RR

```python
pyenv install 3.6.8
pyenv global 3.6.8
eval "$(pyenv init -)"
python -m venv venv
. ./venv/bin/activate
pip install -e .
python -m visualizer
```
